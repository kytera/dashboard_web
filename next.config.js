const withCss = require('@zeit/next-css');

const path = require('path');

module.exports = withCss({
	webpack: (config, { buildId, dev }) => {
		config.resolve.modules.push(path.resolve(__dirname));
		config.module.rules.push(
			{
				test: /\.(graphql|gql|svg)$/,
				loader: 'emit-file-loader',
				options: {
					name: 'dist/[path][name].[ext]',
				},
			} // eslint-disable-line
		);
		config.module.rules.push(
			{
				test: /\.(graphql|gql)$/,
				exclude: /node_modules/,
				loader: 'graphql-tag/loader',
			} // eslint-disable-line
		);
		config.module.rules.push(
			{
				test: /\.svg$/,
				loader: 'svg-inline-loader',
			} // eslint-disable-line
		);

		return config;
	},
});
