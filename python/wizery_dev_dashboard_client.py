from graphqlclient import GraphQLClient
import json, argparse, datetime, bson, time, os
from pymongo import MongoClient
from kconfig import kmongo, get_config

URL="http://ec2-52-29-66-166.eu-central-1.compute.amazonaws.com:60006/dashboard_bridge/"#get_config('rest_apis', 'dashboard_bridge')['private']

def get_kytera_info(args):
    cmd="""curl -i '%sgenerate_kytera_info/'"""%(URL)
    os.system(cmd)

def register_new_subscriber(args):
    cmd="""curl -i '%sregister_new_subscriber/?firstName=%s'"""%(URL, args.first_name)
    os.system(cmd)

def service_transition(args):
    subscriber=kmongo('subscribers').find_one({'firstName': args.first_name})
    assert subscriber
    cmd="""curl -i '%sdashboard_service_change/?operatorid=6523432623&subscriberid=%s&originator=dashboard_client&transition=%s'"""%(URL, str(subscriber['_id']), str(args.transition))
    os.system(cmd)

def send_mobile_app_invitation(args):
    cmd="""curl -i '%ssend_mobile_invitation_email/?email=%s'"""%(URL, args.email)
    print cmd
    os.system(cmd)


def main():
    parser = argparse.ArgumentParser(description='Work with dashboard api')
    subparsers = parser.add_subparsers(title='subcommands')
    
    parser1 = subparsers.add_parser('service')
    parser1.add_argument('first_name')
    parser1.add_argument('transition', choices= ['installation_completed', 'suspend', 'resume', 'activate', 'archive'])
    parser1.set_defaults(func=service_transition)
    
    parser1 = subparsers.add_parser('register_subscriber')
    parser1.add_argument('first_name')
    parser1.set_defaults(func=register_new_subscriber)
    
    
    parser2 = subparsers.add_parser('kytera_info')
    parser2.set_defaults(func=get_kytera_info)
    

    parser3 = subparsers.add_parser('mobile_app_invitation')
    parser3.add_argument('email')
    parser3.set_defaults(func=send_mobile_app_invitation)

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
    

    
    
