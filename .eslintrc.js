const path = require('path');

module.exports = {
	"parser": "babel-eslint",
	"extends": "airbnb",
	"plugins": [
		"graphql",
		"jest",
	],
	"env": {
		"browser": true,
		"es6": true,
		"jest/globals": true,
	},
	"parserOptions": {
		"ecmaFeatures": {
			"defaultParams": true,
			"jsx": true,
			"modules": true,
		}
	},
	"rules": {
		"react/react-in-jsx-scope": 0,
		"react/jsx-indent-props": [
			"error",
			"tab",
		],
		"react/jsx-indent": [
			"error",
			"tab",
		],
		"jsx-quotes": [
			"error",
			"prefer-single",
		],
		"import/no-extraneous-dependencies": [
			"error",
			{
				"devDependencies": [
					"**/*.test.js",
					"**/*.stories.js",
				],
			},
		],
		"import/no-named-as-default": 0,
		"jsx-a11y/aria-role": [2, {
			"ignoreNonDOM": true,
		}],
		"jsx-a11y/href-no-hash": "off",
		"jsx-a11y/label-has-for": "off",
		"jsx-a11y/anchor-is-valid": ["warn", { "aspects": ["invalidHref"] }],
		"no-duplicate-imports": 0,
		"react/jsx-filename-extension": 0,
		"global-require": 0,
		"no-tabs": 0,
		"no-mixed-spaces-and-tabs": [
			"error",
			"smart-tabs",
		],
		"indent": [
			"error",
			"tab",
		],
		"no-underscore-dangle": ["error", { "allow": ["_id", "__typename"] }],
		"max-len": [1, 160],
		"no-console": 0,
		"class-methods-use-this": 0,
		"graphql/template-strings": [
			"error",
			{
				"env": "apollo",
				"schemaJsonFilepath": path.resolve(__dirname, 'graphql.schema.json'),
			},
		],
	},
};
