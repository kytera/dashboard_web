import { graphql } from 'react-apollo';
import PropTypes from 'prop-types';

import COMPANY_QUERY from './graphql/company/company.query.gql';

export default graphql(
	COMPANY_QUERY,
	{
		alias: 'withCompany',
		props: ({ data: { company, ...companyQuery } }) => ({
			company,
			companyQuery,
		}),
		},
);

export const companyPropTypes = {
	company: PropTypes.object,
	companyQuery: PropTypes.shape({
		loading: PropTypes.bool.isRequired,
		error: PropTypes.object,
	}),
};
