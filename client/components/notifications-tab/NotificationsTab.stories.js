import { storiesOf } from '@storybook/react';

import NotificationsTab from './NotificationsTab';
import logs from './notifications.mock';

storiesOf('notifications-tab', module)
	.add('NotificationsTab', () => (
		<div style={{ width: '900px' }}>
			<NotificationsTab logs={logs} subscriberId="123" />
		</div>));
