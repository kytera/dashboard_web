import PropTypes from 'prop-types';
import moment from 'moment';
import { graphql } from 'react-apollo';
import { compose, withProps } from 'recompose';
import { get, groupBy, sortBy, uniqBy } from 'lodash';
import { Component } from 'react';

import Title from '../common/title/Title';
import ListSection from '../alert-tab/logs-list/list-section/ListSection';
import NOTIFICATIONS_QUERY from '../../graphql/log/notifications.query.gql';
import ScrollContainer from '../common/scroll-container/ScrollContainer';
import Button from "../common/button/Button";
import React from "react";
import { MessageContainer } from "../../withQueryStatus";

function sortAndGroupLogs(data) {
	const notifications = get(data, 'notifications.logs', []);
	const sorted = sortBy(notifications, ['timestamp']);

	const grouped = groupBy(sorted, log => moment(log.timestamp).startOf('day').toISOString());
	const logs = Object.keys(grouped).map(key => ({
		groupTitle: moment(key).format('LL'),
		items: grouped[key],
	}));
	return logs;
}

const withLogsData = compose(
	graphql(NOTIFICATIONS_QUERY,
		{
			options: ({subscriberId}) => ({
				variables: {subscriberId},
				// pollInterval: 30000,
				fetchPolicy: process.browser ? 'cache-and-network' : undefined,
			}),
			skip: ({subscriberId}) => !subscriberId,
			props: ({ownProps, data}) => ({
				logs: sortAndGroupLogs(data),
				hasMore: get(data, 'notifications.hasMore', false),
				loadMore: () => {
					const {notifications: {hasMore, nextCursor}, fetchMore} = data;
					const {subscriberId} = ownProps;

					return hasMore && fetchMore({
						query: NOTIFICATIONS_QUERY,
						variables: {subscriberId, cursor: nextCursor},
						updateQuery: (previousResult, {fetchMoreResult}) => {
							const previousEntry = previousResult.notifications;
							const newLogs = fetchMoreResult.notifications.logs;
							const nextCursor = fetchMoreResult.notifications.nextCursor;
							const logs = uniqBy([...newLogs, ...previousEntry.logs], 'id');

							return {
								notifications: {
									__typename: previousResult.notifications.__typename,
									nextCursor: nextCursor,
									hasMore: fetchMoreResult.notifications.hasMore,
									logs,
								}
							};
						},
					});
				},
				pollNewData: () => {
					const {fetchMore} = data;
					const {subscriberId} = ownProps;
					return fetchMore({
						query: NOTIFICATIONS_QUERY,
						variables: {subscriberId},
						updateQuery: (previousResult, {fetchMoreResult}) => {
							if (!previousResult.notifications) {
								return;
							}
							const previousEntry = previousResult.notifications;
							const prevLogs = sortBy(previousEntry.logs, ['timestamp']);
							let lastLog = prevLogs[prevLogs.length - 1];
							const newLogs = fetchMoreResult.notifications.logs.filter(log => new Date(log.timestamp) > new Date(lastLog.timestamp));
							const nextCursor = previousResult.notifications.nextCursor;

							return {
								notifications: {
									__typename: previousResult.notifications.__typename,
									nextCursor: nextCursor,
									hasMore: previousResult.notifications.hasMore,
									logs: [...previousEntry.logs, ...newLogs]
								}
							};
						},
					});
				},
			}),
		},
	),
);

class NotificationsList extends Component {

	state = {
		loadingMore: false,
	};

	constructor(props) {
		super(props);

		if (process.browser) {
			// Remove this manual polling when subscription arrives
			this.pollTimer = setInterval(() => {
				this.disableAutoScroll = true;
				this.props.pollNewData().then(()=>{
					this.disableAutoScroll = false;
				});
			}, 30000)
		}
	}

	componentDidMount() {
		this.scrollContainer && this.scrollContainer.scrollToBottom();
	}

	componentWillUnmount() {
		clearInterval(this.pollTimer);
	}

	componentDidUpdate() {
		if (this.shouldScroll && this.scrollContainer) {
			this.scrollContainer.scrollToBottom();
			this.shouldScroll = false;
		}
	}

	componentWillReceiveProps(newProps) {
		const newData = get(newProps, 'logs');
		const oldData = get(this.props, 'logs');

		// Scroll first time
		// if ((newData && oldData) && Object.keys(newData).length !== 0 && Object.keys(oldData).length === 0) {
		// 	this.shouldScroll = true;
		// }
		if (newData !== oldData && !this.disableAutoScroll){
			this.shouldScroll = true;
		}

		if (newProps.subscriberId !== this.props.subscriberId && this.scrollContainer) {
			this.shouldScroll = true;
			this.scrollContainer.scrollToBottom();
		}
	}

	handleLoadMore = () => {
		this.setState({loadingMore: true});
		const oldScrollHeight = this.scrollContainer.content.scrollHeight;
		this.disableAutoScroll = true;
		this.props.loadMore().then(() => {
			this.setState({loadingMore: false});
			this.goToPrevScroll(oldScrollHeight);
			this.disableAutoScroll = false;
		});
	};

	goToPrevScroll = (oldScrollHeight) => {
		this.scrollContainer.content.scrollTop = this.scrollContainer.content.scrollHeight - oldScrollHeight + this.scrollContainer.content.scrollTop;
	};


	render() {
		const {logs, hasMore} = this.props;

		if (logs && logs.length === 0) {
			return <MessageContainer theme='empty'>This subscriber has no notifications yet</MessageContainer>;
		}
		return (
			<div className='root'>
				{/* language=CSS */}
				<style jsx>{`
					.loading {
						display: flex;
						justify-content: center;
						align-items: center;
					}

					.root {
						background-color: #fff;
						border-top: none;
						overflow-y: auto;
						display: flex;
						flex-direction: column;
						flex: 1;
						max-width: 50%;
					}
				`}</style>
				<style>{`
					.notifications-container .root > .content {
						overflow-y: scroll;
					}
					
					.notifications-container > .root {
						max-height: calc(100vh - 228px);
					}
						
				`}
				</style>
				<div className='notifications-container'>
				<ScrollContainer ref={r => this.scrollContainer = r}>
					{hasMore &&
					<div className='loading'>
						<Button onClick={this.handleLoadMore} theme='accent' loading={this.state.loadingMore}>
							Load More
						</Button>
					</div>}
					{logs.map(log => (<ListSection
						key={log.groupTitle}
						title={log.groupTitle}
						logs={log.items}
					/>))}
				</ScrollContainer>
				</div>
			</div>
		);
	}
}

NotificationsList.propTypes = {
	logs: PropTypes.arrayOf(PropTypes.shape({
		groupTitle: PropTypes.string.isRequired,
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
	})).isRequired,
};


const NotificationsListEnhanced = withLogsData(NotificationsList);

const NotificationsTab = ({subscriberId}) => (
	<div className='root'>
		{/* language=CSS */}
		<style jsx>{`
			.root {
				flex: 1;
				background-color: #fff;
				display: flex;
				flex-direction: column;
			}
		`}</style>
		<Title>Notifications</Title>
		<NotificationsListEnhanced subscriberId={subscriberId}/>
	</div>
);

NotificationsTab.propTypes = {
	subscriberId: PropTypes.string.isRequired,
};

export default NotificationsTab;
