import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import { omit, pick } from 'lodash';
import countries from '../../data/countries';


import PersonalInfo from './PersonalInfo/PersonalInfo';
import CaregiverList from './CaregiverList/CaregiverList';
import EmergencyServicesInfo from './EmergencyServicesInfo/EmergencyServicesInfo';
import MedicalHistoryInfo from './MedicalHistoryInfo/MedicalHistoryInfo';
import SettingsForm from '../wizards/subscriber-settings/SettingsForm';

import CREATE_CAREGIVER_MUTATION from '../../graphql/caregiver/createCaregiver.mutation.gql';
import UPDATE_CAREGIVER_MUTATION from '../../graphql/caregiver/updateCaregiver.mutation.gql';
import SUBSCRIBER_QUERY from '../../graphql/subscriber/subscriberInfo.query.graphql';
import UPDATE_SUBSCRIBER_MUTATION from '../../graphql/subscriber/updateSubscriber.mutation.graphql';
import SET_SUBSCRIBER_SERVICE_STATUS from '../../graphql/subscriber/setSubscriberDashboardServiceStatus.mutation.graphql';
import { apolloPropType } from '../../apolloPropType';
import { caregiverType, caregiverTypeEnum } from '../../../server/graphql/utils/subscriber'
import { normalizeSubscriberInput, normalizeCaregiverInput } from '../../normalizer'
import DeactivateSubscriberModal from './DeactivateSubscriberModal'
import Loading from '../common/loading/Loading'
import Modal from '../common/modal/Modal';
import Button from '../common/button/Button';

const DEFUALT_LOADING_MESSAGE = 'loading...'

const DEFAULT_CAREGIVER_SETTINGS = [
            { settingType: "SYSTEM", value: true},
            { settingType: "BATTERY", value: true},
            { settingType: "WEAR", value: true},
            { settingType: "OUT_OF_HOME", value: true},
			{ settingType: "IRREGULAR_ACTIVITY", value: true},
		];

export class SubscriberTab extends Component {

	static propTypes = {
		data: apolloPropType(SUBSCRIBER_QUERY).isRequired,
		updateSubscriber: PropTypes.func.isRequired,
		deactivateSubscriber: PropTypes.func.isRequired,
	}

	state = {
		editMode: false,
		editSettings: false,
		loading: true,
		saveLoading: false,
		loadingMessage: DEFUALT_LOADING_MESSAGE,
		formData: {},
		subscriberId: null,
		updatedCaregivers: [],
		showSuspendWarning: false,
		showSuspendFail: false
	}

	medicalAlarmEnabled = true

	componentWillReceiveProps({ data }) {
		if (data && data.subscriber && this.props.data && this.props.data.subscriber && this.props.data.subscriber.id !== data.subscriber.id) {
			this.setState({ formData: {}, editMode: false, updatedCaregivers: this.props.data.subscriber.caregivers, loading: false });
		}
	}

	componentDidMount() {
		if(this.props.data.subscriber && this.props.data.subscriber.caregivers ) {
			this.setState({ updatedCaregivers: this.props.data.subscriber.caregivers, loading: false })
		}
	}

	componentDidUpdate(prevProps) {
		if(prevProps.data.subscriber && prevProps.data.subscriber.id !== this.props.data.subscriber.id) {
			this.setState({ updatedCaregivers: this.props.data.subscriber.caregivers, loading: false })
		} else if(this.props.data.subscriber && this.state.loading) {
			this.setState({ loading: false })
		}
	}

	onEditEnable = () => this.setState({ editMode: true });
	onSettingsEnable = () => this.setState({editSettings: true});

	handleSaveSubscriber = async () => {
		if (!Object.keys(normalizeSubscriberInput(this.state.formData)) && this.state.updatedCaregivers.filter(caregiver => caregiver.action).length === 0) { // nothing to save
			this.setState({ editMode: false, editSettings: false, formData: {} });
			return;
		}

		let caregivers = [];

		this.setState({ saveLoading: true, loadingMessage: 'Saving subscriber...' });
		try {
			caregivers = this.state.updatedCaregivers.reduce((cgs, caregiver) => {
				if(!caregiver.action || caregiver.action === 'update') {
					let id = caregiver.id;
					let cg = normalizeCaregiverInput(caregiver);
					cg.id = id;
					cgs.push(cg);
				} else if(caregiver.action === 'create') {
						let cg = normalizeCaregiverInput(caregiver)
						cg.caregiverType = caregiverTypeEnum.CAREGIVER
						cgs.push(cg);
				}
				return cgs;
			},[])

			let sub = Object.keys(this.state.formData).length === 0 ? this.props.data.subscriber : this.state.formData
			sub = normalizeSubscriberInput(sub);

			await this.props.updateSubscriber(sub, caregivers, caregiverTypeEnum.CAREGIVER);
			this.setState({ editMode: false, formData: {}});
		} catch (error) {
			console.error(error);
		} finally {
			this.setState({ saveLoading: false, loadingMessage: DEFUALT_LOADING_MESSAGE});
			this.props.data.refetch().then(() => { this.setState({updatedCaregivers: this.props.data.subscriber.caregivers})})
		}
	};

	onCreateCaregiver = (caregiver) => {
		caregiver.action = 'create';
		caregiver.isPrimary = false;
		caregiver.caregiverType = caregiverTypeEnum.CAREGIVER;
		caregiver.id = Math.ceil(Math.random()*1000000); // for convenience only
		caregiver.settings = DEFAULT_CAREGIVER_SETTINGS;

		if(this.state.updatedCaregivers.filter(cg => !cg.action || cg.action !== 'delete').length === 0) {
			caregiver.isPrimary = true;
		}

		let caregivers = this.state.updatedCaregivers.concat(caregiver);
		this.setState({updatedCaregivers: caregivers});
	}

	onUpdateCaregiver = (caregiver) => {
		caregiver = {...caregiver}; //this one is immutable
		if(!caregiver.action) {
			caregiver.action = 'update';
		}

		let caregivers = this.state.updatedCaregivers.map(currCg => currCg.id === caregiver.id ? caregiver : currCg);

		this.setState({updatedCaregivers: caregivers});
	}

	onDeleteCaregiver = (caregiver) => {
		caregiver = {...caregiver}; //this one is immutable
		let caregivers = [...this.state.updatedCaregivers];

		if(caregiver.action && caregiver.action === 'create') { //never saved to server?
			caregivers = caregivers.filter(cg => cg.id !== caregiver.id);
		}	else {
			caregiver.action = 'delete';
			caregivers = caregivers.map(currCg => currCg.id === caregiver.id ? caregiver : currCg);
		}

		//no primary left?
		if(caregivers.filter(cg => cg.isPrimary && (!cg.action || cg.action !== 'delete')).length === 0) {
			let visible = caregivers.find(cg => !cg.action || cg.action != 'delete');
			if(visible) {
				visible = {...visible}
				visible.isPrimary = true;
				if(!visible.action) {
					visible.action = 'update'
				}
				caregivers = caregivers.map(currCg => currCg.id === visible.id ? visible : currCg);
			}
		}

		this.setState({updatedCaregivers: caregivers});
	}

	onSetPrimaryCaregiver = (caregiver) => {
		caregiver = {...caregiver}; //this one is immutable
		if(!caregiver.action) {
			caregiver.action = 'update';
		}

		let caregivers = [...this.state.updatedCaregivers]

		let prevPrimary = caregivers.find(cg => cg.id !== caregiver.id && cg.isPrimary && (!cg.action || cg.action !== 'delete'))
		if(prevPrimary) {
			prevPrimary = {...prevPrimary};
			prevPrimary.isPrimary = false;
			if(!prevPrimary.action) {
				prevPrimary.action = 'update';
			}

			caregivers = caregivers.map(currCg => currCg.id === prevPrimary.id ? prevPrimary : currCg);
		}

		caregiver.isPrimary = true;
		caregivers = caregivers.map(currCg => currCg.id === caregiver.id ? caregiver : currCg);

		this.setState({updatedCaregivers: caregivers});
	}

	hideModal = e => {
		this.setState({ showSuspendWarning: false })
	}

	onMedicalAlarm = (enabled) => {
		this.medicalAlarmEnabled = enabled;
	}

	onSubscriberUpdate = ({ avatar, fire, medical, police, ...newData }) => {
		var agencies = null, emergency = [];
		if(typeof(fire) === 'string') {
			emergency.push({ agency_type: 'fire', number: fire});
		}
		if(typeof(police) === 'string') {
			emergency.push({ agency_type: 'police', number: police});
		}
		if(typeof(medical) === 'string') {
			emergency.push({ agency_type: 'medical', number: medical});
		}

		if(emergency.length) {
			agencies = { agencies: emergency };
		}

		// Add country code
		let countryCode = null;
		if (newData.address) {
			const country = newData.address.country;
			const countryData = countries.filter(c => c.short_name === country);
			if (countryData && countryData.length > 0) {
				countryCode = '+' + countryData[0].callingCodes[0];
			}
		}

		const formData = {
			...this.state.formData,
			...newData,
			...agencies
		};

		if (!this.medicalAlarmEnabled) {
			formData['medicalAlarmService'] = null;
		}

		if (countryCode) {
			if (newData.phones && newData.phones.length > 0) {
				if (!newData.phones[0].startsWith(countryCode)) {
					formData.phones = [countryCode + ' ' + newData.phones[0].trimLeft()];
				}
			}
			if (newData.homePhone) {
				if (!newData.homePhone.startsWith(countryCode)) {
					formData.homePhone = countryCode + ' ' + newData.homePhone.trimLeft();
				}
			}
		}


		if (avatar instanceof File) {
			formData.avatar = avatar;
		}
		this.setState({ formData });
	}

	suspendSubscriber = async (e) => {
		try {
			this.setState({ saveLoading: true, loadingMessage: 'Suspending subscriber...'});
			await this.props.deactivateSubscriber();
		}
		catch(ex) {
			this.setState({showSuspendFail: true})
		}
		finally {
			this.setState({ saveLoading: false, loadingMessage: DEFUALT_LOADING_MESSAGE});
		}

		this.hideModal();
	}

	render() {
		const { data } = this.props;
		const { editMode, loading, saveLoading, formData, loadingMessage } = this.state;

		if (!data || data.error) {
			return null;
		}

		const { subscriber } = data;

		if(!subscriber) return null;

		const subscriberWithFormData = editMode ? { ...subscriber, ...formData } : subscriber;

		return (
			<div className='container'>
				<Loading isOpen={loading || saveLoading || data.loading} message={loadingMessage} />
				{ /* language=CSS */ }
				<style jsx>{`
					.container {
						display: flex;
						flex: 1;
						flex-direction: column;
						background-color: white;
                        overflow-y: auto;
					}

					.delete-body {
						display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						height: 256px;
						padding: 24px;
						line-height: 1.5rem;
						text-align: center;
					}

				`}</style>
				<PersonalInfo
					onEditEnable={this.onEditEnable}
					onSettingsEnable={this.onSettingsEnable}
					onSuspendSubscriber={()=> this.setState({showSuspendWarning: true}) }
					onEditSave={this.handleSaveSubscriber}
					editMode={editMode}
					subscriber={subscriberWithFormData}
					saveLoading={saveLoading}
					onSubscriberUpdate={this.onSubscriberUpdate}
					onMedicalAlarm={this.onMedicalAlarm}
				/>
				<SettingsForm
					isOpen={this.state.editSettings}
					onClose={() => this.setState({editSettings:false}) }
					initialValues={subscriberWithFormData.settings}
					subscriberId={subscriberWithFormData.id}
					/>
				<CaregiverList
					editMode={editMode}
					caregivers={this.state.updatedCaregivers.filter(caregiver=> !caregiver.action || caregiver.action != 'delete')}
					subscriberId={subscriber.id}
					subscriber={subscriber}
					createCaregiver={this.onCreateCaregiver}
					updateCaregiver={this.onUpdateCaregiver}
					setPrimaryCaregiver={this.onSetPrimaryCaregiver}
					deleteCaregiver={this.onDeleteCaregiver}
					defaultSettings={DEFAULT_CAREGIVER_SETTINGS}
				/>
				<EmergencyServicesInfo
					editMode={editMode}
					info={subscriberWithFormData.agencies}
					onSubscriberUpdate={this.onSubscriberUpdate}
				/>
				<MedicalHistoryInfo
					editMode={editMode}
					info={subscriberWithFormData.medicalHistory}
					onSubscriberUpdate={this.onSubscriberUpdate}
				/>
				{ this.state.showSuspendWarning &&
					<DeactivateSubscriberModal
						title='Suspend Subscriber'
						isOpen={this.state.showSuspendWarning}
						onClose={this.hideModal}
						onConfirm={this.suspendSubscriber}
						subscriber={subscriber} />
				}
				<Modal
					title='Suspend subscriber failed'
					theme='error'
					onSubmit={e => this.setState({showSuspendFail: false})}
					button={<Button theme='error' onClick={e => this.setState({showSuspendFail: false})}>Close</Button>}
					isOpen={this.state.showSuspendFail}
					onClose={e => this.setState({showSuspendFail: false})}>
					<div className='delete-body'>
						Subscriber suspend failed.<br/>
						<span style={{whiteSpace:'nowrap'}}>Please contact <span style={{fontWeight:'600'}}>support@kytera.com</span></span>
					</div>
					</Modal>
			</div>
		);
	}
}

const enhance = compose(
	graphql(CREATE_CAREGIVER_MUTATION,
		{
			props: ({ ownProps: { subscriberId }, mutate }) => ({
				createCaregiver: caregiver => mutate({
					variables: {
						caregiver,
						subscriberId,
					}
				})
			})
		}
	),
	graphql(UPDATE_CAREGIVER_MUTATION,
		{
			props: ({ mutate }) => ({
				updateCaregiver: ({ id, ...caregiver }) => mutate({
					variables: {
						caregiver,
						id,
					},
					optimisticResponse: {
						__typename: 'Mutation',
						updateCaregiver: {
							id,
							__typename: 'Caregiver',
							...caregiver,
						}
					}
				})
			})
		}
	),
	graphql(SUBSCRIBER_QUERY,
		{
			options: ({ subscriberId }) => ({ variables: { id: subscriberId } }),
			skip: ({ subscriberId }) => !subscriberId,
		},
	),
	graphql(UPDATE_SUBSCRIBER_MUTATION,
		{
			props: ({ ownProps: { subscriberId }, mutate }) => ({
				updateSubscriber: (subscriber, caregivers, caregiversType) => mutate({
					variables: {
						subscriber,
						caregivers,
						caregiversType,
						id: subscriberId
					}
				})
			})
		}
	),
	graphql(SET_SUBSCRIBER_SERVICE_STATUS, {
		props: ({ownProps: {subscriberId, operatorId}, mutate}) => ({
			deactivateSubscriber: () => mutate({
				variables: {
					subscriberId,
					operatorId,
					newServiceStatus: 'SUSPEND',
				},
			}),
		}),
	}),
);

export default enhance(SubscriberTab);
