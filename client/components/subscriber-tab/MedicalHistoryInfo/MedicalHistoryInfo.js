import React from 'react';
import PropTypes from 'prop-types';

import Title from '../../common/title/Title';
import Button from '../../common/button/Button';
import Modal from '../../common/modal/Modal'

import PermissionOnly from '../../common/permission-only/PermissionOnly';
import Input from '../../common/input/Input';

class MedicalHistoryInfo extends React.Component {
	static propTypes = {
		onSubscriberUpdate: PropTypes.func.isRequired,
		editMode: PropTypes.bool.isRequired,
		info: PropTypes.string.isRequired
	}

	state = {
		isModalOpen: false,
		medicalHistory: ''
	}

	hideModal = () => {
		this.setState({ isModalOpen: false });
	}

	showModal = () => {
		const { info } = this.props;

		this.setState({ isModalOpen: true, medicalHistory: info });
	}

	saveDetails = () => {
		const { onSubscriberUpdate } = this.props;

		onSubscriberUpdate({medicalHistory: this.state.medicalHistory})
		this.setState({isModalOpen: false})
	}

	render() {
		const { isModalOpen } = this.state;
		const { editMode, info } = this.props;

		const medicalHistory = info;

		return (
			<div className='root'>
				<Modal
					isOpen={isModalOpen}
					title='Edit Medical History'
					onClose={this.hideModal}
					onSubmit={this.saveDetails}
					button={<Button theme='accent' onClick={this.saveDetails}>Done</Button>}
				>
					<div className='modal-container'>
						<div className='inputItem'>
							<Input type='textarea' rows='6' name='medicalHistory' className='' label='Medical History' input={{value: this.state.medicalHistory || '', onChange:e => this.setState({medicalHistory: e.target.value})}} />
						</div>
					</div>
				</Modal>
				{ /* language=CSS */ }
				<style jsx>{`
					.root {
						background-color:#f4f4f4;
					}

					.row {
						flex: 1;
						display: flex;
						flex-direction: row;
						padding:16px 0;
					}

					.inputItem {
						margin-bottom:15px;
					}

					.header {
            text-transform:uppercase;
						color: #3b5173;
						font-size: 12px;
						font-weight: 800;
						row-height:18px;
						margin-bottom:4px;
					}

					.col {
						flex: 1;
						padding:0 22px;
					}

					.edit {
						display: flex;
						flex-direction: row;
						justify-content: space-between;
					}

					.buttonGroup {
						width:40px;
					}

					.modal-container {
						padding:20px;
					}
					
					.medical-history {
						white-space: pre-wrap;
					}
				`}</style>
				<Title size='medium'>MEDICAL HISTORY</Title>
				<div className='row'>
					<div className={`col ${editMode ? 'edit' : ''}`}>
						<div className='medical-history'>{medicalHistory}</div>
						<div>
							{ editMode && <PermissionOnly resource={PermissionOnly.resources.SUBSCRIBER} action='EDIT'>
								<div className='buttonGroup'>
									<Button link icon='pencil' onClick={this.showModal} />
								</div>
							</PermissionOnly>
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}


export default MedicalHistoryInfo

