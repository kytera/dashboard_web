import React from 'react';
import PropTypes from 'prop-types';

import Radio from '../../common/radio/Radio';
import Button from '../../common/button/Button';
import Icon from '../../common/icon/Icon';
import DisplayRawPhone from '../../common/display-phone/DisplayRawPhone';
import relations from '../../../../client/constants/relations';

const CaregiverListItem = ({ item, editMode, onSelectPrimary, onUpdateCaregiver, onCaregiverSettings, onDeleteCaregiver }) => {
	let caregiverName = item.middleName ? `${item.firstName} (${item.middleName}) ${item.lastName}` : `${item.firstName} ${item.lastName}`
	let username = item.username;

	let sendInvite = item.sendInvite;
	let isActivated = item.isActivated;
	let status = '';

	if (sendInvite) {
		status = 'Not Invited'
	} else {
		if (isActivated) {
			status = 'Activated';
		} else {
			status = 'Invited';
		}
	}


	return (
	<div>
		{/* language=SCSS */}
		<style jsx>{`
			.row {
				flex: 1;
				display: flex;
				flex-direction: row;
				align-items: center;
				padding: 0 6px;
				margin: 6px;
				min-height: 36px;
				border-radius: 3px;
			}

			.row:hover {
					background-color: #F9F9F9;
			}

			.column1 {
				flex: 0.6;
				padding-left: 10px;
			}

			.column2, .column3, .column4, .column5, .column6 {
				flex: 2;
				align-items: center;
				display: flex;
			}

			.column7 {
				flex: 3;
				align-items: center;
				display: flex;
				flex-direction: row;
				justify-content: space-between;
			}

			.buttonGroup {
				min-width: 75px;
			}
		`}</style>
		<div className='row'>
			<div className='column1'>
				{editMode
					? <Radio onChange={onSelectPrimary} checked={item.isPrimary} />
					: item.isPrimary && <Icon number='58' size='24px' />
				}
			</div>
			<div className='column2'>{username}</div>
			<div className='column3'>{caregiverName}</div>
			<div className='column4'>{relations[item.relation]}</div>
			<div className='column5'>{item.type}</div>
			<div className='column6'>
				{item.phones.map((phone, phoneIndex) => {
					const key = `${phoneIndex} ${phone}`;
					if (phoneIndex === 0) {
						return (
							<DisplayRawPhone key={key} phone={phone} />
						);
					}
					return (
						<div key={key}>
							&nbsp;,&nbsp;
							<DisplayRawPhone phone={phone} />
						</div>
					);
				})}
			</div>
			<div className='column7'>
				<span>
					{status}
				</span>
				{editMode &&
				<div className='buttonGroup'>
					<Button link icon='pencil' onClick={onUpdateCaregiver} />
					<Button link icon='settings' onClick={onCaregiverSettings} />
					<Button theme='error' icon='close_circle' link onClick={onDeleteCaregiver} />
				</div>
				}
			</div>
		</div>
	</div>
	);
}

CaregiverListItem.propTypes = {
	item: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		middleName: PropTypes.string,
		lastName: PropTypes.string.isRequired,
		username: PropTypes.string.isRequired,
		phones: PropTypes.arrayOf(PropTypes.string).isRequired,
		isPrimary: PropTypes.bool.isRequired,
		type: PropTypes.string
	}).isRequired,
	editMode: PropTypes.bool.isRequired,
	onSelectPrimary: PropTypes.func.isRequired,
	onUpdateCaregiver: PropTypes.func.isRequired,
	onCaregiverSettings: PropTypes.func.isRequired,
	onDeleteCaregiver: PropTypes.func.isRequired,
};
export default CaregiverListItem;
