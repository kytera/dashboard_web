import React from 'react';
import PropTypes from 'prop-types';


import Title from '../../common/title/Title';
import Button from '../../common/button/Button';
import Modal from '../../common/modal/Modal';
import CaregiverWizard, { availableSteps } from '../../wizards/caregiver-wizard/CaregiverWizard';
import CaregiverSettings from '../../wizards/caregiver-settings/CargiverSettings';

import CaregiverListItem from './CaregiverListItem';
import PermissionOnly from '../../common/permission-only/PermissionOnly';

class CaregiverList extends React.Component {
	static propTypes = {
		setPrimaryCaregiver: PropTypes.func.isRequired,
		createCaregiver: PropTypes.func.isRequired,
		updateCaregiver: PropTypes.func.isRequired,
		deleteCaregiver: PropTypes.func.isRequired,
		caregivers: PropTypes.arrayOf(
			PropTypes.shape({
				firstName: PropTypes.string.isRequired,
				middleName: PropTypes.string,
				lastName: PropTypes.string.isRequired,
				email: PropTypes.string.isRequired,
				phones: PropTypes.arrayOf(PropTypes.string).isRequired,
				isPrimary: PropTypes.bool.isRequired,
				type: PropTypes.string
			})).isRequired,
		editMode: PropTypes.bool.isRequired,
		defaultSettings: PropTypes.arrayOf(PropTypes.shape({
			id: String,
			value: Boolean
		})).isRequired
	}

	state = {
		isDeleteModalOpen: false,
		isModalOpen: false,
		isSettingsOpen: false,
		currentCaregiver: null
	}

	onSelectPrimary = currentCuregiver => (event) => {
		if (event.target.checked) {
			this.props.setPrimaryCaregiver(currentCuregiver);
		}
	}

	onUpdateCaregiver = currentCaregiver => () => {
		this.setState({ currentCaregiver, isModalOpen: true });
	}

	onCaregiverSettings = currentCaregiver => () => {
		this.setState({ currentCaregiver, isSettingsOpen: true });
	}

	onDeleteCaregiver = currentCaregiver => () => {
		this.setState({ currentCaregiver, isDeleteModalOpen: true });
	}

	onSettingsSubmit = ({settings}) => {
		if (this.state.currentCaregiver) {
			let caregiver = {...this.state.currentCaregiver, settings}
			this.props.updateCaregiver(caregiver);
		}
		this.hideModal();
	}

	onWizardSubmit = (caregiver) => {
		console.log('onWizardSubmit', caregiver);
		if (this.state.currentCaregiver) {
			this.props.updateCaregiver(caregiver);
		} else {
			this.props.createCaregiver(caregiver);
		}
		this.hideModal();
	}

	deleteCaregiver = () => {
		this.props.deleteCaregiver(this.state.currentCaregiver);
		this.hideModal();
	}

	hideModal = () => {
		this.setState({ isModalOpen: false, isSettingsOpen: false, isDeleteModalOpen: false, currentCaregiver: null });
	}

	showModal = () => {
		this.setState({ isModalOpen: true });
	}

	render() {
		const { isDeleteModalOpen, isModalOpen, isSettingsOpen, currentCaregiver } = this.state;
		const { caregivers, editMode, mutationStatus } = this.props;

		var steps = availableSteps;

		return (
			<div className='root'>
				{ /* language=CSS */ }
				<style jsx>{`
					.list {
						background-color: #f4f4f4;
						display: flex;
						flex-direction: column;
					}

					.header, .row {
						flex: 1;
						display: flex;
						flex-direction: row;
					}

					.header {
						background-color: #DDD;
						padding: 10px;
						color: #3b5173;
						font-size: 14px;
						font-weight: 800;
					}

					.column1 {
						flex: 0.6;
						padding-left: 10px;
					}

					.column2, .column3, .column4, .column5, .column6 {
						flex: 2;
					}

					.column7 {
						flex: 3;
					}
					.empty {
						padding: 10px;
						text-align: center;
					}

					.delete-warning {
						font-weight: 500;
						margin-top: 12px;
					}

					.delete-body {
						display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						height: 256px;
						padding: 24px;
						line-height: 1.5;
						text-align: center;
					}
				`}</style>
				{ isDeleteModalOpen && (<Modal
					title='Delete caregiver'
					theme='error'
					isOpen
					onClose={this.hideModal}
					button={<Button theme='error' onClick={this.deleteCaregiver}>Yes, delete</Button>}
				>
					<div className='delete-body'>
						You are going to delete <br />
						<strong>{currentCaregiver.firstName} {currentCaregiver.middleName ? `(${currentCaregiver.middleName})` :""} {currentCaregiver.lastName}</strong>
						<strong>Warning: this cannot be undone</strong>
					</div>
				</Modal>) }
				<CaregiverWizard
					isOpen={isModalOpen}
					onClose={this.hideModal}
					steps={steps}
					subscriber={this.props.subscriber}
					initialValues={currentCaregiver ||
						{ phones: [], sendInvite: true,
							settings: this.props.defaultSettings
						}
					}
					onSubmit={this.onWizardSubmit}
				/>
				<CaregiverSettings
					isOpen={isSettingsOpen}
					onClose={this.hideModal}
					initialValues={currentCaregiver || { settings:[
						{ settingType: "SYSTEM", value: true},
						{ settingType: "BATTERY", value: true},
						{ settingType: "WEAR", value: true},
						{ settingType: "OUT_OF_HOME", value: true},
						{ settingType: "IRREGULAR_ACTIVITY", value: true}
					] }}
					onSubmit={this.onSettingsSubmit}
				/>
				<Title
					size='medium'
					action={
						editMode ? (<PermissionOnly resource={PermissionOnly.resources.SUBSCRIBER} action='EDIT'>
							<Button link icon='pencil' onClick={this.showModal}>
								Add Caregiver
							</Button>
						</PermissionOnly>) : <React.Fragment />
					}
				>
					CAREGIVERS
				</Title>
				<div className='list'>
					<div className='header'>
						<div className='column1'>Primary</div>
						<div className='column2'>Username</div>
						<div className='column3'>Name</div>
						<div className='column4'>Relationship</div>
						<div className='column5'>Type</div>
						<div className='column6'>Mobile</div>
						<div className='column7'>Status</div>
					</div>
					{caregivers && caregivers.length
						? caregivers.map((item, index) => (
							<CaregiverListItem
								key={item.id}
								item={item}
								index={index}
								editMode={editMode}
								onSelectPrimary={this.onSelectPrimary(item)}
								onUpdateCaregiver={this.onUpdateCaregiver(item)}
								onCaregiverSettings={this.onCaregiverSettings(item)}
								onDeleteCaregiver={this.onDeleteCaregiver(item)}
							/>
						))
						: <div className='empty'>No caregivers found.</div>
					}
				</div>
			</div>
		);
	}
}


export default CaregiverList

