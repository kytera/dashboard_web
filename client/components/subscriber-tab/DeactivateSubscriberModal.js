import React from 'react';
import PropTypes from 'prop-types';
import { propType } from 'graphql-anywhere';

import Modal from '../common/modal/Modal';
import Button from '../common/button/Button';
import subscriberFragment from '../../graphql/subscriber/subscriber.fragment.graphql';

const DeactivateSubscriberModal = ({ isOpen, onClose, onConfirm, subscriber }) => (
	<Modal
		isOpen={isOpen}
		title='SUSPEND SUBSCRIBER'
		theme='error'
		onClose={onClose}
		button={
			<Button
				onClick={onConfirm}
				theme='error'
			>Suspend</Button>
		}
	>
		<div>
			{ subscriber && (
				<div className='container'>
					<div className='msg'>
						You are going to suspend
					</div>
					<div className='msg'>
						{subscriber.firstName} {subscriber.lastName}
					</div>
				</div>
			)}
		</div>
		{ /* language=CSS */ }
		<style jsx>{`
				.container {
					padding: 30px;
					padding-top: 30px;
				}
				.msg, .warning {
					line-height: 20px;
					flex: 1;
					text-align: center;
					color: var(--primary-color);
				}
				.warning {
					margin-top: 30px;
					font-weight: 700;
				}
			`}</style>
	</Modal>
);
DeactivateSubscriberModal.propTypes = {
	onClose: PropTypes.func.isRequired,
	onConfirm: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired,
	subscriber: propType(subscriberFragment),
};
DeactivateSubscriberModal.defaultProps = {
	subscriber: {},
};
export default DeactivateSubscriberModal;
