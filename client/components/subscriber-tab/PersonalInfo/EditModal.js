import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import {compose, withHandlers} from 'recompose';

import Button from '../../common/button/Button';
import Modal from '../../common/modal/Modal';


const EditModal = ({ handleSubmit, component: Component, handleClose, ...rest }) => {
	return (
		<Modal
			{...rest}
			onClose={handleClose}
			onSubmit={handleSubmit}
			button={<Button type='submit' theme='accent'>Done</Button>}
		>
			<Component initialValues={rest.initialValues} form={rest.form}/>
		</Modal>
	);
};

EditModal.propTypes = {
	handleSubmit: PropTypes.func.isRequired,
	component: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
};

export default compose(
	withHandlers({
		onSubmit: props => (...params) => {
			props.onSubmit(...params);
			props.onClose();
		},
	}),
	reduxForm({
		form: 'EDIT_SUBSCRIBER',
		enableReinitialize: true,
		keepDirtyOnReinitialize: true,
	}),
	withHandlers({
		handleClose: props => (...params) => {
			props.reset();
			props.onClose();
		},
	})
)(EditModal)
