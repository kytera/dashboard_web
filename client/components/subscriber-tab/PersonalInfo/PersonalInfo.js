import React from 'react';
import PropTypes from 'prop-types';
import Title from '../../common/title/Title';
import Button from '../../common/button/Button';
import PersonInfo from './PersonInfo';
import AddressInfo from './AddressInfo';
import PermissionOnly from '../../common/permission-only/PermissionOnly';
import {dashboardServiceStatusEnum as serviceStatuses} from '../../../../server/graphql/utils/subscriber';
import PlanInfo from './PlanInfo';


const PersonalInfo = ({ onEditSave, onMedicalAlarm, onEditEnable, onSettingsEnable, onSuspendSubscriber, editMode, subscriber, saveLoading, onSubscriberUpdate }) => {
	const showButtons = {
			suspend : [serviceStatuses.active],
			edit : [serviceStatuses.active, serviceStatuses.pending_activation, serviceStatuses.pending_installation, serviceStatuses.suspended],
			settings: [serviceStatuses.active, serviceStatuses.pending_activation, serviceStatuses.pending_installation, serviceStatuses.suspended]
		}
	return (
	<div>
		<Title
			size='medium'
			action={
				<PermissionOnly resource={PermissionOnly.resources.SUBSCRIBER} action='EDIT'>
					<React.Fragment>
						{ !editMode && showButtons['suspend'].find(item => item === subscriber.dashboardServiceStatus) && <Button style={{marginLeft:'20px'}} theme='error' link icon='stop' onClick={onSuspendSubscriber}>Suspend</Button> }
						{ !editMode && showButtons['settings'].find(item => item === subscriber.dashboardServiceStatus) && <Button onClick={onSettingsEnable} theme='link' loading={saveLoading} icon='settings' style={{marginLeft:'20px'}}>
							Settings
						</Button>}
						{!editMode && showButtons['edit'].find(item => item === subscriber.dashboardServiceStatus) ?
							<Button link icon='pencil' onClick={onEditEnable} style={{marginLeft:'20px'}}>
								Edit
							</Button>
							: showButtons['edit'].find(item => item === subscriber.dashboardServiceStatus) ? <Button onClick={onEditSave} theme='accent' loading={saveLoading} style={{marginLeft:'20px'}}>
								Save
							</Button> : <React.Fragment/>
						}
					</React.Fragment>
				</PermissionOnly>
			}
		>

		</Title>
		<div className='content'>
			<AddressInfo editMode={editMode} info={subscriber} onSubscriberUpdate={onSubscriberUpdate} />
			<PersonInfo editMode={editMode} info={subscriber} onSubscriberUpdate={onSubscriberUpdate} />
			<PlanInfo onEditSave={onEditSave} editMode={editMode} onMedicalAlarm={onMedicalAlarm} info={subscriber} onSubscriberUpdate={onSubscriberUpdate} />
		</div>
		{ /* language=CSS */ }
		<style jsx>{`
		.content {
			display: flex;
			flex-direction: row-reverse;
		}
	`}</style>
	</div>)
};

PersonalInfo.propTypes = {
	onEditEnable: PropTypes.func.isRequired,
	onSettingsEnable: PropTypes.func.isRequired,
	onEditSave: PropTypes.func.isRequired,
	onSubscriberUpdate: PropTypes.func.isRequired,
	saveLoading: PropTypes.bool.isRequired,
	subscriber: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		middleName: PropTypes.string,
		lastName: PropTypes.string.isRequired,
		phones: PropTypes.arrayOf(PropTypes.string).isRequired,
		id: PropTypes.string.isRequired,
		createdAt: PropTypes.string.isRequired,
		gender: PropTypes.string.isRequired,
		birthDate: PropTypes.string,
		avatar: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
		agencies: PropTypes.arrayOf(PropTypes.shape({
			agency_type: PropTypes.string.isRequired,
			number: PropTypes.string.isRequired
		})).isRequired,
		address: PropTypes.object.isRequired,
	}).isRequired,
	editMode: PropTypes.bool.isRequired,
};
export default PersonalInfo;
