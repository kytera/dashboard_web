import React from 'react';
import PropTypes from 'prop-types';
import { withState } from 'recompose';

import Icon from '../../common/icon/Icon';
import Button from '../../common/button/Button';
import AddressStep from '../../wizards/fieldsets/subscriber/address/AddressStep';
import EditModal from './EditModal';
import countries from '../../../data/countries';

const AddressInfo = ({ info, editMode, onSubscriberUpdate, isModalOpen, setModalOpen }) => {
	const { country, street, streetOptional, city, state, zipCode, additional, timezone, county } = info.address || {};

	let displayTimezone = timezone;

	const countryData = countries.filter(c => c.short_name === country);
	if (countryData.length > 0 && countryData[0].timezones) {
		const timezoneData = countryData[0].timezones.filter(t => t.value === timezone);
		if (timezoneData.length > 0) {
			displayTimezone = timezoneData[0].display;
		}
	}

	return (
		<div className='container'>
			<EditModal
				isOpen={isModalOpen}
				title='Edit address'
				onClose={() => setModalOpen(false)}
				onSubmit={onSubscriberUpdate}
				component={AddressStep}
				initialValues={info}
			/>
			<style jsx>{`
			.container {
				display: flex;
				flex: 1;
				align-items:start;
				background-color: #f4f4f4;
			}
			.content {
				flex: 1;
				display: flex;
				flex-direction: column;
				padding: 10px;
			}
			.icon {
				padding: 6px;
			}
			.info {
				flex: 1;
				color: #3e5475 !important;
				display: flex;
				flex-direction: row;
				margin-top: -5px;
			}
			.left {
				flex: 1;
			}
			.right {
				min-width: 36px;
			}
			.header {
				padding-left:0px;
				font-size:13px;
				line-height:18px;
				font-weight:700;
				text-transform: uppercase;
				margin-bottom:6px;
			}
			.address {
				padding:0px 0 0 0px;
				font-size: 13px;
				line-height: 16px;
				margin-bottom: 10px;
			}
			.edit.info:hover {
				background-color: white !important;
				border-radius: 5px;
			}
			.edit.info button {
				cursor: pointer;
			}
			
			.code {
				font-size: 13px;
				line-height: 18px;
				color: #3b5173;
			}
		`}</style>
			<div className='content'>
				<div className='left'>
					<div className='header'>Address</div>
				</div>
				<div className={`${editMode ? 'edit ' : ''} info`}>
					<div className='left'>
						<div className='address'>
							{street}<br/>
							{streetOptional ? <span>{streetOptional}<br/></span> : null}
							{city}{state ? `, ${state}` : ''} {zipCode}<br/>
							{county ? <span>{county} County<br/></span> : null}
							{country}<br/>
							<br/>
							Time Zone: {displayTimezone}
						</div>
						<div className='code'>{additional}</div>
					</div>
					<div className='right'>
						{editMode && <Button link icon='pencil' onClick={() => setModalOpen(true)} /> }
					</div>
				</div>
			</div>
		</div>
	);
};

AddressInfo.propTypes = {
	info: PropTypes.shape({
		country: PropTypes.string,
		street: PropTypes.string,
		streetOptional: PropTypes.string,
		city: PropTypes.string,
		state: PropTypes.string,
		zipCode: PropTypes.string,
		additional: PropTypes.string,
		timezone: PropTypes.string
	}).isRequired,
	editMode: PropTypes.bool.isRequired,
	isModalOpen: PropTypes.bool.isRequired,
	setModalOpen: PropTypes.func.isRequired,
	onSubscriberUpdate: PropTypes.func.isRequired,
};

export default withState('isModalOpen', 'setModalOpen', false)(AddressInfo);
