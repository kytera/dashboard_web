import React from 'react';
import PropTypes from 'prop-types';
import { withState } from 'recompose';

import EditModal from './EditModal';
import Button from '../../common/button/Button'
import PlanStep from '../../wizards/fieldsets/subscriber/plan/PlanStep'

const validatePlans = (subscriber) => {
	var errors = {};

	if(!subscriber.medicalAlarmService && !subscriber.wellnessService ) {
		//errors.medicalAlarmService = 'Required';
		//errors.wellnessService = 'Required';
	}

	return errors;
}


const PlanInfo = ({ info, onMedicalAlarm, onEditSave, editMode, onSubscriberUpdate, isModalOpen, setModalOpen }) => {
	const saveFullName = ({ fullName }) => {
		let [firstName, middleName, lastName] = fullName.split(' ', 3);
		if (!lastName) {
			lastName = middleName;
			middleName = null;
		}
		onSubscriberUpdate({ firstName: firstName, lastName: lastName, middleName: middleName });
		onEditSave();
	}

	const { medicalAlarmService, wellnessService, firstName, middleName, lastName } = info;
	const info2 = { ...info };
	info2.onMedicalAlarm = onMedicalAlarm;
	let fullName = middleName && middleName.length > 0 ? `${firstName} (${middleName}) ${lastName}` : `${firstName} ${lastName}`;
	return (
		<div className='root'>
			<EditModal
				isOpen={isModalOpen}
				title='Edit Plan'
				onClose={() => setModalOpen(false)}
				onSubmit={onSubscriberUpdate}
				component={PlanStep}
				initialValues={info2}
				validate={validatePlans}
			/>
			<div className='content'>
				<div className='row profile'>
					<div className='personName'>
						<div>{fullName}</div>
					</div>
				</div>
				<div className={`${editMode ? 'edit ' : ''} plan`}>
					<div className='row plan-title'>
						<div className='title expand'>
							Plan
						</div>
						<div>
							{editMode && <Button link icon='pencil' onClick={() => setModalOpen(true)} /> }
						</div>
					</div>
					<div className='row'>
						<div className={`${medicalAlarmService === null ? 'disabled' : '' } medical plan-box expand`}>
							<div className='plan-type'>Medical Alarm</div>
							<div className='plan-provider'>{medicalAlarmService ? medicalAlarmService.displayName : 'None'}</div>
						</div>
						<div className={`${wellnessService === null ? 'disabled' : '' } wellness plan-box expand`}>
							<div className='plan-type'>Wellness</div>
							<div className='plan-provider'>{wellnessService ? wellnessService.displayName : 'None'}</div>
						</div>
					</div>
				</div>
			</div>
			{ /* language=CSS */ }
			<style jsx>{`
				.root {
					flex: 1;
					display: flex;
				}

				.content {
					flex: 1;
					display: flex;
					flex-direction: column;
					padding: 10px;
					padding-left: 20px;
					background-color: #f4f4f4;
				}

				.plan {
					padding:5px;
					position:relative;
					left:-5px;
					padding-top: 5px;
				}

				.edit.plan:hover {
					background-color: white !important;
					border-radius: 5px;
				}
				.edit.plan button {
					cursor: pointer;
				}

				.row {
					flex: initial;
					display: flex;
					flex-direction: row;
				}

				.row > .expand {
					flex-grow: 1;
					flex-basis: 0;
				}

				.profile {
					margin-bottom:16px;
				}

				.plan-title {
					line-height:34px;
					margin-bottom:5px;
				}

				.personName {
					font-size:24px;
					text-transform: uppercase;
					font-weight:bold;
				}

				*.disabled {
					color: #818181;
				}

				.title {
					text-transform:uppercase;
					font-weight:bold;
				}

				.plan-box > * {
					line-height: 1.5em;
				}

				.plan-type {
					font-weight: bold;
				}
			`}</style>
		</div>
	);
}

PlanInfo.propTypes = {
	info: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		middleName: PropTypes.string,
		lastName: PropTypes.string.isRequired,
	}).isRequired,
	onSubscriberUpdate: PropTypes.func.isRequired,
	editMode: PropTypes.bool
};


export default withState('isModalOpen', 'setModalOpen', false)(PlanInfo);
