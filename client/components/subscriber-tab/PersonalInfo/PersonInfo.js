import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { capitalize } from 'lodash';
import { withState, compose } from 'recompose';

import DisplayPhone from '../../common/display-phone/DisplayPhone';
import Button from '../../common/button/Button';
import PersonalInfoStep from '../../wizards/fieldsets/subscriber/personal-info/PersonalInfoStep';
import PhoneStep from '../../wizards/fieldsets/subscriber/phone/PhoneStep';
import EditModal from './EditModal';


const PersonInfo = ({
	                    editMode,
	                    info,
	                    isInfoModalOpen,
	                    setInfoModalOpen,
	                    isPhoneModalOpen,
	                    setPhoneModalOpen,
	                    onSubscriberUpdate,
}) => {
	const { firstName, middleName, lastName, id, createdAt, birthDate, phones, homePhone, email } = info;
	const gender = info.gender ? capitalize(info.gender) : '';
	let fullName = middleName && middleName.length > 0 ? `${firstName} (${middleName}) ${lastName}` : `${firstName} ${lastName}`;
	return (
		<div className='root'>
			<EditModal
				isOpen={isInfoModalOpen}
				title='Edit personal info'
				onClose={() => setInfoModalOpen(false)}
				onSubmit={onSubscriberUpdate}
				component={PersonalInfoStep}
				initialValues={info}
			/>
			<EditModal
				isOpen={isPhoneModalOpen}
				title='Edit phones'
				onClose={() => setPhoneModalOpen(false)}
				onSubmit={onSubscriberUpdate}
				component={PhoneStep}
				initialValues={info}
			/>
			<div className='content'>
				<div className='info'>
					<div className='header'>Personal Info</div>
					<div className={`${editMode ? 'edit ' : ''} profile`}>
						<div className='left'>
							{ !!birthDate && <div className='personInfo'>Age: {moment().diff(moment(birthDate), 'years')}</div> }
							{ !!birthDate && <div className='personInfo'>DOB: {moment(birthDate).format('MM/DD/YY')}</div> }
							<div className='personInfo'>Gender: {gender}</div><br />
							{ email && <div className='personInfo'>{email}</div> }
						</div>
						<div className='right'>
							{editMode &&
								<Button link icon='pencil' onClick={() => setInfoModalOpen(true)} />
							}
						</div>
					</div>
					<div className={`${editMode ? 'edit ' : ''} phoneContainer`}>
						<div className='left'>
							<div className='phones'>
								<div className='phone'>
									<span className="inner-phone">Home:</span><div>{ homePhone ? (<DisplayPhone>{homePhone}</DisplayPhone>) : 'None' }</div>
								</div>
								<div className='phone' key={phones[0]}>
									<span className="inner-phone">Mobile:</span><div>{ phones && phones[0] ? (<DisplayPhone>{phones[0]}</DisplayPhone>) : 'None'}</div>
								</div>
							</div>
						</div>
						<div className='right'>
							{editMode && <Button link icon='pencil' onClick={() => setPhoneModalOpen(true)} /> }
						</div>
					</div>
				</div>
			</div>
			{ /* language=CSS */ }
			<style jsx>{`
				.root {
					flex: 1;
					display: flex;
				}

				.content {
					flex: 1;
					display: flex;
					flex-direction: row;
					padding: 10px;
					background-color: #f4f4f4;
				}

				.info {
					flex: 1;
				}

				.header {
					font-size:12px;
					line-height:18px;
					font-weight:700;
					text-transform: uppercase;
					margin-bottom:0px;
				}

				.personName {
					padding-left:4px;
					font-size:24px;
					font-weight:bold;
				}

				.profile, .phone {
					padding:0px 0 0 0px;
					display: flex;
					flex-direction: row;
				}

				.phoneContainer {
					margin-top: 0px;
					padding-left: 0px;
					display: flex;
				}

				.inner-phone {
					margin-right: 5px;
				}

				.phones {
					margin: 0px 0;
					line-height: 1.5;
				}

				.left {
					color: #3e5475;
					flex: 1;
				}

				.edit.profile:hover, .edit.phoneContainer:hover {
					background-color: white !important;
					border-radius: 5px;
				}
				.edit.profile button, .edit.phoneContainer button {
					cursor: pointer;
				}

				.right {
					min-width: 36px;
				}

				.personInfo {
					line-height: 17px;
					font-size: 13px;
					white-space: nowrap;
				}
			`}</style>

		</div>
	);
};

PersonInfo.propTypes = {
	info: PropTypes.shape({
		email: PropTypes.string.isRequired,
		firstName: PropTypes.string.isRequired,
		middleName: PropTypes.string,
		lastName: PropTypes.string.isRequired,
		phones: PropTypes.arrayOf(PropTypes.string).isRequired,
		id: PropTypes.string.isRequired,
		createdAt: PropTypes.string.isRequired,
		gender: PropTypes.string.isRequired,
		birthDate: PropTypes.string
	}).isRequired,
	editMode: PropTypes.bool.isRequired,
	isInfoModalOpen: PropTypes.bool.isRequired,
	setInfoModalOpen: PropTypes.func.isRequired,
	isPhoneModalOpen: PropTypes.bool.isRequired,
	setPhoneModalOpen: PropTypes.func.isRequired,
	onSubscriberUpdate: PropTypes.func.isRequired,
};

PersonInfo.defaultProps = {
	info: {}
};

const enhance = compose(
	// TODO: rewrite with recompose/withStateHandlers
	withState('isInfoModalOpen', 'setInfoModalOpen', false),
	withState('isPhoneModalOpen', 'setPhoneModalOpen', false),
);

export default enhance(PersonInfo);
