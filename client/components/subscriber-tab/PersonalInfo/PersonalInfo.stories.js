import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import SUBSCRIBER from '../subscriber.mock';
import PersonalInfo from './PersonalInfo';

storiesOf('subscriber-tab/PersonalInfo', module)
		.add('default', () => <PersonalInfo
			subscriber={SUBSCRIBER}
			onEditEnable={action('edit')}
			onEditSave={action('save')}
			onSubscriberUpdate={action('update')}
			saveLoading={false}
			editMode={false}
		/>)
		.add('edit', () => <PersonalInfo
			subscriber={SUBSCRIBER}
			onEditEnable={action('edit')}
			onEditSave={action('save')}
			onSubscriberUpdate={action('update')}
			saveLoading={false}
			editMode={true}
		/>)
		.add('loading', () => <PersonalInfo
			subscriber={SUBSCRIBER}
			onEditEnable={action('edit')}
			onEditSave={action('save')}
			onSubscriberUpdate={action('update')}
			saveLoading={true}
			editMode={true}
		/>);
