import React from 'react';
import PropTypes from 'prop-types';

import Title from '../../common/title/Title';
import Button from '../../common/button/Button';
import Modal from '../../common/modal/Modal'

import PermissionOnly from '../../common/permission-only/PermissionOnly';
import DisplayRawPhone from '../../common/display-phone/DisplayRawPhone';
import Input from '../../common/input/Input';

class EmergencyServicesInfo extends React.Component {
	static propTypes = {
		onSubscriberUpdate: PropTypes.func.isRequired,
		editMode: PropTypes.bool.isRequired,
		info: PropTypes.arrayOf(PropTypes.shape({
			agency_type: PropTypes.string.isRequired,
			number: PropTypes.string.isRequired
		})).isRequired
	}

	state = {
		isModalOpen: false,
		medical: '',
		fire: '',
		police: ''
	}

	hideModal = () => {
		this.setState({ isModalOpen: false });
	}

	showModal = () => {
		const { info } = this.props;
		const medical = info.reduce((result, item) => {return result ? result : item.agency_type === 'medical' ? item.number : null},null);
		const police = info.reduce((result, item) => {return result ? result : item.agency_type === 'police' ? item.number : null},null);
		const fire = info.reduce((result, item) => {return result ? result : item.agency_type === 'fire' ? item.number : null},null);

		this.setState({ isModalOpen: true, medical, police, fire });
	}

	saveDetails = () => {
		const { onSubscriberUpdate } = this.props;

		onSubscriberUpdate({fire:this.state.fire, police: this.state.police, medical: this.state.medical})
		this.setState({isModalOpen: false})
	}

	render() {
		const { isModalOpen } = this.state;
		const { editMode, info } = this.props;

		const medical = info.reduce((result, item) => {return result ? result : item.agency_type === 'medical' ? item.number : null},null);
		const police = info.reduce((result, item) => {return result ? result : item.agency_type === 'police' ? item.number : null},null);
		const fire = info.reduce((result, item) => {return result ? result : item.agency_type === 'fire' ? item.number : null},null);

		return (
			<div className='root'>
				<Modal
					isOpen={isModalOpen}
					title='Edit Emergency Services'
					onClose={this.hideModal}
					onSubmit={this.saveDetails}
					button={<Button theme='accent' onClick={this.saveDetails}>Done</Button>}
				>
					<div className='modal-container'>
						<div className='inputItem'>
							<Input name='medical' className='' label='Medical' input={{value: this.state.medical || '', onChange:e => this.setState({medical: e.target.value})}} />
						</div>
						<div className='inputItem'>
							<Input name='fire' label='Fire' input={{value: this.state.fire || '', onChange:e => this.setState({fire: e.target.value})}} />
						</div>
						<div className='inputItem'>
							<Input name='police' label='Police' input={{value: this.state.police || '', onChange:e => this.setState({police: e.target.value})}} />
						</div>
					</div>
				</Modal>
				{ /* language=CSS */ }
				<style jsx>{`
					.root {
						background-color:#f4f4f4;
					}

					.row {
						flex: 1;
						display: flex;
						flex-direction: row;
						padding:16px 0;
					}

					.inputItem {
						margin-bottom:15px;
					}

					.header {
            text-transform:uppercase;
						color: #3b5173;
						font-size: 12px;
						font-weight: 800;
						row-height:18px;
						margin-bottom:4px;
					}

					.col {
						flex: 1;
						padding:0 22px;
					}

					.edit {
						display: flex;
						flex-direction: row;
						justify-content: space-between;
					}

					.buttonGroup {
						width:40px;
					}

					.modal-container {
						padding:20px;
					}
					
				`}</style>
				<Title size='medium'>EMERGENCY SERVICES</Title>
        <div className='row'>
          <div className='col'>
            <div className='header'>medical</div>
			  <div>{medical}</div>
          </div>
          <div className='col'>
            <div className='header'>fire</div>
            <div>{fire}</div>
          </div>
					<div className={`col ${editMode ? 'edit' : ''}`}>
						<div>
							<div className='header'>police</div>
							<div>{police}</div>
						</div>
						{ editMode && <PermissionOnly resource={PermissionOnly.resources.SUBSCRIBER} action='EDIT'>
								<div className='buttonGroup'>
									<Button link icon='pencil' onClick={this.showModal} />
								</div>
							</PermissionOnly>
						}
          </div>
        </div>
			</div>
		);
	}
}


export default EmergencyServicesInfo

