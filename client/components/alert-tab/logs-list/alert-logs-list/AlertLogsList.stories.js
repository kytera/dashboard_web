import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { AlertLogsList } from './AlertLogsList';
import LOGS from './AlertLogsList.mock'

storiesOf('alert-tab/AlertLogsList', module)
	.add('default', () => (
		<AlertLogsList logs={LOGS} />
	))
	.add('empty', () => (
		<AlertLogsList logs={[]} />
	));
