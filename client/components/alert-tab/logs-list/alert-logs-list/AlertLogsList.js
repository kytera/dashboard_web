import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { graphql } from 'react-apollo';
import { compose, withProps } from 'recompose';
import { groupBy, sortBy } from 'lodash';

import ListSection from '../list-section/ListSection';

import ACTIVE_ALERT_LOGS_QUERY from '../../../../graphql/log/activeAlertLogs.query.gql';
import ScrollContainer from '../../../common/scroll-container/ScrollContainer';
import logTypes from '../../../../graphql/log/logTypes';


export class AlertLogsList extends Component {
	componentWillReceiveProps(newProps) {
		const newLogs = newProps.logs;
		const oldLogs = this.props.logs;

		if (newLogs !== oldLogs) {
			this.shouldScroll = true;
		}
	}

	componentDidUpdate() {
		if (this.shouldScroll) {
			this.scrollContainer.scrollToBottom();
			this.shouldScroll = null;
		}
	}

	render() {
		const { logs } = this.props;
		return (
			<div className='logs-list'>
				<ScrollContainer autoScroll={false} ref={(r) => { this.scrollContainer = r; }}>
					{logs.map(log => (<ListSection
						key={log.groupTitle}
						title={log.groupTitle}
						logs={log.items}
					/>))}
				</ScrollContainer>
				{ /* language=CSS */ }
				<style jsx>{`
					.logs-list {
						background-color: #fff;
						border-top: none;
						overflow-y: auto;
						display: flex;
						flex-direction: column;
						flex: 1;
					}
				`}</style>
			</div>
		);
	}
}

AlertLogsList.propTypes = {
	logs: PropTypes.arrayOf(PropTypes.shape({
		groupTitle: PropTypes.string.isRequired,
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
	})).isRequired,
};

const withQuery = compose(
	graphql(ACTIVE_ALERT_LOGS_QUERY,
		{
			options: ({ subscriberId }) => ({
				variables: { subscriberId },
				pollInterval: 30000,
				fetchPolicy: process.browser ? 'cache-and-network' : undefined,
			}),
			skip: ({ subscriberId }) => !subscriberId,
		},
	),
	// withQueryStatus({ emptyText: 'This subscriber has no logs yet', resultPath: 'alertLogs' }),
	withProps(({ data }) => {
		if (!data || !data.activeAlertLogs) {
			return {
				logs: [],
			};
		}

		const filtered = data.activeAlertLogs.filter(log => log.type !== logTypes.ACTIVITY);
		const sorted = sortBy(filtered, ['createdAt']);
		const grouped = groupBy(sorted, log => moment(log.createdAt).startOf('day').toISOString());
		const logs = Object.keys(grouped).map(key => ({
			groupTitle: moment(key).format('LL'),
			items: grouped[key],
		}));
		return {
			logs,
		};
	}),
);

export default withQuery(AlertLogsList);
