export default [
	{
		groupTitle: '01.01.2017',
		items: [
			{
				id: 1,
				timestamp: '2017-08-09T20:29:07.382Z',
				icon: '12',
				title: 'Alert message title',
				message: 'Alert message message',
				frame: true,
			},
			{
				id: 2,
				timestamp: '2017-08-09T20:29:07.382Z',
				icon: '12',
				message: 'John detected! its enough...',
			},
		],
	},
	{
		groupTitle: '02.01.2017',
		items: [
			{
				id: 3,
				timestamp: '2017-08-09T20:29:07.382Z',
				icon: '12',
				title: 'Alert message title',
				message: 'Alert message message',
				frame: true,
			},
			{
				id: 4,
				timestamp: '2017-08-09T20:29:07.382Z',
				icon: '12',
				message: 'John detected! its enough...',
			},
		],
	},
];
