import { storiesOf } from '@storybook/react';

import ListSection from './ListSection';

const logs = [
	{
		id: '123',
		timestamp: "2017-08-09T21:05:02.213Z",
		icon: '12',
		message: 'John detected crawling'
	},
];

storiesOf('alert-tab/logs-list/ListSection', module)
	.add('default', () => (
		<div style={{backgroundColor: '#fff', padding: '20px'}}>
			<ListSection title='Kitchen' logs={logs} />
		</div>))
	.add('empty state', () => (
		<div style={{backgroundColor: '#fff', padding: '20px'}}>
			<ListSection title='Title' logs={[]} />
		</div>));
