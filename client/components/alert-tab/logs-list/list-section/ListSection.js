import PropTypes from 'prop-types';

import ListItem from '../list-item/ListItem';
import Divider from '../../../common/content-divider/Divider';
import {DEFAULT_ROOM} from "../../../alerts-history-tab/groupLogsList";

const ListSection = ({ title, logs }) => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				width: 100%;
				margin-bottom: -15px;
				padding: 10px 0;
			}

			.divider {
				margin-bottom: 15px;
			}

			.list {
				padding: 0 8px;
			}
		`}</style>
		{ title !== DEFAULT_ROOM && (
			<div className='divider'>
				<Divider title={title} />
			</div>
		)}
		<div className='list'>
			{logs.map(item => <ListItem {...item} key={item.id} />)}
		</div>
	</div>
);

ListSection.propTypes = {
	title: PropTypes.string.isRequired,
	logs: PropTypes.arrayOf(PropTypes.shape({
		icon: PropTypes.string,
		title: PropTypes.string,
	})).isRequired,
};

export default ListSection;
