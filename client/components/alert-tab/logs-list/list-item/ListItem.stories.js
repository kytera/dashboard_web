import { storiesOf } from '@storybook/react';

import ListItem from './ListItem';

storiesOf('alert-tab/logs-list/ListItem', module)
	.add('default', () => (
		<div style={{ backgroundColor: '#fff', padding: '10px' }}>
			<ListItem
				timestamp="2017-08-09T21:05:02.213Z"
				icon='12'
				message='John detected crawling'
			/>
		</div>
	));
