import PropTypes from 'prop-types';
import Moment from 'react-moment';
import csx from 'classnames';

import Icon from '../../../common/icon/Icon';

const ListItem = ({timestamp, icon, title, message, frame}) => (
	<div className='root'>
		{/* language=CSS */}
		<style jsx>{`
			.root {
				display: flex;
				flex-flow: row nowrap;
				align-items: flex-start;
				justify-content: flex-start;
				padding: 8px 16px;
				color: #787776;
			}

			.date {
				margin-right: 10px;
			}

			.date {
				line-height: 27px;
				width: 75px;

			}

			.icon {
				margin-top: 1px;
				color: var(--primary-color);
			}

			.icon.crawling {
				color: var(--error-color);
			}

			.green .icon {
				color: var(--success-color);
			}

			.content {
				margin: 0px 10px;
				min-height: 27px;
				line-height: 20px;
			}

			.title {
				color: var(--primary-color);
				font-weight: bold;
				display: block;
				margin-bottom: 5px;
			}

			.alert-box {
				display: flex;
				flex-flow: row nowrap;
				justify-content: flex-start;
				width: 100%;
				margin-right: 10px;
				padding: 3px 10px 10px 10px;
			}

			.alert-box.green {
				border: 2px solid var(--success-color);
				border-radius: 3px;
				margin-top: -7px;
				margin-left: -7px;
			}
		`}</style>
		<div className='date'>
			<Moment format='hh:mm A'>{timestamp}</Moment>
		</div>
		<div className={csx('alert-box', {green: frame})}>
			<div className={csx('icon', icon)}>
				<Icon number={icon || '49'} size='20px'/>
			</div>
			<div className='content'>
				{!!title && <div className='title'>{title}</div>}
				{message}
			</div>
		</div>
	</div>
);

ListItem.propTypes = {
	timestamp: PropTypes.string.isRequired,
	icon: PropTypes.string.isRequired,
	message: PropTypes.string.isRequired,
	title: PropTypes.string,
	frame: PropTypes.bool,
};

ListItem.defaultProps = {
	title: null,
	frame: false,
};

export default ListItem;
