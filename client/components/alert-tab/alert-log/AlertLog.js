import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';

import Title from '../../common/title/Title';
import UpdateCaregiverForm from '../../common/update-caregiver-form/UpdateCaregiverForm';
import PermissionOnly from '../../common/permission-only/PermissionOnly';
import ACTIVE_ALERT_LOGS_QUERY from '../../../graphql/log/activeAlertLogs.query.gql';
import CREATE_UPDATE_LOG_MUTATION from '../../../graphql/log/createUpdateLog.mutation.gql';
import AlertLogsList from '../logs-list/alert-logs-list/AlertLogsList';

export const AlertLog = ({ addUpdate, subscriberId }) => (
	<div className='root'>
		{/* language=CSS */}
		<style jsx>{`
			.root {
				flex: 1;
				display: flex;
				flex-direction: column;
				background-color: #fff;
			}

			.update-form {
				padding: 16px;
			}
		`}</style>
		<Title>Alert Log</Title>
		<AlertLogsList subscriberId={subscriberId} shouldScroll={true} />
		{!!addUpdate && <PermissionOnly resource={PermissionOnly.resources.ALERT} action='EDIT'>
			{!!addUpdate && <div className='update-form'>
				<UpdateCaregiverForm onAddUpdate={addUpdate} showTitleBar />
			</div>}
		</PermissionOnly>}
	</div>
);

AlertLog.propTypes = {
	subscriberId: PropTypes.string.isRequired,
	addUpdate: PropTypes.func.isRequired,
};

const withFormHandlers = compose(
	graphql(CREATE_UPDATE_LOG_MUTATION, {
		props: ({ ownProps: { subscriberId, alertId }, mutate }) => ({
			addUpdate: log => mutate({
				variables: { subscriberId, alertId, log },
				refetchQueries: [
					{
						query: ACTIVE_ALERT_LOGS_QUERY,
						variables: { subscriberId },
					},
				],
			}),
		}),
	}),
);

export default withFormHandlers(AlertLog);
