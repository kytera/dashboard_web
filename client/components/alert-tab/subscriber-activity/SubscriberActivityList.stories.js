import { storiesOf } from '@storybook/react';

import { SubscriberActivityList } from './SubscriberActivityList';

import logs from './SubscriberActivity.mock';

storiesOf('alert-tab/SubscriberActivityList', module)
	.add('default', () => (
		<SubscriberActivityList logs={logs} />
	));
