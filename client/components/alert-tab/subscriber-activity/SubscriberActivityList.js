import {Component} from 'react';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo';
import {compose, withProps} from 'recompose';
import {sortBy} from 'lodash';

import ListSection from '../logs-list/list-section/ListSection';

import ACTIVE_ALERT_LOGS_QUERY from '../../../graphql/log/activeAlertLogs.query.gql';
import ScrollContainer from '../../common/scroll-container/ScrollContainer';
import logTypes from '../../../graphql/log/logTypes';
import {groupByRoom} from './groupByRoom';

class SubscriberActivityList extends Component {
	componentDidMount() {
		this.scrollContainer.scrollToBottom();
	}

	render() {
		let {logs} = this.props;

		return (
			<div className='root'>
				<ScrollContainer ref={r => this.scrollContainer = r}>
					{logs.map((log, i) => (<ListSection
						key={`${log.groupTitle}${i}`}
						title={log.groupTitle}
						logs={log.items}
					/>))}
				</ScrollContainer>
				{/* language=CSS */}
				<style jsx>{`
					.root {
						background-color: #fff;
						border-top: none;
						overflow-y: auto;
						display: flex;
						flex-direction: column;
						flex: 1;
					}
				`}</style>
			</div>
		);
	}
}

SubscriberActivityList.propTypes = {
	logs: PropTypes.arrayOf(PropTypes.shape({
		groupTitle: PropTypes.string.isRequired,
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
	})).isRequired,
};


const enhance = compose(
	graphql(ACTIVE_ALERT_LOGS_QUERY,
		{
			options: ({subscriberId}) => ({
				variables: {subscriberId},
				pollInterval: 30000,
				fetchPolicy: process.browser ? 'cache-and-network' : undefined,
			}),
			skip: ({subscriberId}) => !subscriberId,
		},
	),
	// withQueryStatus({ emptyText: 'This subscriber has no activity yet', resultPath: 'subscriberActivity' }),
	withProps(({data}) => {
		if (!data || !data.activeAlertLogs) {
			return {
				logs: [],
			};
		}

		const filtered = data.activeAlertLogs.filter(log => log.type === logTypes.ACTIVITY);
		const sorted = sortBy(filtered, ['timestamp']);
		const groupedLogs = groupByRoom(sorted);
		return {
			logs: groupedLogs,
		};
	}),
);

export default enhance(SubscriberActivityList);
