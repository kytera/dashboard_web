import PropTypes from 'prop-types';

import Title from '../../common/title/Title';
import SubscriberActivityList from './SubscriberActivityList';


export const SubscriberActivity = ({ subscriberId }) => (
	<div className='root'>
		{/* language=CSS */}
		<style jsx>{`
			.root {
				flex: 1;
				display: flex;
				flex-direction: column;
				border-right: 2px solid var(--disabled-color);
				background: #ffffff;
			}
		`}</style>
		<Title size='medium'>Subscriber Activity</Title>
		<SubscriberActivityList subscriberId={subscriberId} />
	</div>
);

SubscriberActivity.propTypes = {
	subscriberId: PropTypes.string.isRequired,
};

export default SubscriberActivity;
