export const defaultRoomName = 'no_room';

export const groupByRoom = (logs) => {
	const output = [];
	logs.forEach((log) => {
		const currentRoomName = log.room || defaultRoomName;
		const prevGroup = output[output.length - 1];

		if ((prevGroup && prevGroup.room === currentRoomName) || (prevGroup && !log.room)) {
			prevGroup.items.push(log);
		} else {
			output.push({
				room: currentRoomName,
				groupTitle: currentRoomName,
				items: [log],
			});
		}
	});

	return output;
};
