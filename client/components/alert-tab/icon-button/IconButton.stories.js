import { storiesOf } from '@storybook/react';

import IconButton from './IconButton';

storiesOf('alert-tab/IconButton', module)
	.add('active', () => <IconButton icon='person' isActive />)
	.add('not active', () => <IconButton icon='person' isActive={false} />);
