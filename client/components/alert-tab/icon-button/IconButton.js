import PropTypes from 'prop-types';
import csx from 'classnames';

import Icon from '../../common/icon/Icon';

const IconButton = ({ icon, isActive, ...rest }) => (
	<button
		className={csx('icon-button', { isActive })}
		{...rest}
	>
		{/* language=CSS */}
		<style jsx>{`
			.icon-button {
				outline: none;
				box-shadow: none;
				border: none;
				background-color: #fff;
				margin: 0 4px;
				padding: 6px;
				border-radius: 50%;
				display: flex;
				align-items: center;
				justify-content: center;
				cursor: pointer;
				color: var(--primary-color);
			}

			.icon-button.isActive {
				background-color: var(--primary-color);
				color: #fff;
			}
		`}</style>
		<Icon type={icon} size='24px' />
	</button>
);

IconButton.propTypes = {
	icon: PropTypes.string.isRequired,
	isActive: PropTypes.bool.isRequired,
};

export default IconButton;
