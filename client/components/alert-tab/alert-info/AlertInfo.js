import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import moment from 'moment';
import { graphql } from 'react-apollo';
import { branch, compose, renderNothing } from 'recompose';
import Alert from 'react-s-alert';
import ReactModal from 'react-modal';

import Button from '../../common/button/Button';
import Icon from '../../common/icon/Icon';
import ACTIVE_ALERT_QUERY from '../../../graphql/alert/activeAlert.query.gql';
import ALERTS_QUERY from '../../../graphql/alert/alerts.query.gql';
import CLOSE_ALERT_MUTATION from '../../../graphql/alert/closeAlert.mutation.gql';
import { apolloPropType } from '../../../apolloPropType';
import { Router } from '../../../../lib/routes';
import Radio, { RadioGroup } from '../../common/radio/Radio';

export class AlertInfo extends Component {
	state = {
		showAlertModal: false,
		closeAlertReason: null,
		falseAlertExplanation: null
	};

	constructor(props) {
		super(props);

		this.onCloseEvent = this.onCloseEvent.bind(this);
	}

	componentDidMount() {
		this.interval = setInterval(() => this.forceUpdate(), 30000);
		this.setState({ closeAlertReason: null, falseAlertExplanation: null });
	}

	componentWillUnmount() {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	async onCloseEvent() {
		this.setState({ showAlertModal: true });
	}

	async closeAlert() {
		try {
			this.setState({ showAlertModal: false });

			const subscriberId = this.props.subscriberId;
			await this.props.closeEvent({ closeAlertReason: this.state.closeAlertReason, closeAlertExplanation: this.state.falseAlertExplanation || '' });
			this.setState({ closeAlertReason: null, falseAlertExplanation: null });
			Router.replaceRoute('subscriber-info', {id: subscriberId});
		} catch (e) {
			console.error('error while closing event', e);
			Alert.error('Error while close event', {
				position: 'bottom-left',
				effect: 'stackslide',
			});
		}
	}

	closeModal() {
		this.setState({ showAlertModal: false, closeAlertReason: null, falseAlertExplanation: null });
	}

	handleReasonChange(reason) {
		this.setState({ closeAlertReason: reason });
	}

	onExplanationChange(value) {
		this.setState({ falseAlertExplanation: value });
	}

	render() {
		const {data: {activeAlert}} = this.props;
		const {title, icon, timestamp, stopTimestamp} = activeAlert;


		const duration = (stopTimestamp ? new Date(stopTimestamp).getTime() : Date.now()) - new Date(timestamp).getTime();
		const alertDuration = moment.duration(duration);
		const hours = Math.floor(alertDuration.asHours());
		const minutes = alertDuration.minutes();

		return (
			<div className='root'>
				<div className='alert-title'>
					<div>
						<Icon number={icon} size='15px'/>
					</div>
					<span>{title}</span>
				</div>
				<div className='alert-time'>
					<div>
						<Icon type='clock_start' size='15px'/>
					</div>
					<div className='alert-time-label'>
						<span className='header'>Start time</span>
						<Moment locale='en' format='hh:mm A'>{timestamp}</Moment>
					</div>
				</div>
				<div className='alert-time'>
					<div>
						<Icon type='clock_duration' size='15px'/>
					</div>
					<div className='alert-time-label'>
						<span className='header'>Duration</span>
						<div>
							{hours
								? <span>{hours}h</span>
								: null
							}
							{minutes
								? <span>&nbsp;{minutes}m</span>
								: null
							}
						</div>
					</div>
				</div>
				<div className='button-container'>
					<button className='close-btn' onClick={this.onCloseEvent}>
						<Icon type='close' size='15px'/>
						<span>Close event</span>
					</button>
				</div>

				<ReactModal
					isOpen={this.state.showAlertModal}
					ariaHideApp={false}
					style={{
						content: {
							position: 'absolute',
							width: '332px',
							height: '407px',
							borderRadius: '3px',
							top: '50%',
							left: '50%',
							backgroundColor: '#ffffff',
							transform: 'translate(-50%, -50%)',
							boxShadow: '0 2px 54px 0 rgba(0, 0, 0, 0.5)',
							zIndex: 999,
							padding: 0
						},
						overlay: {
							position: 'fixed',
							top: '0',
							left: '0',
							right: '0',
							bottom: '0',
							backgroundColor: 'rgba(255, 255, 255, 0.8)',
							zIndex: 999
						}
					}}
				>
					<div className="modal-title">
						<span>Close Alert</span>
						<div onClick={() => this.closeModal()} className="modal-close-alert">
							<Icon type='close' size='15px'/>
						</div>
					</div>
					<div className="modal-content">
						<div className="modal-header">Choose the reason for closing the alert</div>

						<div className="reason-container">
							<RadioGroup onChange={(evt) => { this.handleReasonChange(evt.target.value); } } name='reason'>
								<Radio value="treated">Alert was treated</Radio>
								<Radio value="false_alert">It was a false alert</Radio>
							</RadioGroup>
						</div>

						{
							this.state.closeAlertReason === 'false_alert' &&
						(
							<div>
								<div className='input-label'>
									<span>
										Please explain
									</span>
								</div>
								<textarea
									className='explanation'
									type='textarea'
									name='falseAlertExplanation'
									onChange={(event) => this.onExplanationChange(event.target.value)}
									rows='6'
								/>
							</div>
						)
						}

						<div className="buttons-container">
							<Button theme='link' onClick={() => this.closeModal()} style={{ color: '#dd2f37' }}>
								Cancel
							</Button>

							<Button theme='default'  disabled={this.state.closeAlertReason === null ? 'true' : undefined } onClick={() => this.closeAlert()} style={{ backgroundColor: this.state.closeAlertReason !== null ? '#dd2f37' : '#e9e9e9', marginLeft: 'auto' }}>
								Close
							</Button>
						</div>
					</div>
				</ReactModal>

				{/* language=CSS */}
				<style jsx>{`
					.root {
						width: 100%;
						background-color: var(--error-color);
						display: flex;
						flex-flow: row nowrap;
						justify-content: space-between;
						align-items: center;
						padding: 10px;
						color: #fff;
						font-size: 15px;
					}
					
					.buttons-container {
						margin-top: 20px;
						display: flex;
					}
					
					.explanation {
						width: 100%;
						height: 100px;
					}
					
					input, textarea {
						box-shadow: none;
						background: transparent none;
						appearance: none;
						padding: 8px 10px;
						z-index: 1;
						position: relative;
						width: 100%;
						color: var(--primary-color);
						border-radius: 3px;
						border: solid 2px #e9e9e9;
					}

					textarea {
						resize: none;
						padding: 12px 10px;
					}

					input:focus, textarea:focus {
						outline: none;
					}
					
					.input-label {
						padding-bottom: 5px;
						margin-top: 20px;
					}

					.input-label > span {
						color: var(--primary-color);
						font-weight: bold;
					}

					.reason-container {
						margin-top: 20px;
					}
					
					.modal-title {
						background-color: #dd2f37;
						height: 46px;
						font-size: 14px;
						font-weight: bold;
						color: #ffffff;
						text-transform: uppercase;
						line-height: 46px;
						padding-left: 20px;
						padding-right: 20px;
						display: flex;
					}
					
					.modal-close-alert {
						margin-left: auto;
						cursor: pointer;
					}
					
					.modal-content {
						padding: 20px;
					}
					
					.modal-header {
						font-weight: bold;
						color: #3b5173;
						text-align: center;
					}

					.alert-title {
						display: flex;
						align-items: flex-start;
						max-width: 50%;
					}

					.alert-title span {
						display: inline-block;
						margin-left: 10px;
					}

					.alert-time {
						display: flex;
						flex-flow: row nowrap;
						justify-content: space-between;
					}

					.alert-time-label {
						display: flex;
						flex-flow: column nowrap;
						margin-left: 10px;
						align-items: flex-start;
						font-weight: bold;
					}

					.alert-time-label span.header {
						font-size: 11px;
					}

					.button-container {
						width: 180px;
					}

					.close-btn {
						display: flex;
						flex-flow: row nowrap;
						align-items: center;
						justify-content: space-between;
						text-transform: uppercase;
						outline: none;
						box-shadow: none;
						border: none;
						border-radius: 3px;
						padding: 5px 8px;
						background: #fff;
						color: var(--error-color);
						cursor: pointer;
						height: 30px;
					}

					.close-btn span {
						margin-left: 5px;
						font-weight: bold;
					}
				`}</style>
			</div>
		);
	}
}

AlertInfo.propTypes = {
	closeEvent: PropTypes.func.isRequired,
	data: apolloPropType(ACTIVE_ALERT_QUERY).isRequired,
};

const enhance = compose(
	branch(
		props => !props.data || !props.data.activeAlert,
		renderNothing,
	),
	graphql(CLOSE_ALERT_MUTATION, {
		props: ({ownProps: {data, subscriberId, state}, mutate}) => ({
			closeEvent: ({ closeAlertExplanation, closeAlertReason }) => {
				console.log('closeEvent', closeAlertReason, closeAlertExplanation);
				return mutate({
					variables: { subscriberId, alertId: data.activeAlert.alertId, closeAlertReason, closeAlertExplanation },
					refetchQueries: [
						{
							query: ACTIVE_ALERT_QUERY,
							variables: {
								subscriberId,
							},
						},
						{
							query: ALERTS_QUERY,
							variables: {
								subscriberId,
							},
						},
					],
				});
			}
		}),
	}),
);

export default enhance(AlertInfo);
