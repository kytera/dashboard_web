import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { AlertInfo } from './AlertInfo';

const ACTIVE_ALERT = {
	title: 'Test title',
	icon: '12',
	timestamp: "2017-08-09T20:16:37.646Z",
	stopTimestamp: "2017-08-09T21:18:37.646Z",
};

storiesOf('alert-tab/AlertInfo', module)
	.add('default', () => <div style={{ width: '900px' }}>
		<AlertInfo
			data={{
				loading: false, error: null,
				activeAlert: ACTIVE_ALERT,
			}}
			closeEvent={action('close')}
		/>
	</div>);
