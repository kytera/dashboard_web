import React from 'react';
import PropTypes from 'prop-types';

import Modal from '../common/modal/Modal';
import Button from '../common/button/Button';
import Input from '../common/input/Input';

export default class SetPasswordModal extends React.Component {
	static propTypes = {
		setPassword: PropTypes.func.isRequired,
		onClose: PropTypes.func.isRequired,
		isClosable: PropTypes.bool,
	}

	static defaultProps = {
		isClosable: true,
	}

	state = {
		loading: false,
		error: null,
		password: '',
	}


	onSubmitForm = async (event) => {
		event.preventDefault();
		this.setState({ loading: true, error: null });
		const data = new FormData(event.target);
		try {
			await this.props.setPassword(data.get('password'));
			this.setState({ error: null, loading: false });
			this.props.onClose();
		} catch (error) {
			this.setState({ error: error.message, loading: false });
			console.error(error);
		}
	}

	onPasswordInput = (e) => {
		this.setState({ password: e.target.value });
	}

	render() {
		const { loading, error, password } = this.state;
		const { isClosable, ...modalProps } = this.props;

		if (!isClosable) {
			delete modalProps.onClose; // prevent modal from manually closing by user
		}

		return (
			<div>
				{ /* language=CSS */ }
				<style jsx>{`
					.error {
						padding: 16px;
						background: var(--error-color);
						color: #fff;
					}

					.input {
						padding: 16px;
					}
				`}</style>
				<Modal
					{...modalProps}
					title='Change password'
					onSubmit={this.onSubmitForm}
					button={<Button theme='accent' type='submit' loading={loading}>Set password</Button>}
				>
					<div className='content'>
						{ !!error && <div className='error'>{error}</div> }
						<div className='input'>
							<Input
								name='password'
								type='password'
								placeholder='Password'
								readOnly={loading}
								minLength={6}
								value={password}
								onChange={this.onPasswordInput}
								required
							/>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
}
