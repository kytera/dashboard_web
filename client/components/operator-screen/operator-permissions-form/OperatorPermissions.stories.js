import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import OperatorPermissions from './OperatorPermissions';
import PermissionsTableButton from './PermissionsTableButton';
import PermissionsTableHeader from './PermissionsTableHeader';
import PermissionsTableRow from './PermissionsTableRow';

import permissions from './OperatorPermissions.mock';

storiesOf('operator-screen/permissions-form/OperatorPermissions', module)
	.add('entire component', () => (
		<OperatorPermissions
			permissions={permissions}
			onChange={action('onchange')}
			onAllowEditChanged={action('onchange')}
			subscribersEdit
		/>))
	.add('view/edit button', () => (
		<PermissionsTableButton
			type='view'
			isActive
			alias='ALERT'
			onChange={action('onchange')}
		/>))
	.add('table header', () => <table style={{ width: '400px' }}>
		<tbody><PermissionsTableHeader /></tbody>
	</table>)
	.add('table row', () => (
		<table style={{ width: '400px' }}>
			<tbody>
			<PermissionsTableRow
				title='Title'
				values={[]}
				alias='Alias'
				onChange={action('onchange')}
			/>
			</tbody>
		</table>));
