import PropTypes from 'prop-types';

import Switch from '../../common/switch/Switch';
import PermissionsTableHeader from './PermissionsTableHeader';
import PermissionsTableRow from './PermissionsTableRow';

const PERMISSIONS_ALIASES = [
	{
		name: 'Alert',
		alias: 'ALERT',
	},
	{
		name: 'Alert History',
		alias: 'ALERT_HISTORY',
	},
	{
		name: 'Service',
		alias: 'SERVICE',
	},
	{
		name: 'Notifications',
		alias: 'NOTIFICATIONS',
	},
	{
		name: 'Subscriber',
		alias: 'SUBSCRIBER',
	},
	{
		name: 'Home Care',
		alias: 'HOMECARE'
	}
];

const OperatorPermissions = ({ onChange, permissions, subscribersEdit, onAllowEditChanged }) => {
	const onToggleButtons = (alias, type, value) => {
		if (!value) {
			const permission = permissions
				.findIndex(item => item.resource === alias && item.action === type);

			return onChange([
				...permissions.slice(0, permission),
				...permissions.slice(permission + 1, permissions.length),
			]);
		}

		return onChange([...permissions, {
			resource: alias,
			action: type,
		}]);
	};

	return (
		<div className='root'>
			{ /* language=CSS */ }
			<style jsx>{`
				.root {
					width: 100%;
					color: var(--primary-color);
					background-color: #fff;
					padding-left: 14px;
				}

				.permissions-table {
					width: 100%;
					border-bottom: 1px solid var(--disabled-color);
				}

				.subscriber-switch div {
					font-weight: bold;
					display: flex;
					flex-flow: row nowrap;
					justify-content: space-between;
					align-items: center;
					padding: 12px;
					background-color: #f5f5f5;
					border-radius: 4px;
				}

				.subscriber-switch {
					padding: 6px 2px;
				}

				tbody {
					padding: 2px;
				}
			`}</style>
			<table className='permissions-table'>
				<thead>
					<PermissionsTableHeader />
				</thead>
				<tbody>
					{
						PERMISSIONS_ALIASES.map(item => (
							<PermissionsTableRow
								title={item.name}
								key={item.alias}
								alias={item.alias}
								onChange={onToggleButtons}
								values={permissions.filter(permission => permission.resource === item.alias)}
							/>
						))
					}
				</tbody>
			</table>
			<div className='subscriber-switch'>
				<div>
					<span>
						Allowed edit subscribers
					</span>
					<Switch
						name='subscribers-edit'
						checked={subscribersEdit}
						onChange={e => onAllowEditChanged(e.target.checked)}
					/>
				</div>
			</div>
		</div>
	);
};

OperatorPermissions.propTypes = {
	onChange: PropTypes.func.isRequired,
	permissions: PropTypes.arrayOf(PropTypes.object).isRequired,
	subscribersEdit: PropTypes.bool.isRequired,
	onAllowEditChanged: PropTypes.func.isRequired,
};

export default OperatorPermissions;
