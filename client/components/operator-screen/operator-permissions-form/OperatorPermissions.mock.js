export default [
	{
		resource: 'ALERT',
		action: 'view',
	},
	{
		resource: 'ALERT',
		action: 'edit',
	},
	{
		resource: 'ALERT_HISTORY',
		action: 'view',
	},
	{
		resource: 'NOTIFICATIONS',
		action: 'edit',
	},
	{
		resource: 'SUBSCRIBER',
		action: 'view',
	},
];
