export default () => (
	<tr className='table-header'>
		{ /* language=CSS */ }
		<style jsx>{`
			.table-header th {
				height: 50px;
				border-bottom: 2px var(--disabled-color) solid;
			}

			.table-header th:first-child {
				text-transform: uppercase;
				text-align: left;
				padding-left: 2px;
			}
		`}</style>
		<th>
			permissions
		</th>
		<th>
			View
		</th>
	</tr>
);
