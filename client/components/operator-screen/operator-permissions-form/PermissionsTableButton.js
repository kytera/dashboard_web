import PropTypes from 'prop-types';
import csx from 'classnames';

import Icon from '../../common/icon/Icon';

const PermissionsTableButton = ({ alias, type, onChange, isActive, ...rest }) => (
	<button
		className={csx('root', { isActive })}
		{...rest}
		onClick={() => onChange(alias, type, !isActive)}
	>
		{ /* language=CSS */ }
		<style jsx>{`
				.root {
					color: #fff;
					background-color: var(--disabled-color);
					border-radius: 50%;
					box-shadow: none;
					border: none;
					outline: none;
					padding: 7px;
					display: inline-flex;
					justify-content: center;
					cursor: pointer;
				}

				.root.isActive {
					background-color: var(--success-color);
				}
			`}</style>
		<Icon type={type === 'VIEW' ? 'search' : 'pencil'} size='20px' />
	</button>
);

PermissionsTableButton.propTypes = {
	type: PropTypes.string.isRequired,
	isActive: PropTypes.bool.isRequired,
	onChange: PropTypes.func.isRequired,
	alias: PropTypes.string.isRequired,
};

export default PermissionsTableButton;
