import PropTypes from 'prop-types';

import PermissionsTableButton from './PermissionsTableButton';

const PermissionsTableRow = ({ title, alias, values, onChange }) => (
	<tr className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				width: 100%;
				color: var(--primary-color);
				background-color: #fff;
				text-align: center;
				border: 2px solid #fff;
				border-right: none;
				border-left: none;
				margin:4px 0px;
			}

			td {
				padding: 4px 0;
				background-color: #f5f5f5;
				border-top: 6px #fff solid;
				border-bottom: 6px #fff solid;
				border-radius: 4px;
			}

			td:first-child {
				text-align: left;
				padding-left: 20px;
				font-weight: bold;
			}
		`}</style>
		<td>
			{title}
		</td>
		<td>
			<PermissionsTableButton
				type='VIEW'
				isActive={values.some(item => item.action === 'VIEW')}
				onChange={onChange}
				alias={alias}
			/>
		</td>
	</tr>
);

PermissionsTableRow.propTypes = {
	title: PropTypes.string.isRequired,
	// eslint-disable-next-line react/forbid-prop-types
	values: PropTypes.array.isRequired,
	alias: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
};

export default PermissionsTableRow;
