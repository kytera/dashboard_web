import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { propType } from 'graphql-anywhere';
import { compose, withState } from 'recompose';
import {omit, pick} from 'lodash';

import OPERATOR_QUERY from '../../graphql/operator/operator.query.gql';
import UPDATE_OPERATOR_MUTATION from '../../graphql/operator/updateOperator.mutation.gql';
import SET_OPERATOR_IS_ACTIVE_MUTATION from '../../graphql/operator/setOperatorIsActive.mutation.gql';

import Title from '../common/title/Title';
import Button from '../common/button/Button';
import withMutationStatus from '../../withMutationStatus';
import OperatorPermissionsForm from './operator-permissions-form/OperatorPermissions';
import OperatorInfoForm from './operator-info-form/OperatorInfoForm';

const cutFields = (operator) => {
	const newFields = omit(operator, ['__typename', 'id', 'createdAt', 'role', 'permissions', 'isActive']);
	if (process.browser && !(operator.avatar instanceof File)) {
		delete newFields.avatar;
	}

	return newFields;
};

export const OperatorScreen = ({ data, setOperatorIsActive, updateOperator, updateOperatorPermissions, editMode, setEditMode, mutationStatus }) => {
	if (!data || data.error) {
		return null;
	}

	const { loading, operator } = data;

	if (loading) {
		return (<div className='loading'>
			{ /* language=CSS */ }
			<style jsx>{`
					.loading {
						flex: 1;
						display: flex;
						justify-content: center;
						align-items: center;
						font-weight: 500;
						font-size: 36px;
						color: var(--primary-color);
					}
				`}</style>
			<span>Loading...</span>
		</div>);
	}

	const action = operator.isActive
		? <Button theme='error' link icon='stop' onClick={() => setOperatorIsActive(false)}><span style={{ fontSize: '14px', fontWeight: 'bold' }}>Deactivate operator</span></Button>
		: <Button theme='success' size='large' onClick={() => setOperatorIsActive(true)}><span style={{ fontSize: '14px', fontWeight: 'bold' }}>Activate operator</span></Button>;


	return (
		<div className='root'>
			{ /* language=CSS */ }
			<style jsx>{`
				.root {
					flex: 1;
					background: #ffffff;
					display: flex;
					flex-direction: column;
				}
				.content {
					display: flex;
					flex: 1;
					position: relative;
				}
				.info-form {
					width: 50%;
					display: flex;
				}
				.permissions-form {
					width: 50%;
					display: flex;
				}
			`}</style>
			<Title
				size='large'
				subtitle={operator.jobTitle}
				action={action}
			>
				{operator.firstName} {operator.lastName}
			</Title>
			<div className='content'>
				<div className='info-form'>
					<OperatorInfoForm
						onSubmit={updateOperator}
						initialValues={operator}
						editMode={editMode}
						setEditMode={setEditMode}
						loading={mutationStatus.loading}
					/>
				</div>
				<div className='permissions-form'>
					<OperatorPermissionsForm
						onChange={updateOperatorPermissions}
						permissions={operator.permissions}
						onAllowEditChanged={canEditSubscribers => updateOperator({ ...operator, canEditSubscribers })}
						subscribersEdit={operator.canEditSubscribers}
					/>
				</div>
			</div>
		</div>
	);
};


OperatorScreen.propTypes = {
	data: propType(OPERATOR_QUERY),
	setOperatorIsActive: PropTypes.func.isRequired,
	updateOperator: PropTypes.func.isRequired,
	updateOperatorPermissions: PropTypes.func.isRequired,
	editMode: PropTypes.bool.isRequired,
	setEditMode: PropTypes.func.isRequired,
	mutationStatus: PropTypes.shape({ loading: PropTypes.bool.isRequired, error: PropTypes.string }).isRequired,
};

OperatorScreen.defaultProps = {
	data: null,
};

const enhance = compose(
	withState('editMode', 'setEditMode', false),
	graphql(OPERATOR_QUERY, {
		options: ({ operatorId }) => ({
			variables: { id: operatorId },
			skip: !operatorId,
		}),
	}),
	graphql(UPDATE_OPERATOR_MUTATION,
		{
			props: ({ ownProps: { operatorId, setEditMode, data }, mutate }) => ({
				updateOperator: async (operator) => {
					const result = await mutate({
						variables: {
							operator: cutFields(operator),
							id: operatorId,
						},
						// optimisticResponse: {
						// 	__typename: 'Mutation',
						// 	updateOperator: {
						// 		id: operatorId,
						// 		...operator
						// 	},
						// },
					});
					setEditMode(false);
					return result;
				},
				updateOperatorPermissions: permissions => mutate({
					variables: {
						operator: {
							permissions: permissions.map(item => omit(item, '__typename')),
						},
						id: operatorId,
					},
					// optimisticResponse: {
					// 	__typename: 'Mutation',
					// 	updateOperator: {
					// 		...data.operator,
					// 		permissions: permissions.map(item => ({ __typename: 'Permission', ...item })),
					// 	},
					// },
				}),
			}),
		},
	),
	withMutationStatus('updateOperator'),
	graphql(SET_OPERATOR_IS_ACTIVE_MUTATION, {
		props: ({ ownProps: { data, operatorId }, mutate }) => ({
			setOperatorIsActive: isActive => mutate({
				variables: {
					id: operatorId,
					isActive,
				},
				optimisticResponse: {
					__typename: 'Mutation',
					setOperatorIsActive: {
						...data.operator,
						isActive,
					},
				},
			}),
		}),
	}),
);

export default enhance(OperatorScreen);
