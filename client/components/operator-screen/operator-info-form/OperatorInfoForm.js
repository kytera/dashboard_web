import PropTypes from 'prop-types';
import { reduxForm, getFormValues, Field } from 'redux-form';
import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import { propType } from 'graphql-anywhere';

import OPERATOR_FIELDS from '../../../graphql/operator/operator.fragment.gql';

import Title from '../../common/title/Title';
import Button from '../../common/button/Button';
import AvatarInput from '../../common/avatar-input/AvatarInput';
import OperatorInfo from '../../wizards/fieldsets/operator/OperatorInfo';
import OperatorInfoDisplay from './OperatorInfoDisplay';
import Modal from '../../common/modal/Modal';
import ProfilePictureEditor from '../../wizards/fieldsets/common/profile-picture-editor/ProfilePictureEditor';

const FORM_NAME = 'OPERATOR_INFO_FORM';

export const OperatorInfoForm = ({ handleSubmit, editMode, setEditMode, operator, isAvatarModalOpen, setIsAvatarModalOpen, loading }) => {
	const action = !editMode ?
		<Button link icon='pencil' onClick={() => setEditMode(true)}>Edit operator&#39;s data</Button> : null;

	return (
		<div className='root'>
			<Modal
				isOpen={isAvatarModalOpen}
				title='Edit profile picture'
				onClose={() => setIsAvatarModalOpen(false)}
				button={<Button theme='accent' onClick={() => setIsAvatarModalOpen(false)}>Save</Button>}
			>
				<ProfilePictureEditor />
			</Modal>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					flex: 1;
					background: #f4f4f4;
				}
				.avatar {
					padding: 12px;
					padding-right: 36px;
				}
				form {
					display: flex;
					padding: 24px;
				}
				.submit-button {
					position: absolute;
					bottom: 24px;
					right: 24px;
				}
			`}</style>
			<Title action={action} size='medium'>Personal info</Title>
			<form onSubmit={handleSubmit}>
				<div className='avatar'>
					<Field component={AvatarInput} name='avatar' onEdit={editMode ? () => setIsAvatarModalOpen(true) : null} />
				</div>
				{!editMode ? <OperatorInfoDisplay operator={operator} /> : <OperatorInfo showPasswordField={false} /> }
				{ editMode && (<div className='submit-button'>
					<Button type='submit' theme='accent' loading={loading}>Apply changes</Button>
				</div>) }
			</form>
		</div>
	);
};

OperatorInfoForm.propTypes = {
	handleSubmit: PropTypes.func.isRequired,
	operator: propType(OPERATOR_FIELDS).isRequired,
	editMode: PropTypes.bool.isRequired,
	setEditMode: PropTypes.func.isRequired,
	isAvatarModalOpen: PropTypes.bool.isRequired,
	setIsAvatarModalOpen: PropTypes.func.isRequired,
	loading: PropTypes.bool.isRequired,
};

export default compose(
	withState('isAvatarModalOpen', 'setIsAvatarModalOpen', false),
	reduxForm({
		form: FORM_NAME,
		enableReinitialize: true,
	}),
	connect(state => ({
		operator: getFormValues(FORM_NAME)(state),
	})),
)(OperatorInfoForm);
