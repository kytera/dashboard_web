import { propType } from 'graphql-anywhere';
import { get } from 'lodash';

import OPERATOR_FIELDS from '../../../graphql/operator/operator.fragment.gql';

import Icon from '../../common/icon/Icon';
import DisplayPhone from '../../common/display-phone/DisplayPhone';

const OperatorInfoDisplay = ({ operator }) => {
	return (
		<div className='root'>
			{/* language=CSS */}
			<style jsx>{`
			.root {
				display: flex;
			}

			.icon {
				margin-right: 16px;
			}

			.info {
				flex: 1;
				line-height: 1.5;
			}

			.email {
				color: var(--accent-color);
			}

			.phones-item {
				margin-top: 16px;
			}
		`}</style>
			<div className='icon'>
				<Icon type='person' />
			</div>
			<div className='info'>
				<div className='info-item'>Name: {operator.firstName} {operator.lastName}</div>
				<div className='info-item'>Role: {operator.jobTitle}</div>
				<div className='info-item'>ID: {operator.id}</div>
				<br />
				<div className='info-item'>Email: <br />
					<a className='email' href={`mailto:${operator.email}`}>{operator.email}</a>
				</div>
				{get(operator, 'phones.length') && (<div className='info-item phones-item'>Phone: <br />
					{operator.phones.map((phone,i) => <div key={i}><DisplayPhone>{phone}</DisplayPhone></div>)}
				</div>)}
			</div>
		</div>
	);
}

OperatorInfoDisplay.propTypes = {
	operator: propType(OPERATOR_FIELDS),
};

OperatorInfoDisplay.defaultProps = {
	operator: {},
};

export default OperatorInfoDisplay;
