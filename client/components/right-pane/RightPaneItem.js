import PropTypes from 'prop-types';

import Icon from '../common/icon/Icon';

const RightPaneItem = ({ title, content, icon, iconNumber }) => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				display: flex;
				color: var(--primary-color);
				margin: 8px 0;
				line-height: 1.5;
			}

			.title {
				font-weight: 600;
				font-size: 13px;
			}

			.icon {
				flex-basis: 64px;
				text-align: center;
				padding-left: 4px;
				padding-top: 4px;
			}

			.payload {
				flex: 1;
				font-size: 13px;
			}
		`}</style>
		<div className='icon'>
			{ !!icon && <Icon type={icon} size={16} /> }
			{ !!iconNumber && <Icon number={iconNumber} size={16} /> }
		</div>
		<div className='payload'>
			<div className='title'>{title}</div>
			<div className='content'>{content}</div>
		</div>
	</div>
);

RightPaneItem.propTypes = {
	title: PropTypes.node.isRequired,
	content: PropTypes.node,
	icon: PropTypes.string,
};

RightPaneItem.defaultProps = {
	content: null,
	icon: null,
};

export default RightPaneItem;
