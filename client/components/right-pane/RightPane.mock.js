export default {
	caregivers: [
		{
			id: 0,
			firstName: 'Anna',
			lastName: 'Mass',
			phone: '9992222',
			address: 'New York, NY 10128, USA',
		},
		{
			id: 1,
			firstName: 'Peter',
			lastName: 'Mass',
			phone: '9992222',
			address: 'New York, NY 10128, USA',
		},
	],
};
