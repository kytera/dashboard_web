import { storiesOf } from '@storybook/react';

import {RightPane} from './RightPane';

const SUBSCRIBER = {
	firstName: 'John',
	lastName: 'Doe',
	createdAt: "2017-08-09T22:50:58.556Z",
	id: '123',
	address: {
		street: 'test str.'
	},
	caregivers: [],
	phones: ['+1 123213'],
	ambulancePhones: ['+1 123213'],
};

storiesOf('RightPane', module)
	.add('default', () => <RightPane
		data={{ subscriber: SUBSCRIBER }}
	/>);
