import Moment from 'react-moment';
import { graphql } from 'react-apollo';
import { propType } from 'graphql-anywhere';
import { branch, renderNothing, compose } from 'recompose';
import { get, capitalize } from 'lodash';

import DisplayPhone from '../common/display-phone/DisplayPhone';
import RightPaneItem from './RightPaneItem';
import SUBSCRIBER_INFO_QUERY from '../../graphql/subscriber/subscriberInfo.query.graphql';


export const RightPane = ({ data }) => {

	if (!data.subscriber) return null;
	const { caregivers, address, ...subscriber } = data.subscriber;

	const phone = get(subscriber, 'phones[0]');
	const phoneStr = phone ? phone : 'None';
	const homePhone = get(subscriber, 'homePhone');
	const homePhoneStr = homePhone ? homePhone : 'None';

	const gender = subscriber.gender ? capitalize(subscriber.gender) : '';

	return (
		<div className='root'>
			{/* language=CSS */}
			<style jsx>{`
			.root {
				width: 300px;
				height: 100%;
				background: #E9E9E9;
				padding: 4px 0;
				overflow-y: auto;
				padding-right: 16px;
			}

			.divider {
				height: 1px;
				margin-left: 16px;
				margin-right: 16px;
				background-color: rgba(255, 255, 255, .5);
			}

			.title {
				font-weight: 500;
				margin-top: 12px;
				padding-left: 20px;
				color: var(--primary-color);
				font-size: 13px;
				font-weight: 600;
			}

			.status {
				color: var(--success-color);
				font-size: 12px;
			}

		`}</style>
			<RightPaneItem
				title='Personal info'
				icon='person'
				content={(
					<div>
						<div>
							Name: {subscriber.firstName} {subscriber.lastName}
						</div>
						<div>
							Gender: {gender}
						</div>
						<div>
							Home: {homePhoneStr}
						</div>
						<div>
							Mobile: {phoneStr}
						</div>
					</div>
				)}
			/>
			{subscriber.agencies.length > 0 && <RightPaneItem
				title='Emergency Services'
				icon='ambulance_bell'
				content={(
					<div>
						{subscriber.agencies.map(phone => (<div key={phone.number}>
							<DisplayPhone>{phone.number}</DisplayPhone>
						</div>))}
						<div className='status' hidden>Called before 3 min</div>
					</div>
				)}
			/>}
			<RightPaneItem
				title='Address'
				icon='location'
				content={(
					<div>
						{address.street} {address.streetOptional}<br />
						{address.city}, {address.state} {address.zipCode}<br />
						{address.country}
					</div>
				)}
			/>
			<div className='divider' />
			<div className='title'>Caregivers</div>
			{caregivers.length > 0 && caregivers.map((caregiver, idx) => (
				<RightPaneItem
					key={caregiver.id}
					title={`${caregiver.firstName} ${caregiver.lastName}`}
					iconNumber={caregiver.isPrimary ? 58 : null}
					content={(
						<div>
							{caregiver.phones.map(phone => (<div key={phone}>
								<DisplayPhone>{phone}</DisplayPhone>
							</div>))}
							<div className='status' hidden>Called before 3 min</div>
						</div>
					)}
				/>
			))}
		</div>
	);
};

RightPane.propTypes = {
	data: propType(SUBSCRIBER_INFO_QUERY).isRequired,
};

RightPane.defaultProps = {
	subscriber: {},
};

const enhance = compose(
	graphql(SUBSCRIBER_INFO_QUERY, {
		options: ({ subscriberId }) => ({
			variables: { id: subscriberId } ,
			fetchPolicy: 'cache-and-network'
		}),
	}),
);

export default enhance(RightPane);
