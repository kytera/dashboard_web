import { storiesOf } from '@storybook/react';

import RightPaneItem from './RightPaneItem';

storiesOf('RightPane', module)
	.add('item', () => <div style={{ width: 256 }}><RightPaneItem title='Title' content='Subtitle' icon='plus' /></div>);
