import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { get } from 'lodash';
import React from 'react';
import {compose} from "recompose";
import { Router } from '../../../lib/routes';

import Alert from 'react-s-alert';
import NavTabs from './subscribers/nav-tabs/NavTabs';
import LeftPane from './subscribers/left-pane/LeftPane';
import AdminLeftPane from './admin/admin-left-pane/AdminLeftPane';
import Header from './header/HeaderWrapper';
import StatusHeader from './subscribers/status-header/StatusHeader';
import SetPasswordModal from '../set-password-modal/SetPasswordModal';
import SET_USER_PASSWORD_MUTATION from '../../graphql/user/setUserPassword.mutation.gql';
import SUBSCRIBERS_QUERY from '../../graphql/subscriber/subscribers.query.graphql';
import SUBSCRIBER_STATUS_QUERY from '../../graphql/subscriber/subscriberStatus.query.graphql';
import SYSTEM_QUERY from '../../graphql/system/system.query.graphql';
import {SystemLayout} from "../service-tab/system/SystemLayout";

import "react-select/dist/react-select.css";
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import 'react-s-alert/dist/s-alert-css-effects/stackslide.css';

import '../../../variables.css';
import '../../../global.css';
import { getUserCompanyIds } from "../../withCurrentUser";
import { OfflineChecker } from "../common/offline-checker/OfflineChecker";
let Offline;

class Layout extends React.Component {
	static propTypes = {
		// eslint-disable-next-line react/forbid-prop-types
		currentUser: PropTypes.object.isRequired,
		setPassword: PropTypes.func.isRequired,
		// eslint-disable-next-line react/forbid-prop-types
		url: PropTypes.object.isRequired,
		children: PropTypes.node,
		isAdmin: PropTypes.bool,
	};

	static defaultProps = {
		isAdmin: false,
		children: null,
	};

	state = {
		isSetPasswordModalOpen: this.props.currentUser.isPasswordTemporary || false,
	};

	constructor(props) {
		super(props);
		if (process.browser) {
			Offline = require('react-detect-offline').Offline;
		}
	}

	componentWillReceiveProps(newProps) {
		if (newProps.currentUser.isPasswordTemporary) {
			this.setState({ isSetPasswordModalOpen: true });
		}
	}

	closeSetPasswordModal = () => {
		this.setState({ isSetPasswordModalOpen: false });
	}

	render() {
		const { currentUser, setPassword, url, children, isAdmin } = this.props;

		const subscriberId = get(url, 'query.id');
		const operatorId = currentUser.id;
		return (
			<div className='root'>
				{ /* language=CSS */ }
				<style jsx>{`
					.root {
						width: auto;
						height: 100vh;
						margin: 0 auto;
						display: flex;
						flex-direction: column;
					}

					.page-wrapper {
						display: flex;
						flex: 1;
					}

					.content-wrapper {
						display: flex;
						flex-direction: column;
						flex: 1;
						border-left: 2px #E9E9E9 solid;
					}

					main {
						display: flex;
						flex: 1;
					}
				`}</style>
				<SetPasswordModal
					setPassword={setPassword}
					isOpen={this.state.isSetPasswordModalOpen}
					onClose={this.closeSetPasswordModal}
					isClosable={false}
				/>
				<Header user={currentUser} pathname={url.pathname} isDark={isAdmin} />
				<div className='page-wrapper'>
					{ isAdmin
						? <AdminLeftPane companiesIds={getUserCompanyIds(currentUser)} query={url.query} />
						: <LeftPane companiesIds={getUserCompanyIds(currentUser)} url={url} />
					}
					<div className='content-wrapper'>
						{ !isAdmin && subscriberId && <StatusHeader subscriberId={subscriberId} operatorId={operatorId}/> }
						{ !isAdmin && <NavTabs url={url} /> }
						<main>{ children }</main>
					</div>
				</div>
				<Alert timeout={4000} stack={{ limit: 3 }} />
				{process.browser && <Offline>
					<OfflineChecker/>
				</Offline>}
			</div>
		);
	}
}

export default compose(
	graphql(SUBSCRIBERS_QUERY, {
		options: (props) => ({
			variables: {
				companiesIds: getUserCompanyIds(props.currentUser),
			},
			pollInterval: 30000
		}),
		skip: props => !get(props, 'currentUser.company.id') && !get(props, 'currentUser.companies'),
	}),
	graphql(SUBSCRIBER_STATUS_QUERY,
		{
			options: (props) => ({
				variables: { subscriberId: get(props, 'url.query.id') },
				pollInterval: 30000
			}),
			skip: (props) => !get(props, 'url.query.id'),
		},
	),
	graphql(SYSTEM_QUERY,
		{
			options: (props) => ({
				variables: { subscriberId: get(props, 'url.query.id') },
				pollInterval: 30000,
			}),
			skip: (props) => !get(props, 'url.query.id'),
		},
	),

	graphql(SET_USER_PASSWORD_MUTATION, {
		props: ({ ownProps, mutate }) => ({
			setPassword: password => mutate({ variables: { id: ownProps.currentUser.id, password } }),
		}),
	})
)(Layout);
