import csx from 'classnames';
import PropTypes from 'prop-types';

import Header from './Header';
import Logo from './Logo';

import withCurrentUser from '../../../withCurrentUser';

export const HeaderWrapper = ({ user, pathname, isDark }) => (
	<header className={csx('root', { isDark })}>
		{ /* language=CSS */ }
		<style jsx>{`
			header {
				box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 8px 1px inset;
				display: flex;
				position: relative;
			}

			/*  logo background */
			header::before {
				left: 0;
				width: var(--left-side-with);
				top: 0;
				bottom: 0;
				background: #F4F4F4;
				content: "";
				z-index: -1;
				position: absolute;
			}

			header.isDark::before {
				background-color: var(--primary-color);
			}

			/*  header background */
			header::after {
				left: var(--left-side-with);
				top: 0;
				bottom: 0;
				right: 0;
				background: #fff;
				content: "";
				z-index: -1;
				position: absolute;
			}
		`}
		</style>
		<Logo isDark={isDark} />
		<Header currentUser={user} pathname={pathname} isDark={isDark} />
	</header>
);

HeaderWrapper.propTypes = {
// eslint-disable-next-line react/forbid-prop-types
	user: PropTypes.object.isRequired,
	pathname: PropTypes.string.isRequired,
	isDark: PropTypes.bool,
};

HeaderWrapper.defaultProps = {
	isDark: false,
};

export default withCurrentUser(HeaderWrapper);
