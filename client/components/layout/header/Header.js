/* eslint-disable import/no-named-as-default */
import Link from 'next/link';
import csx from 'classnames';
import { withState, compose, withHandlers } from 'recompose';
import PropTypes from 'prop-types';

import withAuthControl, { authControlPropTypes } from '../../../withAuthControl';
import RolesOnly, { ROLES } from '../../common/roles-only/RolesOnly';
import HeaderDropdown from './HeaderDropdown';
import AddSubscriberWizard from '../../wizards/create-subscriber-wizard/CreateSubscriberWizard';
import AddOperatorWizard from '../../wizards/operator-wizard/AddOperatorWizard';
import { Router } from '../../../../lib/routes'
import { getUserCompanyIds } from "../../../withCurrentUser";

const Header = ({
									currentUser,
									pathname,
									authControl,
									isAddSubscriberWizardOpen,
									setAddSubscriberWizardOpen,
									isAddOperatorWizardOpen,
									setAddOperatorWizardOpen,
									subscriberWizardOnClose,
								}) => {
	const isSuperAdmin = currentUser.role === 'SUPER_ADMIN';
	return (
		<div className='header'>
			<div className='title'>{isSuperAdmin ? currentUser.companies.map(c => c.name).join(', ') : currentUser.company.name}</div>
			<RolesOnly role={[ROLES.ADMIN, ROLES.SUPER_ADMIN]}>
				<div className='admin-nav'>
					<Link href='/admin'><a className={csx('admin-nav-item', {isActive: pathname === '/admin'})}>Admin
						view</a></Link>
					<Link href='/'>
						<a className={csx('admin-nav-item', {isActive: pathname !== '/admin'})}>
							Subscribers
						</a>
					</Link>
				</div>
			</RolesOnly>
			<div className='dropdown'>
				<HeaderDropdown title={(
					<span className='username'>Hi, {!isSuperAdmin ? currentUser.firstName || 'Unknown user' : 'Super Admin'}</span>
				)}
				>
					<div className='menu'>
						{(currentUser.role === ROLES.SUPER_ADMIN || currentUser.company.type === 'DEALER') && (<a
							tabIndex='0'
							role='button'
							onClick={() => setAddSubscriberWizardOpen(true)}
							className='menu-link'
						>Add subscriber</a>)}
						<RolesOnly role={[ROLES.ADMIN, ROLES.SUPER_ADMIN]}>
							<a
								tabIndex='0'
								role='button'
								onClick={() => setAddOperatorWizardOpen(true)}
								className='menu-link'
							>Add operator</a>
						</RolesOnly>
						<div className='divider'/>
						<a role='button' tabIndex={0} className='menu-link' onClick={authControl.logout}>Logout</a>
					</div>
				</HeaderDropdown>
			</div>
			{isAddSubscriberWizardOpen && <AddSubscriberWizard
				onClose={subscriberWizardOnClose}
			/>}
			{isAddOperatorWizardOpen && <AddOperatorWizard
				onClose={() => setAddOperatorWizardOpen(false)}
			/>}
			{/* language=CSS */}
			<style jsx>{`
			.header {
				color: var(--primary-color);
				display: flex;
				align-items: center;
				padding: 0 10px 0 28px;
				flex: 1;
				height: 64px;
			}

			.title {
				color: #A3A3A3;
				font-weight: 500;
				text-transform: uppercase;
				font-size: 20px;
				flex: 1;
			}

			.divider {
				height: 2px;
				background: var(--disabled-color);
				margin: 4px 0;
			}

			.link-settings {
				margin-right: 24px;
			}

			.username {
				font-weight: 600;
				font-size: 16px;
			}

			.menu-link {
				display: block;
				font-weight: 500;
				padding: 8px;
			}

			.menu-link:hover {
				background-color: rgba(0, 0, 0, .05);
			}

			.menu {
				background: #ffffff;
				padding: 4px 0;
			}

			.admin-nav {
				display: flex;
				padding: 0 8px;
				overflow-y: hidden;
			}

			.admin-nav-item {
				background-color: rgba(0, 0, 0, .05);
				display: flex;
				align-items: center;
				justify-content: center;
				padding: 0 36px;
				font-weight: 500;
				font-size: 16px;
				text-transform: uppercase;
				height: 64px;
			}

			.admin-nav-item.isActive {
				background-color: #fff;
				box-shadow: rgba(0, 0, 0, 0.2) 0 0 8px 1px;
				pointer-events: none;
				position: relative;
			}

			.dropdown {
				margin-left: 16px;
			}

		`}</style>
		</div>
	);
};


Header.propTypes = {
// eslint-disable-next-line react/forbid-prop-types
	currentUser: PropTypes.object.isRequired,
	pathname: PropTypes.string.isRequired,
	isAddSubscriberWizardOpen: PropTypes.bool.isRequired,
	setAddSubscriberWizardOpen: PropTypes.func.isRequired,
	isAddOperatorWizardOpen: PropTypes.bool.isRequired,
	setAddOperatorWizardOpen: PropTypes.func.isRequired,
	subscriberWizardOnClose: PropTypes.func.isRequired,
	...authControlPropTypes,
};

const enhance = compose(
	withAuthControl,
	withState('isAddSubscriberWizardOpen', 'setAddSubscriberWizardOpen', false),
	withState('isAddOperatorWizardOpen', 'setAddOperatorWizardOpen', false),
	withHandlers({
		subscriberWizardOnClose: ({setAddSubscriberWizardOpen}) => subscriberId => {
			setAddSubscriberWizardOpen(false);
			if (subscriberId) {
				Router.pushRoute(`/subscriber/${subscriberId}/info`);
			}
		}
	})
);

export default enhance(Header);
