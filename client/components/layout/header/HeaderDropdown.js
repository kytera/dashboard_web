import { withState } from 'recompose';
import csx from 'classnames';
import PropTypes from 'prop-types';

import Icon from '../../common/icon/Icon';

const HeaderDropdown = ({ opened, setOpened, title, children }) => (
	/* eslint-disable jsx-a11y/no-static-element-interactions */
	<div className={csx('root', { opened })} onClick={() => setOpened(!opened)}>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				display: block;
				position: relative;
				user-select: none;
			}

			.title {
				cursor: pointer;
				display: inline-flex;
				align-items: center;
				border-radius: 2px;
				padding: 8px;
			}

			.content {
				position: absolute;
				right: 0;
				min-width: 100%;
				bottom: -8px;
				transform: translateY(110%);
				border: 2px var(--disabled-color) solid;
				border-radius: 2px;
				white-space: nowrap;
				box-shadow: rgba(0,0,0,0.3) 0 2px 6px;

				transition: all .15s var(--timing-standard);
				visibility: hidden;
				opacity: 0;
				z-index: 10;
			}

			.opened .content {
				visibility: visible;
				opacity: 1;
				transform: translateY(100%);
			}

			.title > span {
				margin-right: 8px;
			}

			.opened .click-wrapper {
				position: fixed;
				background-color: transparent;
				top: 0;
				right: 0;
				bottom: 0;
				left: 0;
			}

			.arrow-icon {
				transition: all .15s;
			}

			.opened .arrow-icon {
				transform: rotate(180deg);
			}
		`}</style>
		<div className='click-wrapper' />
		<span className='title'>
			<span>{title}</span> <span className='arrow-icon'><Icon type='chevron_down' size={16} /></span>
		</span>
		<div className='content'>{children}</div>
	</div>
);

HeaderDropdown.propTypes = {
	opened: PropTypes.bool.isRequired,
	setOpened: PropTypes.func.isRequired,
	title: PropTypes.node.isRequired,
	children: PropTypes.node.isRequired,
};

export default withState('opened', 'setOpened', false)(HeaderDropdown);
