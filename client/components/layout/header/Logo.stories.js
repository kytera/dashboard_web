import { storiesOf } from '@storybook/react';

import Logo from './Logo';

storiesOf('layout/header/Logo', module)
	.add('Logo', () => (
		<div style={{ width: 300, height: 64 }}>
			<Logo />
		</div>
	));
