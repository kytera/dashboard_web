import Link from 'next/link';
import PropTypes from 'prop-types';
import {Icon62} from "../../common/icon/svgs";

const Logo = ({ isDark }) => (
	<div className={isDark ? 'isDark' : ''}>
		{ /* language=CSS */ }
		<style jsx>{`
			.link {
				height: 64px;
				width: var(--left-side-with);
				display: flex;
				align-items: center;
				padding-left: 16px;
			}
			.svg :global(svg){
				height: 40px;
			}
			.isDark .svg :global(svg){
				fill: white;
			}
		`}
		</style>
		<Link href='/'>
			<a className='link'>
				<span dangerouslySetInnerHTML={{ __html: Icon62 }} className='svg' />
				{/*<img*/}
					{/*src={isDark ? '/static/images/logo_white.png' : '/static/images/logo.png'}*/}
					{/*alt='Kytera'*/}
				{/*/>*/}
			</a>
		</Link>
	</div>
);

Logo.propTypes = {
	isDark: PropTypes.bool,
};

Logo.defaultProps = {
	isDark: false,
};

export default Logo;
