import { storiesOf } from '@storybook/react';

import UserItem from './UserItem';
import mocks from './UserItem.mock';


storiesOf('layout/admin/AdminLeftPane/UserItem', module)
	.add('default', () => <div style={{ width: 300 }}><UserItem user={mocks.USER} /></div>)
	.add('isActive', () => <div style={{ width: 300 }}><UserItem isActive user={mocks.USER} /></div>);
