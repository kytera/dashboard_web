import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { compose, withState, withHandlers, withProps } from 'recompose';

import Input from '../../../common/input/Input';
import Select, { Option } from '../../../common/select/Select';
import OPERATORS_QUERY from '../../../../graphql/operator/operators.query.gql';
import UserItem from './UserItem';


export const AdminLeftPane = ({ operators, query, search, onSearchChange }) => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				width: var(--left-side-with);
				height: 100%;
				background-color: var(--primary-color);
				display: flex;
				flex-direction: column;
			}

			.input-group {
				display: flex;
				padding: 12px;
			}

			.input-group > :global(:last-child) {
				flex: 1;
			}

			.filters {
				padding: 12px;
			}

			.users {
				flex: 1;
				overflow-y: auto;
				border-top: 2px var(--disabled-color) solid;
			}

			.empty-state {
				padding: 24px 16px;
				color: rgba(255, 255, 255, .4);
			}
		`}</style>
		<div className='input-group'>
			<Select grouped>
				<Option>Operators</Option>
			</Select>
			<Input icon='search' placeholder='Filter' type='search' grouped onChange={onSearchChange} value={search} />
		</div>
		<div className='users'>
			{ operators.length ? operators.map(operator => (
				<UserItem isActive={query.operatorId === operator.id} key={operator.id} user={operator} />
			)) : <div className='empty-state'>No operators found</div> }
		</div>
	</div>
);

AdminLeftPane.propTypes = {
	operators: PropTypes.arrayOf(PropTypes.object).isRequired,
	query: PropTypes.objectOf(PropTypes.string).isRequired,
	onSearchChange: PropTypes.func.isRequired,
	search: PropTypes.string.isRequired,
};

const enhance = compose(
	withState('search', 'setSearch', ''),
	withHandlers({
		onSearchChange: props => (event) => {
			props.setSearch(event.target.value);
		},
	}),
	graphql(OPERATORS_QUERY, {
		options: ({ companiesIds }) => ({ variables: { companiesIds } }),
	}),
	withProps(({ data: { operators = [] }, search }) => ({
		operators: search ? operators.filter(user =>
			user.firstName.toUpperCase().includes(
				search.toUpperCase()) || user.lastName.toUpperCase().includes(search.toUpperCase(),
			),
		) : operators,
	})),
);

export default enhance(AdminLeftPane);
