import PropTypes from 'prop-types';
import csx from 'classnames';
import { Link } from '../../../../../lib/routes';

const UserItem = ({ user, isActive }) => (
	<Link route='admin-operator' params={{ operatorId: user.id }}>
		<a className={csx('root', { isActive })}>
			{ /* language=SCSS */ }
			<style jsx>{`
				.root {
					display: block;
					padding: 12px 16px;
					position: relative;
					line-height: 1.5;
				}
				.root:hover {
					background-color: rgba(255, 255, 255, .05);
				}
				.root.isActive {
					background-color: rgba(255, 255, 255, .2);
				}
				.root:not(:last-child)::after {
						height: 1px;
						content: "";
						display: block;
						background-color: rgba(255, 255, 255, .2);
						position: absolute;
						left: 16px;
						right: 16px;
						bottom: 0;
				}

				.title {
					font-weight: 500;
					font-size: 13px;
					color: #fff;
				}

				.subtitle {
					font-weight: 400;
					font-size: 12px;
					color: rgba(255, 255, 255, .6);
				}

			`}</style>
			<div className='title'>{user.firstName} {user.lastName}</div>
			<div className='subtitle'>{user.jobTitle}</div>
		</a>
	</Link>
);

UserItem.propTypes = {
	user: PropTypes.shape({
		id: PropTypes.string.isRequired,
		firstName: PropTypes.string.isRequired,
		lastName: PropTypes.string.isRequired,
		jobTitle: PropTypes.string,
	}).isRequired,
	isActive: PropTypes.bool,
};

UserItem.defaultProps = {
	isActive: false,
};

export default UserItem;
