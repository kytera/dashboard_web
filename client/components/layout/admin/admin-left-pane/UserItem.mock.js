export default {
	USER: {
		firstName: 'John',
		lastName: 'Doe',
		jobTitle: 'VRI',
		createdAt: '2017-08-09T20:48:19.646Z',
		id: '123',
		address: '1732, 1st Ave #0095',
	},
};
