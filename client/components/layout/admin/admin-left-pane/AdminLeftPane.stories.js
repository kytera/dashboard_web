import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { AdminLeftPane } from './AdminLeftPane';
import mocks from './UserItem.mock';

storiesOf('admin/AdminLeftPane', module)
	.add('default', () => (
		<AdminLeftPane query={{}} operators={[mocks.USER]} onSearchChange={action('onSearchChange')} search='' />
	))
	.add('empty', () => (
		<AdminLeftPane query={{}} operators={[]} search='' onSearchChange={action('onSearchChange')} />
	));
