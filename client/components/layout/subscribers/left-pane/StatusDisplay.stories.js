import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import StatusDisplay from './StatusDisplay';

storiesOf('layout/subscribers/left-pane/StatusDisplay', module)
	.add('default', () => <StatusDisplay onChange={action('enable')} icon='bell' />)
	.add('with notification', () => <StatusDisplay icon='bell' hasNotification />)
	.add('active', () => <StatusDisplay isActive onChange={action('disable')} icon='bell' />)
	.add('active error type', () => <StatusDisplay isActive icon='bell' type='error' />);
