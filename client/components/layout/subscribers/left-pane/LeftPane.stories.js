import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { LeftPane } from './LeftPane';
import mocks from './UserItem.mock';

storiesOf('layout/subscribers/left-pane/LeftPane', module)
	.add('default', () => (
		<LeftPane
			filteredSubscribers={[mocks.USER]}
			onSearchChange={action('onSearchChange')}
			setFilterStatus={action('setFilterStatus')}
			data={{
				subscribers: [mocks.USER],
				loading: false,
				error: null,
			}}
			search=''
		/>
	))
	.add('empty', () => (
		<LeftPane
			filteredSubscribers={[]}
			onSearchChange={action('onSearchChange')}
			setFilterStatus={action('setFilterStatus')}
			data={{
				subscribers: [],
				loading: false,
				error: null,
			}}
			search=''
		/>
	));
