export default {
	USER: {
		firstName: 'John',
		lastName: 'Doe',
		role: 'VRI',
		id: '123',
		isActive: true,
		status: {
			openAlert: true,
			openServiceCall: true,
			openNotification: true,
		},
	},
};
