import csx from 'classnames';
import PropTypes from 'prop-types';

import Icon from '../../../common/icon/Icon';

const FilterButton = ({ isActive, icon, children, ...rest }) => (
	<span className={csx('root', { isActive })} {...rest}>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				display: inline-flex;
				background: #E9E9E9;
				color: var(--primary-color);
				border-radius: 3px;
				padding: 8px;
				margin: 2px;
				cursor: pointer;
				font-weight: 600;
				align-items: center;
			}

			.isActive {
				background: var(--primary-color);
				color: #fff;
				pointer-events: none;
			}

			.value {
				padding-left: 6px;
				min-width: 30px;
				display: inline-block;
			}
		`}</style>
		<Icon type={icon} className='icon' size={14} />
		<span className='value'>{children}</span>
	</span>
);

FilterButton.propTypes = {
	isActive: PropTypes.bool,
	icon: PropTypes.string,
	children: PropTypes.node.isRequired,
};

FilterButton.defaultProps = {
	isActive: false,
	icon: null,
};

export default FilterButton;
