import PropTypes from 'prop-types';
import { withProps } from 'recompose';

import FilterButton from './FilterButton';
import { dashboardServiceStatusEnum } from '../../../../../server/graphql/utils/subscriber'

const FilterButtons = ({ selected, onChange, statusCounters, subscribers, filterType }) => (
	<div className='root'>
		{/* language=SCSS */}
		<style jsx>{`
			.root {
				padding: 12px;
			}
		`}</style>
		<FilterButton
			icon='person'
			isActive={!selected}
			onClick={() => onChange(null)}
		>
			{statusCounters.subscribers}
		</FilterButton>
		<FilterButton
			icon='alert_triangle'
			isActive={selected === 'openAlert'}
			onClick={() => onChange('openAlert')}
		>
			{statusCounters.openAlert}
		</FilterButton>
		<FilterButton
			icon='wrench'
			isActive={selected === 'openServiceCall'}
			onClick={() => onChange('openServiceCall')}
		>
			{statusCounters.openServiceCall}
		</FilterButton>
		<FilterButton
			icon='bell'
			isActive={selected === 'openNotification'}
			onClick={() => onChange('openNotification')}
		>
			{statusCounters.openNotification}
		</FilterButton>
	</div>
);

FilterButtons.propTypes = {
	onChange: PropTypes.func.isRequired,
	selected: PropTypes.string,
	statusCounters: PropTypes.shape({
		openAlert: PropTypes.number,
		openServiceCall: PropTypes.number,
		openNotification: PropTypes.number,
	}),
	subscribers: PropTypes.arrayOf(PropTypes.object),
	filterType: PropTypes.string
};

FilterButtons.defaultProps = {
	subscribers: [],
	selected: null,
	statusCounters: null,
	filterType: dashboardServiceStatusEnum.active
};

const enhance = withProps(props => ({
	statusCounters: (props.subscribers || []).reduce(
		(accumulator, subscriber) => {
			const systemStatus = subscriber.status;
			const dashboardServiceStatus = props.filterType == 'all' ? 1 : (subscriber.dashboardServiceStatus === props.filterType);
			const countSubscriber = dashboardServiceStatus;
			const openAlert = systemStatus && systemStatus.openAlert && (subscriber.dashboardServiceStatus === dashboardServiceStatusEnum.active) ? systemStatus.openAlert : 0;
			const openServiceCall = systemStatus && systemStatus.openServiceCall && (subscriber.dashboardServiceStatus === dashboardServiceStatusEnum.active) ? systemStatus.openServiceCall : 0;
			const openNotification = systemStatus && systemStatus.openNotification && (subscriber.dashboardServiceStatus === dashboardServiceStatusEnum.active) ? systemStatus.openNotification : 0;
			return {
				openAlert: accumulator.openAlert + Number(openAlert),
				openServiceCall: accumulator.openServiceCall + Number(openServiceCall),
				openNotification: accumulator.openNotification + Number(openNotification),
				subscribers: accumulator.subscribers + Number(countSubscriber),
			};
		},
		{
			openAlert: 0,
			openServiceCall: 0,
			openNotification: 0,
			subscribers: 0
		},
	),
}));

export default enhance(FilterButtons);
