import csx from 'classnames';
import PropTypes from 'prop-types';

import Icon from '../../../common/icon/Icon';

const StatusDisplay = ({ isActive, theme, hasNotification, icon }) => (
	<span className={csx('root', { error: theme === 'error', isActive, hasNotification })}>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				cursor: pointer;
				color: var(--disabled-color);
				position: relative;
				margin: 0 8px;
			}

			.hasNotification::after {
				position: absolute;
				top: 0;
				right: 0;
				height: 8px;
				width: 8px;
				border-radius: 50%;
				content: "";
				transform: translate(0, -100%);
				background-color: var(--notification-color);
			}

			.isActive {
				color: var(--primary-color);
			}

			.error.isActive {
				color: var(--error-color);
			}
		`}</style>
		<Icon size={20} type={icon} />
	</span>
);

StatusDisplay.propTypes = {
	isActive: PropTypes.bool,
	theme: PropTypes.oneOf(['error', 'default']),
	hasNotification: PropTypes.bool,
	icon: PropTypes.string,
};

StatusDisplay.defaultProps = {
	isActive: false,
	theme: 'default',
	hasNotification: false,
	icon: null,
};

export default StatusDisplay;
