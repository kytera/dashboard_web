import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { compose, withState, withHandlers, withProps, lifecycle } from 'recompose';
import { propType } from 'graphql-anywhere';
import { sortBy } from 'lodash';
const ls = require('local-storage');

import Input from '../../../common/input/Input';
import Select, { Option } from '../../../common/select/Select';
import { dashboardServiceStatusEnum } from '../../../../../server/graphql/utils/subscriber'
import SUBSCRIBERS_QUERY from '../../../../graphql/subscriber/subscribers.query.graphql';
import UserItem from './UserItem';
import FilterButtons from './FilterButtons';

const filterBySearch = search => (user) => {
	if (!search) return true;
	const firstName = user.firstName || '';
	const lastName = user.lastName || '';
	return (firstName && firstName.toUpperCase().includes(search.toUpperCase()))
		|| (lastName && lastName.toUpperCase().includes(search.toUpperCase()))
		|| ((firstName || lastName) && `${firstName} ${lastName}`.toUpperCase().includes(search.toUpperCase()))
		|| user.id === search;
};

const filterByStatus = status => (user) => {
	if (!status) return true;
	return !!user.status && user.status[status] === true && user.dashboardServiceStatus !== dashboardServiceStatusEnum.archived &&
		user.dashboardServiceStatus !== dashboardServiceStatusEnum.pending_installation;
};

const filterByType = type => (user) => {
	if (!type || type==='all') return true;
	return user.dashboardServiceStatus === type;
};


export const LeftPane = ({ filteredSubscribers, data, search, onSearchChange, onTypeChange, currentTab, subscriberId, onFilterStatus, setFilterStatus, setFilterType, setSearch, filterStatus, filterType }) => {
	/*
	if (this.lastSetFilter !== window.location.href) {
		setTimeout(() => {
			setFilterStatus(ls.get('filterStatus') || '');
			setFilterType(ls.get('filterType') || 'all');
			setSearch(ls.get('filterSearch') || '');
		}, 50);
		this.lastSetFilter = window.location.href ;
	}
	*/

	return (
		<div className='root'>
			{/* language=CSS */}
			<style jsx>{`
			.root {
				width: var(--left-side-with);
				min-width: var(--left-side-with);
				height: 100%;
				background-color: #F4F4F4;
				display: flex;
				flex-direction: column;
			}

			.input-group {
				display: flex;
				padding: 12px;
			}

			.input-group > :global(:last-child) {
				flex: 1;
			}
			
			.input-group > :global(div.root.grouped) {
				line-height: 0 !important;
			}

			.users {
				flex: 1;
				overflow-y: auto;
				border-top: 2px var(--disabled-color) solid;
				padding-bottom: 30px;
			}

			.empty-state {
				opacity: .4;
				padding: 24px 16px;
			}
		`}</style>
			<FilterButtons
				onChange={onFilterStatus}
				selected={filterStatus}
				subscribers={data.subscribers}
				filterType={filterType}
			/>
			<div className='input-group'>
				<Select onChange={onTypeChange} value={filterType} grouped>
					<Option value="all">All</Option>
					<Option value={dashboardServiceStatusEnum.active}>Active</Option>
					<Option value={dashboardServiceStatusEnum.pending_installation}>Pending Installation</Option>
					<Option value={dashboardServiceStatusEnum.pending_activation}>Pending Activation</Option>
					<Option value={dashboardServiceStatusEnum.suspended}>Suspended</Option>
					<Option value={dashboardServiceStatusEnum.archived}>Archived</Option>
				</Select>
				<Input
					icon='search'
					placeholder='Filter'
					type='search'
					grouped
					onChange={onSearchChange}
					value={search}
				/>
			</div>
			<div className='users'>
				{filteredSubscribers.length ? filteredSubscribers.map(subscriber => (
					<UserItem
						key={subscriber.id}
						isActive={subscriberId === subscriber.id}
						user={subscriber}
						currentTab={currentTab}
					/>
				)) : <div className='empty-state'>No subscribers found</div>}
			</div>
		</div>
	);
}

LeftPane.propTypes = {
	filteredSubscribers: PropTypes.arrayOf(PropTypes.object).isRequired,
	onSearchChange: PropTypes.func.isRequired,
	onTypeChange: PropTypes.func.isRequired,
	onFilterStatus: PropTypes.func.isRequired,
	search: PropTypes.string.isRequired,
	setFilterStatus: PropTypes.func.isRequired,
	setFilterType: PropTypes.func.isRequired,
	setSearch: PropTypes.func.isRequired,
	filterStatus: PropTypes.string,
	filterType: PropTypes.string,
	subscriberId: PropTypes.string,
	currentTab: PropTypes.string,
	data: propType(SUBSCRIBERS_QUERY),
};

LeftPane.defaultProps = {
	subscriberId: null,
	filterStatus: null,
	filterType: 'all',
	currentTab: null,
};


const enhance = compose(
	withState('search', 'setSearch', ''),
	withState('filterStatus', 'setFilterStatus', ''),
	withState('filterType', 'setFilterType', 'all'),
	withHandlers({
		onSearchChange: props => (event) => {
			ls.set('filterSearch', event.target.value);
			props.setSearch(event.target.value);
		},
		onTypeChange: props => (event) => {
			ls.set('filterType', event.target.value);
			props.setFilterType(event.target.value);

			if (event.target.value !== dashboardServiceStatusEnum.active) {
				props.setFilterStatus(null);
				ls.set('filterStatus', null);
			}
		},
		onFilterStatus: props => (value) => {
			ls.set('filterStatus', value);
			props.setFilterStatus(value);
			if (value !== null) {
				props.setFilterType(dashboardServiceStatusEnum.active);
				ls.set('filterType', dashboardServiceStatusEnum.active);
			}
		}
	}),
	graphql(SUBSCRIBERS_QUERY, {
		options: ({ companiesIds }) => ({
			variables: { companiesIds },
		}),
	}),
	withProps(({ data: { subscribers = [] }, search, url, filterStatus, filterType }) => ({
		filteredSubscribers: sortBy(subscribers
			.filter(filterBySearch(search))
			.filter(filterByType(filterType))
			.filter(filterByStatus(filterStatus)), ['lastName', 'firstName']),
		currentTab: url.pathname.split('/')[2],
		subscriberId: url.query.id,
	})),
);

export default enhance(lifecycle({
	componentDidMount() {
		console.log('mounted', this);
		this.props.setFilterStatus(ls.get('filterStatus') || '');
		this.props.setFilterType(ls.get('filterType') || 'all');
		this.props.setSearch(ls.get('filterSearch') || '');
	}
})(LeftPane));
