import { storiesOf } from '@storybook/react';

import FilterButton from './FilterButton';

storiesOf('layout/subscribers/left-pane/FilterButton', module)
	.add('default', () => (
		<FilterButton icon='bell'>123</FilterButton>
	))
	.add('active', () => (
		<FilterButton icon='bell' isActive>123</FilterButton>
	));
