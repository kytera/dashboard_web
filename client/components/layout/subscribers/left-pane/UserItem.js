import PropTypes from 'prop-types';
import csx from 'classnames';
import { Link } from '../../../../../lib/routes';
import { dashboardServiceStatusEnum } from '../../../../../server/graphql/utils/subscriber'

import StatusDisplay from './StatusDisplay';

const UserItem = ({ user, isActive, currentTab }) =>{
	var deactivationText = null;
	if([dashboardServiceStatusEnum.suspended,
		dashboardServiceStatusEnum.pending_activation,
		dashboardServiceStatusEnum.pending_installation,
		dashboardServiceStatusEnum.archived].includes(user.dashboardServiceStatus)) {
		deactivationText = user.dashboardServiceStatus.replace(/\_/,' ');
	}

	let status = user.dashboardServiceStatus

	const labelColor = status === dashboardServiceStatusEnum.pending_activation ? 'success' :
		status === dashboardServiceStatusEnum.archived ? 'disabled' : 'primary';

	return (
	<Link route={currentTab ? `subscriber-info` : 'subscriber-info'} params={{ id: user.id }}>
		<a className={csx('root', { isActive })}>
			{/* language=SCSS */}
			<style jsx>{`
				.root {
					display: flex;
					flex-direction: row;
					//align-items: flex-start;
					//justify-content: space-between;
					padding: 12px 16px;
				}

				.root:hover {
					background-color: rgba(255, 255, 255, .4);
				}

				.root.isActive {
					background-color: rgba(255, 255, 255, .9);
					box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.3);
				}

				.title {
					font-weight: 600;
					font-size: 13px;
					color: var(--primary-color);
					text-overflow: ellipsis;
    			overflow: hidden;
					white-space: nowrap;
					line-height: 18px;
				}

				.subtitle {
					font-weight: 400;
					font-size: 12px;
					color: rgba(0, 0, 0, .4);
				}

				.content {
					flex: 1;
					text-overflow: ellipsis;
    			overflow-x: hidden;
				}

				.actions {
					width: 110px;
				}

				.deactivated {
					border-radius: 99px;
					background-color: var(--primary-color);
					color: #fff;
					font-size: 12px;
					padding: 2px 12px;
					line-height: 1;
					color: rgba(255, 255, 255, .9);
					text-transform:capitalize;
				}
				.success {
					background-color: var(--success-color);
				}
				.disabled {
					background-color: #e9e9e9;
					color:#3b5173;
				}
			`}</style>
			<div className='content'>
				<div className='title'>{user.lastName} {user.firstName}</div>
				<div className='subtitle'>{user.jobTitle}</div>
			</div>
			{user.dashboardServiceStatus === 'active' && !!user.status && (<div className='actions'>
				<StatusDisplay icon='alert_triangle' isActive={user.status.openAlert} theme='error' />
				<StatusDisplay icon='wrench' isActive={user.status.openServiceCall} theme='error' />
				<StatusDisplay
					icon='bell'
					isActive={user.status.openNotification}
					hasNotification={user.status.openNotification}
				/>
			</div>)}
			{ deactivationText && <div className={`${labelColor} deactivated`}>{deactivationText}</div>}
		</a>
	</Link>
)
};

UserItem.propTypes = {
	user: PropTypes.shape({
		id: PropTypes.string.isRequired,
		firstName: PropTypes.string,
		lastName: PropTypes.string,
		dashboardServiceStatus: PropTypes.string.isRequired,
	}).isRequired,
	isActive: PropTypes.bool,
	currentTab: PropTypes.string,
};

UserItem.defaultProps = {
	isActive: false,
	currentTab: null,
};

export default UserItem;
