import PropTypes from 'prop-types';
import csx from 'classnames';
import { Link } from '../../../../../lib/routes';

const NavTab = ({ tab, children, hasNotification, isHighlighted, url }) => {
	const isDisabled = !url.query.id;
	const route = isDisabled ? '/' : `subscriber-${tab}`;
	const isActive = url.pathname.split('/')[2] === tab;

	return (
		<div className={csx('tab', { isActive, isHighlighted, isDisabled })}>
			{ /* language=CSS */ }
			<style jsx>{`
				.tab {
					font-weight: 400;
					flex: 1;
					text-align: center;
					color: #fff;
					text-transform: uppercase;
					transition: all .1s ease;
				}

				.tab.isActive {
					background-color:var(--active-color);
					border-top:solid 4px var(--accent-color);
					position: relative;
					height: calc( 100% + 4px );
					top: -4px;
				}

				.tab:hover {
					background-color: rgba(255, 255, 255, .05);
				}

				.link {
					display: block;
					padding: 16px;
				}

				.isHighlighted {
					background-color: var(--notification-color) !important;
				}

				.isActive {
					background-color: rgba(255, 255, 255, .25);
					pointer-events: none;
				}

				.isDisabled {
					pointer-events: none;
					background-color: #bebebe;
				}

				.link-text {
					display: inline-block;
					position: relative;
				}

				.hasNotification::after {
					position: absolute;
					top: 0;
					right: 0;
					height: 10px;
					width: 10px;
					border-radius: 50%;
					content: "";
					transform: translate(100%, -50%);
					background-color: var(--notification-color);
				}
			`}</style>
			<Link route={route} params={{ id: url.query.id }} prefetch >
				<a className='link'>
					<span className={csx('link-text', { hasNotification })}>{children}</span>
				</a>
			</Link>
		</div>
	);
};

NavTab.propTypes = {
	children: PropTypes.node.isRequired,
	hasNotification: PropTypes.bool,
	isHighlighted: PropTypes.bool,
	url: PropTypes.shape({ pathname: PropTypes.string, query: PropTypes.object }).isRequired,
	tab: PropTypes.string.isRequired,
};

NavTab.defaultProps = {
	hasNotification: false,
	isHighlighted: false,
};

export default NavTab;
