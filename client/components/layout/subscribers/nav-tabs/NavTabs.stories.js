import { storiesOf } from '@storybook/react';

import {NavTabs} from './NavTabs';

storiesOf('layout/nav-tabs/NavTabs', module)
	.add('default', () => <NavTabs url={{ pathname: '/', query: {} }} hasActiveAlert={false} />)
	.add('with active alert', () => <NavTabs url={{ pathname: '/subscriber/123/alert', query: {} }} hasActiveAlert={true} />);
