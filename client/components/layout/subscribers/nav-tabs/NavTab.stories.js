import { storiesOf } from '@storybook/react';

import NavTab from './NavTab';

storiesOf('layout/nav-tabs/NavTab', module)
	.add('default', () => <NavTab tab='alert' link='/' url={{ pathname: '/', query: {} }}>Main page</NavTab>)
	.add('with notification', () => <NavTab tab='alert' url={{ pathname: '/', query: {} }} link='/' hasNotification>Main page</NavTab>)
	.add('active', () => <NavTab tab='alert' isActive url={{ pathname: '/', query: {} }} link='/'>Main page</NavTab>);
