import PropTypes from 'prop-types';
import { compose, withProps } from 'recompose';
import { graphql } from 'react-apollo';
import { get } from 'lodash';

import SUBSCRIBER_QUERY from '../../../../graphql/subscriber/subscriber.query.graphql';

/* eslint-disable import/no-named-as-default */
import PermissionOnly from '../../../common/permission-only/PermissionOnly';
import NavTab from './NavTab';
import {dashboardServiceStatusEnum} from '../../../../../server/graphql/utils/subscriber'

export const NavTabs = ({ url, systemStatus, dashboardServiceStatus }) => {
	const openAlert = systemStatus && systemStatus.openAlert;
	const openNotification = systemStatus && systemStatus.openNotification;
	const openServiceCall = systemStatus && systemStatus.openServiceCall;
	return (
		<nav className='tabs'>
			{ /* language=CSS */ }
			<style jsx>{`
			.tabs {
				display: flex;
				background-color: var(--primary-color);
			}
		`}</style>
			{
				openAlert ? (
					<PermissionOnly resource={PermissionOnly.resources.ALERT}>
						<NavTab tab='alert' url={url} isHighlighted={openAlert}>Alert</NavTab>
					</PermissionOnly>
				) : null
			}
			<PermissionOnly resource={PermissionOnly.resources.ALERT}>
				<NavTab tab='alerts-history' url={url}>Alerts history</NavTab>
			</PermissionOnly>
			{dashboardServiceStatus && dashboardServiceStatusEnum.archived !== dashboardServiceStatus && (<PermissionOnly resource={PermissionOnly.resources.SERVICE}>
				<NavTab tab='service' url={url} isHighlighted={openServiceCall} >Service</NavTab>
			</PermissionOnly>)}
			<PermissionOnly resource={PermissionOnly.resources.NOTIFICATIONS}>
				<NavTab tab='notifications' url={url} hasNotification={openNotification} >Notifications</NavTab>
			</PermissionOnly>
			<PermissionOnly resource={PermissionOnly.resources.SUBSCRIBER}>
				<NavTab tab='info' url={url}>Subscriber</NavTab>
			</PermissionOnly>
			<PermissionOnly resource={PermissionOnly.resources.HOMECARE}>
				<NavTab tab='home-care' url={url}>Home care</NavTab>
			</PermissionOnly>
			<PermissionOnly resource={PermissionOnly.resources.KYTERA}>
				<NavTab tab='kytera' url={url}>Kytera</NavTab>
			</PermissionOnly>
		</nav>
	);
};

NavTabs.propTypes = {
	url: PropTypes.shape({ pathname: PropTypes.string, query: PropTypes.object }).isRequired,
	status: PropTypes.shape({
		openAlert: PropTypes.bool,
		openNotification: PropTypes.bool,
		openServiceCall: PropTypes.bool,
	}),
};

NavTabs.defaultProps = {
	status: {
		openAlert: false,
		openNotification: false,
		openServiceCall: false,
	},
};

const enhance = compose(
	graphql(SUBSCRIBER_QUERY,
		{
			options: ({ url }) => ({
				variables: { id: get(url, 'query.id')},
				fetchPolicy: 'cache-and-network',
			}),
			skip: ({ url }) => !get(url, 'query.id'),
		},
	),
	withProps(({ data, url }) => ({
		url,
		systemStatus: get(data, 'subscriber.status', null),
		dashboardServiceStatus: get(data, 'subscriber.dashboardServiceStatus', null),
	})),
);

export default enhance(NavTabs);