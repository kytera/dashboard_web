import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { StatusHeader } from './StatusHeader';

const SUBSCRIBER = {
	firstName: 'John',
	lastName: 'Doe',
	role: 'VRI',
	createdAt: "2017-08-09T22:37:05.746Z",
	id: '123',
	address: '1732, 1st Ave #0095',
	isActive: true,
	status: null,
};

storiesOf('layout/StatusHeader', module)
	.add('default', () => <StatusHeader
		data={{
			loading: false,
			error: null,
			subscriber: SUBSCRIBER,
		}}
		mutationStatus={{ loading: false, error: null }}
		activateSubscriber={action('activate')}
	/>)
	.add('deactivated', () => <StatusHeader
		data={{
			loading: false,
			error: null,
			subscriber: {
				...SUBSCRIBER,
				isActive: false,
			},
		}}
		mutationStatus={{ loading: false, error: null }}
		activateSubscriber={action('activate')}
	/>);
