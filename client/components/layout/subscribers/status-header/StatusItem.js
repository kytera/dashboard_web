import PropTypes from 'prop-types';
import {compose, branch, renderNothing} from "recompose";

import Icon from '../../../common/icon/Icon';


const StatusItem = ({ title, subtitle, icon }) => (
	<div className='item'>
		{ /* language=CSS */ }
		<style jsx>{`
			.item {
				display: flex;
				color: var(--primary-color);
				margin-left: 24px;
				align-items: center;
				line-height: 1.5;
			}
			.title {
				font-size: 13px;
				font-weight: 500;
			}
			.subtitle {
				font-size: 12px;
			}
			.icon {
				width: 32px;
				height: 36px;
				margin-right: 0px;
			}
		`}
		</style>
		<div className='icon'>
			<Icon height='28px' number={icon} />
		</div>
		<div className='content'>
			<div className='title'>{title}</div>
			<div className='subtitle'>{subtitle}</div>
		</div>
	</div>
);

StatusItem.propTypes = {
	title: PropTypes.node.isRequired,
	subtitle: PropTypes.node,
	icon: PropTypes.string,
};

StatusItem.defaultProps = {
	subtitle: null,
	icon: null,
};

export default compose(
	branch(ownProps => !ownProps.icon || !ownProps.title, renderNothing),
)(StatusItem);
