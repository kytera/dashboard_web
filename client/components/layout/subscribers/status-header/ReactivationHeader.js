import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo';

import SET_SUBSCRIBER_SERVICE_STATUS from '../../../../graphql/subscriber/setSubscriberDashboardServiceStatus.mutation.graphql';
import { dashboardServiceStatusEnum } from '../../../../../server/graphql/utils/subscriber';
import Modal from '../../../common/modal/Modal';
import Loading from '../../../common/loading/Loading';
import Button from '../../../common/button/Button';

const MESSAGES = {
	ARCHIVE: {
		ACTION_TITLE: 'Archive Subscriber',
		PROCESSING: 'Archiving subscriber',
		SUBMIT: 'Archive',
		ACTION: 'archiving'
	},
	RESUME: {
		ACTION_TITLE: 'Resume Subscriber',
		PROCESSING: 'Resuming subscriber',
		SUBMIT: 'Resume',
		ACTION: 'resuming'
	},
	ACTIVATE: {
		ACTION_TITLE: 'Activate Subscriber',
		PROCESSING: 'Activating subscriber',
		SUBMIT: 'Activate',
		ACTION: 'activation'
	},
}

class ReactivationHeader extends Component {

	constructor(props) {
		super(props);

		this.state = {
			loadingMessage: null,
			isLoading: false,
			showResultModal: false,
			showConfirmArchive: false,
			actionSuccess: false,
			currentAction: null,
			notificationEmails: []
		}
	}

	getNotifiedEmails = () => {
		let { subscriber } = this.props;

		if(!subscriber.caregivers) return []

		let emails = subscriber.caregivers.reduce((list, caregiver) => {
			if(caregiver.sendInvite) {
				list.push(caregiver.email)
			}
			return list
		}, [])
		
		return emails
	}

	setSubscriberStatus = async status => {
		try {
			this.setState({
				isLoading: true,
				loadingMessage: MESSAGES[status].PROCESSING,
				notificationEmails: status === 'ACTIVATE' ? this.getNotifiedEmails() : [],
				currentAction: status
			});

			await this.props.activateSubscriber(status);
			this.setState({actionSuccess: true})
		}
		catch(ex) {
			this.setState({actionSuccess: false})
		}
		finally {
			this.setState({showResultModal: true, isLoading: false, loadingMessage: null});
		}
	}

	onArchiveConfirmed = () => {
		this.setState({showConfirmArchive: false})
		this.setSubscriberStatus('ARCHIVE')
	}

	renderConfirmArchive = () => {
		return (<Modal 
			title={MESSAGES.ARCHIVE.ACTION_TITLE}
			theme='error'
			button={<Button theme='error' onClick={this.onArchiveConfirmed}>{MESSAGES.ARCHIVE.SUBMIT}</Button>}
			isOpen={this.state.showConfirmArchive}
			onClose={e => this.setState({showConfirmArchive: false})}>
			<div className='delete-body'>
				<style jsx>{`
					.delete-body {
						display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						height: 256px;
						padding: 24px;
						line-height: 1.5rem;
						text-align: center;
					}
				`}
				</style>
				<p><strong>Archiving subscriber will result in permanently disconnecting all services from the subscriber.</strong></p>
				<p>Subscriber information will still be available, but services cannot be resumed. Please note that subscriber archive <strong>cannot be undone!</strong></p>
				<p>Are you sure you would like to continue?</p>
			</div>
		</Modal>)
	}
	hideResultMessage = () => {
		this.setState({showResultModal: false, currentAction: false})
	}

	renderResultMessage = () => {
		let action = this.state.currentAction;

		if(!action) return <React.Fragment/>

		let emailSent = action === 'ACTIVATE' && this.state.notificationEmails.length > 0 ? (<span><br/><span>Invitation email has been sent to:<br/>
			<span style={{fontWeight:'600'}}>{this.state.notificationEmails.join(', ')}</span></span></span>) : '';

		const modalMessage = !this.state.actionSuccess ?
			(<span>
				Subscriber {MESSAGES[action].ACTION} failed.<br/>
				<span style={{whiteSpace:'nowrap'}}>Please contact <span style={{fontWeight:'600'}}>support@kytera.com</span></span>
			</span>) :
			(<span>Subscriber has been {MESSAGES[action].SUBMIT.toLowerCase()}d {emailSent}</span>)

		return (<Modal 
			title={ this.state.actionSuccess ? `Subscriber ${MESSAGES[action].SUBMIT}d` : `{MESSAGES[action].SUBMIT} Subscriber Failed` }
			theme={ this.state.actionSuccess ? 'default' : 'error'}
			onSubmit={this.hideResultMessage}
			button={<Button theme={this.state.actionSuccess ? 'default' : 'error'} onClick={this.hideResultMessage}>Close</Button>}
			isOpen={this.state.showResultModal}
			onClose={this.hideResultMessage}>
			<div className='delete-body'>
				<style jsx>{`
					.delete-body {
						display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						height: 256px;
						padding: 24px;
						line-height: 1.5rem;
						text-align: center;
					}
				`}
				</style>
				{modalMessage}
			</div>
		</Modal>)
	}

	renderButtons = () => {
		const {subscriber } = this.props;

		switch(subscriber.dashboardServiceStatus) {
			case dashboardServiceStatusEnum.suspended:
				return(
					<React.Fragment>
						<Button
							theme='success'
							size='large'
							loading={this.state.isLoading}
							onClick={() => this.setSubscriberStatus('RESUME')}
						>
							RESUME
						</Button>
						<Button
							theme='error'
							size='large'
							loading={this.state.isLoading}
							onClick={() => this.setState({showConfirmArchive: true})}
							style={{marginLeft:'10px'}}
						>
							ARCHIVE
						</Button>
					</React.Fragment>
				)
				break;
			case dashboardServiceStatusEnum.pending_activation:
				return (<Button
					theme='success'
					size='large'
					loading={this.state.isLoading}
					onClick={() => this.setSubscriberStatus('ACTIVATE')}
				>
					ACTIVATE
				</Button>)
				break;
			default:
		}
	}

	render() {
		const {subscriber } = this.props;

		if(this.state.isLoading) {
			return <Loading isOpen={true} message={this.state.loadingMessage} />
		}

		if (!subscriber ||
			![dashboardServiceStatusEnum.suspended, dashboardServiceStatusEnum.pending_activation].includes(subscriber.dashboardServiceStatus)
			) {
			return (<div className='empty'>
				{this.renderResultMessage()}
				{this.renderConfirmArchive()}
				{/* language=CSS */}
				<style jsx>{`
					.empty {
						height: 64px;
						background: #ffffff;
					}
					.button-spacer {
						margin-left:10px;
					}
				`}</style>
			</div>);
		}
  
		var deactivationReason = '';

		switch(subscriber.dashboardServiceStatus) {
			case dashboardServiceStatusEnum.suspended:
				deactivationReason = 'This subscriber is suspended';
				break;
			case dashboardServiceStatusEnum.pending_activation:
				deactivationReason = 'This subscriber is pending activation';
			break;
			default:
		}

		return (
			<div className='deactivated'>
				{this.renderResultMessage()}
				{this.renderConfirmArchive()}
				<span className='deactivated-message'>{deactivationReason}</span>
				<style jsx>{`
					.deactivated-message {
						font-weight: 500;
						margin-right: 24px;
					}
					button.large {
						margin-left:10px;
					}
			`}</style>
			{this.renderButtons()}
			</div>
		);
	}
}

export default compose(
	graphql(SET_SUBSCRIBER_SERVICE_STATUS, {
		props: ({ownProps: {subscriber, operatorId}, mutate}) => ({
			activateSubscriber: newServiceStatus => mutate({
				variables: {
					subscriberId : subscriber.id,
					operatorId,
					newServiceStatus,
				},
			}),
		}),
	}),
)(ReactivationHeader);
