import { storiesOf } from '@storybook/react';

import StatusItem from './StatusItem';

storiesOf('layout/StatusHeader', module)
	.add('item', () => <StatusItem title='Lorem inpum' subtitle='Lorem ipsum' icon='12' />);
