import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';

import Button from '../../../common/button/Button';
import StatusItem from './StatusItem';
import ReactivationHeader from './ReactivationHeader'

import SUBSCRIBER_STATUS_QUERY from '../../../../graphql/subscriber/subscriberStatus.query.graphql';
import { apolloPropType } from '../../../../apolloPropType';
import { dashboardServiceStatusEnum } from '../../../../../server/graphql/utils/subscriber'

export const StatusHeader = ({data, ...rest}) => {
	if (!data || !data.subscriber) {
		return (<div className='empty'>
			{/* language=CSS */}
			<style jsx>{`
				.empty {
					height: 64px;
					background: #ffffff;
				}
			`}</style>
		</div>);
	}
	const {subscriber: {status, ...subscriber}} = data;
	const showStatus = subscriber.dashboardServiceStatus === dashboardServiceStatusEnum.active && status;

	var deactivationReason = '';
	var buttonText = '';
	var newStatus = null;

	var deactivationText = null;
	if([dashboardServiceStatusEnum.pending_installation,
		dashboardServiceStatusEnum.archived].includes(subscriber.dashboardServiceStatus)) {
		deactivationText = subscriber.dashboardServiceStatus.replace(/\_/,' ');
	}

	return (
		<div className={ subscriber.dashboardServiceStatus !== dashboardServiceStatusEnum.active ? 'root inactive' : 'root' }>
			<div className='user'>
				<div className='user-name'>{subscriber.firstName} {subscriber.lastName}
					<div className='user-status'>{deactivationText}</div>
				</div>
				<div className='user-role'>{subscriber.role}</div>
			</div>
			{showStatus && (
				<div className='statuses'>
					<StatusItem
						title={status.distress_alert.title}
						subtitle={status.distress_alert.message}
						icon={status.distress_alert.icon}
					/>
					<StatusItem
						title={status.home.title}
						subtitle={status.home.message}
						icon={status.home.icon}
					/>
					<StatusItem
						title={status.wear.title}
						subtitle={status.wear.message}
						icon={status.wear.icon}
					/>
					<StatusItem
						title={status.battery.title}
						subtitle={status.battery.message}
						icon={status.battery.icon}
					/>
					<StatusItem
						title={status.sleep.title}
						subtitle={status.sleep.message}
						icon={status.sleep.icon}
					/>
					<StatusItem
						title={status.outdoor.title}
						subtitle={status.outdoor.message}
						icon={status.outdoor.icon}
					/>
				</div>
			)}
			<ReactivationHeader subscriber={subscriber} operatorId={rest.operatorId}/>
			{/* language=SCSS */}
			<style jsx>{`
				.root {
					display: flex;
					padding: 0 24px;
					height: 64px;
					color: var(--primary-color);
					line-height: 1.5;
					background: #ffffff;
					align-items: center;
				}

				.inactive {
					background: #fefdd0;
				}
	
				.user {
					flex: 1;
				}

				.user-name {
					font-weight: 600;
					font-size: 20px;
					white-space: nowrap;
					overflow: hidden;
					text-overflow: ellipsis;
				}
				
				.user-status {
					font-size: 12px;
					font-weight: normal;
					color: #3b5173;
					float: right;
					text-transform: capitalize;
					margin-top: 8px;
				}

				.user-role {
					color: rgba(0, 0, 0, .5);
					font-size: 12px;
				}

				.statuses {
					display: flex;
					align-items: center;
				}
			`}</style>
		</div>
	);
};

StatusHeader.propTypes = {
	data: apolloPropType(SUBSCRIBER_STATUS_QUERY),
};

StatusHeader.defaultProps = {
	data: null,
};

export default compose(
	graphql(SUBSCRIBER_STATUS_QUERY,
		{
			options: ({subscriberId}) => ({
				variables: {subscriberId},
				fetchPolicy: 'cache-and-network',
			}),
			skip: ({subscriberId}) => !subscriberId,
		},
	),
)(StatusHeader);
