export default {
	distress_alert: {
		title: 'Attention',
		subtitle: 'Alert',
		icon: 'person',
	},
	home: {
		title: 'Subscriber',
		subtitle: 'Is at home',
		icon: 'home',
	},
	wear: {
		title: 'Wrisband',
		subtitle: 'Is ON',
		icon: 'wristband',
	},
	battery: {
		title: 'Battery',
		subtitle: '89%',
		icon: 'wristband',
	},
	sleep: {
		title: 'Sleeping',
		subtitle: 'Today: 12h 15m',
		icon: 'sleep',
	},
	outdoor: {
		title: 'Outdoor',
		subtitle: 'Today: 12h 15m',
		icon: 'home',
	},
};
