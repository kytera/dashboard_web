import React from 'react';
import PropTypes from 'prop-types';

import Button from '../../common/button/Button';
import DisplayRawPhone from '../../common/display-phone/DisplayRawPhone';

const CaregiverListItem = ({ item, editMode, onUpdateCaregiver, onCaregiverSettings, onDeleteCaregiver }) => {
	let caregiverName = item.middleName ? `${item.firstName} (${item.middleName}) ${item.lastName}` : `${item.firstName} ${item.lastName}`
	return (
	<div>
		{/* language=SCSS */}
		<style jsx>{`
			.row {
				flex: 1;
				display: flex;
				flex-direction: row;
				align-items: center;
				padding: 0 6px;
				margin: 6px;
				min-height: 36px;
				border-radius: 3px;
			}

			.row:hover {
					background-color: #F9F9F9;
			}


			.column1 {
				flex: 2;
				padding-left: 10px;
			}

			.column2 {
				color: #415677;
				font-size: 13px;
				flex: 2;
				align-items: center;
				display: flex;
			}

			.column3 {
				flex: 3;
				align-items: center;
				display: flex;
				flex-direction: row;
				justify-content: space-between;
			}

			.email {
				color: var(--accent-color);
			}

			.email:hover {
				text-decoration: dashed;
			}

			.buttonGroup {
				min-width: 75px;
			}
		`}</style>
		<div className='row'>
			<div className='column1'>{caregiverName}</div>
			<div className='column2'>
				{item.phones.map((phone, phoneIndex) => {
					const key = `${phoneIndex} ${phone}`;
					if (phoneIndex === 0) {
						return (
							<DisplayRawPhone key={key} phone={phone} />
						);
					}
					return (
						<div key={key}>
							&nbsp;,&nbsp;
							<DisplayRawPhone phone={phone} />
						</div>
					);
				})}
			</div>
			<div className='column3'>
				<a href={`mailto:${item.email}`} className='email'>
					{item.email}
				</a>
				{editMode &&
				<div className='buttonGroup'>
					<Button link icon='pencil' onClick={onUpdateCaregiver} />
					<Button link icon='settings' onClick={onCaregiverSettings} />
					<Button theme='error' icon='close_circle' link onClick={onDeleteCaregiver} />
				</div>
				}
			</div>
		</div>
	</div>
	);
}

CaregiverListItem.propTypes = {
	item: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		middleName: PropTypes.string,
		lastName: PropTypes.string.isRequired,
		email: PropTypes.string.isRequired,
		phones: PropTypes.arrayOf(PropTypes.string).isRequired,
		type: PropTypes.string
	}).isRequired,
	editMode: PropTypes.bool.isRequired,
	onUpdateCaregiver: PropTypes.func.isRequired,
	onCaregiverSettings: PropTypes.func.isRequired,
	onDeleteCaregiver: PropTypes.func.isRequired,
};
export default CaregiverListItem;
