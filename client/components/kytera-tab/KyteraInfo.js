import React from 'react'

export default class KyteraInfo extends React.Component {
  render() {
    const { info } = this.props;
    return (
      <div className="container">
        <style jsx>{`
        .container {
          display: flex;
          flex: 1;
          padding:40px;
          flex-direction: column;
          background-color: white;
        }
        h1.main-title {
          height: 33px;
          font-size: 24px;
          font-weight: bold;
          color: #3b5173;
          text-transform: uppercase;
          margin-bottom:34px;
        }
        .section {
          margin-bottom:12px;
        }
        .title {
          font-weight: 700;
          font-size:14px;
          text-transform:uppercase;
          margin-bottom: 12px;
          display:inline-block;
        }
        .item {
          background-color: #f4f4f4;
          border-bottom:solid 1px #e9e9e9;
        }
        .item.default {
          border-left:solid 3px transparent;
        }
        .item.success {
          border-left:solid 3px #7ed320;
        }
        .item.error {
          border-left:solid 3px #dd3c37;
        }
        .item.warning {
          border-left:solid 3px #f4c432;
        }

        .item:last-child {
          border-bottom: none;
        }

        .label {
          
          padding-left:18px;
          display:inline-block;
          height: 32px;
          line-height:32px;
          font-weight:bold;
        }
        .value {
          padding-left:18px;
        }
    `}</style>
        <h1 className='main-title'>System Information</h1>
        {
          info && info.map(section => {
            return (
                <span className='section' key={section.section}>
                  <span className='title'>{section.section}</span>
                  <div className='section-items'> {
                    section.values.map(item=>{
                      return (<div className={`item ${item.theme ? item.theme.toLowerCase(): 'default'}`} key={item.key}>
                        <span className="label">{item.key}</span>
                        <span className="value" dangerouslySetInnerHTML={{__html:item.value}} title={item.tooltip} />
                      </div>)
                    })
                  }
                  </div>
                </span>
          )})
        }
      </div>
    )
  }
}