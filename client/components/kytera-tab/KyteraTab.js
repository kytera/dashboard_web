import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';

import CaregiverList from './CaregiverList/CaregiverList';
import KyteraInfo from './KyteraInfo';

import KYTERA_SETTINGS_QUERY from '../../graphql/common/kyteraSettings.query.gql';
import KYTERA_INFO_QUERY from '../../graphql/common/kyteraInfo.query.gql';
import CAREGIVERS_QUERY from '../../graphql/subscriber/subscriberCaregivers.query.graphql';
import UPDATE_SUBSCRIBER_MUTATION from '../../graphql/subscriber/updateSubscriber.mutation.graphql';
import { apolloPropType } from '../../apolloPropType';
import { normalizeCaregiverInput } from '../../normalizer'
import Loading from '../common/loading/Loading'
import { caregiverTypeEnum } from '../../../server/graphql/utils/subscriber'
import SUBSCRIBER_QUERY from '../../graphql/subscriber/subscriberInfo.query.graphql';

const DEFUALT_LOADING_MESSAGE = 'loading...'
export class KyteraTab extends Component {
	static propTypes = {
		data: apolloPropType(CAREGIVERS_QUERY).isRequired,
		updateSubscriber: PropTypes.func.isRequired,
	}

	constructor(props) {
		super(props);

		this.state = {
			editMode: false,
			loading: true,
			saveLoading: false,
			loadingMessage: DEFUALT_LOADING_MESSAGE,
			caregivers: []
		};

	}

	parseNotifications = (caregivers) => {
		const notifications = this.props.settings.kyteraSettings.kytera_caregiver_notifications;

		var list = caregivers.map(caregiver => {
			var item = {...caregiver, settings: notifications.map(notification => {
				const setting = caregiver.settings.find(item => item.settingType === notification);
				const val = setting ? setting.value : true;
				return { settingType: notification, value: val};
			})}
			return item;
		});

		return list;
	}

	componentWillReceiveProps({ data, subscriberId }) {
		if(subscriberId != this.props.subscriberId) {
			this.setState({ loading: true });
			data.refetch();
		} else if (data && data.caregiversByType) {
			let caregivers = this.parseNotifications(data.caregiversByType);
			this.setState({ editMode: false, caregivers: caregivers, loading: false });
		}
	}

	componentDidMount() {
		if(this.props.data.caregiversByType ) {
			let caregivers = this.parseNotifications(this.props.data.caregiversByType);
			this.setState({ caregivers: caregivers, loading: false });
		}
	}

	componentDidUpdate(prevProps) {
		if(prevProps.subscriberId !== this.props.subscriberId) {
			let caregivers = this.parseNotifications(this.props.data.caregiversByType);
			this.setState({ caregivers: caregivers, loading: false });
		}
	}

	onEditEnable = () => this.setState({ editMode: true });

	handleSaveSubscriber = async () => {

		if (this.state.caregivers.filter(caregiver => caregiver.action).length === 0) { // nothing to save
			this.setState({ editMode: false });
			return;
		}

		let caregivers = [];

		this.setState({ saveLoading: true, loadingMessage: 'Saving subscriber...' });
		try {
			caregivers = this.state.caregivers.reduce((cgs, caregiver) => {
				if(!caregiver.action || caregiver.action === 'update') {
					let id = caregiver.id;
					let cg = normalizeCaregiverInput(caregiver);
					cg.id = id;
					cgs.push(cg);
				} else if(caregiver.action === 'create') {
						let cg = normalizeCaregiverInput(caregiver)
						cgs.push(cg);
				}
				return cgs;
			},[])

			await this.props.updateSubscriber({}, caregivers, caregiverTypeEnum.KYTERA);
			this.setState({ editMode: false });
		} catch (error) {
			console.error(error);
		} finally {
			this.setState({ saveLoading: false, loadingMessage: DEFUALT_LOADING_MESSAGE});
			this.props.data.refetch().then(() => {
				let caregivers = this.parseNotifications(this.props.data.caregiversByType);
				this.setState({ caregivers: caregivers });
			})
		}
	};

	onCreateCaregiver = (caregiver) => {
		caregiver.action = 'create';
		caregiver.caregiverType = caregiverTypeEnum.KYTERA
		caregiver.id = Math.ceil(Math.random()*1000000); // for convenience only
		if(!Array.isArray(caregiver.phones)) {
			caregiver.phones = [caregiver.phones];
		}

		let caregivers = this.state.caregivers.concat(caregiver);
		this.setState({caregivers: caregivers});
	}

	onUpdateCaregiver = (caregiver) => {
		caregiver = {...caregiver}; //this one is immutable
		if(!caregiver.action) {
			caregiver.action = 'update';
		}
		if(!Array.isArray(caregiver.phones)) {
			caregiver.phones = [caregiver.phones];
		}

		let caregivers = this.state.caregivers.map(currCg => currCg.id === caregiver.id ? caregiver : currCg);

		this.setState({caregivers: caregivers});
	}

	onDeleteCaregiver = (caregiver) => {
		caregiver = {...caregiver}; //this one is immutable
		let caregivers = [...this.state.caregivers];

		if(caregiver.action && caregiver.action === 'create') { //never saved to server?
			caregivers = caregivers.filter(cg => cg.id !== caregiver.id);
		}	else {
			caregiver.action = 'delete';
			caregivers = caregivers.map(currCg => currCg.id === caregiver.id ? caregiver : currCg);
		}

		this.setState({caregivers: caregivers});
	}

	render() {
		const { data, settings, kyteraInfo} = this.props;
		const { editMode,loading, saveLoading, loadingMessage, caregivers } = this.state;

		if (!data || data.error) {
			return null;
		}

		const subscriber = this.props.subscriberResult.subscriber;
		console.log(loading || saveLoading || data.loading ? "SHOW LOADING": "DONT SHOW LOADING");

		return (
			<div className='container'>
				<Loading isOpen={loading || saveLoading || data.loading} message={loadingMessage} />
				{ /* language=CSS */ }
				<style jsx>{`
					.container {
						display: flex;
						flex: 1;
						flex-direction: column;
						background-color: white;
                        overflow-y: auto;
					}

					.delete-body {
						display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						height: 256px;
						padding: 24px;
						line-height: 1.5rem;
						text-align: center;
					}

				`}</style>
				<CaregiverList
					editMode={editMode}
					subscriber={subscriber}
					caregivers={caregivers.filter(caregiver=> !caregiver.action || caregiver.action != 'delete')}
					subscriberId={this.props.subscriberId}
					createCaregiver={this.onCreateCaregiver}
          updateCaregiver={this.onUpdateCaregiver}
					deleteCaregiver={this.onDeleteCaregiver}
					onEditEnable={this.onEditEnable}
					onEditSave={this.handleSaveSubscriber}
					saveLoading={saveLoading}
					defaultSettings={settings.kyteraSettings ? settings.kyteraSettings.kytera_caregiver_notifications.map(setting => { return { settingType: setting,value:true }}) : []}
				/>
				<KyteraInfo info={kyteraInfo ? kyteraInfo.kyteraInfo : []}/>
			</div>
		);
	}
}

const enhance = compose(
	graphql(CAREGIVERS_QUERY,
		{
			options: ({ subscriberId }) => {
        return ({ variables: { id: subscriberId, type: caregiverTypeEnum.KYTERA } })
      },
			skip: ({ subscriberId }) => {
        return !subscriberId
      }
		},
	),
	graphql(SUBSCRIBER_QUERY,
		{
			name: "subscriberResult",
			options: ({ subscriberId }) => ({ variables: { id: subscriberId } }),
			skip: ({ subscriberId }) => !subscriberId,
		},
	),
	graphql(KYTERA_SETTINGS_QUERY,{
		name: "settings"
	}),
	graphql(KYTERA_INFO_QUERY,
		{
			name: "kyteraInfo",
			options: ({ subscriberId }) => {
				return ({variables: { subscriberId }})
			}
		},
	),
	graphql(UPDATE_SUBSCRIBER_MUTATION,
		{
			props: ({ ownProps: { subscriberId }, mutate }) => ({
				updateSubscriber: (subscriber, caregivers, caregiversType) => mutate({
					variables: {
						subscriber,
						caregivers,
						caregiversType,
						id: subscriberId
					}
				})
			})
		}
	),
);

export default enhance(KyteraTab);
