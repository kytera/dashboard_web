import React, { Component, useRef } from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';

import CaregiverList from './CaregiverList/CaregiverList';

import CAREGIVERS_QUERY from '../../graphql/subscriber/subscriberCaregivers.query.graphql';
import UPDATE_SUBSCRIBER_MUTATION from '../../graphql/subscriber/updateSubscriber.mutation.graphql';
import { apolloPropType } from '../../apolloPropType';
import { normalizeCaregiverInput } from '../../normalizer'
import Loading from '../common/loading/Loading'
import { caregiverTypeEnum } from '../../../server/graphql/utils/subscriber'

const DEFUALT_LOADING_MESSAGE = 'loading...'
export class HomeCareTab extends Component {
	static propTypes = {
		data: apolloPropType(CAREGIVERS_QUERY).isRequired,
		updateSubscriber: PropTypes.func.isRequired,
	}

	static defaultProps = {
	}

	constructor(props) {
		super(props);

		this.state = {
			editMode: false,
			loading: true,
			saveLoading: false,
			loadingMessage: DEFUALT_LOADING_MESSAGE,
			caregivers: []
		}
	}

	componentWillReceiveProps({ data, subscriberId }) {
		if(subscriberId != this.props.subscriberId) {
			this.setState({ loading: true });
			data.refetch();
		}
		else if (data && data.caregiversByType) {
			this.setState({ editMode: false, caregivers: data.caregiversByType, loading: false });
		}
	}

	componentDidMount() {
		if(this.props.data.caregiversByType ) {
			this.setState({ caregivers: this.props.data.caregiversByType, loading: false })
		}
	}

	componentDidUpdate(prevProps) {
		if(prevProps.subscriberId !== this.props.subscriberId) {
		 	this.setState({ caregivers: this.props.data.caregiversByType, loading: false })
		}
	}

	onEditEnable = () => this.setState({ editMode: true });

	handleSaveSubscriber = async () => {
		
		if (this.state.caregivers.filter(caregiver => caregiver.action).length === 0) { // nothing to save
			this.setState({ editMode: false });
			return;
		}

		let caregivers = [];

		this.setState({ saveLoading: true, loadingMessage: 'Saving subscriber...' });
		try {
			caregivers = this.state.caregivers.reduce((cgs, caregiver) => {
				if(!caregiver.action || caregiver.action === 'update') {
					let id = caregiver.id;
					let cg = normalizeCaregiverInput(caregiver);
					cg.id = id;
					cgs.push(cg);
				} else if(caregiver.action === 'create') {
						let cg = normalizeCaregiverInput(caregiver)
						cgs.push(cg);
				}
				return cgs;
			},[])

			await this.props.updateSubscriber({}, caregivers, caregiverTypeEnum.HOMECARE);
			this.setState({ editMode: false });
		} catch (error) {
			console.error(error);
		} finally {
			this.setState({ saveLoading: false, loadingMessage: DEFUALT_LOADING_MESSAGE});
			this.props.data.refetch().then(() => {
				this.setState({caregivers: this.props.data.caregiversByType})
			})
		}
	};

	onCreateCaregiver = (caregiver) => {
		caregiver.action = 'create';
		caregiver.caregiverType = caregiverTypeEnum.HOMECARE
		caregiver.id = Math.ceil(Math.random()*1000000); // for convenience only
		if(!Array.isArray(caregiver.phones)) {
			caregiver.phones = [caregiver.phones];
		}

		let caregivers = this.state.caregivers.concat(caregiver);
		this.setState({caregivers: caregivers});
	}

	onUpdateCaregiver = (caregiver) => {
		caregiver = {...caregiver}; //this one is immutable
		if(!caregiver.action) {
			caregiver.action = 'update';
		}
		if(!Array.isArray(caregiver.phones)) {
			caregiver.phones = [caregiver.phones];
		}

		let caregivers = this.state.caregivers.map(currCg => currCg.id === caregiver.id ? caregiver : currCg);

		this.setState({caregivers: caregivers});
	}

	onDeleteCaregiver = (caregiver) => {
		caregiver = {...caregiver}; //this one is immutable
		let caregivers = [...this.state.caregivers];

		if(caregiver.action && caregiver.action === 'create') { //never saved to server?
			caregivers = caregivers.filter(cg => cg.id !== caregiver.id);
		}	else {
			caregiver.action = 'delete';
			caregivers = caregivers.map(currCg => currCg.id === caregiver.id ? caregiver : currCg);
		}

		this.setState({caregivers: caregivers});
	}

	render() {
		const { data } = this.props;
		const { editMode, loading, saveLoading, loadingMessage, caregivers } = this.state;

		if (!data || data.error) {
			return null;
		}

		console.log('loading should NOT show')
		return (
			<div className='container'>
				<Loading isOpen={data.loading || loading || saveLoading} message={loadingMessage} />
				{ /* language=CSS */ }
				<style jsx>{`
					.container {
						display: flex;
						flex: 1;
						flex-direction: column;
						background-color: white;
                        overflow-y: auto;
					}

					.delete-body {
						display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						height: 256px;
						padding: 24px;
						line-height: 1.5rem;
						text-align: center;
					}

				`}</style>
				<CaregiverList
					editMode={editMode}
					caregivers={caregivers.filter(caregiver=> !caregiver.action || caregiver.action != 'delete')}
					subscriberId={this.props.subscriberId}
					createCaregiver={this.onCreateCaregiver}
          updateCaregiver={this.onUpdateCaregiver}
					deleteCaregiver={this.onDeleteCaregiver}
					onEditEnable={this.onEditEnable}
					onEditSave={this.handleSaveSubscriber}
					saveLoading={saveLoading}
				/>
			</div>
		);
	}
}

const enhance = compose(
	graphql(CAREGIVERS_QUERY,
		{
			options: ({ subscriberId }) => {
        return ({ variables: { id: subscriberId, type: caregiverTypeEnum.HOMECARE } })
      },
			skip: ({ subscriberId }) => {
        return !subscriberId
      }
		},
	),
	graphql(UPDATE_SUBSCRIBER_MUTATION,
		{
			props: ({ ownProps: { subscriberId }, mutate }) => ({
				updateSubscriber: (subscriber, caregivers, caregiversType) => mutate({
					variables: {
						subscriber,
						caregivers,
						caregiversType,
						id: subscriberId
					}
				})
			})
		}
	),
);

export default enhance(HomeCareTab);
