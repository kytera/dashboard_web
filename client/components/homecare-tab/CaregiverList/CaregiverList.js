import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';

import SUBSCRIBER_QUERY from '../../../graphql/subscriber/subscriberInfo.query.graphql';


import Title from '../../common/title/Title';
import Button from '../../common/button/Button';
import Modal from '../../common/modal/Modal';
import HomecareWizard, { availableSteps } from '../../wizards/homecare-wizard/HomecareWizard';

import CaregiverListItem from './CaregiverListItem';
import PermissionOnly from '../../common/permission-only/PermissionOnly';
import { dashboardServiceStatusEnum } from '../../../../server/graphql/utils/subscriber';

class CaregiverList extends React.Component {
	static propTypes = {
		caregivers: PropTypes.arrayOf(
			PropTypes.shape({
				firstName: PropTypes.string.isRequired,
				middleName: PropTypes.string,
				lastName: PropTypes.string.isRequired,
				email: PropTypes.string.isRequired,
				phones: PropTypes.arrayOf(PropTypes.string).isRequired,
			})).isRequired,
		editMode: PropTypes.bool.isRequired,
		onEditEnable: PropTypes.func.isRequired,
		onEditSave: PropTypes.func.isRequired,
		updateCaregiver: PropTypes.func.isRequired,
		createCaregiver: PropTypes.func.isRequired,
		deleteCaregiver: PropTypes.func.isRequired,
		saveLoading: PropTypes.bool.isRequired
	}

	state = {
		currentCaregiver: null,
		isWizardOpen: false,
		isDeleteModalOpen: false,
		isEditNotifications: false,
		isEditInfo: false
	}

	onEditInfo = currentCaregiver => () => {
		this.setState({ currentCaregiver, isWizardOpen: true, isEditInfo: true });
	}

	onCaregiverSettings = currentCaregiver => () => {
		this.setState({ currentCaregiver, isWizardOpen: true, isEditNotifications: true });
	}

	onShowDeleteCaregiver = currentCaregiver => () => {
		this.setState({ currentCaregiver, isDeleteModalOpen: true });
	}

	onSettingsSubmit = (settings) => {
		if (this.state.currentCaregiver) {
			let caregiver = {...this.state.currentCaregiver, settings}
			this.props.updateCaregiver(caregiver);
		}
		this.hideModal();
	}

	onWizardSubmit = (caregiver) => {
		if (this.state.currentCaregiver) {
			this.props.updateCaregiver(caregiver);
		} else {
			this.props.createCaregiver(caregiver);
		}
		this.hideModal();
	}

	onAddCaregiver = () => {
		this.setState({ isWizardOpen: true });
	}

	deleteCaregiver = () => {
		this.props.deleteCaregiver(this.state.currentCaregiver);
		this.hideModal();
	}

	hideModal = () => {
		this.setState({ isWizardOpen: false, isEditNotifications: false, isDeleteModalOpen: false, isEditInfo: false, currentCaregiver: null });
	}

	render() {
		const { currentCaregiver, isEditNotifications, isWizardOpen, isDeleteModalOpen, isEditInfo } = this.state;
		const { caregivers, editMode, onEditSave, onEditEnable, saveLoading, data } = this.props;

		var titleAction = <React.Fragment />;
		if(data.subscriber && data.subscriber.dashboardServiceStatus !== dashboardServiceStatusEnum.archived) {
			titleAction = editMode ?
			(<PermissionOnly resource={PermissionOnly.resources.SUBSCRIBER} action='EDIT'>
				<Button onClick={this.onAddCaregiver} theme='link' loading={saveLoading} icon='plus' style={{marginLeft:'20px'}}>Add caregiver</Button>
				<Button onClick={onEditSave} theme='default' loading={saveLoading} style={{marginLeft:'20px'}}>Save</Button>
				</PermissionOnly>) :
			(<PermissionOnly resource={PermissionOnly.resources.SUBSCRIBER} action='EDIT'>
				<Button theme='accent' onClick={onEditEnable} style={{marginLeft:'20px'}}>Edit</Button>
				</PermissionOnly>)
		}
		var steps = availableSteps;
		if(isEditInfo) {
			steps = [{...availableSteps[0]}]
		} else if (isEditNotifications) {
			steps = [{...availableSteps[1]}]
		}

		return (
			<div className='root'>
			{ /* language=CSS */ }
				<style jsx>{`
					.list {
						background-color: #f4f4f4;
						display: flex;
						flex-direction: column;
					}

					.header, .row {
						flex: 1;
						display: flex;
						flex-direction: row;
					}

					.header {
						background-color: #DDD;
						padding: 10px;
						color: #3b5173;
						font-size: 14px;
						font-weight: 800;
					}

					.column1 {
						flex: 2;
					}

					.column2 {
						flex: 2;
					}

					.column3 {
						flex: 3;
					}

					.empty {
						padding: 10px;
						text-align: center;
					}

					.delete-warning {
						font-weight: 500;
						margin-top: 12px;
					}

					.delete-body {
						display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						height: 256px;
						padding: 24px;
						line-height: 1.5;
						text-align: center;
					}
				`}</style>
				{ isDeleteModalOpen && (<Modal
					title='Delete caregiver'
					theme='error'
					isOpen
					onClose={this.hideModal}
					button={<Button theme='error' onClick={this.deleteCaregiver}>Yes, delete</Button>}
				>
					<div className='delete-body'>
						You are going to delete <br />
						<strong>{currentCaregiver.firstName} {currentCaregiver.middleName ? `(${currentCaregiver.middleName})` :""} {currentCaregiver.lastName}</strong>
						<strong>Warning: this cannot be undone</strong>
					</div>
				</Modal>) }
				<HomecareWizard
					isOpen={isWizardOpen}
					steps={steps}
					onClose={this.hideModal}
					subscriber={data.subscriber}
					initialValues={currentCaregiver ||
						{ phones: [], sendInvite: true,
							settings: [
								{ settingType: "SYSTEM", value: true},
								{ settingType: "BATTERY", value: true},
								{ settingType: "WEAR", value: true},
								{ settingType: "OUT_OF_HOME", value: true},
								{ settingType: "IRREGULAR_ACTIVITY", value: true}
							]
						}
					}
					createCaregiver={this.onWizardSubmit}
				/>
				<Title
					size='medium'
					action={titleAction}
				>
					CAREGIVERS
				</Title>
				<div className='list'>
					<div className='header'>
						<div className='column1'>Name</div>
						<div className='column2'>Mobile</div>
						<div className='column3'>Email</div>
					</div>
					{caregivers && caregivers.length
						? caregivers.map((item, index) => (
							<CaregiverListItem
								key={item.id}
								item={item}
								index={index}
								editMode={editMode}
								onUpdateCaregiver={this.onEditInfo(item)}
								onCaregiverSettings={this.onCaregiverSettings(item)}
								onDeleteCaregiver={this.onShowDeleteCaregiver(item)}
							/>
						))
						: <div className='empty'>No caregivers found.</div>
					}
				</div>
			</div>
		);
	}
}

const enhance = compose(
	graphql(SUBSCRIBER_QUERY,
		{
			options: ({ subscriberId }) => ({ variables: { id: subscriberId } }),
			skip: ({ subscriberId }) => !subscriberId,
		}
	)
)

export default enhance(CaregiverList)
