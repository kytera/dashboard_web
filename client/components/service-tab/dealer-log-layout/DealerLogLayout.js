import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { get, groupBy, sortBy, uniqBy } from 'lodash';
import { graphql, compose } from 'react-apollo';

import Title from '../../common/title/Title';
import ServiceLogSection from '../service-log-section/ServiceLogSection';
import ServiceLogsQuery from '../../../graphql/system/serviceLogs.query.graphql';
import AddServiceLogsMutation from '../../../graphql/system/addServiceLog.mutation.graphql';
import UpdateCaregiverForm from '../../common/update-caregiver-form/UpdateCaregiverForm';
import { apolloPropType } from '../../../apolloPropType';
import Button from "../../common/button/Button";

function sortAndGroupLogs(logs) {
	if (!logs) return null;
	logs = sortBy(logs, ['timestamp']);
	logs = uniqBy(logs,'id');
	return groupBy(logs, log => moment(new Date(log.timestamp)).format('LL'));
}


class DealerLogLayout extends Component {

	state = {
		loadingMore: false,
	};

	constructor(props) {
		super(props);
		this.onAddUpdate = this.onAddUpdate.bind(this);

		if (process.browser) {
			// Remove this manual polling when subscription arrives
			this.pollTimer = setInterval(() => {
				this.props.pollNewData();
			}, 30000)
		}

	}

	componentDidMount() {
		this.scrollToBottom();
	}

	componentWillUnmount() {
		clearInterval(this.pollTimer);
	}

	componentWillReceiveProps(newProps) {
		const newData = get(newProps, 'groupedLogs');
		const oldData = get(this.props, 'groupedLogs');

		// Scroll first time
		if ((newData && oldData) && Object.keys(newData).length !== 0 && Object.keys(oldData).length === 0) {
			this.shouldScroll = true;
		}

		if (newProps.subscriberId !== this.props.subscriberId) {
			this.shouldScroll = true;
			this.scrollToBottom();
		}
	}

	componentDidUpdate() {
		if (this.shouldScroll) {
			this.scrollToBottom();
			this.shouldScroll = false;
		}
	}

	onAddUpdate(logData) {
		const message = logData.message;
		this.shouldScroll = true;
		return this.props.addServiceLog({
			log: {message},
		});
	}

	scrollToBottom() {
		if (this.list) {
			this.list.scrollTop = this.list.scrollHeight;
		}
	}

	goToPrevScroll = (oldScrollHeight) => {
		this.list.scrollTop = this.list.scrollHeight - oldScrollHeight + this.list.scrollTop;
	};

	handleLoadMore = () => {
		this.setState({loadingMore: true});
		const oldScrollHeight = this.list.scrollHeight;
		this.props.loadMore().then(() => {
			this.setState({loadingMore: false});
			this.goToPrevScroll(oldScrollHeight)
		});
	};


	render() {
		const {groupedLogs, hasMore} = this.props;
		if (!groupedLogs) return null;
		return (
			<section className='root activity-list-container'>
				<Title style={{minHeight: '54px'}}>Dealer Log</Title>
				<div className='activity-list'
					ref={(r) => {
						this.list = r;
					}}
				>
					{hasMore &&
					<div className='loading'>
						<Button onClick={this.handleLoadMore} theme='accent' loading={this.state.loadingMore}>
							Load More
						</Button>
					</div>}
					{
						Object.keys(groupedLogs).map(key => (
							<ServiceLogSection
								key={key}
								title={key}
								items={groupedLogs[key]}
							/>))
					}
				</div>
				<div className='form'>
					<UpdateCaregiverForm onAddUpdate={this.onAddUpdate} showTitleBar={false}/>
				</div>
				{/* language=CSS */}
				<style jsx>{`
					.root {
						flex: 1;
						display: flex;
						flex-direction: column;
						background-color: white;
					}

					.loading {
						display: flex;
						justify-content: center;
						align-items: center;
					}
					
					.activity-list-container {
						max-height: calc(100vh - 178px);
					}

					.activity-list {
						width: auto;
						padding: 15px 0 20px 20px;
						overflow-y: auto;
						flex: 1;
						border-left: 0px;
					}

					.activity-list::-webkit-scrollbar {
						width: 8px;
					}

					.activity-list::-webkit-scrollbar-thumb {
						background-color: var(--disabled-color);
						border-radius: 4px;
					}

					.form {
						padding: 16px;
					}
				`}</style>
			</section>
		);
	}
}

DealerLogLayout.propTypes = {
	data: apolloPropType(ServiceLogsQuery),
	addServiceLog: PropTypes.func.isRequired,
	loadMore: PropTypes.func.isRequired,
};

DealerLogLayout.defaultProps = {
	data: null,
};

const enhance = compose(
	graphql(ServiceLogsQuery,
		{
			options: ({subscriberId}) => ({
				variables: {subscriberId},
				// pollInterval: 30000,
				fetchPolicy: process.browser ? 'cache-and-network' : undefined,
			}),
			skip: ({subscriberId}) => !subscriberId,
			props: ({ownProps, data}) => ({
				groupedLogs: sortAndGroupLogs(get(data, 'serviceLogs.logs', [])),
				hasMore: get(data, 'serviceLogs.hasMore', false),
				loadMore: () => {
					const {serviceLogs: {hasMore, nextCursor}, fetchMore} = data;
					const {subscriberId} = ownProps;

					return hasMore && fetchMore({
						query: ServiceLogsQuery,
						variables: {subscriberId, cursor: nextCursor},
						updateQuery: (previousResult, {fetchMoreResult}) => {
							const previousEntry = previousResult.serviceLogs;
							const newLogs = fetchMoreResult.serviceLogs.logs;
							const nextCursor = fetchMoreResult.serviceLogs.nextCursor;
							const logs = uniqBy([...newLogs, ...previousEntry.logs], 'id');

							return {
								serviceLogs: {
									__typename: previousResult.serviceLogs.__typename,
									nextCursor: nextCursor,
									hasMore: fetchMoreResult.serviceLogs.hasMore,
									logs,
								}
							};
						},
					});
				},
				pollNewData: () => {
					const {fetchMore} = data;
					const {subscriberId} = ownProps;
					return fetchMore({
						query: ServiceLogsQuery,
						variables: {subscriberId},
						updateQuery: (previousResult, {fetchMoreResult}) => {
							if (!previousResult.serviceLogs) {
								return;
							}
							const previousEntry = previousResult.serviceLogs;
							const prevLogs = sortBy(previousEntry.logs, ['timestamp']);
							let lastLog = prevLogs[prevLogs.length - 1];
							const newLogs = fetchMoreResult.serviceLogs.logs.filter(log => new Date(log.timestamp) > new Date(lastLog.timestamp));
							const nextCursor = previousResult.serviceLogs.nextCursor;

							return {
								serviceLogs: {
									__typename: previousResult.serviceLogs.__typename,
									nextCursor: nextCursor,
									hasMore: previousResult.serviceLogs.hasMore,
									logs: [...previousEntry.logs, ...newLogs]
								}
							};
						},
					});
				},
			})
		},
	),
	graphql(AddServiceLogsMutation,
		{
			props: ({ownProps: {subscriberId}, mutate}) => ({
				addServiceLog: ({log}) => mutate({
					variables: {
						subscriberId,
						log,
					},
					optimisticResponse: {
						__typename: 'Mutation',
						addServiceLogUpdate: {
							__typename: 'ServiceLog',
							id: "-1",
							createdAt: new Date().toISOString(),
							timestamp: new Date().toISOString(),
							frame: false,
							sysid: '',
							iconNumber: '49',
							title: '',
							...log,
						},
					},
					update: (store, { data: { addServiceLogUpdate } }) => {
						const data = store.readQuery({ query: ServiceLogsQuery, variables: { subscriberId } });
						data.serviceLogs.logs.push(addServiceLogUpdate);
						store.writeQuery({ query: ServiceLogsQuery, variables: { subscriberId }, data });
					},
				}),
			}),
		},
	),
);

export default enhance(DealerLogLayout);
