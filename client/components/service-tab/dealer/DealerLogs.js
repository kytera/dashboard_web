import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';

import Title from '../../common/title/Title';
import UpdateCaregiverForm from '../../common/update-caregiver-form/UpdateCaregiverForm';

import DEALER_LOGS_QUERY from '../../../graphql/log/serviceLogs.query.gql';
import CREATE_DEALER_LOG_MUTATION from '../../../graphql/system/addServiceLog.mutation.graphql';
import PermissionOnly from '../../common/permission-only/PermissionOnly';
import DealerLogsList from './DealerLogsList';


const DealerLog = ({ addUpdate, subscriberId }) => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				flex: 1;
				display: flex;
				flex-direction: column;
				background-color: #fff;
			}

			.title {
				color: var(--primary-color);
				font-weight: bold;
			}

			.activity-list {
				max-height: 50%;
				width: 100%;
				padding: 15px 0 20px 20px;
				border-left: 2px solid var(--disabled-color);
				max-height: 200px;
				overflow-y: auto;
			}
			.activity-list.dealer {
				max-height: 400px;
				border-left: 0px;
			}
			.activity-list::-webkit-scrollbar {
				width: 8px;
				padding-right: 1px;
			}

			.activity-list::-webkit-scrollbar-thumb {
				background-color: var(--disabled-color);
				border-radius: 4px;
			}

			.update-form {
				padding: 16px;
			}
			`}</style>
		<Title>Dealer Log</Title>
		<DealerLogsList subscriberId={subscriberId} />
		<PermissionOnly resource={PermissionOnly.resources.SERVICE} action='EDIT'>
			{!!addUpdate && <div className='update-form'>
				<UpdateCaregiverForm onAddUpdate={addUpdate} showTitleBar={false} />
			</div>}
		</PermissionOnly>
	</div>
);

DealerLog.propTypes = {
	addUpdate: PropTypes.func.isRequired,
	subscriberId: PropTypes.string.isRequired,
};

DealerLog.defaultProps = {
	type: null,
};

const withFormHandlers = compose(
	graphql(CREATE_DEALER_LOG_MUTATION, {
		props: ({ ownProps: { subscriberId }, mutate }) => ({
			addUpdate: log => mutate({
				variables: { subscriberId, log },
				optimisticResponse: {
					__typename: 'Mutation',
					createDealerLog: {
						__typename: 'Log',
						id: -1,
						createdAt: new Date().toISOString(),
						frame: false,
						...log,
					},
				},
				update: (store, { data: { createDealerLog } }) => {
					const data = store.readQuery({ query: DEALER_LOGS_QUERY, variables: { subscriberId } });
					data.dealerLogs.push(createDealerLog);
					store.writeQuery({ query: DEALER_LOGS_QUERY, variables: { subscriberId }, data });
				},
			}),
		}),
	}),
);

export default withFormHandlers(DealerLog);
