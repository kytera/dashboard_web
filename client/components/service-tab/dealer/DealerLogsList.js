import PropTypes from 'prop-types';
import { groupBy } from 'lodash';
import moment from 'moment';
import { compose, withProps } from 'recompose';
import { graphql } from 'react-apollo';


import ScrollContainer from '../../common/scroll-container/ScrollContainer';
import withQueryStatus from '../../../withQueryStatus';
import ListSection from '../../alert-tab/logs-list/list-section/ListSection';
import DEALER_LOGS_QUERY from '../../../graphql/log/serviceLogs.query.gql';

export const DealerLogsList = ({ logs }) => (
	<div className='root'>
		{/* language=CSS */}
		<style jsx>{`
			.root {
				background-color: #fff;
				border-top: none;
				overflow-y: auto;
				display: flex;
				flex-direction: column;
				flex: 1;
			}
		`}</style>
		<ScrollContainer autoScroll>
			{logs.map(log => (<ListSection
				key={log.groupTitle}
				title={log.groupTitle}
				logs={log.items}
			/>))}
		</ScrollContainer>
	</div>
);

DealerLogsList.propTypes = {
	logs: PropTypes.arrayOf(PropTypes.shape({
		groupTitle: PropTypes.string.isRequired,
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
	})).isRequired,
};


const withLogsData = compose(
	graphql(DEALER_LOGS_QUERY,
		{
			options: ({ subscriberId }) => ({
				variables: { subscriberId },
				fetchPolicy: 'cache-and-network',
			}),
			skip: ({ subscriberId }) => !subscriberId,
		},
	),
	withQueryStatus({ emptyText: 'This subscriber has no logs yet', resultPath: 'dealerLogs' }),
	withProps(({ data }) => {
		const grouped = groupBy(data.dealerLogs, log => moment(log.createdAt).startOf('day').toISOString());
		const logs = Object.keys(grouped).map(key => ({
			groupTitle: moment(key).format('LL'),
			items: grouped[key],
		}));
		return {
			logs,
		};
	}),
);

export default withLogsData(DealerLogsList);
