import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';


import Modal from '../../common/modal/Modal';
import Button from '../../common/button/Button';
import Input from '../../common/input/Input';

const DeviceModal = (props) => {

	const { isOpen, onClose, handleSubmit, title, requiredField, reset } = props;
	const onCloseClicked = () => {
		reset();
		onClose();
	}

	return (
		<Modal
			isOpen={isOpen}
			title={title}
			onClose={onCloseClicked}
			onSubmit={handleSubmit}
			button={
				<Button
					type='submit'
					theme='accent'
				>Done</Button>
			}
		>
			<div>
				{/* language=CSS */}
				<style jsx>{`
				.container {
					padding: 30px;
					padding-top: 10px;
				}

				.row {
					margin-top: 20px;
				}
			`}</style>
				<div className='container'>
					<div className='row'>
						<Field
							component={Input}
							name='serialNumber'
							label='Serial Number'
							required={requiredField}
						/>
					</div>
				</div>
			</div>
		</Modal>
	)
};

DeviceModal.propTypes = {
	onClose: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired,
	handleSubmit: PropTypes.func.isRequired,
	title: PropTypes.string.isRequired,
	locationDisabled: PropTypes.bool,
	requiredField: PropTypes.bool,
};
DeviceModal.defaultProps = {
	locationDisabled: false,
};

export default reduxForm({
	form: 'DEVICE_FORM',
	enableReinitialize: true,
})(DeviceModal);
