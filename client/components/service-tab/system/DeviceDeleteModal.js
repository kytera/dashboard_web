import React from 'react';
import PropTypes from 'prop-types';
import { propType } from 'graphql-anywhere';

import Modal from '../../common/modal/Modal';
import Button from '../../common/button/Button';
import DeviceFragment from './Device.fragment.graphql';

const DeviceDeleteModal = ({ isOpen, onClose, onConfirm, device }) => (
	<Modal
		isOpen={isOpen}
		title='Sensor'
		theme='error'
		onClose={onClose}
		button={
			<Button
				onClick={onConfirm}
				theme='error'
			>Yes, delete</Button>
		}
	>
		<div>
			{ device && (
				<div className='container'>
					<div className='msg'>
						You are going to delete
					</div>
					<div className='msg'>
						{device.location}
					</div>
					<div className='msg'>
						{device.serialNumber}
					</div>
					<div className='warning'>
						Warning: this cannot be undone.
					</div>
				</div>
			)}
		</div>
		{ /* language=CSS */ }
		<style jsx>{`
				.container {
					padding: 30px;
					padding-top: 30px;
				}
				.msg, .warning {
					line-height: 20px;
					flex: 1;
					text-align: center;
					color: var(--primary-color);
				}
				.warning {
					margin-top: 30px;
					font-weight: 700;
				}
			`}</style>
	</Modal>
);
DeviceDeleteModal.propTypes = {
	onClose: PropTypes.func.isRequired,
	onConfirm: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired,
	device: propType(DeviceFragment),
};
DeviceDeleteModal.defaultProps = {
	device: {},
};
export default DeviceDeleteModal;
