import React from 'react';
import PropTypes from 'prop-types';
import csx from 'classnames';
import { propType } from 'graphql-anywhere';

import Button from '../../common/button/Button';
import { DEVICE_STATUS } from './SystemLayout.consts';
import DeviceFragment from './Device.fragment.graphql';

const ListItem = (props) => {
	const {
		isSystemStopped,
		device,
		showEditButton,
		showDeleteButton,
		showResetButton,
		onClickEditItem,
		onClickDeleteItem,
		onClickResetItem,
	} = props;

	const {
		location,
		serialNumber,
		status,
		errorMessage,
		errorNumber,
	} = device;

	const statusClass = isSystemStopped || !status ? DEVICE_STATUS.DISABLED : status;
	return (
		<div className={csx('root', statusClass, { 'with-error': !!errorMessage })}>
			<div className='item-container'>
				<div className='status' />
				<div className='name'>{location}</div>
				<div className='serial-number'>{serialNumber && `#${serialNumber}`}</div>
				<div className='button-group'>
					{showEditButton && (
						<Button link icon='pencil' onClick={() => onClickEditItem(device)} />
					)}
					{showDeleteButton && (
						<div>
							<Button theme='error' icon='close_circle' link onClick={() => onClickDeleteItem(device)} />
						</div>
					)}
					{showResetButton &&
					<Button theme='accent' onClick={() => onClickResetItem(device)}>
						Reset
					</Button>
					}
				</div>
			</div>
			{status !== 'OK' && errorMessage && (
				<div className='error-container'>
					<span className='error-message'>{errorMessage}</span>
				</div>
			)}
			{ /* language=CSS */ }
			<style jsx>{`
				.root {
					margin-left: 20px;
					margin-right: 20px;
					margin-top: 10px;
					margin-bottom: 5px;
					display: flex;
					flex-direction: column;
					border-width: 0;
				}

				.root.with-error {
					border-width: 3px;
				}

				.root.ERROR {
					border-color: var(--error-color);
					border-style: solid;
					border-radius: 5px;
					margin-left: 15px;
					margin-right: 17px;
				}

				.root.WARNING {
					border-color: var(--warning-color);
					border-style: solid;
					border-radius: 5px;
					margin-left: 15px;
					margin-right: 17px;
				}

				.root.UNKNOWN {
					border-color: var(--unknown-color);
					border-style: solid;
					border-radius: 5px;
					margin-left: 15px;
					margin-right: 17px;
				}

				.error-container, .item-container {
					flex: 1;
					padding-left: 10px;
					padding-right: 10px;
				}

				.item-container {
					display: flex;
					flex-direction: row;
					align-items: center;
				}

				.item-container .status {
					width: 10px;
					height: 10px;
					border-radius: 5px;
				}

				.root.ERROR .status {
					background-color: var(--error-color);
				}

				.root.WARNING .status {
					background-color: var(--warning-color);
				}

				.root.OK .status {
					background-color: var(--success-color);
				}

				.root.DISABLED .status {
					background-color: var(--disabled-color);
				}

				.root.UNKNOWN .status {
					background-color: var(--unknown-color);
				}

				.serial-number {
					color: #818181;
					font-size: 13px;
					width: 130px;
					overflow-x: hidden;
					text-overflow: ellipsis;
					white-space: nowrap;
				}

				.root.ERROR .error-container {
					background-color: var(--error-color);
					padding-top: 6px;
					padding-bottom: 6px;
				}

				.root.WARNING .error-container {
					background-color: var(--warning-color);
					padding-top: 6px;
					padding-bottom: 6px;
				}

				.root.UNKNOWN .error-container {
					background-color: var(--unknown-color);
					padding-top: 6px;
					padding-bottom: 6px;
				}

				.error-code, .error-message {
					padding-top: 5px;
					padding-bottom: 5px;
					color: white;
					line-height: 18px;
				}

				.error-code {
					/*width: 50px;*/
					margin-right: 10px;
					font-size: 14px;
					display: inline-block;
				}

				.error-message {
					max-width: 300px;
					font-size: 12px;
				}

				.button-group {
					display: flex;
					flex: 1;
					justify-content: flex-end;
				}

				.name {
					flex: 1;
					padding: 12px;
					overflow-x: hidden;
					text-overflow: ellipsis;
					white-space: nowrap;
				}
			`}</style>
		</div>
	);
};

ListItem.propTypes = {
	isSystemStopped: PropTypes.bool,
	device: propType(DeviceFragment).isRequired,
	showEditButton: PropTypes.bool,
	showDeleteButton: PropTypes.bool,
	showResetButton: PropTypes.bool,
	onClickEditItem: PropTypes.func,
	onClickDeleteItem: PropTypes.func,
	onClickResetItem: PropTypes.func,
};

ListItem.defaultProps = {
	isSystemStopped: false,
	showEditButton: false,
	showDeleteButton: false,
	showResetButton: false,
	onClickResetItem: null,
	onClickEditItem: null,
	onClickDeleteItem: null,
};

export default ListItem;
