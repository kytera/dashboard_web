export default {
	id: '123',
	subscriberId: '123',
	isStopped: false,
	generalSystem: {
		status: 'OK',
	},
	monitoringStation: {
		status: 'OK',
	},
	kyteraCloud: {
		status: 'OK',
	},
	cellularLink: {
		status: 'OK',
	},
	baseStation: {
		status: 'OK',
		id: '123',
		location: 'Kitchen',
		serialNumber: '123',
	},
	wristband: {
		status: 'OK',
		id: '1234',
		serialNumber: '1234',
		location: 'Kitchen',
	},
	sensors: [],
};

