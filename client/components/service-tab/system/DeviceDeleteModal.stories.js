import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DeviceDeleteModal from './DeviceDeleteModal';
import DEVICES from './Device.mock'


storiesOf('service-tab/system/DeviceDeleteModal', module)
		.add('default', () =>
			<DeviceDeleteModal
				isOpen
				onClose={action('close')}
				onConfirm={action('confirm')}
				device={DEVICES[0]} />
		);
