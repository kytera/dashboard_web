import React from 'react';
import PropTypes from 'prop-types';
import { propType } from 'graphql-anywhere';

import Title from '../../common/title/Title';
import Button from '../../common/button/Button';
import Device from './Device';
import DeviceFragment from './Device.fragment.graphql';
import { MODES } from './SystemLayout.consts';


const SensorsList = (props) => {
	const {
		list,
		title,
		mode,
		onClickAddItem,
		onClickEditItem,
		onClickDeleteItem,
		onClickResetItem,
		isSuspendedService,
		isSystemStopped } = props;

	const editMode = mode === MODES.EDIT;
	const showEditButton = !isSystemStopped && editMode;
	const showResetButton = isSystemStopped;
	const showDeleteButton = showEditButton && !isSuspendedService;
	return (
		<div>
			<Title
				size='small'
				action={editMode && !isSuspendedService && (
					<Button link icon='pencil' onClick={onClickAddItem}>
						Add Sensor
					</Button>
				)}
			>{title}</Title>
			{list.map(device => (
				<Device
					key={device.id}
					device={device}
					showEditButton={showEditButton}
					showDeleteButton={showDeleteButton}
					showResetButton={showResetButton}
					onClickEditItem={onClickEditItem}
					onClickDeleteItem={onClickDeleteItem}
					onClickResetItem={onClickResetItem}
					isSystemStopped={isSystemStopped}
				/>
			))}
		</div>
	);
};

SensorsList.propTypes = {
	list: PropTypes.arrayOf(propType(DeviceFragment)).isRequired,
	title: PropTypes.string.isRequired,
	mode: PropTypes.oneOf(Object.keys(MODES)).isRequired,
	isSystemStopped: PropTypes.bool,
	isSuspendedService: PropTypes.bool,
	onClickAddItem: PropTypes.func.isRequired,
	onClickEditItem: PropTypes.func.isRequired,
	onClickDeleteItem: PropTypes.func.isRequired,
	onClickResetItem: PropTypes.func.isRequired,
};

SensorsList.defaultProps = {
	isSystemStopped: false,
};

export default SensorsList;
