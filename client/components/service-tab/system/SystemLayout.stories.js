import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { SystemLayout } from './SystemLayout';

import system from './service.mockup';

storiesOf('service-tab/system/SystemLayout', module)
	.add('normal mode', () =>
		<SystemLayout
			data={{ system, loading: false, error: true }}
			updateSystem={action('update')}
			mutationStatus={{ loading: false, error: null }}
		/>,
	);
