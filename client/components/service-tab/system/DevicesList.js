import React from 'react';
import PropTypes from 'prop-types';

import Title from '../../common/title/Title';
import Device from './Device';
import DeviceFragment from './Device.fragment.graphql';
import { MODES } from './SystemLayout.consts';
import { apolloFragmentPropType } from '../../../apolloFragmentPropType';


const DevicesList = (props) => {
	const {
		list,
		title,
		mode,
		onClickResetItem,
		onClickEditItem,
		isSystemStopped } = props;

	const editMode = mode === MODES.EDIT;
	// const showEditButton = !isSystemStopped && editMode;
	const showResetButton = isSystemStopped;
	return (
		<div>
			<Title
				size='small'
			>{title}</Title>
			{list.map(device => (
				<Device
					key={device.id}
					device={device}
					showEditButton={editMode}
					showResetButton={showResetButton}
					onClickResetItem={onClickResetItem}
					onClickEditItem={onClickEditItem}
					isSystemStopped={isSystemStopped}
				/>
			))}
		</div>
	);
};

DevicesList.propTypes = {
	list: PropTypes.arrayOf(apolloFragmentPropType(DeviceFragment)).isRequired,
	title: PropTypes.string.isRequired,
	mode: PropTypes.oneOf(Object.keys(MODES)).isRequired,
	isSystemStopped: PropTypes.bool,
	onClickEditItem: PropTypes.func.isRequired,
	onClickResetItem: PropTypes.func.isRequired,
};

DevicesList.defaultProps = {
	isSystemStopped: false,
};

export default DevicesList;
