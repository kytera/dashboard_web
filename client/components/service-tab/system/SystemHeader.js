import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../common/button/Button';
import Title from '../../common/title/Title';
import { MODES } from './SystemLayout.consts';

const SystemHeader = ({loadingData, serviceStatus, isStopped, alertCount, onEditSystem, onStopSystem, onSaveSystem, mode, hasSystem, loading}) => {
	const actions = [];
	const isSuspended = serviceStatus === 'suspended';
	if (serviceStatus !== 'active') {
		if (!loadingData && isStopped) {
			actions.push(
				<Button key='save' theme='accent' onClick={onSaveSystem}>
					Save and run system
				</Button>,
			);
		} else if (mode === MODES.NORMAL) {
			actions.push(
				<Button key='edit' link icon='pencil' onClick={onEditSystem} loading={loading}>
					{isSuspended ? 'Manage System' : 'Configure System'}
				</Button>,
			);
		} else if (mode === MODES.EDIT) {
			actions.push(
				<Button key='save' theme='accent' onClick={onSaveSystem} loading={loading}>
					Save and run system
				</Button>,
			);
		}
	}
	const subtitle = alertCount > 0 ? <span>({alertCount} {alertCount > 1 ? 'alerts' : 'alert'})</span> : null;
	return (
		<Title subtitle={subtitle} action={actions} style={{height: '54px'}}>System</Title>
	);
};
SystemHeader.propTypes = {
	isStopped: PropTypes.bool,
	loadingData: PropTypes.bool,
	loading: PropTypes.bool,
	hasSystem: PropTypes.bool,
	alertCount: PropTypes.number,
	onEditSystem: PropTypes.func,
	onStopSystem: PropTypes.func,
	onSaveSystem: PropTypes.func,
	mode: PropTypes.oneOf(Object.keys(MODES)),
	isActiveService: PropTypes.bool,
};
SystemHeader.defaultProps = {
	isStopped: true,
	loading: false,
};
export default SystemHeader;
