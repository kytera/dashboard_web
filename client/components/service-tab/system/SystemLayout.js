import React, { Component } from 'react';
import Alert from 'react-s-alert';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import { pick, get } from 'lodash';
import { shallowEqual } from 'recompose';

import DeviceModal from './DeviceModal';
import DeviceDeleteModal from './DeviceDeleteModal';
import SystemHeader from './SystemHeader';
import DevicesList from './DevicesList';
import SensorsList from './SensorsList';
import Device from './Device';

import SYSTEM_QUERY from '../../../graphql/system/system.query.graphql';
import UPDATE_SYSTEM_MUTATION from '../../../graphql/system/updateSystem.mutation.graphql';
import ACTIVE_ALERT_QUERY from '../../../graphql/alert/activeAlert.query.gql';
import SUBSCRIBER_STATUS_QUERY from '../../../graphql/subscriber/subscriberStatus.query.graphql';
import { MODES, DEVICE_STATUS } from './SystemLayout.consts';
import { apolloPropType } from '../../../apolloPropType';

const DEVICE = {
	baseStation: 'baseStation',
	wristband: 'wristband',
};
const DEVICE_MODAL_TITLE = {
	DEVICE: 'Device',
	SENSOR: 'Sensor',
};

export class SystemLayout extends Component {
	static isDeviceASensor = device => !DEVICE[device.id];
	static validateSystemInput = (systemInput) => {
		// if (!get(systemInput, 'baseStation.serialNumber')) throw new Error('Base station serial number is mandatory');
		// if (!get(systemInput, 'wristband.serialNumber')) throw new Error('Wristband serial number is mandatory');
	};

	constructor(props) {
		super(props);

		this.onClickAddItem = this.onClickAddItem.bind(this);
		this.onClickEditItem = this.onClickEditItem.bind(this);
		this.onClickResetItem = this.onClickResetItem.bind(this);
		this.onClickDeleteItem = this.onClickDeleteItem.bind(this);
		this.onDeviceModalSubmit = this.onDeviceModalSubmit.bind(this);
	}

	state = {
		currentDevice: null,
		mode: MODES.NORMAL,
		showDeviceModal: false,
		system: get(this.props, 'data.system', null),
		loading: false,
	};

	componentWillReceiveProps(newProps) {
		const newSystem = get(newProps, 'data.system', null);
		const oldSystem = get(this.props, 'data.system', null);

		if(newProps.subscriberId !== this.props.subscriberId) {
			newProps.data.refetch();
		}
		if (!shallowEqual(newSystem, oldSystem)) {
			this.setState({
				system: newSystem,
				mode: MODES.NORMAL,
			});
		}
	}

	onClickEditSystem = () => {
		if (this.state.loading) {
			return;
		}
		this.setState({ mode: MODES.EDIT });
	};

	onClickStopSystem = async () => {
		if (this.state.loading) {
			return;
		}

		const updatedSystem = {
			...this.state.system,
			isStopped: true,
		};
		try {
			this.setState({ loading: true });
			await this.props.updateSystem(updatedSystem);
			this.setState({ loading: false });
			Alert.success('System was stopped successfully', {
				position: 'bottom-left',
				effect: 'stackslide',
			});
		} catch (e) {
			console.error('onClickStopSystem', e);
			this.setState({ loading: false });
			Alert.error('Error while stopping system', {
				position: 'bottom-left',
				effect: 'stackslide',
			});
		}
	};

	onSaveSystem = async () => {
		if (this.state.loading) {
			return;
		}

		const updatedSystem = {
			...this.state.system,
			isStopped: false,
		};

		try {
			SystemLayout.validateSystemInput(updatedSystem);
		} catch (e) {
			Alert.error(`Error: ${e.message}`, {
				position: 'bottom-left',
				effect: 'stackslide',
			});
			return;
		}

		try {
			this.setState({ loading: true });
			await this.props.updateSystem(updatedSystem);
			this.setState({ mode: MODES.NORMAL, loading: false });
			Alert.success('System was updated successfully', {
				position: 'bottom-left',
				effect: 'stackslide',
			});
		} catch (e) {
			console.error('onSaveSystem', e);
			this.setState({ loading: false });
			const firstErrorMessage = get(e, 'graphQLErrors[0].message', e.message);
			Alert.error(`Error: ${firstErrorMessage}`, {
				position: 'bottom-left',
				effect: 'stackslide',
			});
		}
	};

	onClickResetItem(device) {
		this.onClickEditItem(device);
	}

	onClickEditItem(device) {
		this.setState({
			currentDevice: device,
			showDeviceModal: true,
		});
	}

	onClickDeleteItem(device) {
		this.setState({
			currentDevice: device,
		});
		this.onDeleteDevice(device);
	}

	onClickAddItem() {
		this.setState({
			currentDevice: null,
			showDeviceModal: true,
		});
	}

	onCloseModal = () => {
		this.setState({
			showDeviceModal: false,
			currentDevice: null,
		});
	};

	onDeviceModalSubmit = (data) => {
		const { system, currentDevice } = this.state;
		const sensors = get(system, 'sensors', []);
		const updateObj = {};
		const isSensor = currentDevice && SystemLayout.isDeviceASensor(currentDevice);
		if (!currentDevice) {
			// if not current device is set, it means we are adding new sensor.
			updateObj.sensors = [
				...sensors,
				{
					id: new Date().valueOf(),
					status: DEVICE_STATUS.DISABLED,
					...data,
				},
			];
		} else if (isSensor) {
			updateObj.sensors = system.sensors.map(sensor => (sensor.id === currentDevice.id ? { ...currentDevice, ...data } : sensor));
		} else {
			updateObj[currentDevice.id] = {
				...currentDevice,
				...data,
			};
		}
		this.setState({
			system: {
				...system,
				...updateObj,
			},
		});
		this.onCloseModal();
	};

	onDeleteDevice = (sensorDevice) => {
		this.setState({
			system: {
				...this.state.system,
				sensors: this.state.system.sensors.filter(sensor => sensor.id !== sensorDevice.id),
			},
		});
		this.onCloseModal();
	};

	render() {
		const { mode, currentDevice, showDeviceModal, system } = this.state;

		const hasSystem = !!system;
		const baseStation = get(system, DEVICE.baseStation, {});
		const wristband = get(system, DEVICE.wristband, {});
		const sensors = get(system, 'sensors', []);
		const isStopped = get(system, 'isStopped', false);
		const isActiveService = this.props.dashboardServiceStatus === 'active';
		const isSuspendedService = this.props.dashboardServiceStatus === 'suspended';

		const kyteraCloud = {
			id: 'kyteraCloud',
			location: 'Kytera Cloud',
			status: isStopped ? DEVICE_STATUS.DISABLED : get(system, 'kyteraCloud.status', DEVICE_STATUS.DISABLED),
			serialNumber: '',
			errorNumber: '',
			errorMessage: '',
		};
		const monitoringStation = {
			id: 'monitoringStation',
			location: 'Monitoring Station',
			status: isStopped ? DEVICE_STATUS.DISABLED : get(system, 'monitoringStation.status', DEVICE_STATUS.DISABLED),
			serialNumber: '',
			errorNumber: '',
			errorMessage: '',
		};
		const cellularLink = {
			id: 'cellularLink',
			location: 'Cellular Link',
			status: isStopped ? DEVICE_STATUS.DISABLED : get(system, 'cellularLink.status', DEVICE_STATUS.DISABLED),
			serialNumber: '',
			errorNumber: '',
			errorMessage: '',
		};
		const devices = [
			{
				status: '',
				serialNumber: '',
				errorNumber: '',
				errorMessage: '',
				...baseStation,
				location: 'Base Unit',
				id: DEVICE.baseStation,
			},
			{
				serialNumber: '',
				errorNumber: '',
				errorMessage: '',
				...wristband,
				location: 'Wristband',
				id: DEVICE.wristband,
			},
		];
		const isSensor = currentDevice && SystemLayout.isDeviceASensor(currentDevice);
		const locationDisabled = currentDevice && !isSensor;
		const deviceModalTitle = currentDevice ? (currentDevice.location? currentDevice.location: 'sensor') : 'sensor';

		if(get(this.props, 'data.loading')) {
			return (
				<section className='root'>
					<SystemHeader
						loadingData
						alertCount={0}
						isActiveService={isActiveService}
					/>
					{/* language=CSS */}
					<style jsx>{`
					.root {
						flex: 1;
						display: block;
						background-color: white;
						border-right: 2px solid #e9e9e9;
						overflow-y: auto;
					}
				`}</style>
				</section>
			);
		}

		return (
			<section className='root'>
				{/* language=CSS */}
				<style jsx>{`
					.root {
						flex: 1;
						display: block;
						background-color: white;
						border-right: 2px solid #e9e9e9;
						overflow-y: auto;
					}
				`}</style>
				<DeviceModal
					title={deviceModalTitle}
					onSubmit={this.onDeviceModalSubmit}
					initialValues={currentDevice}
					onClose={this.onCloseModal}
					isOpen={showDeviceModal}
					locationDisabled={locationDisabled}
					requiredField={isSuspendedService || !currentDevice || SystemLayout.isDeviceASensor(currentDevice)}
				/>
				<SystemHeader
					hasSystem={hasSystem}
					isStopped={isStopped}
					alertCount={0}
					onStopSystem={this.onClickStopSystem}
					onEditSystem={this.onClickEditSystem}
					onSaveSystem={this.onSaveSystem}
					mode={mode}
					loading={this.state.loading}
					serviceStatus={this.props.dashboardServiceStatus}
				/>
				<Device
					device={kyteraCloud}
				/>
				<Device
					device={monitoringStation}
				/>
				<Device
					device={cellularLink}
				/>
				<DevicesList
					isSystemStopped={isStopped}
					title='Devices'
					list={devices}
					mode={mode}
					onClickEditItem={this.onClickEditItem}
					onClickResetItem={this.onClickResetItem}
				/>
				<SensorsList
					isSuspendedService={isSuspendedService}
					isSystemStopped={isStopped}
					title='Sensors'
					list={sensors}
					mode={mode}
					onClickAddItem={this.onClickAddItem}
					onClickEditItem={this.onClickEditItem}
					onClickDeleteItem={this.onClickDeleteItem}
					onClickResetItem={this.onClickResetItem}
				/>
			</section>
		);
	}
}

SystemLayout.propTypes = {
// eslint-disable-next-line react/no-unused-prop-types
	data: apolloPropType(SYSTEM_QUERY),
	updateSystem: PropTypes.func.isRequired,
};

SystemLayout.defaultProps = {
	data: null,
};

const enhance = compose(
	graphql(SUBSCRIBER_STATUS_QUERY,
		{
			options: ({ subscriberId }) => ({
				variables: { subscriberId },
			}),
			props: ({data}) =>({
				dashboardServiceStatus: data.subscriber.dashboardServiceStatus,
			}),
			skip: ({ subscriberId }) => !subscriberId,
		},
	),
	graphql(SYSTEM_QUERY,
		{
			options: ({ subscriberId }) => ({
				variables: { subscriberId },
				pollInterval: 30000,
			}),
			skip: ({ subscriberId }) => !subscriberId,
		},
	),
	graphql(UPDATE_SYSTEM_MUTATION,
		{
			props: ({ ownProps: { subscriberId }, mutate }) => ({
				updateSystem: ({ baseStation, wristband, sensors, isStopped }) => mutate({
					variables: {
						system: {
							isStopped: !!isStopped,
							baseStation: pick(baseStation, ['serialNumber']),
							wristband: pick(wristband, ['serialNumber']),
							sensors: sensors && sensors.map(s => pick(s, ['serialNumber', 'location'])),
						},
						subscriberId,
					},
					refetchQueries: [
						{
							query: ACTIVE_ALERT_QUERY,
							variables: { subscriberId },
						},
					],
				}),
			}),
		},
	),
);

export default enhance(SystemLayout);
