export default [
	{
		id: 'device1',
		location: 'Kytera Cloud',
		status: 'OK',
		serialNumber: 'serialNumber',
		errorNumber: '',
		errorMessage: '',
	},
	{
		id: 'device2',
		location: 'Kytera Cloud',
		status: 'OK',
		serialNumber: 'serialNumber',
		errorNumber: '123',
		errorMessage: 'Error error error',
	},
	{
		id: 'device3',
		location: 'Kytera Cloud',
		status: 'WARNING',
		serialNumber: 'serialNumber',
		errorNumber: '',
		errorMessage: '',
	},
];
