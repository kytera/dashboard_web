import React from 'react';
import Moment from 'react-moment';
import csx from 'classnames';
import ServiceLogItemFragment from './ServiceLogItem.fragment.graphql';

import Icon from '../../common/icon/Icon';
import { apolloFragmentPropType } from '../../../apolloFragmentPropType';

const ServiceLogItem = ({item}) => {
	const {title, message, timestamp, frame, iconNumber} = item;

	return (
		<div className='root'>
			<div className='date'>
				<Moment format='hh:mm A'>{timestamp}</Moment>
			</div>
			<div className={csx('alert-box', {frame})}>
				<div className='icon'>
					{iconNumber && (<Icon number={iconNumber} size='24px'/>)}
				</div>
				<div className='content'>
					{!!title && <div className='title'>{title}</div>}
					{message}
				</div>
			</div>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					display: flex;
					flex-flow: row nowrap;
					align-items: flex-start;
					justify-content: flex-start;
					padding: 10px 0 20px 0;
					color: #787776;
				}

				.date, .icon {
					margin-right: 10px;

				}

				.date {
					color: var(--primary-color);
					line-height: 27px;
					width: 75px;

				}

				.icon {
					color: var(--error-color);
				}

				.content {
					margin: 6px 10px 0 0;
					min-height: 27px;
				}

				.title {
					color: var(--primary-color);
					font-weight: bold;
					display: block;
					margin-bottom: 5px;
				}

				.alert-box {
					display: flex;
					flex-flow: row nowrap;
					justify-content: flex-start;
					width: 100%;
					margin-right: 10px;
				}

				.alert-box.frame {
					border: 2px solid var(--success-color);
					border-radius: 3px;
					padding: 5px;
					margin-top: -7px;
					margin-left: -7px;
				}
			`}</style>
		</div>
	);
};

ServiceLogItem.propTypes = {
	item: apolloFragmentPropType(ServiceLogItemFragment).isRequired,
};

ServiceLogItem.defaultProps = {};

export default ServiceLogItem;
