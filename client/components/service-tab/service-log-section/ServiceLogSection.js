import PropTypes from 'prop-types';

import ServiceLogItem from '../service-log-item/ServiceLogItem';
import Divider from '../../common/content-divider/Divider';

const ServiceLogSection = ({ title, items }) => (
	<div className='root'>
		<div className='divider'>
			<Divider title={title} />
		</div>
		<div className='list'>
			{items.map(item => <ServiceLogItem item={item} key={item.id} />)}
		</div>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				width: 100%;
				margin-bottom: -15px;
				padding: 10px 0;
			}

			.divider {
				margin-bottom: 15px;
			}

			.list {
				padding-left: 8px;
			}
		`}</style>
	</div>
);

ServiceLogSection.propTypes = {
	title: PropTypes.string.isRequired,
	items: PropTypes.arrayOf(ServiceLogItem.propTypes.item).isRequired,
};

export default ServiceLogSection;
