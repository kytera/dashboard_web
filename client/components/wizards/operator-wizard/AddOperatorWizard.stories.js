import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { AddOperatorWizard } from './AddOperatorWizard';

storiesOf('wizards', module)
    .add('AddOperatorWizard', () => <AddOperatorWizard
	    isOpen
	    onClose={action('close')}
	    mutationStatus={{ loading: true, error: null }}
	    setStep={action('setStep')}
	    step={0}
	    handleSubmit={action('submit')} />);

