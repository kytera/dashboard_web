import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import generatePassword from 'password-generator';
import { reduxForm, getFormValues } from 'redux-form';
import { compose, withHandlers, withState, withProps } from 'recompose';
import { connect } from 'react-redux';

import CREATE_OPERATOR_MUTATION from '../../../graphql/operator/createOperator.mutation.gql';
import OPERATORS_QUERY from '../../../graphql/operator/operators.query.gql';

import withMutationStatus from '../../../withMutationStatus';
import Modal from '../../common/modal/Modal';
import Button from '../../common/button/Button';
import StepInfo from '../fieldsets/operator/OperatorInfo';
import FinishProfile from '../fieldsets/common/finish-profile/FinishProfileStep';
import ProfilePictureEditor from '../fieldsets/common/profile-picture-editor/ProfilePictureEditor';
import withCurrentUser, { getUserCompanyIds } from "../../../withCurrentUser";
import ROLES from "../../../constants/roles";


const FORM_NAME = 'CREATE_OPERATOR_WIZARD';

const steps = [
	{
		Component: StepInfo,
		title: 'Personal Info',
		nextText: 'Finish',
	},
	{
		Component: ProfilePictureEditor,
		title: 'Set profile picture',
		nextText: 'Save',
	},
];


export const AddOperatorWizard = reduxForm({form: FORM_NAME})(({onClose, mutationStatus: {loading, error}, handleSubmit, step, setStep}) => {
	const currentStep = steps[step];

	return (
		<div>
			{/* language=CSS */}
			<style jsx>{`
				.skip-link {
					color: var(--accent-color);
				}

				.skip-link:hover {
					text-decoration: none;
				}

				.content {
					min-height: 360px;
				}

				.error {
					padding: 24px;
					color: #fff;
					font-size: 13px;
					background-color: var(--error-color);
				}
			`}</style>
			<div className='root'>
				<Modal
					isOpen
					title={currentStep.title}
					theme={!error ? 'default' : 'error'}
					button={<Button
						theme='accent'
						type='submit'
						loading={loading}
					>
						{currentStep.nextText}
					</Button>}
					onClose={onClose}
					onSubmit={handleSubmit}
				>
					<div className='content'>
						{!!error && <div className='error'>{error}</div>}
						{<currentStep.Component step={step} setStep={setStep}/>}
					</div>
				</Modal>
			</div>
		</div>
	);
});


AddOperatorWizard.propTypes = {
	onClose: PropTypes.func.isRequired,
	handleSubmit: PropTypes.func,
	setStep: PropTypes.func.isRequired,
	step: PropTypes.number.isRequired,
	mutationStatus: PropTypes.shape({loading: PropTypes.bool.isRequired, error: PropTypes.string}).isRequired,
};

export default compose(
	withCurrentUser,
	graphql(CREATE_OPERATOR_MUTATION, {
		props: ({ownProps: {companyId, currentUser}, mutate}) => ({
			createOperator: operatorData => {
				let company = currentUser.company;
				if (currentUser.role === ROLES.SUPER_ADMIN && currentUser.companies && operatorData.companyId) {
					company = currentUser.companies.find(c => c.id === operatorData.companyId);
					delete operatorData.companyId;
				}
				return mutate({
					variables: {
						operator: operatorData,
						companyId: company.id
					},
					update: (store, {data: {createOperator}}) => {
						try {
							const data = store.readQuery({
								query: OPERATORS_QUERY,
								variables: {companiesIds: getUserCompanyIds(currentUser)},
							});
							data.operators.push(createOperator);
							store.writeQuery({
								query: OPERATORS_QUERY,
								data,
								variables: {companiesIds: getUserCompanyIds(currentUser)},
							});
						} catch (e) {
							console.log('update:readQuery failed: The OPERATORS_QUERY data do not exist in the cache')
						}
					},
				})
			},
		}),
	}),
	withMutationStatus('createOperator'),
	withState('step', 'setStep', 0),
	withHandlers({
		onSubmit: ({step, setStep, createOperator, onClose}) => async (data) => {
			const shouldSubmit = step >= steps.length - 2;
			if (shouldSubmit) {
				await createOperator(data);
				onClose();
			} else {
				setStep(step + 1);
			}
		},
	}),
	withProps(() => ({
		initialValues: {phones: [''], password: generatePassword(8, false)},
	})),
)(AddOperatorWizard);
