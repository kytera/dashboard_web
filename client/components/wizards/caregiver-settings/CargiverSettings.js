import PropTypes from 'prop-types';
import { reduxForm, Field, reset } from 'redux-form';
import { capitalize } from 'lodash'

import Modal from '../../common/modal/Modal';
import Switch from '../../common/switch/Switch';
import Button from '../../common/button/Button';

const CaregiverSettings = ({ isOpen, onClose, handleSubmit, initialValues } ) => {
  const {settings}  = initialValues;

  return (<Modal
		isOpen={isOpen}
		title='Careviger Notifications'
		onClose={onClose}
		onSubmit={handleSubmit}
		theme='default'
		button={
			<Button
				type='submit'
				theme='accent'
			>Done</Button>
		}
	>
		<div className='root'>
    { /* language=CSS */ }
    <style jsx>{`
      .row {
        padding: 12px 24px;
        height:74px;
        border-bottom: solid 2px #eeeeee;
        display: grid;
        grid-template-columns: 1fr 1fr;
        align-items: center;
        margin:0 20px;
      }
      
      .row:last-child {
        border:0;
      }
            
      .row > .right {
        justify-self:end;
      }

      .header {
        font-size: 13px;
        line-height: 18px;
        font-weight: 700;
        color:#3b5173;
        justify-self:start;
      }

    `}</style>
    { settings.map( (setting, index) => {
      const label = capitalize(setting.settingType.toLowerCase().replace(/_/g,' '));  
      return (
      <div className='row' key={setting.settingType}>
        <span className="header">{label}</span>
        <span className="right">
          <Field name={`settings[${index}].value`}
            component={Switch}
          />
        </span>
      </div>
      )}) 
    }
    </div>
	</Modal>);
}

CaregiverSettings.propTypes = {
	onClose: PropTypes.func.isRequired,
	handleSubmit: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired,
};

const FORM_NAME = 'CAREGIVER_SETTINGS';

const asyncValidate = values => {
  return Promise.resolve()
}

export default reduxForm({
	form: FORM_NAME,
  enableReinitialize: true,
  asyncValidate,
	touchOnBlur: false,
	touchOnChange: false,
	onSubmitSuccess: (result, dispatch) => dispatch(reset(FORM_NAME)),
})(CaregiverSettings);
