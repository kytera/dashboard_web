import { Field } from 'redux-form';
import PropTypes from 'prop-types';

import Input from '../../../common/input/Input';
import Select, { Option } from '../../../common/select/Select';
import PhoneStep from '../subscriber/phone/PhoneStep';
import CustomSelect from "../../../common/custom-select/CustomSelect";
import { ROLES } from "../../../../constants";
import RolesOnly from "../../../common/roles-only/RolesOnly";
import React from "react";
import withCurrentUser from "../../../../withCurrentUser";

const required = value => value ? undefined : 'Required';

const StepInfo = (props) => {
	const { showPasswordField, currentUser } = props;
	let companies = [];
	if (currentUser.role === ROLES.SUPER_ADMIN) {
		companies = props.currentUser.companies.map(company => ({
			label: company.name,
			value: company.id,
		}));
	}

	return (
		<div className='root'>
			{ /* language=CSS */ }
			<style jsx>{`
			.root {
				padding: 12px 0;
				flex: 1;
			}

			.input-container {
				padding: 12px 24px;
			}

			.radio-label {
				color: var(--primary-color);
			}
		`}</style>
			<RolesOnly role={[ROLES.SUPER_ADMIN]}>
				<div className='input-container'>
					<Field
						component={CustomSelect}
						options={companies}
						name='companyId'
						label='Company'
						required
						validate={[required]}
					/>
				</div>
			</RolesOnly>
			<div className='input-container'>
				<Field label='Email' required placeholder='Enter Operator Email' type='email' name='email' component={Input} />
			</div>
			{ showPasswordField && <div className='input-container'>
				<Field label='Password' readOnly type='text' name='password' component={Input} />
			</div> }
			<div className='input-container'>
				<Field label='Name' required placeholder='Enter Operator First Name' name='firstName' component={Input} />
			</div>
			<div className='input-container'>
				<Field label='Last Name' required placeholder='Enter Operator Last Name' name='lastName' component={Input} />
			</div>
			<div className='input-container'>
				<Field label='Role' placeholder='Enter Operator Role' name='jobTitle' component={Input} />
			</div>
			<PhoneStep isOperator={true} />
		</div>
	);
}

StepInfo.propTypes = {
	showPasswordField: PropTypes.bool,
};

StepInfo.defaultProps = {
	showPasswordField: true,
};

export default withCurrentUser(StepInfo);
