import { Field } from 'redux-form';

import Input from "../../../../common/input/Input";

const AgenciesStep = () => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				padding: 24px;
			}
			.input-container {
				margin-bottom: 20px;
			}
		`}</style>
		<div className='input-container'>
			<Field
				component={Input}
				name='medical'
				label='Medical'
				placeholder=''
				validate={[]}
			/>
		</div>
		<div className='input-container'>
			<Field
				component={Input}
				name='police'
				label='Police'
				placeholder=''
				validate={[]}
			/>
		</div>
		<div className='input-container'>
			<Field
				component={Input}
				name='fire'
				label='Fire'
				placeholder=''
				validate={[]}
			/>
		</div>
	</div>
);

export default AgenciesStep;
