import { Field } from 'redux-form';
import Input from '../../../../common/input/Input';
import DateInput from '../../../../common/date-input/DateInput';

const required = value => value ? undefined : 'Required';

const EmergencyServicesStep = props => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				padding: 24px;
			}

			.input-container {
				margin-bottom: 20px;
			}

		`}</style>

		<div className='input-container'>
			<Field
				type='text'
				name='medical'
				label='Medical'
				component={Input}
				validate={[required]}
			/>
		</div>

		<div className='input-container'>
			<Field
				type='text'
				name='police'
				label='Police'
				component={Input}
				validate={[required]}
			/>
		</div>

		<div className='input-container'>
			<Field
				type='text'
				name='fire'
				label='Fire'
				component={Input}
				validate={[required]}
			/>
		</div>
	</div>
);

export default EmergencyServicesStep;
