import { FieldArray } from 'redux-form';

import { Field } from 'redux-form';
import Input from '../../../../common/input/Input';
import countries from '../../../../../data/countries';
import withCurrentUser from "../../../../../withCurrentUser";

const required = value => value ? undefined : 'Required';

const normalizePhone = value => {
	if (!value) {
		return value
	}

	const onlyNums = value.replace(/[^\d]/g, '')
	if (onlyNums.length <= 3) {
		return onlyNums
	}
	if (onlyNums.length <= 7) {
		return `${onlyNums.slice(0, 3)}-${onlyNums.slice(3)}`
	}
	if (onlyNums.length == 9) {
		return `(${onlyNums.slice(0, 2)}) ${onlyNums.slice(2, 5)}-${onlyNums.slice(
			5,
			9
		)}`
	}
	return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(
		6,
		10
	)}`
};

const normalizePhones = value => {
	if (!value) {
		return []
	}

	const onlyNums = value.replace(/[^\d]/g, '')
	if (onlyNums.length <= 3) {
		return [onlyNums]
	}
	if (onlyNums.length <= 7) {
		return [`${onlyNums.slice(0, 3)}-${onlyNums.slice(3)}`]
	}
	if (onlyNums.length == 9) {
		return [`(${onlyNums.slice(0, 2)}) ${onlyNums.slice(2, 5)}-${onlyNums.slice(
			5,
			9
		)}`]
	}
	return [`(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(
		6,
		10
	)}`]
};

const removeCountryCode = (countryCode, phone) => {
	if (!countryCode) return phone;
	const isArray = (typeof phone) !== 'string';

	if (isArray) {
		phone = phone[0];
	}

	if (phone) {
		const prefix = countryCode;
		if (phone.startsWith(prefix)) {
			phone = phone.substr(prefix.length).trimLeft();
		}
	}

	return isArray ? [phone] : phone;

};

const PhoneStep = props => {
	const {currentUser} = props;

	const isOperator = props.isOperator;
	const baseUser = (props.formValues || props.initialValues);
	let countryCode = '';
	if (baseUser) {
		const country = baseUser.address.country;
		const countryData = countries.filter(c => c.short_name === country);
		if (countryData && countryData.length > 0) {
			countryCode = '+' + countryData[0].callingCodes[0];
		}
	}


	return (
		<div className='root'>
			{ /* language=CSS */}
			<style jsx>{`
			.root {
				padding: 24px;
			}

			.input-container {
				margin-bottom: 20px;
			}

		`}</style>

			{!isOperator && <div className='input-container'>
				<Field
					type='email'
					name='email'
					label='Email'
					component={Input}
					validate={[required]}
				/>
			</div>}

			{!isOperator && <div className='input-container'>
				<Field
					name='homePhone'
					label='Home Phone'
					placeholder='(111) 222-3333'
					component={Input}
					prefixLabel={countryCode}
					validate={[required]}
					normalize={normalizePhone}
					postDisplay={(val) => removeCountryCode(countryCode, val)}
				/>
			</div>}

			<div className='input-container'>
				<Field
					name='phones'
					label='Mobile Phone'
					placeholder='(111) 222-3333'
					prefixLabel={countryCode}
					component={Input}
					validate={[required]}
					normalize={normalizePhones}
					postDisplay={(val) => removeCountryCode(countryCode, val)}
				/>
			</div>
		</div>
	);
}

export default withCurrentUser(PhoneStep);
