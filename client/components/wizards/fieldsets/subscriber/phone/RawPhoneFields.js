import PropTypes from 'prop-types';
import React from 'react';
import { compose, withHandlers } from 'recompose';
import { Field } from 'redux-form';

import Input from '../../../../common/input/Input';
import Button from '../../../../common/button/Button';
import Icon from "../../../../common/icon/Icon";

const PhoneFields = ({ fields, label, additionalLabel, meta, required = true, showError, removePhoneNumber }) => {
	return (
		<div>
			{fields.map((name, idx) => (
				<div key={`${name}${idx}`}>
					<span className='control-label'>{label}</span>
					{ required && idx === 0 && <span className='required'>&nbsp;(required)</span> }
					<div className='form-group'>
						<div className='input'>
							<Field component={Input} name={name} />
						</div>
						<div className='remove-btn-wrapper'>
							{
								idx > 0 && (
									<button className='remove-btn' onClick={() => removePhoneNumber(idx)}>
										<Icon number={'82'} width={16} height={16}/>
									</button>
								)
							}
						</div>
					</div>
				</div>
			))}
			{ showError && meta && (meta.touched || meta.dirty) && meta.error && (
				<div className="error"> {meta.error} </div>
			)}
			<Button theme='link' icon='plus' onClick={() => fields.push('')}>Add phone number</Button>
			{ /* language=CSS */ }
			<style jsx>{`
					.form-group {
						display: flex;
						flex-flow: row nowrap;
						justify-content: space-between;
						align-items: center;
						color: var(--primary-color);
						position: relative;
						margin-top: 5px;
						margin-bottom: 15px;
					}

					.input {
						width: 84%;
					}

					.remove-btn-wrapper {
						width: 10%
					}
						
					.remove-btn {
						background-color: transparent;
					}

					.remove-btn-wrapper button {
						outline: none;
						border: 0;
					}

					.control-label {
						font-weight: bold;
					}

					.required, .control-label {
						color: var(--primary-color);
					}

					.error {
						margin-top:6px;
						margin-bottom:8px;
						font-size: 12px;
						color: var(--error-color);
					}
				`}</style>
		</div>
	);
};

PhoneFields.propTypes = {
	fields: PropTypes.shape({ push: PropTypes.func }).isRequired,
	label: PropTypes.string.isRequired,
	additionalLabel: PropTypes.string.isRequired,
};

export default compose(
	withHandlers({
		removePhoneNumber: ownProps => (index) => {
			if(index > 0) {
				ownProps.fields.remove(index);
			}
		}
	})
)(PhoneFields)

