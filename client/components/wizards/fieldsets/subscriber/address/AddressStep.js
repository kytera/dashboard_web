import { Field, formValues, change } from 'redux-form';
import {compose, withHandlers} from 'recompose';

import CustomSelect from '../../../../common/custom-select/CustomSelect';
import Input from '../../../../common/input/Input';
import countries from '../../../../../data/countries';
import {connect} from "react-redux";

const required = value => value ? undefined : 'Required';

let setDefaultCountry = false;

const getCountriesAsOptions = () => {
	const participating = countries.filter(country => country.code)

	return participating.map(country => {return {value: country.short_name, label: country.short_name}})
};

const getStatesOptions = countryName => {
	let country = countries.find(cty => cty.short_name === countryName);
	return country.states.map(state => { return {label: state.name, value: state.name } });
}

const getTimezoneOptions = countryName => {
	let country = countries.find(cty => cty.short_name === countryName);
	return country.timezones.map(zone => { return {label: zone.display, value: zone.value} });
}

const AddressStep = ({ country, resetCountryInfo }) => {

	const countryInfo = countries.find(c => c.short_name === country)
	setDefaultCountry = false;

	return (<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				padding: 24px;
			}

			.input-container {
				margin-bottom: 20px;
			}

			.radio-label {
				color: var(--primary-color);
			}
		`}</style>
		<div className='input-container' style={{marginBottom: '10px'}}>
			<Field
				component={Input}
				name='address[street]'
				label='Street Address'
				placeholder='Address line 1'
				validate={[required]}
			/>
		</div>

		<div className='input-container'>
			<Field
				component={Input}
				name='address[streetOptional]'
				placeholder='Address line 2'
			/>
		</div>

		<div className='input-container'>
			<Field component={Input} name='address[city]' label='City' />
		</div>

		<div className='input-container'>
			<Field
				component={Input}
				name='address[county]'
				label='County'
			/>
		</div>

		<div className='input-container'>
			<Field
				component={CustomSelect}
				options={getCountriesAsOptions()}
				name='address[country]'
				label='Country'
				validate={[required]}
				onChange={() => resetCountryInfo()}
			/>
		</div>
		{countryInfo && countryInfo.states ?
			(<div className='input-container'>
				<Field
					component={CustomSelect}
					type='select'
					options={getStatesOptions(country)}
					name='address[state]'
					label='State/Province'
					validate={[required]}
				/>
			</div>) : <React.Fragment/>
		}

		<div className='input-container'>
			<Field
				component={Input}
				name='address[zipCode]'
				label='Zip Code'
				validate={[required]}
			/>
		</div>

		<div className='input-container'>
			<Field
				component={Input}
				name='address[additional]'
				type='textarea'
				label='Dispatch Directions'
				rows='6'
				placeholder='Additional instructions such as key location, lock-box code, intercom, gate code, etc...'
			/>
		</div>

		{ countryInfo && countryInfo.timezones && countryInfo.timezones.length > 1 ? (<div className='input-container'>
			<Field
				component={CustomSelect}
				options={getTimezoneOptions(country)}
				name='address[timezone]'
				label='Time zone'
				placeholder='Enter time zone'
				validate={[required]}
			/>
		</div>) : <React.Fragment/>
		}
	</div>
)
};

const mapActions = (dispatch, ownProps) => {
	if (!setDefaultCountry) {
		//setDefaultCountry = true;
		setTimeout(() => {
			//dispatch(change(ownProps.form, 'address.country', 'United States'));
		}, 100);
	}

	return {
		resetCountryInfo: () => {
			setDefaultCountry = true;
			dispatch(change(ownProps.form, 'address.state', ''));
			dispatch(change(ownProps.form, 'address.timezone', ''));
		}
	};
}

export default compose(
	formValues({ country: 'address.country'}),
	connect(null, mapActions),
)(AddressStep);
