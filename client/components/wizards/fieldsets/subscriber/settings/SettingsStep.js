import { Field } from 'redux-form';
import Input from '../../../../common/input/Input';
import { SegmentedControl } from 'segmented-control-react';

const SettingsStep = props => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				padding: 24px;
			}

			.input-container {
				margin-bottom: 0px;
			}
			
			.select-label {
				font-weight: bold;
				color: var(--primary-color);
				margin-bottom: 5px;
			}
		`}</style>

		<div className='input-container'>
			<div className='select-label'><span>Unwear Grace Time (Minutes)</span></div>
			<Field
				type='text'
				name='unwearGraceTime'
				component={SegmentedControl}
				selected={1}
				variant="blueish"
				segments={[
					{ name: "15" },
					{ name: "30" },
					{ name: "60" }
				]}
			/>
		</div>
	</div>
);

export default SettingsStep;
