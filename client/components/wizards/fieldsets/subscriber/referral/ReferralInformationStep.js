import { Field } from 'redux-form';
import Input from '../../../../common/input/Input';
import DateInput from '../../../../common/date-input/DateInput';

const required = value => value ? undefined : 'Required';

const ReferralInformationStep = props => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				padding: 24px;
			}

			.input-container {
				margin-bottom: 20px;
			}

		`}</style>

		<div className='input-container'>
			<Field
				component={Input}
				name='referralSource'
				type='textarea'
				label='Source'
				rows='6'
			/>
		</div>

		<div className='input-container'>
			<Field
				name='receivedDate'
				label='Received Date'
				component={DateInput}
				validate={[required]}
			/>
		</div>
	</div>
);

export default ReferralInformationStep;
