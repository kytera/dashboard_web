import { Field, change, formValueSelector } from 'redux-form';
import { FieldArray } from 'redux-form';
import { withState, compose, withProps } from 'recompose'
import { connect } from 'react-redux'
import { pick } from 'lodash'

import Input from "../../../../common/input/Input";
import Switch from '../../../../common/switch/Switch'
import CustomSelect from '../../../../common/custom-select/CustomSelect'
import withCompany from "../../../../../withCompany";
import withCurrentUser from '../../../../../withCurrentUser';

const formatMedical = (value) => {
  return value ? value.displayName : value;
}
const formatWellness = (value) => {
  return value ? value.displayName : value;
}

const normalizeField = (value, list, pickFields, setHas) => {
  if(!value) {
    return null;
  }

  let item = list.find(item => item.displayName === value)
  return pick(item, pickFields)
}

const PlanStep = (props) => {
  let {company} = props;
  var medicalAlarmServiceList = [];
  var wellnessServiceList = [];
  var medicalAlarm = null;
  var medicalAlarmVisible = false;

  if (!company) {
  	company = props.currentUser.company;
  }

  if(company) {
    if(company.medicalAlarmServices.length > 0) {
      medicalAlarmServiceList = company.medicalAlarmServices.map(service => { return {label: service.displayName, value: service.displayName}});
    }

    if(company.wellnessServices.length > 0) {
      wellnessServiceList = company.wellnessServices.map(service => { return {label: service.displayName, value: service.displayName}});
    }
  }

  if (props.initialValues || props.formValues) {
	  medicalAlarm = (props.initialValues || props.formValues).medicalAlarmService;
	  if (medicalAlarm) {
		  medicalAlarmVisible = true;
		  // medicalAlarmServiceOn
	  }
  }

  return (
    <div className='root'>
      { /* language=CSS */ }
      <style jsx>{`
        .root {
          padding: 24px;
        }
        .row {
          margin-bottom: 20px;
        }
        .row.input-container {
          display: grid;
          grid-template-columns: 1fr 1fr;
        }
        .right {
          justify-self:end;
        }
        .header {
          font-size: 13px;
          line-height: 18px;
          font-weight: 700;
          color:#3b5173;
          justify-self:start;
        }
        .switch-row {
        	display: flex;
        }
        .switch {
        	margin-left: auto;
        }
        .switch-label {
			font-weight: bold;
        }
        .separator {
			display: block;
			height: 1px;
		    border: solid 1px #eeeeee;
		    width: 100%;
		    margin-top: 20px;
			margin-bottom: 20px;
        }

      `}</style>
	<div className='row switch-row'>
		<span className="switch-label">Medical Alarm</span>
		<div className="switch">
			<Field name='medicalAlarmServiceOn'
				   component={Switch}
				   input={{
				   	   value: medicalAlarmVisible,
					   onChange: (e) => {
						   medicalAlarmVisible = !medicalAlarmVisible;
						   const target = e.target;
						   setTimeout(() => {
							   target.checked = medicalAlarmVisible;
						   }, 50);
						   document.getElementById('medicalAlarm').style.display = medicalAlarmVisible ? 'block' : 'none';
						   if (props.initialValues && props.initialValues.onMedicalAlarm) props.initialValues.onMedicalAlarm(medicalAlarmVisible);

						   if (!medicalAlarmVisible) {
						   		props.formValues.medicalAlarmService = null;
						   }
					    }
				   }}
			/>
		</div>
	</div>
      <div className='row' id='medicalAlarm' style={{ display: medicalAlarmVisible ? 'block' : 'none' }}>
		  <Field
			  autoFocus
			  component={CustomSelect}
			  name='medicalAlarmService'
			  options={medicalAlarmServiceList}
			  format={formatMedical}
			  normalize={value => {
			  	return normalizeField(value, company.medicalAlarmServices, ['displayName', 'siteGroupPrefix', 'siteGroupId'], props.setHasMedical);
			  }}
		  />
      </div>
  	  <div className='separator'></div>
	  <div className='row switch-row' style={{ display: wellnessServiceList.length === 0 ? 'none' : 'visible' }}>
		<span className="switch-label">Wellness</span>
 	    <div className="switch">
		<Switch
			name='wellnessServiceOn'
			className="switch"
			onChange={e => { document.getElementById('wellness').style.display = e.target.checked ? 'block' : 'none' }}
		/>
		</div>
	</div>
      <div className='row' id='wellness' style={{ display: 'none' }}>
        <Field
          component={CustomSelect}
          name='wellnessService'
          options={wellnessServiceList}
          format={formatWellness}
          normalize={value=> normalizeField(value, company.wellnessServices,['displayName','additionalInfo'], props.setHasWellness)}
        />
      </div>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  let companyId = null;
  const {values} = state.form[ownProps.form];

  console.log('mapStateToProps', values, ownProps.form, state.form[ownProps.form]);

  return { companyId: values.companyId || values.dealerCompanyId || null}
}

const enhance = compose(
  withCurrentUser,
  withCompany,
)

export default connect(mapStateToProps,{})(enhance(PlanStep));
