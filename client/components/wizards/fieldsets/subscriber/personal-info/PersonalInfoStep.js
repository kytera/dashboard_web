import { Field } from 'redux-form';

import Input from '../../../../common/input/Input';
import Radio from '../../../../common/radio/Radio';
import DateInput from '../../../../common/date-input/DateInput';
import CustomSelect from "../../../../common/custom-select/CustomSelect";
import React from "react";
import withCurrentUser from "../../../../../withCurrentUser";
import { ROLES } from "../../../../../constants";
import RolesOnly from "../../../../common/roles-only/RolesOnly";

const required = value => value ? undefined : 'Required';

const PersonalInfoStep = (props) => {
	const {currentUser} = props;
	let companies = [];
	if (currentUser.role === ROLES.SUPER_ADMIN) {
		companies = props.currentUser.companies.map(company => ({
			label: company.name,
			value: company.id,
		}));
	}
	return (
		<div className='root'>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					padding: 24px;
				}

				.input {
					margin-bottom: 20px;
				}

				.radio-label {
					color: var(--primary-color);
				}

				.label {
					font-weight: bold;
					color: var(--primary-color);
					margin-bottom: 5px;
				}
			`}</style>
			<RolesOnly role={[ROLES.SUPER_ADMIN]}>
				<div className='input'>
					<Field
						component={CustomSelect}
						options={companies}
						name='companyId'
						label='Company'
						required
						validate={[required]}
					/>
				</div>
			</RolesOnly>
			<div className='input'>
				<Field
					label='First Name'
					validate={[required]}
					name='firstName'
					component={Input}
				/>
			</div>
			<div className='input'>
				<Field
					label='Middle Name'
					name='middleName'
					component={Input}
				/>
			</div>
			<div className='input'>
				<Field
					label='Last Name'
					validate={[required]}
					name='lastName'
					component={Input}
				/>
			</div>
			<div className='input'>
				<Field
					name='birthDate'
					label='Date of Birth'
					component={DateInput}
					validate={[required]}
				/>
			</div>
			<div className='input'>
				<div className='label'>Gender</div>
				<Field name='gender' type='radio' value='MALE' component={Radio}>
					<span className='radio-label'>Male</span>
				</Field>
				<Field name='gender' type='radio' value='FEMALE' component={Radio}>
					<span className='radio-label'>Female</span>
				</Field>
			</div>

		</div>
	);
}

export default withCurrentUser(PersonalInfoStep);
