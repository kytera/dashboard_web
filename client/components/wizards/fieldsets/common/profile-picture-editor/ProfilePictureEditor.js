import AvatarEditor from 'react-avatar-editor';
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { debounce } from 'lodash';

if (process.browser) {
	import('blueimp-canvas-to-blob');
}

export class ProfilePictureEditor extends React.Component {
	static propTypes = {
		input: PropTypes.shape({
			value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
			onChange: PropTypes.func.isRequired,
		}).isRequired,
	}

	state = {
		image: null,
		imageFile: null,
	}

	componentWillMount() {
		this.updateImage(this.props);
		this.onImageChange = debounce(this.onImageChangeImpl, 100);
	}

	componentWillReceiveProps(props) {
		this.updateImage(props);
	}

	onImageChangeImpl = () => {
		this.editor.getImage().toBlob((blob) => {
			const file = new File([blob], 'avatar.jpg');
			this.setState({ imageFile: file }, () => {
				this.props.input.onChange(file);
			});
		}, 'image/jpeg');
	}

	setEditorRef = (editor) => {
		this.editor = editor;
	}

	updateImage(props) {
		if (props.input.value) {
			if (props.input.value instanceof File) {
				if (this.state.imageFile !== props.input.value) { // we don't want recursive update
					const reader = new FileReader();
					reader.onload = (e) => {
						if (e.target.result !== this.state.image) {
							this.setState({ image: e.target.result });
						}
					};
					reader.readAsDataURL(props.input.value);
				}
			} else {
				this.setState({ image: props.input.value });
			}
		}
	}


	render() {
		const { image } = this.state;

		return (
			<div className='root'>
				{ /* language=CSS */ }
				<style jsx>{`
					.root {
						position: relative;
						height: 360px;
						overflow: hidden;
					}

					.hint {
						position: absolute;
						top: 270px;
						text-align: center;
						left: 0;
						right: 0;
						color: rgba(255, 255, 255, .7);
					}
				`}</style>
				{ image && process.browser && <AvatarEditor
					ref={this.setEditorRef}
					image={image}
					width={160}
					height={160}
					border={100}
					borderRadius={100}
					color={[51, 71, 104, 0.7]}
					scale={1}
					rotate={0}
					onImageChange={this.onImageChange}
					onImageReady={this.onImageChange}
					{...this.props}
				/> }
				<div className='hint'>
					Drag to reposition photo<br />
					Drag and drop picture here
				</div>
			</div>
		);
	}
}

const ProfilePictureEditorStep = () => <Field component={ProfilePictureEditor} name='avatar' />;

export default ProfilePictureEditorStep;
