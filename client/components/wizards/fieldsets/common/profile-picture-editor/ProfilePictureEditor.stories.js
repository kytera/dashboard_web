import { storiesOf, addDecorator } from '@storybook/react';
import { reduxForm } from 'redux-form';

import Modal from '../../../../common/modal/Modal';
import Button from '../../../../common/button/Button';

import ProfilePictureEditor from './ProfilePictureEditor';

const ProfilePictureEditorWithForm = reduxForm({ form: 'FORM', initialValues: { avatar: '/static/images/default-avatar.png' } })(ProfilePictureEditor);


storiesOf('wizards/fieldsets/common', module)
	.add('ProfilePictureEditor', () => (
		<Modal
			isOpen
			title='Profile picture'
			button={[
				<Button key={1} theme='accent'>Done</Button>,
				<Button key={0} theme='link'>Cancel</Button>,
			]}
		>
			<ProfilePictureEditorWithForm />
		</Modal>
	));
