import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import Input from '../../../../common/input/Input';

import AvatarInput from '../../../../common/avatar-input/AvatarInput';

function getNameText(firstName, lastName) {
	if(firstName || lastName) {
		return `${firstName} ${lastName} `;
	} else {
		return '';
	}
}

const FinishProfileStep = ({ step, setStep, formValues: { email, firstName, lastName, password } }) => (
	<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				padding: 24px;
			}
		`}</style>

		<div className='input-container'>
			<Field
				component={Input}
				name='medicalHistory'
				type='textarea'
				rows='6'
			/>
		</div>
	</div>
);

FinishProfileStep.propTypes = {
	step: PropTypes.number.isRequired,
	setStep: PropTypes.func.isRequired,
	formValues: PropTypes.shape({
		email: PropTypes.string, firstName: PropTypes.string, lastName: PropTypes.string, password: PropTypes.string,
	}).isRequired,
};

export default FinishProfileStep;
