import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';
import { Field, formValueSelector, change, reduxForm } from 'redux-form';
import Modal from '../../common/modal/Modal'
import PropTypes from 'prop-types'
import UnwearTimeOptionField from './UnwearTimeOptionField';
import Button from '../../common/button/Button'
import { pick } from 'lodash'

import UPDATE_SUBSCRIBER_SETTINGS_MUTATION from '../../../graphql/subscriber/updateSubscriberSettings.mutation.graphql';


const FORM_NAME = 'FORM_SETTINGS';

class SettingsForm extends Component {
	state = {
		isLoading : false
	}

	render() {

		const { isOpen, onClose, handleSubmit, updateSubscriberSettings } = this.props;

	 	return (
			<Modal
				title="Settings"
				theme="default"
				onSubmit={handleSubmit(async (values) => {
					this.setState({isLoading: true});
					try {
						await updateSubscriberSettings(values);
						onClose();
					} finally {
						this.setState({isLoading: false});
					}
				})}
				onClose={onClose}
				isOpen={isOpen}
				button={<Button loading={this.state.isLoading} theme='accent' type='submit'>Save</Button>}
			>
				<div className='content'>
					{/* language=CSS */}
					<style jsx>{`
						.content {
							display: flex;
							flex-direction: column;
							padding: 20px;
							max-width: 400px;
						}
					`}</style>
					<Field name='UNWEAR_TIME' component={() => 
						<UnwearTimeOptionField label='Unwear grace time (minutes)' input={{value: this.props.UNWEAR_TIME, onChange:this.setUnwearTime}} />
					} />
				</div>
			</Modal>
		)
	}

	setUnwearTime = (value) => {
		this.props.change('UNWEAR_TIME', value)
	}	
}

SettingsForm.propTypes = {
	onSubmit: PropTypes.func, // if onSubmit passed, form will be rendered
	isOpen: PropTypes.bool.isRequired,
};

var settingsForm = compose(
	graphql(UPDATE_SUBSCRIBER_SETTINGS_MUTATION,
		{
			props: ({ownProps: {subscriberId, data}, mutate}) => ({
				updateSubscriberSettings: settings => {
					let settingsInput = pick(settings, ['UNWEAR_TIME']);
					return mutate({
						variables: {
							settings: settingsInput,
							id: subscriberId,
						}
					})
				},
			}),
		},
	)
)(SettingsForm)

settingsForm = reduxForm({
	form: FORM_NAME,
	enableReinitialize: true,
	keepDirtyOnReinitialize: true,
})(settingsForm)

const mapStateToProps = (state) => {
	return { UNWEAR_TIME: formValueSelector(FORM_NAME)(state,'UNWEAR_TIME') }
}

export default connect(mapStateToProps)(settingsForm);

