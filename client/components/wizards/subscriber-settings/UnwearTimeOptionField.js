import React from 'react';
import csx from 'classnames';
import PropTypes from 'prop-types';
import Title from '../../common/title/Title';

const TIME_VALUES = [15, 30, 60];

const UnwearTimeOptionField = ({ label, input: { value, onChange } }) => (
	<div className='subContainer'>
		{ /* language=SCSS */ }
		<style jsx>{`
			.subContainer {
				padding: 5px 15px;
				color: #415677;
				text-transform:uppercase;
			}
			.timeContainer {
				display: flex;
				margin-top: 10px;
				margin-bottom: 15px;
				flex-direction: row;
			}
			.timeValue {
				flex: 1;
				display: flex;
				border: 2px solid #e9e9e9;
				border-right-width: 1px;
				border-left-width: 1px;
				padding: 8px 5px;
				background-color: white;
				justify-content: center;
				outline: none;
				cursor: pointer;
			}

			.timeValue:first-child {
					border-top-left-radius: 4px;
					border-bottom-left-radius: 4px;
					border-left-width: 2px;
			}

			.timeValue:last-child {
					border-top-right-radius: 4px;
					border-bottom-right-radius: 4px;
					border-right-width: 2px;
			}

			.active {
				background-color: #415677;
				color: white;
			}
		`}</style>
		<Title size='thin'>{label}</Title> 
		<div className='timeContainer'>
			{TIME_VALUES.map(item => (
				<button
					key={item}
					type='button'
					className={csx('timeValue', { active: value === item })}
					onClick={() => onChange(item)}
				>
					{item}
				</button>
			))}
		</div>
	</div>
);

UnwearTimeOptionField.propTypes = {
	input: PropTypes.shape({ value: PropTypes.number, onChange: PropTypes.func.isRequired }).isRequired,
};
export default UnwearTimeOptionField;
