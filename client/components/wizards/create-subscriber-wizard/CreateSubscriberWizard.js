import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { reduxForm, getFormValues } from 'redux-form';
import { compose, withHandlers, withState } from 'recompose';
import { connect } from 'react-redux';
import { get, pick } from 'lodash';

import CREATE_SUBSCRIBER_MUTATION from '../../../graphql/subscriber/createSubscriber.mutation.graphql';
import SUBSCRIBERS_QUERY from '../../../graphql/subscriber/subscribers.query.graphql';

import withCurrentUser, { getUserCompanyIds } from '../../../withCurrentUser';
import withMutationStatus from '../../../withMutationStatus';
import Modal from '../../common/modal/Modal';
import Button from '../../common/button/Button';
import countries from '../../../data/countries';

import PersonalInfoStep from '../fieldsets/subscriber/personal-info/PersonalInfoStep';
import AddressStep from '../fieldsets/subscriber/address/AddressStep';
import PhoneStep from '../fieldsets/subscriber/phone/PhoneStep';
import PlanStep from '../fieldsets/subscriber/plan/PlanStep';
import ReferralInformationStep from '../fieldsets/subscriber/referral/ReferralInformationStep';
import EmergencyServicesStep from '../fieldsets/subscriber/emergency-services/EmergencyServicesStep';
import SettingsStep from '../fieldsets/subscriber/settings/SettingsStep';
import SubscribersProfileStep from '../fieldsets/common/finish-profile/FinishProfileStep';
import ProfilePictureEditor from '../fieldsets/common/profile-picture-editor/ProfilePictureEditor';
import ROLES from "../../../constants/roles";


const FORM_NAME = 'CREATE_SUBSCRIBER_WIZARD';

const steps = [
	{
		Component: PersonalInfoStep,
		title: 'Personal Info',
		nextText: 'Next'
	},
	{
		Component: AddressStep,
		title: 'Address',
		nextText: 'Next',
		prevText: 'Back'
	},
	{
		Component: connect(state => ({
			formValues: getFormValues(FORM_NAME)(state),
		}))(PlanStep),
		title: 'Plan',
		nextText: 'Next',
		prevText: 'Back'
	},
	{
		Component: connect(state => ({
			formValues: getFormValues(FORM_NAME)(state),
		}))(PhoneStep),
		title: 'Contact Information',
		nextText: 'Next',
		prevText: 'Back'
	},
	{
		Component: connect(state => ({
			formValues: getFormValues(FORM_NAME)(state),
		}))(SettingsStep),
		title: 'Settings',
		nextText: 'Next',
		prevText: 'Back'
	},
	{
		Component: connect(state => ({
			formValues: getFormValues(FORM_NAME)(state),
		}))(SubscribersProfileStep),
		title: 'Medical History',
		nextText: 'Save',
		prevText: 'Back'
	}
];

const validateSubscriber = values => {
	const errors = {}
	if(values.medicalAlarmService !== undefined || values.wellnessService !== undefined ) {
		if(!values.medicalAlarmService && !values.wellnessService) {
			//errors.medicalAlarmService = 'Required';
			//errors.wellnessService = 'Required';
		}
	}
  return errors;
}

export const CreateSubscriberWizard =
	reduxForm({
		form: FORM_NAME,
		validate: validateSubscriber,
		initialValues: {
			gender: 'MALE',
			phones: [''],
			agencies: [],
			'address.country': 'United States',
			'country': 'United States'
		},
	})(({onClose, handleSubmit, mutationStatus, setStep, step, setShowError, showError, reset, form}) => {
			const {error, loading} = mutationStatus;
			const currentStep = steps[step];

			const backButton = currentStep.prevText ?
				(<Button
					theme='link'
					type='button'
					loading={loading}
					onClick={() => setStep(step-1)}
				>
					{currentStep.prevText}
				</Button>) : <React.Fragment/>

			const actions = [
				<React.Fragment key={1}>
					<Button
						theme='accent'
						type='submit'
						loading={loading}
					>
						{currentStep.nextText}
					</Button>
					{backButton}
				</React.Fragment>
			];

			const onCloseHandler = () => {
				console.log('onCloseHandler');
				reset();
				if (showError && error) {
					setShowError(false);
				} else {
					onClose()
				}
			}

			return (
				<div>
					<div className='root'>
						<Modal
							isOpen
							title={currentStep.title}
							theme={!error ? 'default' : 'error'}
							button={actions}
							onClose={onCloseHandler}
							onSubmit={handleSubmit}
						>
							<div className='content'>
								{showError && !!error && <div className='error'>{error}</div>}
								{<currentStep.Component step={step} setStep={setStep} form={form}/>}
							</div>
						</Modal>
					</div>
					{/* language=CSS */}
					<style jsx>{`
						.skip-link {
							color: var(--accent-color);
						}

						.skip-link:hover {
							text-decoration: none;
						}

						.content {
							min-height: 360px;
						}

						.error {
							padding: 24px;
							color: #fff;
							font-size: 13px;
							background-color: var(--error-color);
						}
					`}</style>
				</div>
			);
		},
	);

CreateSubscriberWizard.propTypes = {
	onClose: PropTypes.func.isRequired,
	handleSubmit: PropTypes.func,
	setStep: PropTypes.func.isRequired,
	step: PropTypes.number.isRequired,
	mutationStatus: PropTypes.shape({loading: PropTypes.bool.isRequired, error: PropTypes.string}).isRequired,
};


export default compose(
	withCurrentUser,
	graphql(CREATE_SUBSCRIBER_MUTATION, {
		props: ({ownProps: {currentUser}, mutate}) => ({
			createSubscriber: ({police, fire, medical, ...subscriberData}) => {
				var agencies = null, emergency = [];
				if(fire) {
					emergency.push({ agency_type: 'fire', number: fire});
				}
				if(police) {
					emergency.push({ agency_type: 'police', number: police});
				}
				if(medical) {
					emergency.push({ agency_type: 'medical', number: medical});
				}
				if(emergency.length) {
					agencies = { agencies: emergency };
				}
				subscriberData = {...subscriberData, ...agencies}

				let company = currentUser.company;
				if (currentUser.role === ROLES.SUPER_ADMIN && currentUser.companies && subscriberData.companyId) {
					company = currentUser.companies.find(c => c.id === subscriberData.companyId);
					delete subscriberData.companyId;
				}

				return mutate({
					variables: {
						subscriber: subscriberData,
						[company.type === 'DEALER' ? 'dealerCompanyId' : 'callCenterCompanyId']: company.id,
					},
					update: (store, {data: {createSubscriber}}) => {
						const data = store.readQuery({
							query: SUBSCRIBERS_QUERY,
							variables: {companiesIds: getUserCompanyIds(currentUser)},
						});
						data.subscribers.push(createSubscriber);
						store.writeQuery({
							query: SUBSCRIBERS_QUERY,
							variables: {companiesIds: getUserCompanyIds(currentUser)},
							data,
						});
					},
				})
			},
		}),
	}),
	withMutationStatus('createSubscriber', 'mutationStatus', true),
	withState('step', 'setStep', 0),
	withState('showError', 'setShowError', false),
	withHandlers({
		onSubmit: ({step, setStep, createSubscriber, onClose, setShowError}) => async (data) => {
			const shouldSubmit = step >= steps.length - 1;
			if (shouldSubmit) {
				try {
					console.log('createSubscriber', data);
					delete data.country;
					delete data['address.country'];

					// Add country code
					const country = data.address.country;
					const countryData = countries.filter(c => c.short_name === country);
					let countryCode = null;
					if (countryData && countryData.length > 0) {
						countryCode = '+' + countryData[0].callingCodes[0];
					}

					if (countryCode) {
						if (data.phones && data.phones.length > 0) {
							if (!data.phones[0].startsWith(countryCode)) {
								data.phones = [countryCode + ' ' + data.phones[0]];
							}
						}
						if (data.homePhone) {
							if (!data.homePhone.startsWith(countryCode)) {
								data.homePhone = countryCode + ' ' + data.homePhone;
							}
						}
					}
					const result = await createSubscriber(data);
					const subscriberId = get(result, 'data.createSubscriber.id');
					onClose(subscriberId);
				} catch (e) {
					setShowError(true);
				}
			} else {
				setStep(step + 1);
			}
		},
	}),
)(CreateSubscriberWizard);
