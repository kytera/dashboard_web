import React from 'react';
import { compose, withHandlers } from 'recompose';
import { Field } from 'redux-form';
import countries from '../../../data/countries';

import Input from '../../common/input/Input';

const normalizePhones = value => {
	if (!value) {
		return []
	}

	const onlyNums = value.replace(/[^\d]/g, '')
	if (onlyNums.length <= 3) {
		return [onlyNums]
	}
	if (onlyNums.length <= 7) {
		return [`${onlyNums.slice(0, 3)}-${onlyNums.slice(3)}`]
	}
	if (onlyNums.length == 9) {
		return [`(${onlyNums.slice(0, 2)}) ${onlyNums.slice(2, 5)}-${onlyNums.slice(
			5,
			9
		)}`]
	}
	return [`(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(
		6,
		10
	)}`]
};

const removeCountryCode = (countryCode, phone) => {
	if (!countryCode) return phone;
	const isArray = (typeof phone) !== 'string';

	if (isArray) {
		phone = phone[0];
	}

	if (phone) {
		const prefix = countryCode;
		if (phone.startsWith(prefix)) {
			phone = phone.substr(prefix.length).trimLeft();
		}
	}

	return isArray ? [phone] : phone;

};

const removeUsername = (email) => {
	return email.replace(/(.+) \(username .+\)/, '$1');
};


const formatPhones = phones => Array.isArray(phones) ? phones.length > 0 ? phones[0] : '' : phones;

const CaregiveInfo = props => {
	const country = props.subscriber.address.country;
	const countryData = countries.filter(c => c.short_name === country);
	let countryCode = '';
	if (countryData && countryData.length > 0) {
		countryCode = '+' + countryData[0].callingCodes[0];
	}

	return (<div className='root'>
		{ /* language=CSS */ }
		<style jsx>{`
				.row {
					padding: 24px;
					padding-top:0;
				}

				.row:first-child {
					padding-top:12px;
				}

				.error {
					color: #fff;
					padding: 24px;
					background-color: var(--error-color);
				}
			`}</style>
		<div className='row'>
			<Field name='firstName' label='First Name' autoFocus component={Input} />
		</div>
		<div className='row'>
			<Field name='middleName' label='Middle Name' component={Input} />
		</div>
		<div className='row'>
			<Field name='lastName' label='Last Name' component={Input} />
		</div>
		<div className='row'>
			<Field name='email' label='Email' type='email' component={Input} postDisplay={(val) => removeUsername(val) } />
		</div>
		<div className='row'>
			<Field
				name='phones'
				label='Mobile'
				placeholder='(111) 222-3333'
				component={Input}
				prefixLabel={countryCode}
			    normalize={normalizePhones}
				postDisplay={(val) => removeCountryCode(countryCode, val)}
				format={formatPhones} />
		</div>
	</div>)
}

export default CaregiveInfo
