import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import CaregiverWizard from './CaregiverWizard';

storiesOf('wizards', module)
		.add('CaregiverWizard', () => <CaregiverWizard
			isOpen
			onClose={action('close') }
			onSubmit={action('submit')}
		    mutationStatus={{ loading: false, error: null }}
		/>);
