import { Field } from 'redux-form';
import { capitalize } from 'lodash'

import Switch from '../../common/switch/Switch';

const CaregiverNotifications = props => {
	const {settings}  = props.initialValues;

	return (<div className='root'>
			{ /* language=CSS */ }
			<style jsx>{`
      .row {
        padding: 12px 24px;
        height:74px;
        border-bottom: solid 2px #eeeeee;
        display: grid;
        grid-template-columns: 1fr 1fr;
        align-items: center;
        margin:0 20px;
      }
      
      .row:last-child {
        border:0;
      }
            
      .row > .right {
        justify-self:end;
      }

      .header {
        font-size: 13px;
        line-height: 18px;
        font-weight: 700;
        color:#3b5173;
        justify-self:start;
      }

    `}</style>
			{ settings.map( (setting, index) => {
				const label = capitalize(setting.settingType.toLowerCase().replace(/_/g,' '));
				return (
					<div className='row' key={setting.settingType}>
						<span className="header">{label}</span>
						<span className="right">
          <Field name={`settings[${index}].value`}
				 component={Switch}
		  />
        </span>
					</div>
				)}) }
		</div>
	)};

export default CaregiverNotifications;
