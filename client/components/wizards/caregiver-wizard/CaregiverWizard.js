import PropTypes from 'prop-types';
import { reduxForm, Field, reset, change } from 'redux-form';
import { compose, withHandlers, withState } from 'recompose';
import countries from '../../../data/countries';
import axios from 'axios';

import Modal from '../../common/modal/Modal';
import Button from '../../common/button/Button';
import Input from '../../common/input/Input';
import Checkbox from '../../common/checkbox/Checkbox';
import Select, { Option } from '../../common/select/Select'
import PhoneStep from '../fieldsets/subscriber/phone/PhoneStep';
import { isString } from 'util';
import relations from '../../../../client/constants/relations';
import CaregiverInfo from './CaregiverInfo';
import CaregiverNotifications from './CaregiverNotifications';

const doValidations = values => {
	const errors = {}
	if (!values.firstName) {
		errors.firstName = 'Required'
	}
	if (!values.lastName) {
		errors.lastName = 'Required'
	}
	if (!values.email) {
		errors.email = 'Required'
	} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
		errors.email = 'Invalid email address'
	}
	if (!values.phones || !values.phones.length) {
		errors.phones = { _error: 'Phone number is required'}
	}

	return errors;
}


const FORM_NAME = 'CAREGIVER_WIZARD';

const doClose = (dispatch,onClose, setStep) => {
	dispatch(reset(FORM_NAME))
	setStep(0)
	onClose()
}

export const availableSteps = [
	{
		component: CaregiverInfo,
		title: 'Caregiver Information',
	},
	{
		component: CaregiverNotifications,
		title: 'Caregiver Notifications',
	},
];


const CaregiverWizard = ({ isOpen, onClose, handleSubmit, initialValues, steps, step, setStep, ...props} ) => {
	const currentStep = steps[step];

	const nextText = step < steps.length - 1 ? 'Next' : 'Done';
	const backButton = step > 0 ? (<Button theme='link' type='button' onClick={() => setStep(step-1)}>Back</Button>) : <React.Fragment/>

	const actions = [
		<React.Fragment key={1}>
			<Button theme='accent' type='submit'>
				{nextText}
			</Button>
			{backButton}
		</React.Fragment>
	];

	return (
	<Modal
		isOpen={isOpen}
		title={currentStep.title}
		onClose={e => doClose(props.dispatch, onClose, setStep)}
		onSubmit={handleSubmit(props.onSubmit)}
		theme='default'
		button={actions} >
		{ <currentStep.component initialValues={initialValues} isOpen={isOpen} onClose={()=>{}} subscriber={props.subscriber} /> }
	</Modal>
	);
}

CaregiverWizard.propTypes = {
	onClose: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired,
};


CaregiverWizard.defaultProps = {
	steps: availableSteps
}


export default compose(
	reduxForm({
		form: FORM_NAME,
		enableReinitialize: true,
		validate: doValidations,
		touchOnBlur: false,
		touchOnChange: false,
	}),
	withState('step', 'setStep', 0),
	withHandlers({
		onSubmit: ({step, steps, setStep, onSubmit, subscriber}) => data => {
			const submit = step >= steps.length - 1;
			if (submit) {
				// Add country code
				const country = subscriber.address.country;
				const countryData = countries.filter(c => c.short_name === country);
				let countryCode = null;
				if (countryData && countryData.length > 0) {
					countryCode = '+' + countryData[0].callingCodes[0];
				}

				if (countryCode) {
					if (data.phones && data.phones.length > 0) {
						if (!data.phones[0].startsWith(countryCode)) {
							data.phones[0] = countryCode + ' ' + data.phones[0];
						}
					}
				}

				onSubmit(data);
			} else {
				setStep(step + 1);
			}
		}
	})
	)(CaregiverWizard);
