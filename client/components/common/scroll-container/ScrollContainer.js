import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

class ScrollContainer extends Component {
	static propTypes = {
		autoScroll: PropTypes.bool,
		showContentIndicator: PropTypes.bool,
		children: PropTypes.node.isRequired,
	};

	static defaultProps = {
		autoScroll: false,
		showContentIndicator: true,
	};

	state = {
		shadowTop: false,
		shadowBottom: false,
	};

	componentDidUpdate() {
		// TODO: enable check on first mount
		/*		if (this.props.showContentIndicator) {
					this.handleScroll();
				} */

		// FIXME: fix recursion call on scroll
		if (this.props.autoScroll) {
			this.scrollToBottom();
		}
	}

	setRef = (ref) => {
		this.content = ref;
	};

	scrollToBottom() {
		if (this.content.lastChild) {
			this.content.lastChild.scrollIntoView(false);
		}
	}

	handleScroll = () => {
		let shadowTop = false;
		let shadowBottom = false;
		if (this.content.scrollTop > 0) {
			shadowTop = true;
		}
		if ((this.content.scrollTop + this.content.clientHeight) < this.content.scrollHeight) {
			shadowBottom = true;
		}
		if (shadowTop !== this.state.shadowTop || shadowBottom !== this.state.shadowBottom) {
			this.setState({ shadowBottom, shadowTop });
		}
	};

	render() {
		return (
			<div className='root'>
				{/* language=SCSS */}
				<style jsx>{`
					.root {
						flex: 1;
						position: relative;
						z-index: 1;
						display: flex;
						flex-direction: column;
						overflow: hidden;
					}

					.shadow {
						position: absolute;
						right: 0;
						left: 0;
						box-shadow: rgba(0, 0, 0, .9) 0 0 12px;
						transition: all .3s ease;
						visibility: hidden;
						height: 1px;
						opacity: 0;
					}

					.top {
						top: -1px;
					}

					.bottom {
						bottom: -1px;
					}

					.active {
						visibility: visible;
						opacity: 1;
					}

					.content {
						overflow-y: auto;
						flex: 1;
					}

					.content ::-webkit-scrollbar {
						width: 16px;
					}

				`}</style>
				<div className={cn('shadow', 'top', { active: this.state.shadowTop })} />
				<div className={cn('shadow', 'bottom', { active: this.state.shadowBottom })} />
				<div
					className='content'
					ref={this.setRef}
					onScroll={this.props.showContentIndicator ? this.handleScroll : null}
				>
					{this.props.children}
				</div>
			</div>
		);
	}
}

export default ScrollContainer;
