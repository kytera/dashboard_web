import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Radio, { RadioGroup } from './Radio';

storiesOf('common/Radio', module)
	.add('default', () => (
		<RadioGroup onChange={action('change')} name='test'>
			<Radio>One</Radio>
			<Radio>Two</Radio>
			<Radio disabled>Three</Radio>
		</RadioGroup>
	));
