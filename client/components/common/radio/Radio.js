import csx from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';
import { omit } from 'lodash';

const Radio = ({ children, disabled, input, ...rest }) => {
	rest = omit(rest, 'meta');
	return (
		/* eslint-disable jsx-a11y/label-has-for */
		<label className={csx('root', { disabled })}>
			{ /* language=CSS */ }
			<style jsx>{`
			.root {
				cursor: pointer;
				display: inline-flex;
				align-items: center;
				margin: 8px;
			}

			.radio {
				border-radius: 99px;
				display: inline-flex;
				border: 2px var(--primary-color) solid;
				padding: 2px;
				position: relative;
				line-height: 1;
				align-items: center;
				margin-right: 8px;
			}

			.disabled {
				pointer-events: none;
				cursor: not-allowed;
			}

			input {
				position: absolute;
				visibility: hidden;
			}

			input:checked + .circle {
				background-color: var(--primary-color);
				opacity: 1;
				transform: scale(1);
			}

			.circle {
				border-radius: 99px;
				height: 12px;
				width: 12px;
				background-color: rgba(0, 0, 0, .3);
				display: inline-block;
				position: relative;
				transition: all .3s var(--timing-standard);

				opacity: 0;
				transform: scale(0.5);
			}

			.disabled {
				color: var(--disabled-color);
			}

			.disabled .radio {
				border-color: var(--disabled-color);
			}

			.disabled .circle {
				background-color: var(--disabled-color);
			}
		`}</style>
			<span className='radio'>
			<input type='radio' disabled={disabled} {...rest} {...input} />
				<span className='circle' />
		</span>
			{children}
		</label>
	);
};

Radio.propTypes = {
	disabled: PropTypes.bool,
	children: PropTypes.node,
	input: PropTypes.shape({ value: PropTypes.string, onChange: PropTypes.func }), // handle redux-form
};

Radio.defaultProps = {
	disabled: false,
	children: null,
	input: {},
};


const RadioGroup = ({ children, label, required, ...props }) => (
	<fieldset>
		{ /* language=CSS */ }
		<style jsx>{`
			fieldset {
				padding: 0;
				border: 0;
			}

			.radio-label {
				padding-bottom: 5px;
			}

			.radio-label > span {
				font-weight: bold;
				color: var(--primary-color);
			}

			.required {
				font-weight: normal;
			}
		`}</style>
		{ !!label && <div className='radio-label'>
			<span>
				{label}
				{ required && <span className='required'>&nbsp;(required)</span> }
			</span>
		</div> }
		{React.Children.map(children, child => React.cloneElement(child, { required, ...props }))}
	</fieldset>
);

RadioGroup.propTypes = {
	children: PropTypes.node.isRequired,
	label: PropTypes.string,
	required: PropTypes.bool,
};

RadioGroup.defaultProps = {
	label: null,
	required: false,
};

export { Radio as default, RadioGroup };
