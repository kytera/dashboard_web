import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Input from './Input';

storiesOf('common/Input', module)
	.add('default', () => (
		<Input onChange={action('change')} />
	))
	.add('with label', () => (
		<Input onChange={action('change')} label='Any value' />
	))
	.add('required with label', () => (
		<Input onChange={action('change')} label='Any value' required />
	))
	.add('textarea', () => (
		<Input onChange={action('change')} label='Any value' type='textarea' />
	))
	.add('search', () => (
		<Input type='search' icon='search' placeholder='Search...' onChange={action('change')} />
	));
