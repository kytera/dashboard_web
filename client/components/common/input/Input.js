import csx from 'classnames';
import PropTypes from 'prop-types';
import { omit } from 'lodash';

import Icon from '../icon/Icon';

const Input = ({ type = 'text', grouped, prefixLabel, label, required, icon, input, meta, showError, ...rest }) => {
	const isRequiredTouched = meta ? meta.touched && required : required;

	if (rest.postDisplay) {
		input.value = rest.postDisplay(input.value);
		delete rest.postDisplay;
	}

	return (
		<div className={csx('root', { grouped, 'has-error': meta && meta.touched && meta.error })}>
      {!!label && <div className='input-label'>
			<span>
				{label}
        {required && <span className='required'>&nbsp;(required)</span>}
			</span>
			</div>}
			{prefixLabel &&
			<span className='prefix-label'>
			{prefixLabel}
			</span>
			}
			<div className={csx('container', type, prefixLabel ? 'prefix' : null)}>
        {
          type === 'textarea'
            ? <textarea required={isRequiredTouched} {...rest} {...input} />
            : <input type={type} required={isRequiredTouched} {...rest} autoComplete={'��������'} {...input} />
        }
				<div className='border' />
        {icon && (<span className='icon'>
				{ (typeof icon === 'string') ? <Icon type={icon} size='1em' /> : icon }
			</span>)}
			</div>
			{showError && meta && meta.error && meta.touched && (<div className="error">{meta.error}</div>)}
			{/* language=CSS */}
			<style jsx>{`
			.container {
				background: #fff;
				align-items: center;
				position: relative;
				display: block;
				border-radius: 3px;
				padding: 2px;
			}
			
			.prefix-label {
				margin-right: 5px;
			}
			
			.container.prefix {
				display: inline-block;
			}

			.grouped:first-child .border, .grouped:first-child {
				border-top-right-radius: 0;
				border-bottom-right-radius: 0;
				border-right-width: 1px;
			}

			.grouped:last-child .border, .grouped:last-child {
				border-left-width: 1px;
				border-top-left-radius: 0;
				border-bottom-left-radius: 0;
			}

			input, textarea {
				border: none;
				box-shadow: none;
				background: transparent none;
				appearance: none;
				padding: 8px 10px;
				z-index: 1;
				position: relative;
				width: 100%;
				color: var(--primary-color);
			}

			textarea {
				resize: none;
				padding: 12px 10px;
			}

			input:focus, textarea:focus {
				outline: none;
			}

			input[type='search'] {
				padding-right: 30px;
			}

			.has-error input + .border, .has-error textarea + .border {
				border-color: var(--error-color);
			}

			input:invalid + .border, textarea:invalid + .border {
				border-color: var(--error-color);
			}

			input:focus + .border, textarea:focus + .border {
				border-color: var(--accent-color);
			}

			.icon {
				position: absolute;
				top: 0;
				bottom: 0;
				right: 8px;
				display: flex;
				align-items: center;
				z-index: 0;
			}

			.border {
				position: absolute;
				top: 0;
				bottom: 0;
				left: 0;
				right: 0;
				border: 2px #E9E9E9 solid;
				border-radius: 3px;
			}

			.input-label {
				padding-bottom: 5px;
			}

			.input-label > span {
				color: var(--primary-color);
				font-weight: bold;
			}

			.required {
				font-weight: normal;
			}

			.error {
				color: var(--error-color);
				margin-top:3px;
				font-size: 12px;
			}

			[readonly] {
				background-color: var(--disabled-color);
			}
		`}</style>

		</div>
  )
};

Input.propTypes = {
	type: PropTypes.oneOf(['text', 'textarea', 'search', 'email', 'password', 'date', 'tel']),
	grouped: PropTypes.bool,
	label: PropTypes.node,
	prefixLabel: PropTypes.string,
	required: PropTypes.bool,
	icon: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
	input: PropTypes.shape({ value: PropTypes.string, onChange: PropTypes.func }), // handle redux-form
};

Input.defaultProps = {
	type: 'text',
	grouped: false,
	label: null,
	required: false,
	prefixLabel: null,
	icon: null,
	input: {},
};

export default Input;
