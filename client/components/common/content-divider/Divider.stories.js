import { storiesOf } from '@storybook/react';

import Divider from './Divider';

storiesOf('common/Divider', module)
	.add('default', () => (<div style={{backgroundColor: '#fff', padding: '10px'}}>
		<Divider title='Divider' />
	</div>));
