import PropTypes from 'prop-types';

const Divider = ({ title }) => (
	<div className='divider'>
		{ /* language=CSS */ }
		<style jsx>{`
			.title {
				text-transform: uppercase;
				color: var(--primary-color);
				font-weight: bold;
				padding: 4px 8px;
				background-color: var(--disabled-color);
				border-radius: 10px;
				white-space: nowrap;
				font-size: 11px;
			}

			.line {
				height: 2px;
				width: 100%;
				background-color: var(--disabled-color);
			}

			.divider {
				display: flex;
				flex-flow: row nowrap;
				align-items: center;
				padding-left: 12px;
				padding-right: 10px;
			}
		`}</style>
		<span className='title'>
			{title}
		</span>
		<div className='line' />
	</div>
);

Divider.propTypes = {
	title: PropTypes.string.isRequired,
};

export default Divider;
