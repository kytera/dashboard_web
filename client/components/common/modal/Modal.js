import PropTypes from 'prop-types';
import Portal from 'react-portal';
import csx from 'classnames';

import Icon from '../icon/Icon';
import ScrollContainer from '../scroll-container/ScrollContainer';

const Modal = ({ title, theme, onSubmit, button, isOpen, onClose, children, ...rest }) => {
	const ModalElement = onSubmit ? 'form' : 'div';

	return (
		<Portal
			onClose={onClose}
			isOpened={/** @see https://github.com/tajo/react-portal#isopen--bool */ isOpen}
			{...rest}
		>
			<div className='root'>
				<div className='backdrop' />
				<ModalElement onSubmit={onSubmit}>
					<div className={csx('modal', theme)} role='dialog'>
						<div className='title'>
							<span>{title}</span>
							{ !!onClose && <a tabIndex='0' role='button' onClick={onClose}><Icon type='close' height="12px" /></a> }
						</div>
						<div className='body'>
							<ScrollContainer>{children}</ScrollContainer>
						</div>
						<div className='footer'>{button}</div>
					</div>
				</ModalElement>
				{ /* language=CSS */ }
				<style jsx>{`
					.root {
						display: flex;
						align-items: center;
						justify-content: center;
						position: fixed;
						z-index: 10;
						top: 0;
						left: 0;
						right: 0;
						bottom: 0;
						padding: 128px;
						/*transition: all var(--timing-acceleration) .2s;
						opacity: 0;*/
					}

					/*					.root.isOpen {
											transition: all var(--timing-deceleration) .2s;
											opacity: 1;
										}*/

					.backdrop {
						background-color: rgba(255, 255, 255, .8);
						position: absolute;
						top: 0;
						right: 0;
						bottom: 0;
						left: 0;
						z-index: 11;
					}

					.modal {
						position: relative;
						z-index: 12;
						width: 360px;
						background-color: #ffffff;
						box-shadow: rgba(0, 0, 0, 0.3) 0 2px 10px;
						border-radius: 4px;
						overflow: hidden;
						/*
														transform: translateY(-100px);
														transition: all .2s var(--timing-acceleration);*/
					}

					/*					.isOpen .modal {
											transition: all .2s var(--timing-deceleration);
											transform: translateY(0);
										}*/

					.title {
						font-weight: 500;
						font-size: 18px;
						padding: 12px 16px;
						color: #fff;
						display: flex;
						justify-content: space-between;
						align-items: center;
						text-transform: uppercase;
					}

					.footer {
						display: flex;
						flex-direction: row-reverse;
						justify-content: space-between;
						align-items: center;
						padding: 16px;
						box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.3);
					}

					.default .title {
						background-color: var(--primary-color);
					}

					.error .title {
						background-color: var(--error-color);
					}

					.body {
						max-height: calc(100vh - 256px);
						min-height: 250px;
						display: flex;
						flex-direction: column;
					}
				`}</style>
			</div>
		</Portal>
	);
};

Modal.propTypes = {
	title: PropTypes.node.isRequired,
	theme: PropTypes.oneOf(['default', 'error']),
	onSubmit: PropTypes.func, // if onSubmit passed, form will be rendered
	onClose: PropTypes.func,
	isOpen: PropTypes.bool.isRequired,
	button: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]).isRequired,
	children: PropTypes.node.isRequired,
};

Modal.defaultProps = {
	theme: 'default',
	onSubmit: null,
	onClose: undefined,
};

export default Modal;
