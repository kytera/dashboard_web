import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withState } from 'recompose';

import Button from '../button/Button';
import Modal from './Modal';


const ToggleExample = withState('isOpen', 'setOpen', false)(
	({ isOpen, setOpen }) => (
		<div>
			<Button theme='accent' onClick={() => setOpen(true)}>Open</Button>
			<Modal
				isOpen={isOpen}
				title='Test modal'
				onClose={() => setOpen(false)}
				button={[
					<Button key={1} theme='accent'>Confirm</Button>,
					<Button key={0} theme='link'onClick={() => setOpen(false)}>Cancel</Button>,
				]}
			>
				<div style={{ height: 200, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>Modal content</div>
			</Modal>
		</div>
	),
);

storiesOf('common/Modal', module)
	.add('default', () => (<Modal
		isOpen
		title='Test modal'
		onClose={action('close')}
		button={[
			<Button key={1} theme='accent'>Confirm</Button>,
			<Button key={0} theme='link'>Cancel</Button>,
		]}
	>
		<div style={{ height: 200, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>Modal content</div>
	</Modal>))
	.add('error', () => (<Modal
		isOpen
		title='Test modal'
		theme='error'
		button={<Button theme='error'>Confirm</Button>}
	>
		<div style={{ height: 200, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>Modal content</div>
	</Modal>))
	.add('toggle', () => <ToggleExample />);
