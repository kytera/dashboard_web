import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import CustomSelect from './CustomSelect';

const options = [
	{ label: 'United States', value: '+1' },
	{ label: 'Canada', value: '+3' },
	{ label: 'Russian Federation', value: '+7' },
	{ label: 'Australia', value: '+28' },
];

storiesOf('common/CustomSelect', module)
	.add('default', () => <CustomSelect options={options} onChange={action('onchange')} />)
	.add('with label and required', () => (
		<CustomSelect
			options={options}
			label="Label"
			required
			onChange={action('onchange')}
		/>))
	.add('without arrow', () => (
		<CustomSelect
			options={options}
			label="Label"
			required
			withoutArrow
			onChange={action('onchange')}
		/>))
	.add('full width', () => (
		<div style={{width: '400px', position: 'relative', background: '#fff'}}>
			<div style={{width: '25%'}}>
				<CustomSelect
					options={options}
					label="Label"
					required
					fullWidth
					onChange={action('onchange')}
				/>
			</div>
		</div>));
