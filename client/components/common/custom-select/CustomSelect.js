import csx from 'classnames';
import Select from 'react-select';
import PropTypes from 'prop-types';
import { mapProps } from 'recompose';

const CustomSelect = ({
	options,
	label,
	required,
	withoutArrow,
	fullWidth,
	onChange,
	onBlur,
	value,
	meta,
	...rest
}) => {
	return (
	<div className={csx('root', { 'has-error': meta && meta.touched && meta.error})}>
		{ !!label && <div className='select-label'><span>
			{label}
			{ required && <span className='required'>&nbsp;(required)</span> }
		</span></div> }
		<Select
			options={options}
			className={csx('custom-select', { withoutArrow, fullWidth })}
			onChange={e => onChange(e ? e.value : '')}
			clearable={true}
			valueRenderer={option => option.label}
			onBlur={() => onBlur(value)}
			value={value}
			{...rest}
		/>
		{ /* language=CSS */ }
		<style jsx global>{`
			.Select-input {
				height: 30px;
			}

			.Select-control {
				border: 2px #E9E9E9 solid;
				border-radius: 3px;
				height: 34px;
			}

			.Select-placeholder {
				line-height: 30px;
			}

			.custom-select.withoutArrow .Select-arrow-zone {
				display: none;
			}

			.has-error .Select-control {
				border-radius: 3px;
				border-color: var(--error-color) !important;
			}

			.is-open > .Select-control {
				border-radius: 3px;
				border-color: #00B7FF;
			}

			.custom-select.fullWidth {
				position: static;
			}

			.Select-menu-outer {
				top: 125%;
				border: 2px #00B7FF solid;
				border-radius: 3px;
				z-index: 999;
			}
			
			.Select-option, .Select-option.is-focused {
				font-weight: bold;
				color: #334768;
				background-color: transparent;
			}

			.Select-option.is-focused {
				background-color: #EAEAEA;
			}

			.Select-value-label {
				color: #00B7FF;
				font-weight: bold;
			}

			.custom-select.is-focused > .Select-control {
				box-shadow: none;
				border: 2px #00B7FF solid;
			}

			.select-label {
				font-weight: bold;
				color: var(--primary-color);
				margin-bottom: 5px;
			}

			.required {
				font-weight: normal;
			}
		`}</style>
	</div>
)};

CustomSelect.propTypes = {
	options: PropTypes.arrayOf(PropTypes.object).isRequired,
	withoutArrow: PropTypes.bool,
	fullWidth: PropTypes.bool,
	label: PropTypes.string,
	required: PropTypes.bool,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	onChange: PropTypes.func.isRequired,
	onBlur: PropTypes.func,
};

CustomSelect.defaultProps = {
	withoutArrow: false,
	fullWidth: false,
	label: null,
	required: false,
	value: null,
	onBlur: f => f,
};

export default mapProps(({ input, ...props }) => ({ ...input, ...props }))(CustomSelect);
