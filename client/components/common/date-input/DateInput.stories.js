import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DateInput from './DateInput';


storiesOf('common/DateInput', module)
	.add('default', () => <DateInput onChange={action('change')} />);
