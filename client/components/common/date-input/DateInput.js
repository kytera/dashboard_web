import React from 'react';
import PropTypes from 'prop-types';
import csx from 'classnames';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import { range, chunk } from 'lodash';

import Input from '../input/Input';
import CustomSelect from '../custom-select/CustomSelect';

const moment = extendMoment(Moment);

const MONTHS = moment.months().map((month, idx) => ({ label: month, value: idx }));
const YEARS = range(1900, 2018).map(year => ({ label: year, value: year }));
const DEFAULT_VALUE = '1930-01-01';


export default class DateInput extends React.Component {
	static propTypes = {
		name: PropTypes.string,
		input: PropTypes.shape({ value: PropTypes.string, onChange: PropTypes.func }), // handle redux-form
	}

	static defaultProps = {
		name: null,
		input: null,
	}

	state = {
		isOpen: false,
	}

	componentWillMount() {
		if (process.browser) {
			document.body.addEventListener('click', this.checkForClose);
		}
		this.handleValueChanged(this.props);
	}

	componentWillReceiveProps(props) {
		this.handleValueChanged(props);
	}

	componentWillUnmount() {
		document.body.removeEventListener('click', this.checkForClose);
	}

	onMonthChange = (value) => {
		const newVal = moment(this.state.value).month(value);
		this.setState({ value: newVal});
		this.props.input.onChange(newVal.toISOString());
	}

	onYearChange = (value) => {
		const newVal = moment(this.state.value).year(value);
		this.setState({ value: newVal });
		this.props.input.onChange(newVal.toISOString());
	}

	onSelectDate = value => () => {
		this.setState({ value, year: value.year(), month: value.month(), isOpen: false});
		this.props.input.onChange(value.toISOString());
	}

	onUserInput = () => {
		// TODO
	}

	getWeeks(year, month) {
		const startDate = moment([year, month]);
		const firstDay = moment(startDate).startOf('month').startOf('isoweek');
		const lastDay = moment(startDate).endOf('month').endOf('isoweek');
		const monthRange = moment.range(firstDay, lastDay.add(1, 'day'));
		const days = Array.from(monthRange.by('day', { exclusive: true }));
		return chunk(days, 7);
	}

	setRef = (ref) => {
		this.rootRef = ref;
	}

	handleValueChanged(props) {
		let value = DEFAULT_VALUE;
		if (props.input && props.input.value) {
			value = props.input.value;
		}
		if (props.value) {
			value = props.value;
		}
		value = moment(value);
		this.setState({ value });
	}

	open = () => {
		this.setState({ isOpen: true });
	}

	close = () => {
		this.setState({ isOpen: true });
	}

	checkForClose = (event) => {
		if (this.state.isOpen) {
			if (!this.rootRef.contains(event.target)) {
				this.setState({ isOpen: false });
			}
		}
	}

	render() {
		const { value, isOpen } = this.state;
		const { input, ...rest } = this.props;
		const date = moment(value);

		const inputValue = input.value ? moment(input.value).format('LL') : '';

		return (
			<div ref={this.setRef}>
				{ /* language=CSS */ }
				<style jsx>{`
					.picker-wrapper {
						position: relative;
						color: var(--primary-color);
						height:300px;
					}

					.picker-body {
						background: #ffffff;
						position: absolute;
						top: 8px;
						left: 0;
						right: 0;
						border-radius: 3px;
						border: 2px var(--disabled-color) solid;
						min-width: 297px;

						display: none;
						transition: all .15s var(--timing-sharp);
						opacity: 0;
						transform: translateY(30px);
						margin-bottom: 24px;
					}

					.picker-body.isOpen {
						display: block;
						opacity: 1;
						transform: translateY(0);
					}

					.title {
						display: flex;
						padding: 4px;
					}

					.title > :global(div) {
						flex: 1;
						margin: 4px;
					}

					.calendar {
						width: 100%;
						padding: 4px;
					}

					.calendar td {
						text-align: center;
					}

					.day-cell {
						padding: 8px 0;
						width: 30px;
						cursor: pointer;
						text-align: center;
						display: inline-block;
						border-radius: 3px;
						background: #ffffff;
					}

					.day-cell:hover {
						background-color: rgba(0, 0, 0, .05);
					}

					.day-cell.isDisabled {
						color: color(var(--primary-color) alpha(-90%));
					}

					.day-cell.isActive {
						color: #ffffff;
						background: var(--primary-color);
						transition: all .3s linear;
					}

				`}</style>
				<Input
					placeholder='Select Date'
					value={inputValue}
					icon='calendar'
					onFocus={this.open}
					onChange={this.onUserInput}
					{...rest}
				/>
				<div className='picker-wrapper'>
					<div className={csx('picker-body', { isOpen: true })}>
						<div className='title'>
							<CustomSelect
								options={MONTHS}
								onChange={this.onMonthChange}
								value={date.month()}
								searchable={false}
								valueRenderer={item => item.label}

							/>
							<CustomSelect
								options={YEARS}
								onChange={this.onYearChange}
								value={date.year()}
								searchable={false}
							/>
						</div>
						<table className='calendar'>
							<tbody>
								{ this.getWeeks(date.year(), date.month()).map((week, idx) => (
									// eslint-disable-next-line react/no-array-index-key
									<tr key={idx}>
										{week.map(newDate => (<td key={newDate.valueOf()}>
											<span
												tabIndex={0}
												onClick={this.onSelectDate(newDate)}
												role='button'
												className={csx('day-cell', {
													isActive: newDate.valueOf() === date.valueOf(),
													isDisabled: newDate.month() !== date.month(),
												})}
											>
												{newDate.format('D')}</span>
										</td>))}
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}
