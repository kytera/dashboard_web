import { shallow } from 'enzyme';

import { PermissionOnly } from './PermissionOnly';
import { ROLES } from '../../../constants/index';

describe('<PermissionOnly/>', () => {
	it('render allowed permission', () => {
		const wrapper = shallow(<PermissionOnly
			resource={PermissionOnly.resources.NOTIFICATIONS}
			action='EDIT'
			currentUser={{
				permissions: [{ resource: PermissionOnly.resources.NOTIFICATIONS, action: 'EDIT' }],
				role: ROLES.OPERATOR
			}}
		>
			<span>content</span>
		</PermissionOnly>);
		expect(wrapper.text()).toEqual('content');
	});
	it('render allowed permission to admin', () => {
		const wrapper = shallow(<PermissionOnly
			resource={PermissionOnly.resources.NOTIFICATIONS}
			action='EDIT'
			currentUser={{
				permissions: [],
				role: ROLES.ADMIN
			}}
		>
			<span>content</span>
		</PermissionOnly>);
		expect(wrapper.text()).toEqual('content');
	});
	it('skip wrong permission', () => {
		const wrapper = shallow(<PermissionOnly
			resource={PermissionOnly.resources.NOTIFICATIONS}
			action='EDIT'
			currentUser={{
				permissions: [{ resource: PermissionOnly.resources.NOTIFICATIONS, action: 'VIEW' }],
				role: ROLES.OPERATOR
			}}
		>
			<span>content</span>
		</PermissionOnly>);
		expect(wrapper.isEmptyRender()).toBeTruthy();
	});
});
