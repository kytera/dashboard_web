import PropTypes from 'prop-types';

import withCurrentUser, { currentUserPropTypes } from '../../../withCurrentUser';
import { ROLES } from '../../../constants/index';

export const PermissionOnly = ({ resource, action, currentUser, children }) => {
	if (
		currentUser
		&& (
			currentUser.role === ROLES.ADMIN || currentUser.role === ROLES.SUPER_ADMIN
			|| (currentUser.permissions && currentUser.permissions.some(item => item.resource === resource && item.action === action))
		)
	) {
		return children;
	}
	return null;
};

PermissionOnly.resources = {
	ALERT: 'ALERT',
	ALERT_HISTORY: 'ALERT_HISTORY',
	SERVICE: 'SERVICE',
	NOTIFICATIONS: 'NOTIFICATIONS',
	SUBSCRIBER: 'SUBSCRIBER',
	HOMECARE: 'HOMECARE',
	KYTERA: 'KYTERA'
};

PermissionOnly.propTypes = {
	...currentUserPropTypes,
	resource: PropTypes.oneOf(Object.values(PermissionOnly.resources)).isRequired,
	action: PropTypes.oneOf(['VIEW', 'EDIT']),
	children: PropTypes.element.isRequired,
};

PermissionOnly.defaultProps = {
	action: 'VIEW',
};

export default withCurrentUser(PermissionOnly);
