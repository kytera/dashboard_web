import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Checkbox from './Checkbox';

storiesOf('common/Checkbox', module)
	.add('default', () => (
		<Checkbox onChange={action('change')} />
	))
	.add('checked', () => (
		<Checkbox onChange={action('change')} checked />
	))
	.add('disabled', () => (
		<Checkbox onChange={action('change')} disabled />
	));
