import csx from 'classnames';
import PropTypes from 'prop-types';
import Icon from '../icon/Icon';

const Checkbox = ({ disabled, input, label, ...rest }) => (
	/* eslint-disable jsx-a11y/label-has-for */
	<span className='container'>
		{ /* language=CSS */ }
		<style jsx>{`
			.container {
				display: grid;
				grid-template-columns: 28px 1fr;
				align-items: center;
			}
			.root {
				display: inline-flex;
				justify-content: center;
				align-items: center;
				border-radius: 3px;
				height: 20px;
				width: 20px;
				border: 2px solid;
				cursor: pointer;
				position: relative;
				line-height: 1;
				color: var(--primary-color);
				overflow: hidden;
				margin-right: 8px;
			}

			.root.disabled {
				pointer-events: none;
				cursor: not-allowed;
				color: var(--disabled-color);
			}

			input {
				position: absolute;
				visibility: hidden;
			}

			label.title {
				cursor: pointer;
			}

			.checkmark {
				transition: all .15s var(--timing-standard);
				opacity: 0;
				transform: rotate(-15deg);
			}

			input:checked + .checkmark {
				opacity: 1;
				transform: rotate(0);
				color: white;
				background: var(--primary-color);
			}
		`}</style>
		<label className={csx('root', { disabled })}>
			<input type='checkbox' id={input.name} disabled={disabled} checked={input.value === true} {...rest} {...input} />
			<span className='checkmark'>
				<Icon type='checkmark' size={26} />
			</span>
		</label>
		<label className='title' htmlFor={input.name}>
			{ label }
		</label>
	</span>
);

Checkbox.propTypes = {
	disabled: PropTypes.bool,
	input: PropTypes.shape({ checked: PropTypes.bool, onChange: PropTypes.func }), // redux-form
};

Checkbox.defaultProps = {
	disabled: false,
	input: {},
};

export default Checkbox;
