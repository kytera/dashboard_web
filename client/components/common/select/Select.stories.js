import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Select, { Option } from './Select';

storiesOf('common/Select', module)
	.add('default', () => (
		<Select onChange={action('change')}>
			<Option>Value 1</Option>
			<Option>Value 2</Option>
		</Select>
	));
