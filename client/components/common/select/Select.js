import csx from 'classnames';
import PropTypes from 'prop-types';
import { omit } from 'lodash';


import Icon from '../icon/Icon';

const Select = ({ children, label, required, fullWidth, grouped, input, ...rest }) => {
	rest = omit(rest, 'meta');
	return (
		<div className={csx('root', { grouped })}>
			{ /* language=CSS */ }
			<style jsx>{`
			.container {
				background: #fff;
				border: 2px #E9E9E9 solid;
				align-items: center;
				position: relative;
				display: block;
				border-radius: 3px;
			}

			.grouped:first-child .container {
				border-top-right-radius: 0;
				border-bottom-right-radius: 0;
				border-right-width: 1px;
			}

			.grouped:last-child .container {
				border-left-width: 1px;
				border-top-left-radius: 0;
				border-bottom-left-radius: 0;
			}

			select {
				border: none;
				box-shadow: none;
				background: transparent none;
				color: var(--primary-color);
				appearance: none;
				padding: 8px 30px 8px 10px;
				z-index: 1;
				position: relative;
				width: 100%;
			}

			select:focus {
				outline: none;
			}

			.icon {
				position: absolute;
				top: 0;
				bottom: 0;
				right: 8px;
				display: flex;
				align-items: center;
				z-index: 0;
			}

			.select-label {
				font-weight: bold;
				color: var(--primary-color);
				margin-bottom: 5px;
			}

			.required {
				font-weight: normal;
			}
		`}</style>
			{ !!label && <div className='select-label'><span>
			{label}
				{ required && <span className='required'>&nbsp;(required)</span> }
		</span></div> }
			<div className='container'>
				<select required={required} {...rest} {...input}>
				{children}
				</select>
				<span className='icon'>
				<Icon type='chevron_down' size='1em' />
			</span>
			</div>
		</div>
	);
}

const Option = ({ children, ...rest }) => (
	<option {...omit(rest, 'meta')}>{children}</option>
);


Option.propTypes = {
	children: PropTypes.string,
};

Select.propTypes = {
	children: PropTypes.node.isRequired,
	label: PropTypes.string,
	required: PropTypes.bool,
	fullWidth: PropTypes.bool,
	grouped: PropTypes.bool,
	input: PropTypes.shape({ value: PropTypes.string, onChange: PropTypes.func }), // handle redux-form
};

Select.defaultProps = {
	label: null,
	required: false,
	fullWidth: false,
	grouped: false,
	input: {},
};

export { Select as default, Option };
