import PropTypes from 'prop-types';

import Icon from '../icon/Icon';

const Loading = ({isOpen, message, ...rest}) => {
	message = message || "loading...";
	if(!isOpen) return <React.Fragment/>
	return (
		<div className='root'>
			<div className='message'>
			<div className='icon'>
				<Icon type='loading' width="64px" height="64px" />
			</div>
			{message}
			</div>
			{ /* language=CSS */ }
			<style jsx>{`
				.root {
					display: flex;
					flex: 1;
					flex-direction: column;
					align-items: center;
					justify-content: center;
					position: fixed;
					z-index: 10;
					top: 0;
					left: 0;
					width:100%;
					height:100%;
					right: 0;
					bottom: 0;
					background-color: rgba(255, 255, 255, .8);
				}

				.message {
					z-index: 11;
					font-weight: 600;
					font-size: 24px;
					text-align:center;
				}
				
				.message .icon {
					display: block;
					margin-bottom:40px;
				}
			`}</style>
		</div>
	);
};

Loading.propTypes = {
	isOpen: PropTypes.bool.isRequired,
};

Loading.defaultProps = {
	message: "loading..."
}

export default Loading;
