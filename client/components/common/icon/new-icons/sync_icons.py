

def sync_names():
    import os
    import shutil
    import json

    def fix_icon_name(name, num):
        g1 = name.split('.')
        g2 = g1[-2].split('_')
        g3 = g2[-1].split('@')

        if g3[0].isdigit():
            if num == 0:
                return int(g3[0])

            return name # no need to change
        elif num == 0:
            return 0
        g3[0] = g3[0] + '_%d' % num
        g2[-1] = "@".join(g3)
        g1[-2] = "_".join(g2)
        name = ".".join(g1)
        return name

    files = [f for f in os.listdir('.') if '@' not in f and f.endswith('png')]
    currNum = max([fix_icon_name(f, 0) for f in files])+1
    print currNum

    doubleSizeFiles = [f for f in os.listdir('.') if '@' in f and f.endswith('png')]
    doubleSizeFiles_prefix = [f.split('.')[-2].split('@')[-2] for f in doubleSizeFiles]

    def change_name(old, new):
        print old, '->', new
        try:
            shutil.move(old, new)
        except:
            pass

    # First max number
    while len(files) > 0:
        f  = files.pop()
        new_name = fix_icon_name(f, currNum)

        if f.split('.')[-2] in doubleSizeFiles_prefix:
            double_f = doubleSizeFiles[doubleSizeFiles_prefix.index(f.split('.')[-2])]
            new_double_f = fix_icon_name(f, currNum)

            if double_f!= new_double_f:
                change_name(double_f, new_double_f)
        if f != new_name:
            change_name(f, new_name)
            currNum +=1


    with file('mapping.json', 'wb') as f:
        f.write(json.dumps(dict([(fix_icon_name(f, 0), f) for f in os.listdir('.') if '@' not in f and f.endswith('png')]), sort_keys=True, indent=4))



sync_names()
