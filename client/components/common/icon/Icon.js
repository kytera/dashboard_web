import PropTypes from 'prop-types';

import {newIconsMapping} from "./newIconsMapping";
import * as svgs from './svgs';

/* eslint-disable import/no-dynamic-require */

const Icon = ({ size, height, width, type, number }) => {
	let svg, png;

	if (type) {
		svg = svgs[type];
	} else if(number){
		const iconSrc = newIconsMapping[number];
		if (!iconSrc) {

		} else if (iconSrc.indexOf('.png') >= 0) {
			png = `/static/icons/${iconSrc}`;
		} else {
			svg = svgs[`Icon${number}`];
		}
	}

	if (!svg && !png) {
		return (<span></span>);
	}

	return (
		<span className='root' style={{ height: height || size, width: width || size, }}>
	{/* language=CSS */}
			<style jsx>{`
		.root {
			display: inline-block;
			position: relative;
		}

		span > :global(svg) {
			height: 100%;
			width: 100%;
		}

		img {
			height: 100%;
			width: 100%;
		}
	`}
	</style>
			{png
				? <img src={png} alt='' />
				: <span dangerouslySetInnerHTML={{ __html: svg }} /> // eslint-disable-line react/no-danger
			}
</span>)
};

Icon.propTypes = {
	size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	type: PropTypes.string,
	number: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Icon.defaultProps = {
	size: '24px',
	number: null,
	type: null,
};

export default Icon;
