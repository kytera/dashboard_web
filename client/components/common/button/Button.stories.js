import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from './Button';

storiesOf('common/Button', module)
	.add('default', () => <Button onClick={action('click')} theme='default'>Click here</Button>)
	.add('accent', () => <Button onClick={action('click')} theme='accent'>Click here</Button>)
	.add('error', () => <Button onClick={action('click')} theme='error'>Click here</Button>)
	.add('success', () => <Button onClick={action('click')} theme='success'>Click here</Button>)
	.add('disabled', () => <Button onClick={action('click')} disabled>Click here</Button>)
	.add('link', () => <Button onClick={action('click')} link>Click here</Button>)
	.add('loading', () => <Button onClick={action('click')} loading>Click here</Button>)
	.add('loading accent', () => <Button onClick={action('click')} theme='accent' loading>Click here</Button>)
	.add('loading success', () => <Button onClick={action('click')} theme='success' loading>Click here</Button>)
	.add('large', () => <Button onClick={action('click')} size='large'>Click here</Button>)
	.add('link with icon', () => <Button onClick={action('click')} link icon='pencil'>Click here</Button>)
	.add('error link with icon', () => <Button onClick={action('click')} theme='error' icon='pencil' link>Click here</Button>);
