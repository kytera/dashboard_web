import PropTypes from 'prop-types';
import csx from 'classnames';

import Icon from '../icon/Icon';

const Button = ({ theme, loading, link, size, icon, children, ...rest }) => (
	<button className={csx('root', theme, size, { link, loading, hasIcon: icon })} {...rest}>
		{ /* language=SCSS */ }
		<style jsx>{`
			.root {
				color: #fff;
				border: 0;
				border-radius: 3px;
				padding: 6px 24px;
				cursor: pointer;
				display: inline-flex;
				align-items: center;
				line-height: 1;
				outline: none;
			}
			.root.large {
				padding: 10px 24px;
			}

			.root:active {
				opacity: .8;
			}

			.root:hover {
				opacity: .9;
			}

			.default {
				background-color: var(--primary-color);
			}

			.accent {
				background-color: var(--accent-color);
			}

			.error {
				background-color: var(--error-color);
			}

			.success {
				background-color: var(--success-color);
			}

			.link {
				background-color: transparent;
				color: var(--accent-color);
				padding: 6px;
			}
			.link.success {
				color: var(--success-color);
			}
			.link.error {
				color: var(--error-color);
			}

			[disabled] {
				background-color: var(--disabled-color);
				cursor: not-allowed;
			}

			.link[disabled] {
				background-color: transparent;
				color: var(--disabled-color);
			}

			.loading {
				opacity: .5;
				pointer-events: none;
			}

			.loading.default {
				box-shadow: 0 0 0 0 color(var(--primary-color) alpha(-50%));
				animation: pulse-default 1.2s infinite;
			}

			.loading.success {
				box-shadow: 0 0 0 0 color(var(--success-color) alpha(-50%));
				animation: pulse-success 1.2s infinite;
			}

			.loading.accent {
				box-shadow: 0 0 0 0 color(var(--accent-color) alpha(-50%));
				animation: pulse-accent 1.2s infinite;
			}

			.icon {
				margin-right: 8px;
			}

			.content {
				position: relative;
			}

			.loading .content {
				color: transparent;
			}

			.loading .content::before {
				content: '';
				width: 100%;
				color: #fff;
				position: absolute;
				left: 0;
				right: 0;
				bottom: 0;
			}

			.link.hasIcon {
				text-decoration: none;
			}

			@keyframes pulse-default {
				0% {
					box-shadow: 0 0 0 0 color(var(--primary-color) alpha(-50%));
				}
				100% {
					box-shadow: 0 0 0 12px color(var(--primary-color) alpha(-100%));
				}
			}

			@keyframes pulse-accent {
				0% {
					box-shadow: 0 0 0 0 rgba(0, 183, 255, .5);
				}
				100% {
					box-shadow: 0 0 0 12px rgba(0, 183, 255, .0);
				}
			}

			@keyframes pulse-success {
				0% {
					box-shadow: 0 0 0 0 color(var(--success-color) alpha(-50%));
				}
				100% {
					box-shadow: 0 0 0 12px color(var(--success-color) alpha(-100%));
				}
			}
		`}</style>
		{ !!icon && (
			<span className='icon'>
				<Icon type={icon} size={16} />
			</span>
		)}
		<span className='content'>{children}</span>
	</button>
);

Button.propTypes = {
	theme: PropTypes.oneOf(['default', 'accent', 'error', 'success', 'link']),
	type: PropTypes.oneOf(['button', 'reset', 'submit']),
	size: PropTypes.oneOf(['small', 'normal', 'large']),
	loading: PropTypes.bool,
	link: PropTypes.bool,
	icon: PropTypes.string,
	children: PropTypes.node,
};

Button.defaultProps = {
	theme: 'default',
	type: 'button',
	size: 'normal',
	loading: false,
	link: false,
	icon: null,
	children: null,
};

export default Button;
