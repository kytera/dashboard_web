import csx from 'classnames';
import PropTypes from 'prop-types';

const Switch = ({ disabled, input, ...rest }) => {
	return (
	/* eslint-disable jsx-a11y/label-has-for */
	<label className={csx('root', { disabled })}>
		{ /* language=CSS */ }
		<style jsx>{`
			.root {
				display: inline-flex;
				border-radius: 99px;
				width: 48px;
				border: 2px var(--disabled-color) solid;
				background: #ffffff;
				padding: 1px;
				cursor: pointer;
				position: relative;
				line-height: 1;
				align-items: center;
			}

			.disabled {
				pointer-events: none;
				cursor: not-allowed;
			}

			input {
				position: absolute;
				visibility: hidden;
			}

			input:checked + .circle {
				background-color: var(--success-color);
				left: 21px;
			}

			.circle {
				border-radius: 50%;
				height: 20px;
				width: 20px;
				background-color: rgba(255, 0, 0, 1);
				display: inline-block;
				position: relative;
				transition: all .15s var(--timing-standard);
				left: 0;
			}

			.disabled .circle {
				background-color: var(--disabled-color);
			}
		`}</style>
		<input type='checkbox' disabled={disabled} {...rest} {...input} checked={input.value} />
		<span className='circle' />
	</label>
)};

Switch.propTypes = {
	disabled: PropTypes.bool,
	input: PropTypes.shape({ checked: PropTypes.bool, onChange: PropTypes.func }), // redux-form
};

Switch.defaultProps = {
	disabled: false,
	input: {},
};

export default Switch;
