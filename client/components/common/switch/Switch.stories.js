import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Switch from './Switch';

storiesOf('common/Switch', module)
	.add('default', () => (
		<Switch onChange={action('change')} />
	))
	.add('disabled', () => (
		<Switch onChange={action('change')} disabled />
	));
