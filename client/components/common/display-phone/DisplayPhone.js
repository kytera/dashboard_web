import PropTypes from 'prop-types';
import {isString} from "lodash";

// TODO: implement phone format
const DisplayPhone = ({ children }) => {
	return (
		<a href={`${children}`} className='root'>
			{ /* language=CSS */ }
			<style jsx>{`
			.root {
				color: var(--accent-color);
			}
			.root:hover {
				text-decoration: dashed;
			}
		`}</style>
			{children}
		</a>
	);

}

DisplayPhone.propTypes = {};

export default DisplayPhone;
