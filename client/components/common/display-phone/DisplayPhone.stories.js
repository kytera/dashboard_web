import { storiesOf } from '@storybook/react';

import DisplayPhone from './DisplayPhone';

storiesOf('common/DisplayPhone', module)
	.add('default', () => <DisplayPhone>1234567890</DisplayPhone>);
