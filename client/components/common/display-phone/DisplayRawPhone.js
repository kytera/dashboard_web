import PropTypes from 'prop-types';

// TODO: implement phone format
const DisplayRawPhone = ({ phone }) => {
	return (
		<a href={`${phone}`} className='root'>
			{ /* language=CSS */ }
			<style jsx>{`
			.root {
				color: var(--accent-color);
			}
			.root:hover {
				text-decoration: dashed;
			}
		`}</style>
			{phone}
		</a>
	);

}

DisplayRawPhone.propTypes = {
	phone: PropTypes.string
};

export default DisplayRawPhone;
