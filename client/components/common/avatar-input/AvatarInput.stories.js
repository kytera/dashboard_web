import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import AvatarInput from './AvatarInput';

storiesOf('common/AvatarInput', module)
	.add('default', () => <AvatarInput input={{onChange: action('change'), value: null}} />)
	.add('with edit', () => <AvatarInput input={{onChange: action('change'), value: null}} onEdit={action('edit clicked')} />);
