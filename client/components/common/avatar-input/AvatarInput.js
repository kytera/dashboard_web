import PropTypes from 'prop-types';
import React from 'react';

import Icon from '../icon/Icon';

export default class AvatarInput extends React.Component {
	static propTypes = {
		input: PropTypes.shape({
			value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
			onChange: PropTypes.func.isRequired,
		}).isRequired,
		onEdit: PropTypes.func,
		editable: PropTypes.bool
	};

	static defaultProps = {
		input: { value: '/static/images/default-avatar.png' },
		onEdit: null,
		editable: false,
	};

	state = {};

	componentWillMount() {
		this.setImageFromProps(this.props);
	}

	componentWillReceiveProps(props) {
		this.setImageFromProps(props);
	}

	onInputChange = (event) => {
		const input = event.target;
		if (this.props.input.onChange) {
			this.props.input.onChange(input.files[0]);
		}
		if (input.files && input.files[0]) {
			this.setImageFromFile(input.files[0]);
		}
	}

	onEditClick = (event) => {
		if(this.props.input.value) {
			event.preventDefault();
			this.props.onEdit && this.props.onEdit();
		}
	}

	setImageFromFile(file) {
		const reader = new FileReader();
		reader.onload = (e) => {
			this.setState({ image: e.target.result });
		};
		reader.readAsDataURL(file);
	}

	setImageFromProps(props) {
		if (props.input.value) {
			if (process.browser && props.input.value instanceof File) {
				this.setImageFromFile(props.input.value);
			} else {
				this.setState({ image: props.input.value });
			}
		} else {
			this.setState({ image: AvatarInput.defaultProps.input.value });
		}
	}

	render() {
		return (
			// temporary disabled until we have no way to generate a unuqie id and sync it with SSR
			// eslint-disable-next-line jsx-a11y/label-has-for
			<label className='root'>
				{ /* language=CSS */ }
				<style jsx>{`
					.root {
						height: 100px;
						width: 100px;
						position: relative;
						border-radius: 50%;
						display: inline-block;
					}

					input {
						opacity: 0;
						position: fixed;
						top: -100px;
					}

					.border {
						z-index: 1;
						position: absolute;
						top: 0;
						right: 0;
						bottom: 0;
						left: 0;
						border-radius: 50%;
						cursor: pointer;
					}

					.border:hover {
						background: rgba(0, 0, 0, .05);
						border: 2px rgba(0, 0, 0, .15) solid;
					}

					img {
						height: 100%;
						width: 100%;
						object-fit: cover;
						border-radius: 50%;
					}

					.edit-button {
						color: var(--accent-color);
						position: absolute;
						bottom: 0;
						right: 0;
					}
				`}</style>
				<input type='file' accept='image/*' onChange={this.onInputChange} disabled={!this.props.onEdit} />
				<img src={this.state.image} alt='Selected avatar' />
				{this.props.editable && <div>
					<span className='edit-button' onClick={this.onEditClick} role='button' tabIndex={0}>
						<Icon type='pencil' size='20px' />
					</span>
					<div className='border' />
				</div>}
				{/*{ this.props.input.value && this.props.onEdit && () }*/}
			</label>
		);
	}
}
