import { shallow } from 'enzyme';

import { ROLES, RolesOnly } from './RolesOnly';

describe('<RolesOnly/>', () => {
	it('render allowed role', () => {
		const wrapper = shallow(<RolesOnly
			role={ROLES.SUPER_ADMIN}
			currentUser={{ role: ROLES.SUPER_ADMIN }}
		>
			<span>content</span>
		</RolesOnly>);
		expect(wrapper.text()).toEqual('content');
	});
	it('skip wrong role', () => {
		const wrapper = shallow(<RolesOnly
			role={ROLES.SUPER_ADMIN}
			currentUser={{ role: ROLES.CALL_CENTER_ADMIN }}
		>
			<span>content</span>
		</RolesOnly>);
		expect(wrapper.isEmptyRender()).toBeTruthy();
	});
});
