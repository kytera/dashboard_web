import PropTypes from 'prop-types';

import withCurrentUser, { currentUserPropTypes } from '../../../withCurrentUser';
import { ROLES } from '../../../constants';

export const RolesOnly = ({ role, currentUser, children }) => {
	if (!role || (currentUser && currentUser.role && role.includes(currentUser.role))) {
		return children;
	}
	return null;
};

RolesOnly.propTypes = {
	...currentUserPropTypes,
	role: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.oneOf(Object.values(ROLES))),
		PropTypes.oneOf(Object.values(ROLES)),
	]).isRequired,
	children: PropTypes.element.isRequired,
};

export default withCurrentUser(RolesOnly);

export { ROLES };
