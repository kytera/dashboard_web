import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import UpdateCaregiverForm from './UpdateCaregiverForm';

storiesOf('common/UpdateCaregiverForm', module)
	.add('With TitleBar', () => <UpdateCaregiverForm onAddUpdate={action('submit')} showTitleBar />)
	.add('No TitleBar', () => <UpdateCaregiverForm onAddUpdate={action('submit')} showTitleBar={false} />);
