import React from 'react';
import PropTypes from 'prop-types';
import csx from 'classnames';

import IconButton from '../../alert-tab/icon-button/IconButton';
import Icon from '../icon/Icon';
import Button from '../button/Button';

const ALERT_TYPES = [
	{
		icon: 'phone',
		title: 'Phone Call',
	},
	{
		icon: 'ambulance_car',
		title: 'Ambulance',
	},
	{
		icon: 'nurse',
		title: 'Paramedic',
	},
	{
		icon: 'face',
		title: 'Caregiver',
	},
];

class UpdateCaregiverForm extends React.Component {
	static propTypes = {
		onAddUpdate: PropTypes.func.isRequired,
		showTitleBar: PropTypes.bool.isRequired,
	};

	state = {
		isActive: false,
		icon: null,
		text: '',
	};

	onSubmit = async () => {
		const { icon, text } = this.state;
		const currentType = ALERT_TYPES.find(item => item.icon === icon);
		const title = (currentType && currentType.title) || null;

		await this.props.onAddUpdate({ icon, message: text, title });
		this.resetForm();
	};

	toggleButtons = (value) => {
		const { icon } = this.state;
		const nextState = icon === value ? null : value;

		this.setState({ icon: nextState });
	};


	resetForm = () => {
		this.setState({ isActive: false, text: '', icon: null });
	};

	render() {
		const { isActive, icon, text } = this.state;

		return (
			<div className='root'>
				{/* language=CSS */}
				<style jsx>{`
					.root {
						background-color: #fff;
						border: 2px solid var(--disabled-color);
						border-radius: 3px;
					}

					.title {
						padding: 5px 10px;
						color: var(--primary-color);
						background-color: var(--disabled-color);
						display: flex;
						flex-flow: row nowrap;
						justify-content: space-between;
						align-items: center;
					}

					.title span {
						font-weight: bold;
						text-transform: uppercase;
					}

					.title-buttons {
						display: flex;
						flex-flow: row nowrap;
						align-items: center;
					}

					.content {
						display: flex;
						flex-flow: row nowrap;
						align-items: stretch;
						padding: 2px 5px;
						padding-left: 10px;
					}

					.content.isActive {
						padding: 15px 10px 0;
					}

					.content:not(.isActive) .message-icon {
						margin-top: 8px;
					}

					.message-icon {
						display: flex;
						align-items: flex-start;
						margin: 6px 10px 0 0;
					}

					.input {
						margin-right: 10px;
						width: 100%;
					}

					textarea {
						border: none;
						outline: none;
						color: #787776;
						font-size: 18px;
						resize: none;
						width: 100%;
					}

					textarea::-webkit-scrollbar {
						width: 8px;
					}

					textarea::-webkit-scrollbar-thumb {
						background-color: var(--disabled-color);
						border-radius: 4px;
					}

					.input span {
						color: var(--primary-color);
						font-weight: bold;
						font-size: 11px;
					}

					.input-title {
						margin: 3px 0 5px 0;
					}

					.add-button {
						text-align: right;
						margin: 4px;
					}
				`}</style>
				<div className={csx('form', { isActive })}>
					{ isActive && this.props.showTitleBar && <div className='title'>
						<span>
							Update Caregiver
						</span>
						<div className='title-buttons'>
							{ALERT_TYPES.map(item => (<div key={item.icon}>
								<IconButton
									icon={item.icon}
									isActive={icon === item.icon}
									onClick={() => this.toggleButtons(item.icon)}
								/>
							</div>
							))}
						</div>
					</div> }
					<div className={csx('content', { isActive })}>
						<div className='message-icon'>
							<Icon type={icon || 'arrow_right'} size='13px' />
						</div>
						<div className='input'>
							{ icon && <div className='input-title'>
								<span>{ALERT_TYPES.find(item => item.icon === icon).title}</span>
							</div> }
							<textarea
								rows={isActive ? 3 : 1}
								onFocus={() => this.setState({ isActive: true })}
								value={text}
								onChange={e => this.setState({ text: e.target.value })}
							/>
						</div>
						{ isActive && <div className='buttons'>
							<Button
								link
								onClick={this.resetForm}
							>
								Close
							</Button>
						</div> }
					</div>
					{ isActive && <div className='add-button'>
						<Button
							disabled={!text}
							onClick={this.onSubmit}
						>
							{ icon ? 'Update' : 'Add' }
						</Button>
					</div> }
				</div>
			</div>
		);
	}
}

export default UpdateCaregiverForm;
