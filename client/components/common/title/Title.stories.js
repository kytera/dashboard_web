import { storiesOf } from '@storybook/react';

import Title from './Title';
import Button from '../button/Button';

storiesOf('common/Title', module)
	.add('default', () => <Title subtitle='(Lorem inpum)'>Title</Title>)
	.add('with action', () => <Title subtitle='(Lorem inpum)' action={<Button icon='bell' link>Action</Button>}>Title</Title>)
	.add('large', () => <Title size='large' subtitle='Lorem ipsum' action={<Button icon='bell' theme='error' link>Error action</Button>}>Title</Title>)
	.add('small', () => <Title size='small'>Title</Title>)
	.add('thin', () => <Title size='thin'>Title</Title>);
