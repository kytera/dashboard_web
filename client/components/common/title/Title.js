import csx from 'classnames';
import PropTypes from 'prop-types';

const Title = ({ children, action, subtitle, size, transparent, ...rest }) => (
	<div className={csx('root', size, { transparent })} {...rest}>
		{/* language=SCSS */}
		<style jsx>{`
			.root {
				display: flex;
				align-items: center;
				background: #ffffff;
				color: var(--primary-color);
			}
			.root.transparent {
				background-color: transparent;
			}

			.root.small {
				background: var(--disabled-color);
				font-size: 13px;
				padding: 0 24px;
				padding-right: 12px;
				height: 40px;
				border-bottom: 2px var(--disabled-color) solid;
			}

			.root.thin {
				background: transparent;
				font-size: 12px;
			}

			.root.medium {
				font-size: 14px;
				padding: 0 24px;
				padding-right: 12px;
				height: 54px;
				min-height:54px;
				border-bottom: 2px var(--disabled-color) solid;

			}

			.root.medium .title {
					text-transform: uppercase;
				}

			.root.large {
				font-size: 16px;
				padding: 0 24px;
				height: 64px;
				padding-right: 12px;
				border-bottom: 2px var(--disabled-color) solid;
			}

			.root.large .subtitle {
				display: block;
			}

			.title {
				margin-right: 4px;
				font-weight: 700;
			}

			.subtitle {
				font-size: .9em;
				color: rgba(0, 0, 0, .4);
			}

			.actions {
				flex: 1;
				display: flex;
				justify-content: flex-end;
			}
		`}</style>
		<div className='content'>
			<span className='title'>{children}</span>
			<span className='subtitle'>{subtitle}</span>
		</div>
		<div className='actions'>{action}</div>
	</div>
);

Title.propTypes = {
	children: PropTypes.node.isRequired,
	action: PropTypes.node,
	subtitle: PropTypes.node,
	size: PropTypes.oneOf(['thin', 'small', 'medium', 'large']),
	transparent: PropTypes.bool,
};

Title.defaultProps = {
	size: 'medium',
	subtitle: null,
	action: null,
	transparent: false,
};

export default Title;
