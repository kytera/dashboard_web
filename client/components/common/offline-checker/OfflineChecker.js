import React, { Component } from 'react';
import Alert from "react-s-alert";

export class OfflineChecker extends Component {

	constructor(props) {
		super(props);
		this.alertId = null;
	}

	componentDidMount() {
		this.alertId = Alert.error(`Error: You are offline, Check your internet connection`, {
			position: 'bottom-left',
			effect: 'stackslide',
			timeout: 'none',
		});
	}

	componentWillUnmount() {
		if (this.alertId) {
			Alert.close(this.alertId)
		}
	}

	render() {
		return null;
	}
}
