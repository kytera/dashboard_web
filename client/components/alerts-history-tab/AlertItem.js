import { propType } from 'graphql-anywhere';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import cn from 'classnames';

import { Link } from '../../../lib/routes';
import ALERT_FIELDS from '../../graphql/alert/alert.fragment.gql';
import Icon from '../common/icon/Icon';


const AlertItem = ({ alert: { icon, title, timestamp, closeMessage, closeIcon, id, alertId }, isActive, subscriberId }) => (
	<Link route='subscriber-alerts-history' params={{ alertId: id, id: subscriberId }}>
		<a className={cn('root', { isActive })}>
			{/* language=SCSS */}
			<style jsx>{`
			.root {
				display: block;
				background-color: #fff;
				border: 2px var(--disabled-color) solid;
				border-top: 0;
				border-left: 0;
				padding: 24px;
			}

			.root.isActive {
				background-color: #F4F4F4;
				border-right: 0;
			}

			.id {
				font-weight: 500;
				font-size: 14px;
				overflow: hidden;
				text-overflow: ellipsis;
				flex: 1;
				margin-right: 24px;
			}

			.head {
				white-space: nowrap;
				display: flex;
			}

			.content {
				padding: 24px;
			}

			.title {
				display: flex;
				margin: 24px 0;
				color: var(--error-color);
			}

			.title-icon {
				margin-right: 12px;
			}

			.time {
				color: #9c9c9c;
			}

			.close {
				color: var(--primary-color);
				display: flex;
				margin: 24px 0;
			}

			.close-icon {
				margin-right: 12px;
			}
		`}</style>
			<div className='head'>
				<div className='id'>#{alertId}</div>
				<div className='time'>
					<Moment format='D MMM, YYYY [|] LT'>{timestamp}</Moment>
				</div>
			</div>
			<div className='title'>
				<div className='title-icon'>
					<Icon number={icon} size={18} />
				</div>
				<div>{title}</div>
			</div>
			<div className='close'>
				<div className='title-icon'>
					<Icon number={closeIcon} />
				</div>
				<div>{closeMessage}</div>
			</div>
		</a>
	</Link>
);

AlertItem.propTypes = {
	alert: propType(ALERT_FIELDS).isRequired,
	subscriberId: PropTypes.string.isRequired,
	isActive: PropTypes.bool.isRequired,
};

export default AlertItem;
