export const DEFAULT_ROOM = 'No Room';

function logRoom(log) {
	return log.room || DEFAULT_ROOM;
}
function createNewGroup(log) {
	return {
		groupTitle: logRoom(log),
		items: [log],
	};
}

export function groupLogsList(logs) {
	const groupedLogsArr = [];
	logs.forEach((log) => {
		const lastGroup = groupedLogsArr[0] && groupedLogsArr[groupedLogsArr.length - 1];
		const isSameRoomAsLastGroup = lastGroup ? lastGroup.groupTitle === logRoom(log) : false;
		if (isSameRoomAsLastGroup) {
			lastGroup.items.push(log);
		} else {
			const newGroupItem = createNewGroup(log);
			groupedLogsArr.push(newGroupItem);
		}
	});

	return groupedLogsArr;
}
