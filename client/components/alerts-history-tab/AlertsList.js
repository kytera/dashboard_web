import PropTypes from 'prop-types';
import { Component } from 'react';
import { propType } from 'graphql-anywhere';

import ScrollContainer from '../common/scroll-container/ScrollContainer';
import AlertItem from './AlertItem';
import ALERT_FIELDS from '../../graphql/alert/alert.fragment.gql';
import Title from '../common/title/Title';

class AlertsList extends Component {
	componentDidMount() {
		this.scrollContainer.scrollToBottom();
	}

	componentWillReceiveProps(newProps) {
		if(newProps.subscriberId !== this.props.subscriberId) {
			this.scrollContainer.scrollToBottom();
		}
	}

	render() {
		let { alerts, currentAlert, subscriberId } = this.props;

		return (
			<div className='root'>
				{/* language=SCSS */}
				<style jsx>{`
			.root {
				background-color: #fff;
				border-top: none;
				overflow-y: auto;
				display: flex;
				flex-direction: column;
				flex: 0 0 35%;
			}
		`}</style>
				<Title size='medium'>Alerts ({alerts.length})</Title>
				<ScrollContainer ref={r => this.scrollContainer = r}>
					{alerts.map(alert => (<AlertItem
						key={alert.id}
						alert={alert}
						isActive={!!currentAlert && alert.id === currentAlert.id}
						subscriberId={subscriberId}
					/>))}
				</ScrollContainer>
			</div>
		);
	}
}

AlertsList.propTypes = {
	alerts: PropTypes.arrayOf(propType(ALERT_FIELDS)).isRequired,
	currentAlert: propType(ALERT_FIELDS),
	subscriberId: PropTypes.string.isRequired,
};

AlertsList.defaultProps = {
	currentAlert: null,
};

export default AlertsList;
