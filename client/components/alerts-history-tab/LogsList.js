import PropTypes from 'prop-types';
import { Component } from 'react';
import { graphql } from 'react-apollo';
import { compose, withProps } from 'recompose';

import ListSection from '../alert-tab/logs-list/list-section/ListSection';

import ALERT_LOGS_QUERY from '../../graphql/log/alertLogs.query.gql';
import withQueryStatus from '../../withQueryStatus';
import ScrollContainer from '../common/scroll-container/ScrollContainer';
import { groupLogsList } from './groupLogsList';

class LogsList extends Component {
	componentDidMount() {
		this.scrollContainer.scrollToBottom();
	}

	componentWillReceiveProps(newProps) {
		if(newProps.subscriberId !== this.props.subscriberId) {
			this.scrollContainer.scrollToBottom();
		}
	}

	render() {
		const { groupedLogs } = this.props;

		return (
			<div className='root'>
				{/* language=CSS */}
				<style jsx>{`
			.root {
				border-top: none;
				overflow-y: auto;
				display: flex;
				flex-direction: column;
				flex: 1;
			}
		`}</style>
				<ScrollContainer ref={r => this.scrollContainer = r}>
					{groupedLogs.map((group, index) => (<ListSection
						key={`${group.groupTitle}${index}`} // eslint-disable-line
						title={group.groupTitle}
						logs={group.items}
					/>))}
				</ScrollContainer>
			</div>
		);
	}
}

LogsList.propTypes = {
	groupedLogs: PropTypes.arrayOf(PropTypes.shape({
		groupTitle: PropTypes.string.isRequired,
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
	})).isRequired,
};


const enhance = compose(
	graphql(ALERT_LOGS_QUERY,
		{
			options: ({ subscriberId, alertId, showAll }) => ({
				variables: {
					alertId,
					subscriberId,
					types: showAll ? ['log', 'update', 'activity'] : ['log', 'activity'],
				},
				pollInterval: 5 * 60000,
				fetchPolicy: process.browser ? 'cache-and-network' : undefined,
			}),
			skip: ({ alertId }) => !alertId,
		},
	),
	withQueryStatus({ emptyText: 'No logs found', resultPath: 'alertLogs' }),
	withProps(({ data }) => {
		const groupedLogs = groupLogsList(data.alertLogs);
		return {
			groupedLogs,
		};
	}),
);

export default enhance(LogsList);

