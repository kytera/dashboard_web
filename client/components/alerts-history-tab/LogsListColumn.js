import PropTypes from 'prop-types';
import { withState } from 'recompose';

import Title from '../common/title/Title';
import Checkbox from '../common/checkbox/Checkbox';
import LogsList from './LogsList';

export const LogsListColumn = ({ alertId, subscriberId, showAll, setShowAll }) => (
	<div className='root'>
		{/* language=CSS */}
		<style jsx>{`
			.root {
				flex: 0 0 35%;
				display: flex;
				flex-direction: column;
				border-right: 2px solid white;
				background: #F4F4F4;
			}

			.title {
				display: flex;
				align-items: center;
			}

		`}</style>
		<Title size='medium' transparent>
			<label className='title'>
				<Checkbox onChange={e => setShowAll(e.target.checked)} checked={showAll} />
				Show shared with caregivers info
			</label>

		</Title>
		<LogsList alertId={alertId} showAll={showAll} subscriberId={subscriberId} />
	</div>
);

LogsListColumn.propTypes = {
	alertId: PropTypes.string.isRequired,
	subscriberId: PropTypes.string.isRequired,
	showAll: PropTypes.bool.isRequired,
	setShowAll: PropTypes.func.isRequired,
};

export default withState('showAll', 'setShowAll', false)(LogsListColumn);
