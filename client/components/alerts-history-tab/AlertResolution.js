import {propType} from 'graphql-anywhere';
import Moment from 'react-moment';
import moment from 'moment';

import ALERT_FIELDS from '../../graphql/alert/alert.fragment.gql';
import Title from '../common/title/Title';
import Button from '../common/button/Button';
import Icon from '../common/icon/Icon';


const AlertResolution = ({alert: {icon, title, summary, timestamp, stopTimestamp}}) => {
	const duration = moment.utc(moment(stopTimestamp).diff(timestamp));
	const durationFormat = (duration.hours() > 0 ? 'H[h] m[m]' : 'm[m]');
	return (
		<div className='root'>
			{/* language=SCSS */}
			<style jsx>{`
				.root {
					flex: 0 0 30%;
					line-height: 1.5;
				}

				.content {
					padding: 24px;
				}

				.title {
					display: flex;
					margin-bottom: 24px;
					color: var(--error-color);
				}

				.title-icon {
					margin-right: 12px;
				}

				.time {
					color: #9c9c9c;
				}

				.summary {
					margin: 24px 0;
					color: var(--primary-color);
				}
			`}</style>
			<Title
				transparent
				size='medium'
				action={<Button onClick={() => window.print()} icon='print' theme='default' link>Print</Button>}
			>
				RESOLUTION
			</Title>
			<div className='content'>
				<div className='title'>
					<div className='title-icon'>
						<Icon number={icon} size={18}/>
					</div>
					<div>{title}</div>
				</div>
				<div className='time'>
					<div>
						<span className='time-label'>Start time: </span>
						<Moment format='D MMM, YYYY [|] LT'>{timestamp}</Moment>
					</div>
					<div>
						<span className='time-label'>Duration: </span>
						<Moment format={durationFormat}>{duration}</Moment>
					</div>
				</div>
				<div className='summary'>{summary}</div>
			</div>
		</div>
	);
};

AlertResolution.propTypes = {
	alert: propType(ALERT_FIELDS).isRequired,
};

export default AlertResolution;
