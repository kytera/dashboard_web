import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { propType } from 'graphql-anywhere';
import { compose, mapProps } from 'recompose';
import { sortBy } from 'lodash';

import ALERTS_QUERY from '../../graphql/alert/alerts.query.gql';
import ALERT_FIELDS from '../../graphql/alert/alert.fragment.gql';
import withQueryStatus from '../../withQueryStatus';
import AlertsList from './AlertsList';
import AlertResolution from './AlertResolution';
import LogsListColumn from './LogsListColumn';

function sortAlerts(alerts) {
	if(!alerts) return null;
	return sortBy(alerts, ['timestamp']);
}

export const AlertsHistoryTab = ({ alerts, currentAlert, subscriberId }) => (
	<div className='root'>
		{/* language=CSS */}
		<style jsx>{`
			.root {
				flex: 1;
				display: flex;
			}
			.column {

			}
		`}</style>
		<AlertsList currentAlert={currentAlert} alerts={alerts} subscriberId={subscriberId} />
		{ currentAlert && <LogsListColumn alertId={currentAlert.alertId} subscriberId={subscriberId} /> }
		{ currentAlert && <AlertResolution alert={currentAlert} /> }
	</div>
);

AlertsHistoryTab.propTypes = {
	alerts: PropTypes.arrayOf(propType(ALERT_FIELDS)).isRequired,
	currentAlert: propType(ALERT_FIELDS),
	subscriberId: PropTypes.string.isRequired,
};

AlertsHistoryTab.defaultProps = {
	currentAlert: null,
};

const enhance = compose(
	graphql(ALERTS_QUERY,
		{
			options: ({ subscriberId }) => ({
				variables: { subscriberId },
				pollInterval: 5 * 60000,
				fetchPolicy: process.browser ? 'cache-and-network' : undefined,
			}),
			skip: ({ subscriberId }) => !subscriberId,
		},
	),
	withQueryStatus({ emptyText: 'This subscriber has no alerts yet', resultPath: 'alerts' }),
	mapProps(({ data, alertId, subscriberId }) => ({
		alerts: sortAlerts(data.alerts),
		currentAlert: alertId ? data.alerts.find(alert => alert.id === alertId) : null,
		subscriberId,
	})),
);

export default enhance(AlertsHistoryTab);
