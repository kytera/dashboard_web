import { wrapDisplayName, compose } from 'recompose';
import React from 'react';
import PropTypes from 'prop-types';
import { graphql, withApollo } from 'react-apollo';
import Cookie from 'js-cookie';
import { Router } from '../lib/routes';

import { LOCALSTORAGE_JWT, COOKIE_JWT } from './constants';
import LOGIN_MUTATION from './graphql/common/login.mutation.gql';


export default (ComposedComponent) => {
	class WithAuthControl extends React.Component {
		static displayName = wrapDisplayName(ComposedComponent, 'withAuthControl');

		static propTypes = {
			client: PropTypes.object.isRequired, // eslint-disable-line
			mutate: PropTypes.func.isRequired,
		};

		state = {
			loginLoading: false,
			loginError: null,
		};

		login = async (email, password) => {
			this.setState({ loginLoading: true, loginError: null });
			try {
				const response = await this.props.mutate({
					variables: { email, password },
				});
				localStorage.setItem(LOCALSTORAGE_JWT, response.data.login.token);
				Cookie.set(COOKIE_JWT, response.data.login.token, { expires: 365 });
				this.setState({ loginError: null, loginLoading: false });
				return response.data.login.currentUser;
			} catch (error) {
				this.setState({ loginError: error.message, loginLoading: false });
				console.error(error);
				return false;
			}
		};

		logout = async () => {
			await this.props.client.resetStore();
			localStorage.removeItem(LOCALSTORAGE_JWT);
			Cookie.remove(COOKIE_JWT, { path: '' });
			Router.push({ pathname: '/login', query: { refresh: true } });
		};

		render() {
			const authControl = {
				login: this.login,
				loginLoading: this.state.loginLoading,
				loginError: this.state.loginError,
				logout: this.logout,
			};

			return <ComposedComponent authControl={authControl} {...this.props} />;
		}
	}


	const enhance = compose(
		withApollo,
		graphql(LOGIN_MUTATION),
	);

	return enhance(WithAuthControl);
};

export const authControlPropTypes = {
	authControl: PropTypes.shape({
		login: PropTypes.func.isRequired,
		loginLoading: PropTypes.bool.isRequired,
		loginError: PropTypes.string,
		logout: PropTypes.func.isRequired,
	}),
};
