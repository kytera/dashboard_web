import { check } from 'graphql-anywhere';

function PropTypeError(message) {
	this.message = message;
	this.stack = '';
}
// Make `instanceof Error` still work for returned errors.
PropTypeError.prototype = Error.prototype;

export const apolloFragmentPropType = (apolloObject) => { // eslint-disable-line
	const returnFunction = (props, propName) => {
		try {
			check(apolloObject, props[propName]);
			return null;
		} catch (e) {
			return e;
		}
	};
	// hack so we can call .isRequired
	// to not encounter eslint react/require-default-props
	returnFunction.isRequired = returnFunction;
	return returnFunction;
};
