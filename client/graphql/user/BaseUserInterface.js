export default {
	kind: 'INTERFACE',
	name: 'BaseUser',
	possibleTypes: [
		{ name: 'SuperAdmin' },
		{ name: 'User' },
	],
};
