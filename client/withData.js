import React from 'react';
import PropTypes from 'prop-types';
import { ApolloProvider, getDataFromTree } from 'react-apollo';
import { wrapDisplayName } from 'recompose';
import { Provider } from 'react-redux';

import initApollo from './initApollo';
import initRedux from './initRedux';
import config from "./config/config";

export default ComposedComponent => class WithData extends React.Component {
	static displayName = wrapDisplayName(ComposedComponent, 'withData');
	static propTypes = {
		/* eslint-disable react/forbid-prop-types */
		serverState: PropTypes.object.isRequired,
	};

	static async getInitialProps(ctx) {
		let serverState = {};
		let config;
		// Evaluate the composed component's getInitialProps()
		let composedInitialProps = {};
		if (ComposedComponent.getInitialProps) {
			composedInitialProps = await ComposedComponent.getInitialProps(ctx);
		}

		// Run all graphql queries in the component tree
		// and extract the resulting data
		if (!process.browser) {
			try {
				config = JSON.parse(process.env.CLIENT);
			} catch (e) {
				console.warn('Cannot parse config...');
			}

			console.log('config',config);

			const apollo = initApollo(config.GRAPHQL_ENDPOINT, { headers: ctx.req.headers });
			const redux = initRedux(apollo);
			// Provide the `url` prop data in case a GraphQL query uses it
			const url = { query: ctx.query, pathname: ctx.pathname };

			try {
				// Run all GraphQL queries
				await getDataFromTree(
					// redux-form requires redux provider
					<Provider store={ redux }>
						<ApolloProvider client={apollo}>
							<ComposedComponent url={url} {...composedInitialProps}/>
						</ApolloProvider>
					</Provider>,
				);
			} catch (error) {
				// Prevent Apollo Client GraphQL errors from crashing SSR.
				// Handle them in components via the data.error prop:
				// http://dev.apollodata.com/react/api-queries.html#graphql-query-data-error
				console.error(error);
				if (error.graphQLErrors && error.graphQLErrors.some(({ name }) => name === 'UNAUTHENTICATED_ERROR')) {
					ctx.res.writeHead(302, { Location: '/login' });
					ctx.res.end();
				}
			}

			// Extract query data from the store
			serverState = apollo.cache.extract();
		}

		return {
			config,
			serverState,
			...composedInitialProps,
		};
	}

	constructor(props) {
		super(props);

		if (props.config) {
			Object.assign(config, props.config);
		}

		this.apollo = initApollo(config.GRAPHQL_ENDPOINT,{},this.props.serverState);
		this.redux = initRedux(this.apollo, this.props.serverState);
	}

	render() {
		return (
			// redux-form requires redux provider
			<Provider store={ this.redux }>
				<ApolloProvider client={this.apollo}>
					<ComposedComponent {...this.props} />
				</ApolloProvider>
			</Provider>
		);
	}
};
