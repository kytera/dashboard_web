import { omit, pick } from 'lodash';

export const normalizeCaregiverInput = caregiver => {
	let __caregiver = {...caregiver};
	__caregiver = omit(__caregiver, ['__typename', 'id', 'action']);

	if(__caregiver.settings) {
		__caregiver.settings = __caregiver.settings.map(setting => omit(setting, ['__typename']));
	}

	return __caregiver;
};

export const normalizeSubscriberInput = subscriber => {
	let sub = {...subscriber};
	delete sub.companyId;
	delete sub.dashboardServiceStatus;
	if (sub.address) {
		sub.address = omit(sub.address, '__typename');
	}
	sub = omit(sub, ['id', '__typename', 'createdAt', 'isActive', 'status', 'settings', 'caregivers', 'dealerCompanyId']);
	if (process.browser && !(sub.avatar instanceof File)) {
		delete sub.avatar;
	}
	if(sub.agencies) {
		let ag = sub.agencies.map(agency => { return {agency_type: agency.agency_type, number: agency.number} });
		sub.agencies = ag;
	}
	if(sub.medicalAlarmService) {
		sub.medicalAlarmService = omit(sub.medicalAlarmService, ['__typename']);
	}

	if(sub.wellnessService) {
		sub.wellnessService = omit(sub.wellnessService, ['__typename']);
	}

	return sub;
};
