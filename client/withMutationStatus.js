import { wrapDisplayName } from 'recompose';
import React from 'react';

export default function withMutationStatus(mutationNames = 'mutate', statusName = 'mutationStatus', throwhable = false) {
	return (ComposedComponent) => {
		class WithMutationStatus extends React.Component {
			static displayName = wrapDisplayName(ComposedComponent, 'withMutationStatus')

			state = {
				loading: false,
				error: '',
			};

			mutate = mutationName => (variables, { handleError, handleSuccess } = {}) => {
				this.setState({ loading: true, error: '' });

				return this.props[mutationName](variables)
					.then((result) => {
						console.log(`[${mutationName}] Successful!`);
						this.setState({ loading: false });
						if (handleSuccess) {
							handleSuccess();
						}
						return result;
					})
					.catch((error) => {
						console.error(`[${mutationName}] Failed!`, error);
						this.setState({ loading: false, error: error.message });
						if (handleError) {
							handleError();
						}
						if (throwhable) {
							throw error;
						}
					});
			}

			render() {
				const childProps = {
					...this.props,
					[statusName]: {
						loading: this.state.loading,
						error: this.state.error,
					},
				};
				if (mutationNames instanceof Array) {
					mutationNames.forEach((mutationName) => {
						childProps[mutationName] = this.mutate(mutationName);
					});
				} else {
					childProps[mutationNames] = this.mutate(mutationNames);
				}

				return (
					<ComposedComponent {...childProps} />
				);
			}
		}

		return WithMutationStatus;
	};
}
