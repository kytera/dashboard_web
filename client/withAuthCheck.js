import { wrapDisplayName } from 'recompose';
import React from 'react';
import Router from 'next/router';

import { currentUserPropTypes } from './withCurrentUser';

export default ComposedComponent => class WithAuthCheck extends React.Component {
	static displayName = wrapDisplayName(ComposedComponent, 'withAuthCheck');

	static propTypes = {
		...currentUserPropTypes,
	};

	constructor(props) {
		super(props);
		this.checkForCurrentUser(props);
	}

	componentWillReceiveProps(props) {
		this.checkForCurrentUser(props);
	}

	checkForCurrentUser(props) {
		if (props.currentUserQuery && props.currentUserQuery.error) {
			Router.push('/login');
		}
	}

	render() {
		if (this.props.currentUserQuery && !this.props.currentUser) {
			return (
				<div className='loading'>
					{ /* language=CSS */ }
					<style jsx>{`
						.loading {
							position: fixed;
							top: 0;
							right: 0;
							bottom: 0;
							left: 0;
							display: flex;
							align-items: center;
							justify-content: center;
							font-weight: 500;
							font-size: 36px;
						}
					`}</style>
					Loading...
				</div>
			);
		}

		return (<ComposedComponent {...this.props} />);
	}
};
