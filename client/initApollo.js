import { ApolloClient } from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { createUploadLink } from 'apollo-upload-client';
import { BatchHttpLink } from "apollo-link-batch-http";
import { InMemoryCache, IntrospectionFragmentMatcher, defaultDataIdFromObject } from 'apollo-cache-inmemory'
import fetch from 'isomorphic-fetch';
/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */
import Router from 'next/router';

import userTypes from './graphql/user/BaseUserInterface';
import logTypes from './graphql/log/logTypes';
import { LOCALSTORAGE_JWT } from './constants';
let apolloClient = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
	global.fetch = fetch;
}

const authMiddlewareLink = setContext((_, { headers }) => {
	const token = localStorage.getItem(LOCALSTORAGE_JWT);
	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : null,
		}
	}
});


const errorAuthLink = onError(({networkError = {}, graphQLErrors, response}) => {
	if (networkError.statusCode === 401 ||
		(response && response.errors && response.errors.some(error => error.name === 'UNAUTHENTICATED_ERROR'))) {
		localStorage.removeItem(LOCALSTORAGE_JWT);
		Router.push('/login');
	}
	if (graphQLErrors)
		graphQLErrors.map(({message, locations, path}) =>
			console.log(
				`[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(locations)}, Path: ${path}`
			)
		);
});

const createBatchHttpUploadLink = (graphQLEndpoint, opts) => {
	const isObject = node => typeof node === 'object' && node !== null;

  // File type checker
	const hasFiles = (node, found = []) => {
		Object.keys(node).forEach((key) => {
			if (!isObject(node[key]) || found.length > 0) {
				return;
			}

			if (
				(typeof File !== 'undefined' && node[key] instanceof File) ||
				(typeof Blob !== 'undefined' && node[key] instanceof Blob)
			) {
				found.push(node[key]);
				return;
			}

			hasFiles(node[key], found);
		});

		return found.length > 0;
	};

	return ApolloLink.split(
		({variables}) => hasFiles(variables),
		createUploadLink({
				uri: graphQLEndpoint,
				...opts,
			}
		),
		new BatchHttpLink({
				uri: graphQLEndpoint,
				batchInterval: 20,
				...opts,
			}
		),
	);
};

const fragmentMatcher = new IntrospectionFragmentMatcher({
	introspectionQueryResultData: {
		__schema: {
			types: [
				userTypes,
				logTypes,
			],
		},
	},
});

function create(graphQLEndpoint, params, initialApolloState) {
	const opts = { // Additional fetch() options like `credentials` or `headers`
		credentials: 'same-origin',
	};
	if (params && params.headers) {
		opts.headers = params.headers;
	}

	const uploadLink = createBatchHttpUploadLink(graphQLEndpoint, opts);


	let links = [uploadLink];
	if (process.browser) {
		links = [errorAuthLink, authMiddlewareLink, uploadLink];
	}

	const inMemoryCache = new InMemoryCache({
		dataIdFromObject: res => (res.__typename === 'System' ? res.__typename + res.subscriberId : defaultDataIdFromObject(res)),
		fragmentMatcher,
	});

	return new ApolloClient({
		ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
		ssrForceFetchDelay: 100,
		cache: inMemoryCache.restore(initialApolloState),
		link: ApolloLink.from(links),
	});
}

export default function initApollo(graphQLEndpoint, params, initialApolloState = {}) {
	// Make sure to create a new client for every server-side request so that data
	// isn't shared between connections (which would be bad)
	if (!process.browser) {
		return create(graphQLEndpoint, params, initialApolloState);
	}

	// Reuse client on the client-side
	if (!apolloClient) {
		apolloClient = create(graphQLEndpoint,{}, initialApolloState);
	}

	return apolloClient;
}
