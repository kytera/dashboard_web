import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import reducers from './reducers';

let reduxStore = null;

// Get the Redux DevTools extension and fallback to a no-op function
let devtools = f => f;
/* eslint-disable no-underscore-dangle */
if (process.browser && window.__REDUX_DEVTOOLS_EXTENSION__) {
	devtools = window.__REDUX_DEVTOOLS_EXTENSION__();
}

function create(initialState = {}) {
	return createStore(
		combineReducers({ // Setup reducers
			...reducers,
			form: formReducer,
		}),
		initialState,
		devtools,
	);
}

export default function initRedux() {
	// Make sure to create a new store for every server-side request so that data
	// isn't shared between connections (which would be bad)
	if (!process.browser) {
		return create();
	}

	// Reuse store on the client-side
	if (!reduxStore) {
		reduxStore = create();
	}

	return reduxStore;
}
