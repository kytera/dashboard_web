import { graphql } from 'react-apollo';
import PropTypes from 'prop-types';

import CURRENT_USER_QUERY from './graphql/user/currentUser.query.gql';

export const getUserCompanyIds = (currentUser) => {
	if (currentUser.role !== 'SUPER_ADMIN'){
		return [currentUser.company.id];
	} else {
		return currentUser.companies.map(com=> com.id);
	}
};

export default graphql(
	CURRENT_USER_QUERY,
	{
		alias: 'withCurrentUser',
		props: ({ data: { currentUser, ...currentUserQuery } }) => ({
			currentUser,
			currentUserQuery,
		}),
	},
);

export const currentUserPropTypes = {
	currentUser: PropTypes.object,
	currentUserQuery: PropTypes.shape({
		loading: PropTypes.bool.isRequired,
		error: PropTypes.object,
	}),
};
