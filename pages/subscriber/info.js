import React from 'react';
import { compose } from 'recompose';

import Layout from '../../client/components/layout/Layout';
import SubscriberTab from '../../client/components/subscriber-tab/SubscriberTab';
import withData from '../../client/withData';
import withCurrentUser from '../../client/withCurrentUser';
import withAuthCheck from '../../client/withAuthCheck';

const enhance = compose(
	withData,
	withCurrentUser,
	withAuthCheck,
);

export default enhance(props => (
	<Layout {...props}>
		<SubscriberTab subscriberId={props.url.query.id} operatorId={props.currentUser.id} />
	</Layout>
));
