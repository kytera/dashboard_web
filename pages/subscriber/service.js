import React from 'react';
import { compose } from 'recompose';

import Layout from '../../client/components/layout/Layout';
import RightPane from '../../client/components/right-pane/RightPane';
import SystemLayout from '../../client/components/service-tab/system/SystemLayout';
import DealerLogLayout from '../../client/components/service-tab/dealer-log-layout/DealerLogLayout';
import withData from '../../client/withData';
import withCurrentUser from '../../client/withCurrentUser';
import withAuthCheck from '../../client/withAuthCheck';

const enhance = compose(
	withData,
	withCurrentUser,
	withAuthCheck,
);

export default enhance(props => {
	return (!props.url.query.id ? <Layout {...props} /> : (
		<Layout {...props}>
			<SystemLayout subscriberId={props.url.query.id}/>
			<DealerLogLayout subscriberId={props.url.query.id}/>
			<RightPane subscriberId={props.url.query.id}/>
		</Layout>
	))
});

