import React from 'react';
import { compose } from 'recompose';
import { graphql } from 'react-apollo';
import { get } from 'lodash';

import Layout from '../../client/components/layout/Layout';
import RightPane from '../../client/components/right-pane/RightPane';
import SubscriberActivity from '../../client/components/alert-tab/subscriber-activity/SubscriberActivity';
import AlertLog from '../../client/components/alert-tab/alert-log/AlertLog';
import withData from '../../client/withData';
import withCurrentUser from '../../client/withCurrentUser';
import withAuthCheck from '../../client/withAuthCheck';
import AlertInfo from '../../client/components/alert-tab/alert-info/AlertInfo';
import ACTIVE_ALERT_QUERY from '../../client/graphql/alert/activeAlert.query.gql';

const enhance = compose(
	withData,
	withCurrentUser,
	withAuthCheck,
	graphql(ACTIVE_ALERT_QUERY,
		{
			options: ({ url }) => ({
				variables: { subscriberId: get(url, 'query.id') },
				pollInterval: 500,
			}),
			skip: ({ url }) => !get(url, 'query.id'),
		},
	),
);

export default enhance(props => ((!props.url.query.id || !get(props, 'data.activeAlert.alertId')) ? <Layout {...props} /> : (
	<Layout {...props}>
		{ /* language=CSS */ }
		<style jsx>{`
			.content {
				flex: 1;
				flex-direction: column;
				display: flex;
			}
			.columns {
				flex: 1;
				display: flex;
			}
		`}</style>
		<div className='content'>
			<AlertInfo subscriberId={props.url.query.id} data={props.data}/>
			<div className='columns'>
				<SubscriberActivity subscriberId={props.url.query.id} />
				<AlertLog subscriberId={props.url.query.id} alertId={get(props, 'data.activeAlert.alertId')} />
			</div>
		</div>
		<RightPane subscriberId={props.url.query.id} />
	</Layout>
)));
