import React from 'react';
import { compose } from 'recompose';

import Layout from '../../client/components/layout/Layout';
import withData from '../../client/withData';
import withCurrentUser from '../../client/withCurrentUser';
import withAuthCheck from '../../client/withAuthCheck';
import NotificationsTab from '../../client/components/notifications-tab/NotificationsTab';
import RightPane from '../../client/components/right-pane/RightPane';

const enhance = compose(
	withData,
	withCurrentUser,
	withAuthCheck,
);

export default enhance(props => (!props.url.query.id ? <Layout {...props} /> : (
	<Layout {...props}>
		<NotificationsTab subscriberId={props.url.query.id} />
		<RightPane subscriberId={props.url.query.id} />
	</Layout>
)));
