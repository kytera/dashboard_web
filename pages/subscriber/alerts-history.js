import React from 'react';
import { compose } from 'recompose';

import Layout from '../../client/components/layout/Layout';
import AlertsHistoryTab from '../../client/components/alerts-history-tab/AlertsHistoryTab';
import withData from '../../client/withData';
import withCurrentUser from '../../client/withCurrentUser';
import withAuthCheck from '../../client/withAuthCheck';

const enhance = compose(
	withData,
	withCurrentUser,
	withAuthCheck,
);

export default enhance(props => (
	<Layout {...props}>
		<AlertsHistoryTab subscriberId={props.url.query.id} alertId={props.url.query.alertId} />
	</Layout>
));
