import { compose } from 'recompose';

import Layout from '../../client/components/layout/Layout';
import RightPane from '../../client/components/right-pane/RightPane';
import withData from '../../client/withData';
import withCurrentUser from '../../client/withCurrentUser';
import withAuthCheck from '../../client/withAuthCheck';

const enhance = compose(
	withData,
	withCurrentUser,
	withAuthCheck,
);

export default enhance(props => (
	<Layout {...props}>
		{ /* language=CSS */ }
		<style jsx>{`
			section {
				flex: 1;
			}
		`}</style>
		<section>Index page</section>
		<aside>
			<RightPane user={props.currentUser} />
		</aside>
	</Layout>
));
