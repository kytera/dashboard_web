import { compose } from 'recompose';
import React from 'react';
import PropTypes from 'prop-types';

import Layout from '../client/components/layout/Layout';
import withData from '../client/withData';
import withCurrentUser from '../client/withCurrentUser';
import withAuthCheck from '../client/withAuthCheck';



const IndexPage = props => (
	<Layout {...props} />
);

IndexPage.propTypes = {
	// eslint-disable-next-line react/forbid-prop-types
	url: PropTypes.object.isRequired,
};

export default compose(
	withData,
	withCurrentUser,
	withAuthCheck,
)(IndexPage);
