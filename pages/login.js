import React from 'react';
import { compose } from 'react-apollo';
import { Router } from '../lib/routes';

import withData from '../client/withData';
import withAuthControl, { authControlPropTypes } from '../client/withAuthControl';
import Modal from '../client/components/common/modal/Modal';
import Button from '../client/components/common/button/Button';
import Input from '../client/components/common/input/Input';
import { ROLES } from '../client/constants';

class LoginPage extends React.Component {
	static propTypes = {
		...authControlPropTypes,
	}

	state = {
		message: null,
		loading: false,
		email: '',
		password: '',
	}


	onSubmitForm = async (event) => {
		event.preventDefault();
		const {email, password} = this.state;
		this.setState({loading: true, message: null});
		const currentUser = await this.props.authControl.login(email, password);
		if (currentUser) {
			Router.pushRoute('/');
		} else {
			this.setState({loading: false});
		}
	}

	onEmailChange = (e) => {
		this.setState({email: e.target.value});
	}

	onPasswordChange = (e) => {
		this.setState({password: e.target.value});
	}

	render() {
		if (this.props.url.query.refresh) {
			window.location.href = 'login';
		}

		const {loginError, logout} = this.props.authControl;
		const {message, email, password, loading} = this.state;

		return (
			<div>
				{/* language=CSS */}
				<style jsx>{`
					.error {
						padding: 16px;
						background: var(--error-color);
						color: #fff;
					}

					.message {
						padding: 16px;
						background: var(--success-color);
						color: #fff;
					}

					a {
						text-decoration: underline;
					}

					.input {
						padding: 16px;
					}
				`}</style>
				<Modal
					isOpen
					title='Login to Kytera'
					onSubmit={this.onSubmitForm}
					button={(<Button key={1} theme='accent' type='submit' loading={loading}>Login</Button>)}
				>
					<div className='content'>
						{!!loginError && <div className='error'>Login Failed - invalid email/password</div>}
						{!!message && (<div className='message'>
							{message} <a role='button' tabIndex={0} onClick={logout}>Logout</a>
						</div>)}
						<div className='input'>
							<Input
								type='email'
								placeholder='Email'
								readOnly={loading}
								name='email'
								value={email}
								onChange={this.onEmailChange}
							/>
						</div>
						<div className='input'>
							<Input
								type='password'
								placeholder='Password'
								readOnly={loading}
								name='password'
								value={password}
								onChange={this.onPasswordChange}
							/>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
}

export default compose(
	withData,
	withAuthControl,
)(LoginPage);
