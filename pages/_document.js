import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import flush from 'styled-jsx/server';


/* eslint-disable react/no-danger */
export default class CustomDocument extends Document {
	static getInitialProps({ renderPage }) {
		const { html, head, errorHtml, chunks } = renderPage();
		const styles = flush();
		return { html, head, errorHtml, chunks, styles };
	}

	render() {
		return (
			<html lang="en">
				<Head>
					<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
					<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.css' />
					<link rel='stylesheet' href='/_next/static/style.css' />
					<style>{this.props.styles}</style>
					<meta charSet='utf-8' />
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</html>
		);
	}
}
