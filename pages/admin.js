import { compose } from 'recompose';
import React from 'react';
import PropTypes from 'prop-types';

import AdminLayout from '../client/components/layout/AdminLayout';
import OperatorScreen from '../client/components/operator-screen/OperatorScreen';
import withData from '../client/withData';
import withCurrentUser from '../client/withCurrentUser';
import withAuthCheck from '../client/withAuthCheck';


const enhance = compose(
	withData,
	withCurrentUser,
	withAuthCheck,
);

const PageAdmin = props => (
	<AdminLayout isAdmin {...props}>
		<OperatorScreen operatorId={props.url.query.operatorId} />
	</AdminLayout>
);

PageAdmin.propTypes = {
	// eslint-disable-next-line react/forbid-prop-types
	url: PropTypes.object.isRequired,
};

export default enhance(PageAdmin);
