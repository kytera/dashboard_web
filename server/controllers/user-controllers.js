import { InvalidUserCredentialsError } from '../utils/errors';

export default {
	async login({ User, AccessToken }, email, password) {
		const user = await User.findByCredentials(email, password);
		if (!user) {
			throw new InvalidUserCredentialsError();
		}
		const token = await AccessToken.createJWTAccessToken(user._id);
		return {
			currentUser: user,
			token,
		};
	},
};
