import cloudinary from 'cloudinary';

export default (image, width, height) => cloudinary.url(image.public_id, { secure: true, width, height, crop: 'fill' });
