import bcrypt from 'bcryptjs';

const SALT_WORK_FACTOR = 10;

export default async function hashPassword(userPassword) {
	const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
	return bcrypt.hash(userPassword, salt);
}

