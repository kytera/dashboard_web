import bcrypt from 'bcryptjs';

export default function comparePasswords(hashedPassword, candidatePassword) {
	return bcrypt.compare(candidatePassword, hashedPassword);
}
