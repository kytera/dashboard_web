import { createError } from 'apollo-errors';

export const QueryTooLargeError = createError('QUERY_TOO_LARGE', {
	message: 'The query sent to GraphQL was too large > 2000 chars',
});

export const InputMissingError = createError('INPUT_MISSING_ERROR', {
	message: 'An input is missing',
});

export const AccessTokenInvalidError = createError('ACCESS_TOKEN_INVALID', {
	message: 'The access token provided in the headers was invalid.',
});

export const JWTVerifyFailedError = createError('JWT_TOKEN_VERIFY_FAILED', {
	message: 'An error occurred while trying to verify the JWT token',
});

export const UnauthenticatedError = createError('UNAUTHENTICATED_ERROR', {
	message: 'You must be authenticated to perform this request',
});

export const UnauthorizedError = createError('UNAUTHORIZED_ERROR', {
	message: 'You have not enough rights to perform this request',
});

export const DuplicateEmailError = createError('DUPLICATE_EMAIL_ERROR', {
	message: 'User with same email already exists',
});

export const InvalidUserCredentialsError = createError('INVALID_USER_CREDENTIALS_ERROR', {
	message: 'User with same email or password does not exist',
});

export const InvalidArgumentsError = createError('INVALID_ARGUMENTS_ERROR', {
	message: 'You have passed invalid arguments to mutation',
});

export const UserNotFoundError = createError('UNKNOWN_USER_ERROR', {
	message: 'User not found',
});

export const UploadFileError = createError('UPLOAD_FILE_ERROR', {
	message: 'Unable to process one of the files that you have uploaded',
});

export const SerialNumberNotFound = createError('SERIAL_NUMBER_NOT_FOUND', {
	message: 'Unable to find the serial number for the current company',
});

export const SerialNumberNotAvailable = createError('SERIAL_NUMBER_NOT_AVAILABLE', {
	message: 'The serial number is already set for another system,',
});

export const SerialNumberDeviceTypeIncorrect = createError('SERIAL_NUMBER_DEVICE_TYPE_INCORRECT', {
	message: 'The serial number not fit the device type',
});

export const IncorrectSysidForTransaction = createError('INCORRECT_SYS_ID', {
	message: 'The provided sysid does not fit the most recent sysid',
});

export const SerialNumberIsAlreadyTaken = createError('SERIAL_NUMBER_TAKEN', {
	message: 'Cannot add new device because the serial number is alreadu taken',
});

export const CompanyNotFound = createError('COMPANY_NOT_FOUND', {
	message: 'Company with the given company id was not found',
});

export const NoSystem = createError('NoSystem', {
	message: 'No system was found for user',
});
