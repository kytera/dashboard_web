import bunyan from 'bunyan';

const level = process.env.LOG_LEVEL || 'debug';

export default bunyan.createLogger({ name: 'server', level });
export const graphqlLogger = bunyan.createLogger({ name: 'graphql', level });
export const mongodbLogger = bunyan.createLogger({ name: 'mongodb', level });
