import mongoose from 'mongoose';
import context from '../context';
import { initAcl } from './initACL';
import logger from '../utils/logger';


function connectMongo() {
  const MONGO_URL = process.env.MONGO_URL;
  mongoose.Promise = Promise;
	return new Promise((resolve, reject) => {
		logger.info(`> Connecting to Mongo url=${MONGO_URL}...`);
		mongoose.connect(MONGO_URL, { useMongoClient: true });
		mongoose.set('debug', process.env.NODE_ENV !== 'production');
		const db = mongoose.connection;

		db.on('error', err => reject(err));

		db.once('open', () => {
			logger.info('> Connected successfully to Mongo.');
			resolve(db);
		});
	});
}
export async function initMongo() {
// eslint-disable-next-line no-extend-native
	String.prototype.toObjectId = function toObjectId() {
		const ObjectId = (mongoose.Types.ObjectId);
		return new ObjectId(this.toString());
	};

	const db = await connectMongo();
	await initAcl(db);

	context.db = db;
	return db;
}
