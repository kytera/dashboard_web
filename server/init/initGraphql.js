import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { apolloUploadExpress } from 'apollo-upload-server';
// import { SubscriptionServer } from 'subscriptions-transport-ws';
import { formatError } from 'apollo-errors';

import {
	User, AccessToken, Company, Subscriber, Caregiver, System,
	InventoryTransaction, NotificationLog, ServiceLog, UserStatus,
	DistressAlert, DistressAlertLog,
} from '../models';
import schema from '../graphql/schema';
import { QueryTooLargeError } from '../utils/errors';

const checkQuerySize = (req, res, next) => {
	const query = req.query.query || (req.body && req.body.query);
	if (query && query.length > 2000) {
		throw new QueryTooLargeError();
	}
	next();
};

export function initGraphQL(server) {
	server.use('/graphql', checkQuerySize, apolloUploadExpress(), graphqlExpress(async (req, res) => {
		let currentUser = null;
		if (req.user) {
			// eslint-disable-next-line no-underscore-dangle
			currentUser = await User.findById(req.user._id);
		}
		return {
			schema,
			context: {
				currentUser,
				models: {
					User,
					AccessToken,
					InventoryTransaction,
					Company,
					Subscriber,
					Caregiver,
					System,
					UserStatus,
					NotificationLog,
					ServiceLog,
					DistressAlert,
					DistressAlertLog,
				},
				httpRequest: req,
				httpResponse: res,
			},
			formatError,
		};
	}));

	server.use('/graphiql', graphiqlExpress({
		endpointURL: '/graphql',
		// TODO: enable subscriptions
		// subscriptionsEndpoint,
	}));
}
