import { initGraphQL } from './initGraphql';
import registerPassportJWT from '../services/auth/register-passport-jwt';
import authMiddleware from '../services/auth/auth-middleware';

export async function initServer(server) {

	// Configure express middlewares
	registerPassportJWT();
	server.use('/graphql', authMiddleware);

	// Init graphql endpoint
  initGraphQL(server);
}
