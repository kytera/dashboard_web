import acl from 'acl';
import context from '../context';

export async function initAcl(db) {
// eslint-disable-next-line new-cap
	context.acl = await new acl(new acl.mongodbBackend(db, 'ACL'));
}
