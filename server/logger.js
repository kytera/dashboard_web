import bunyan from 'bunyan';

const logLevel = process.env.logLevel || 'debug';
export const logger = bunyan.createLogger({ name: 'server', level: logLevel });
