export default {
	SUPER_ADMIN: 'SUPER_ADMIN',
	ADMIN: 'ADMIN',
	OPERATOR: 'OPERATOR',
};

export const PermissionResources = {
	ALERT: 'ALERT',
	ALERT_HISTORY: 'ALERT_HISTORY',
	SERVICE: 'SERVICE',
	NOTIFICATIONS: 'NOTIFICATIONS',
	SUBSCRIBER: 'SUBSCRIBER',
	SETTINGS: 'SETTINGS',
    HOMECARE: 'HOMECARE',
};
