export const SUBSCRIPTIONS_PATH = '/subscriptions';
export const AUTH_COOKIE_NAME = 'JWT';
export const AUTH_TOKEN_URL_QUERY = 'access_token';
