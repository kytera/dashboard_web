import axios from 'axios';
import mongoose from 'mongoose';

let conn;

const API = axios.create({
	baseURL: process.env.KYTERA_API_URL,
});

export const changeDashboardServiceStatus = (subscriberid, operatorid, transition) => {
	if (!["suspend", "resume", "activate", "archive"].includes(transition)) {
		throw new Error(`changeDashboardServiceStatus: unknown transition requested [${transition}]`);
	}

	return API.get(KYTERA_API_METHODS.dashboardServiceStatusChange, {
		params: {
			subscriberid,
			operatorid,
			transition,
		}
	});
};

export const KYTERA_API_METHODS = {
	onSystemCreated: 'new_sysid_created/',
	updateServiceCall: 'update_service_call/',
	updateDistressAlert: 'update_distress_alert/',
	openAlert: 'open_alert/',
	closeAlert: 'close_alert/',
	dashboardServiceStatusChange: 'dashboard_service_change/',
	sendAppInvite: 'send_mobile_invitation_email/',
	createSystem: 'register_new_subscriber/',
	kyteraInfo: 'generate_kytera_info/',
	newCaregiver: 'new_caregiver/'
};

export const newCaregiver = (firstName, middleName, lastName, email, phones, settings) => {
	return API.get(KYTERA_API_METHODS.newCaregiver, {
		params: {
			firstName, middleName, lastName, email, phones, settings
		}
	});
}

export const onSystemCreated = sysid => API.get(KYTERA_API_METHODS.onSystemCreated, { params: { sysid } });
export const addServiceLogUpdate = (sysid, message, operatorId) => API.get(KYTERA_API_METHODS.updateServiceCall, {
	params: {
		sysid,
		message,
		operatorid: operatorId,
	},
});
export const createSystem = subscriber => API.get(KYTERA_API_METHODS.createSystem, {
	params: {
		firstName: subscriber.firstName,
		subscriber : JSON.stringify(subscriber)
	}
});

export const updateDistressAlert = (sysid, alertId, operatorId, message, iconNumber) => API.get(KYTERA_API_METHODS.updateDistressAlert, {
	params: {
		sysid,
		alertid: alertId,
		message,
		operatorid: operatorId,
		icon_selection: iconNumber || 0,
	},
});
export const openAlert = (sysid, alertId, operatorId) => API.get(KYTERA_API_METHODS.openAlert, {
	params: {
		sysid,
		alertid: alertId,
		operatorid: operatorId,
	},
});
export const closeAlert = (sysid, alertId, operatorId, closeAlertReason, closeAlertExplanation) => {
	return API.get(KYTERA_API_METHODS.closeAlert, {
		params: {
			sysid,
			alertid: alertId,
			operatorid: operatorId,
			closeAlertReason,
			closeAlertExplanation
		}
	});
}
export const sendAppInvite = (username) => API.get(KYTERA_API_METHODS.sendAppInvite, { params: { username: username } });

export const kyteraInfo = sysid => API.get(KYTERA_API_METHODS.kyteraInfo, { params: {sysid}});


export function getKyteraDBConnection() {
	if (conn) {
		return conn;
	} else if (process.env.KYTERA_MONGO_URL) {
		conn = mongoose.createConnection(process.env.KYTERA_MONGO_URL);
		return conn;
	}
	return null;
}
