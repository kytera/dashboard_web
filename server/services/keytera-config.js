import mongoose from "mongoose";

const connectionString = process.env.KYTERA_SETTINGS_MONGO_URL

const SETTINGS_SCHEMA = 'settings'

export const getSettings = async () => {
  const dbconn = mongoose.createConnection(connectionString,{useNewUrlParser:true});

  dbconn.on('error', err => {throw err});

  const settingsSchema = new mongoose.Schema({
    kytera_caregiver_notifications : []
  });

  const db = dbconn.useDb('kytera_config');

  var SettingsModel = db.model(SETTINGS_SCHEMA, settingsSchema);

  let result = await SettingsModel.findOne({})

  console.log('KYTERA-SETTINGS', result)

  return result

}


