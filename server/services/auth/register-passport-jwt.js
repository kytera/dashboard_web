import passport from 'passport';
import PassportJWT from 'passport-jwt';
import User from '../../models/User';
import AccessToken from '../../models/AccessToken';
import { AUTH_TOKEN_URL_QUERY, AUTH_COOKIE_NAME } from '../../config/constants/index';

function cookieExtractor(req) {
	let token = null;
	if (req && req.cookies) {
		token = req.cookies[AUTH_COOKIE_NAME];
	}
	return token;
}

const { Strategy, ExtractJwt } = PassportJWT;

export default function registerPassportJWT() {
	const opts = {
		secretOrKey: process.env.SECRET,
		jwtFromRequest: ExtractJwt.fromExtractors([
			ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
			cookieExtractor,
			ExtractJwt.fromUrlQueryParameter(AUTH_TOKEN_URL_QUERY),
		]),
	};

	passport.use(new Strategy(opts, async (jwtPayload, done) => {
		try {
			await AccessToken.validateJWTToken(jwtPayload);
			const user = await User.findById(jwtPayload.userId);
			done(null, user);
		} catch (e) {
			done(e);
		}
	}));
}
