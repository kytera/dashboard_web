import jwt from 'jsonwebtoken';
/**
 * Generate a signed JWT access token
 * @param userId the id of the user requesting the token
 * @param tokenRef the token reference that we store in DB
 * @returns {*}
 */
export default (userId, tokenRef) => {
	const secret = process.env.SECRET;

	return jwt.sign({
		exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 365),
		userId,
		tokenRef,
	}, secret);
};
