import passport from 'passport';

import logger from '../../utils/logger';

export default (req, res, next) => {
	const middleware = passport.authenticate('jwt', { session: false }, (err, user) => {
		if (err) {
			logger.warn('Error while trying to authenticate', err.message);
		} else if (user) {
			req.user = user;
		}

		next();
	});

	middleware(req, res, next);
};
