export default {
	operators({ companyId }, args, { models: { User } }) {
		return User.find({ companyId });
	},
};
