import { rolesOnly, uploadImage } from '../../decorators';
import ROLES, { PermissionResources } from '../../../config/constants/acl';
import { UnauthorizedError } from '../../../utils/errors';
import permissionOnly from '../../decorators/permissionOnly';

export default {
	@rolesOnly(ROLES.SUPER_ADMIN)
	async createAdmin(root, { admin, companyId }, { models: { User } }) {
		const user = new User({ ...admin, companyId, role: ROLES.ADMIN });
		return user.save();
	},
	@rolesOnly(ROLES.SUPER_ADMIN)
	async createCompany(root, { name, type }, { models: { Company } }) {
		const company = new Company({ name, type });
		return company.save();
	},
	@rolesOnly(ROLES.ADMIN)
	@uploadImage('operator.avatar')
	async createOperator(root, { operator: operatorInput, companyId }, { models: { User }, currentUser }) {
		if (currentUser.companyId.toString() !== companyId && currentUser.role !== ROLES.SUPER_ADMIN) {
			throw new UnauthorizedError();
		}
		const operator = new User({ ...operatorInput, companyId, role: ROLES.OPERATOR });
		return operator.save();
	},
	@permissionOnly(PermissionResources.SUBSCRIBER, 'EDIT')
	@uploadImage('operator.avatar')
	async updateOperator(root, { id, operator: operatorInput }, { models: { User } }) {
		const operator = await User.findOneOrError({ _id: id, role: ROLES.OPERATOR });
		operator.set(operatorInput);
		return operator.save();
	},
	@permissionOnly(PermissionResources.SUBSCRIBER, 'EDIT')
	async setOperatorIsActive(root, { id, isActive }, { models: { User } }) {
		const operator = await User.findOneOrError({ _id: id, role: ROLES.OPERATOR });
		operator.isActive = isActive;
		return operator.save();
	},
};
