import { rolesOnly } from '../../decorators';
import ROLES from '../../../config/constants/acl';
import { UnauthorizedError } from '../../../utils/errors';

export default {
	companies(_, args, {models: {Company}}) {
		return Company.find({});
	},

	@rolesOnly(ROLES.ADMIN)
	operators(root, {companiesIds}, {models: {User}, currentUser}) {
		if (currentUser.role !== ROLES.SUPER_ADMIN && !companiesIds.includes(currentUser.companyId.toString())) {
			throw new UnauthorizedError();
		}
		return User.find({ companyId: {$in: companiesIds}, role: ROLES.OPERATOR });
	},
	@rolesOnly(ROLES.ADMIN)
	operator(root, {id}, {models: {User}}) {
		return User.findOneOrError(id);
	},

	@rolesOnly(ROLES.ADMIN, ROLES.OPERATOR, ROLES.SUPER_ADMIN)
	company(root, {companyId}, {models: {Company}, currentUser}) {
		
		if (currentUser.role !== ROLES.SUPER_ADMIN && companyId !== currentUser.companyId.toString()) {
			throw new UnauthorizedError();
		}

		return Company.findById(companyId);
	},
};
