import getImageUrl from '../../../utils/get-image-url';

export default {
	company(doc, args, { models: { Company } }) {
		return Company.findById(doc.companyId);
	},
	avatar(doc, { size }) {
		return doc.avatar ? getImageUrl(doc.avatar, size, size) : null;
	},
};
