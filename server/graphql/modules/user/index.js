import schema from './schema.graphqls';

export { default as Query } from './Query';
export { default as Mutation } from './Mutation';
export { default as User } from './User';
export { default as SuperAdmin} from './SuperAdmin';
export { default as BaseUser } from './BaseUser';

export { schema };
