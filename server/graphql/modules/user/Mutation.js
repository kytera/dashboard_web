import { rolesOnly, authenticatedOnly } from '../../decorators';
import ROLES from '../../../config/constants/acl';
import { UnauthorizedError } from '../../../utils/errors';

export default {
	@rolesOnly(ROLES.SUPER_ADMIN)
	async setUserRole(root, {id, role}, {models: {User}}) {
		const user = User.findOneOrError(id);
		user.role = role;
		return user.save();
	},
	async setSuperAdminCompanies(root, {id, companiesIds}, {models: {User}}) {
		const user = await User.findOneOrError(id);
		user.companiesIds = companiesIds;
		return user.save();
	},
	async createSuperAdmin(root, {email, password, companiesIds}, {models: {User}}) {
		const user = new User({email, password, companiesIds, role: ROLES.SUPER_ADMIN});
		user.isPasswordTemporary = false;
		return user.save();
	},
	@authenticatedOnly
	async setUserPassword(root, {id, password}, {models: {User}, currentUser}) {
		if (id !== currentUser.id && currentUser.role !== ROLES.SUPER_ADMIN) {
			throw new UnauthorizedError();
		}

		const user = await User.findOneOrError(id);
		user.password = password;
		user.isPasswordTemporary = false;
		return user.save();
	},
	@rolesOnly(ROLES.ADMIN)
	async setUserPermissions(root, {id, permissions}, {models: {User}}) {
		const user = User.findOneOrError({id, role: ROLES.OPERATOR});
		user.permissions = permissions;
		return user.save();
	},
};
