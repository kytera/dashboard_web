import { authenticatedOnly } from '../../decorators';

export default {
	@authenticatedOnly
	currentUser(root, args, { currentUser }) {
		return currentUser;
	},
};
