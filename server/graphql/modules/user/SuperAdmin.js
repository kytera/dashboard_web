export default {
	companies(doc, args, {models: {Company}}) {
		return Company.find({_id: {$in: doc.companiesIds}});
	},

	isActive: ()=> true,
};
