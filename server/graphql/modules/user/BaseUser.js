import ROLES from '../../../config/constants/acl';

export default {
	__resolveType(doc) {
		switch (doc.role) {
		case (ROLES.SUPER_ADMIN):
			return 'SuperAdmin';
		case (ROLES.ADMIN):
			return 'User';
		case (ROLES.OPERATOR):
			return 'User';
		default:
			return 'User';
		}
	},
};
