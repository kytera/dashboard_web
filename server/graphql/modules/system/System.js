import { get } from 'lodash';

function convertKyteraDevice(device = {}) {
	return {
		status: device.status ? device.status.toUpperCase() : null,
		errorNumber: device['errorNum'],
		errorMessage: device['errorMessage'],
	};
}
export default {
	id(system) {
		return system.id || system._id;
	},
	generalSystem(system) {
		const kyteraGeneralSystem = get(system, 'kyteraSystem.system.service.generalSystem', {});
		return {
			id: 'generalSystem',
			...convertKyteraDevice(kyteraGeneralSystem),
		};
	},
	kyteraCloud(system) {
		const kyteraCloudSystem = get(system, 'kyteraSystem.system.service.kyteraCloud', {});
		return {
			id: 'kyteraCloud',
			...convertKyteraDevice(kyteraCloudSystem),
		};
	},
	cellularLink(system) {
		const kyteraCellularLink = get(system, 'kyteraSystem.system.service.cellularLink', {});
		return {
			id: 'cellularLink',
			...convertKyteraDevice(kyteraCellularLink),
		};
	},
	monitoringStation(system) {
		const kyteraMonitoringStation = get(system, 'kyteraSystem.system.service.monitoringStation', {});
		return {
			id: 'monitoringStation',
			...convertKyteraDevice(kyteraMonitoringStation),
		};
	},
	baseStation(system) {
		const kyteraBaseStation = get(system, 'kyteraSystem.system.service.baseStation', {});
		const baseStation = system.baseStation.toObject ? system.baseStation.toObject() : system.baseStation;
		return {
			id: 'baseStation',
			...baseStation,
			...convertKyteraDevice(kyteraBaseStation),
		};
	},
	wristband(system) {
		const kyteraWristband = get(system, 'kyteraSystem.system.service.wristband', {});
		const wristband = system.wristband.toObject ? system.wristband.toObject() : system.wristband;
		return {
			id: 'wristband',
			...wristband,
			...convertKyteraDevice(kyteraWristband),
		};
	},
	sensors(system) {
		if (!system.sensors) return null;

		return system.sensors.map((sensor) => {
			const kyteraSensor = get(system, `kyteraSystem.system.service.sensors.${sensor.serialNumber}`);
			const newSensor = sensor.toObject ? sensor.toObject() : sensor;
			return {
				...newSensor,
				...convertKyteraDevice(kyteraSensor),
			};
		});
	},
};
