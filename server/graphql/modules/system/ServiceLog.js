export default {
	id(root) {
		return root.id || root._id || null;
	},
	title(root) {
		return root.title || null;
	},
	frame(root) {
		return root.frame || false;
	},
	iconNumber(root) {
		return root.icon || null;
	},
};

