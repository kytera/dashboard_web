export default {
	id(root) {
		return root._id || root.id || root.location || null;
	},
	serialNumber(root) {
		return root.serialNumber || null;
	},
	status(root) {
		return root.status || 'DISABLED';
	},
	errorNumber(root) {
		return root.errorNumber || null;
	},
	errorMessage(root) {
		return root.errorMessage || null;
	},
	location(root) {
		return root.location || null;
	},
	deviceType(root) {
		return root.deviceType || null;
	},
};
