import schema from './schema.graphqls';

export { default as Query } from './Query';
export { default as Mutation } from './Mutation';
export { default as System } from './System';
export { default as Device } from './Device';
export { default as ServiceLog } from './ServiceLog';

export { schema };
