import { permissionOnly } from '../../decorators';
import { PermissionResources } from '../../../config/constants/acl';
import { addServiceLogUpdate, onSystemCreated } from '../../../services/kytera';


export default {
	@permissionOnly(PermissionResources.SERVICE, 'EDIT')
	async updateSystem(_, {subscriberId, system: systemInput}, {models: {System, Subscriber, InventoryTransaction}, currentUser}) {


		// 1. find subscriber by subscriberId
		const subscriber = await Subscriber.findOneOrError(subscriberId);


		// 2. find last system by subscriberId
		const oldSystem = await System.findOne({subscriberId}).sort({createdAt: -1}).lean();
		const oldBaseStation = oldSystem.baseStation;
		const oldWristband = oldSystem.wristband;
		const updatedAt = oldSystem && oldSystem.updatedAt;
		const lastSysid = oldSystem && oldSystem.sysid;

		if (!oldSystem) {
			throw new Error(`Subscriber (${subscriberId}) does not have a system`);
		}


		// 3. verify serial numbers
		const companyId = subscriber.dealerCompanyId;
		const {
			SENSOR,
			BASE_STATION,
			WRISTBAND,
		} = InventoryTransaction.DEVICE_TYPES;

		if (systemInput.baseStation.serialNumber) {
			await InventoryTransaction.verifySerialNumber(systemInput.baseStation.serialNumber, companyId, lastSysid, BASE_STATION);
		}
		if (systemInput.wristband.serialNumber) {
			await InventoryTransaction.verifySerialNumber(systemInput.wristband.serialNumber, companyId, lastSysid, WRISTBAND);
		}

		if (systemInput.sensors) {
			for (const sensor of systemInput.sensors) { // eslint-disable-line
				await InventoryTransaction.verifySerialNumber(sensor.serialNumber, companyId, lastSysid, SENSOR); // eslint-disable-line no-await-in-loop
			}
		}


		// 4. update the current system
		const foundAfterUpdate = await System.findOneAndUpdate(
			{
				subscriberId,
				updatedAt
			},
			{
				$set: {
					...systemInput,
					isStopped: false,
					createdByUserId: currentUser.id,
				}
			}, {new: true});

		if (!foundAfterUpdate) {
			throw new Error('Someone already updated the system');
		}


		// 5. add transactions for all new devices and sensors
		if (systemInput.baseStation.serialNumber) {
			await InventoryTransaction.addTransaction(systemInput.baseStation.serialNumber, companyId, lastSysid, lastSysid);
		}
		if (systemInput.wristband.serialNumber) {
			await InventoryTransaction.addTransaction(systemInput.wristband.serialNumber, companyId, lastSysid, lastSysid);
		}
		if (systemInput.sensors) {
			for (const sensor of systemInput.sensors) { // eslint-disable-line
				await InventoryTransaction.addTransaction(sensor.serialNumber, companyId, lastSysid, lastSysid); // eslint-disable-line
			}
		}

		// 6. Release old serial numbers
		if (oldBaseStation && oldBaseStation.serialNumber) {
			await InventoryTransaction.addTransaction(oldBaseStation.serialNumber, companyId, lastSysid, null); // eslint-disable-line
		}

		if (oldWristband && oldWristband.serialNumber) {
			await InventoryTransaction.addTransaction(oldWristband.serialNumber, companyId, lastSysid, null); // eslint-disable-line
		}

		if (oldSystem && oldSystem.sensors) {
			for (const sensor of oldSystem.sensors) { // eslint-disable-line
				if (!systemInput.sensors.find(s => s.serialNumber === sensor.serialNumber)) {
					await InventoryTransaction.addTransaction(sensor.serialNumber, companyId, lastSysid, null); // eslint-disable-line
				}
			}
		}

		try {
			await onSystemCreated(lastSysid);
		} catch (e) {
			console.error('Error while simulating onSystem created in kytera');
		}

		return Subscriber.findById(subscriberId);
	},
	@permissionOnly(PermissionResources.SERVICE, 'EDIT')
	async addServiceLogUpdate(_, {subscriberId, log}, {models: {System, ServiceLog}, currentUser}) {
		const lastSystem = await System.findOne({subscriberId}).sort({createdAt: -1});
		if (!lastSystem) {
			throw new Error('no system');
		}

		try {
			const result = await addServiceLogUpdate(lastSystem.sysid, log.message, currentUser._id);
			// Kytera API should return the log object... work around
			if (result.data === 'Success') {
				const createdLog = await ServiceLog.findOne({
					sysid: lastSystem.sysid,
					message: log.message
				}).sort({createdAt: -1});
				console.log(createdLog);
				return createdLog;
			}
		} catch (e) {
			console.error('error addServiceLogUpdate', e);
		}
		return null;
	},
};
