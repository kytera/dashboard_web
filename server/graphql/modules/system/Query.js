import { permissionOnly } from '../../decorators';
import { PermissionResources } from '../../../config/constants/acl';

export default {
	@permissionOnly(PermissionResources.SERVICE, 'VIEW')
	async system(_, { subscriberId }, { models: { System, UserStatus } }) {
		const system = await System.findOne({ subscriberId }).sort({ createdAt: -1 });

		if (!system || !system.baseStation) {
			return null;
		}

		const kyteraSystem = await UserStatus.findOne({ sysid: system.sysid }).sort({ time: -1 });
		return {
			...system.toObject(),
			kyteraSystem: kyteraSystem ? kyteraSystem.toObject() : {},
		};
	},
	@permissionOnly(PermissionResources.SERVICE, 'VIEW')
	async serviceLogs(_, { subscriberId, cursor, count = 200 }, { models: { System, ServiceLog } }) {
		const system = await System.findOne({ subscriberId }).sort({ createdAt: -1 });
		if (!system) {
			return {
				nextCursor: null,
				hasMore: false,
				logs: [],
			};
		}
		const sysid = system.sysid;
		const query = { sysid };
		if (cursor){
			query.timestamp = {'$lt': cursor};
		}
		const logs = await ServiceLog.find(query).sort({timestamp : -1}).limit(count);
		let hasMore = false;
		let nextCursor = null;
		if (logs && logs.length >= count) {
			hasMore = true;
			nextCursor = logs[count -1].timestamp;
		}
		return {
			nextCursor,
			hasMore,
			logs
		};
	},
};
