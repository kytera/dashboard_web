import { permissionOnly } from '../../decorators';
import { PermissionResources } from '../../../config/constants/acl';
import { openAlert, closeAlert } from '../../../services/kytera';
import { NoSystem } from '../../../utils/errors';

export default {
	@permissionOnly(PermissionResources.ALERT, 'VIEW')
	async openAlert(root, { subscriberId, alertId }, { currentUser, models: { System } }) {
		const system = await System.findOne({ subscriberId }).sort({ createdAt: -1 });

		if (!system) {
			throw new NoSystem();
		}

		return openAlert(system.sysid, alertId, currentUser._id);
	},
	@permissionOnly(PermissionResources.ALERT, 'VIEW')
	async closeAlert(root, { subscriberId, alertId, closeAlertReason, closeAlertExplanation }, { currentUser, models: { System, Subscriber } }) {
		console.log('Server: closeAlert mutation', subscriberId, alertId, closeAlertReason, closeAlertExplanation);
		const system = await System.findOne({ subscriberId }).sort({ createdAt: -1 });

		if (!system) {
			throw new NoSystem();
		}

		await closeAlert(system.sysid, alertId, currentUser._id, closeAlertReason, closeAlertExplanation)
			.catch((e) => {
				console.error('Error while trying to closeAlert with Kytera', e);
				throw e;
			});

		return Subscriber.findById(subscriberId);
	},
};
