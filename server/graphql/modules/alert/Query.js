import { permissionOnly } from '../../decorators';
import { PermissionResources } from '../../../config/constants/acl';
import { NoSystem } from '../../../utils/errors';

export default {
	@permissionOnly(PermissionResources.ALERT_HISTORY, 'VIEW')
	async alerts(_, { subscriberId }, { models: { DistressAlert, System } }) {
		const system = await System.findOne({ subscriberId }).sort({ createdAt: -1 }); // TODO: move receiving system to context or decorator

		if (!system) {
			return [];
		}

		return DistressAlert.find({ sysid: system.sysid, status: DistressAlert.STATUSES.CLOSED })
			.lean();
	},
	@permissionOnly(PermissionResources.ALERT, 'VIEW')
	async activeAlert(_, { subscriberId }, { models: { System, DistressAlert } }) {
		const system = await System.findOne({ subscriberId }).sort({ createdAt: -1 });

		if (!system) {
			throw new NoSystem();
		}

		return DistressAlert
			.findOne({ sysid: system.sysid, status: DistressAlert.STATUSES.OPEN })
			.sort({ timestamp: -1 })
			.lean();
	},
};
