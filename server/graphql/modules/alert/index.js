import schema from './schema.graphqls';

export { default as Query } from './Query';
export { default as Mutation } from './Mutation';
export { default as Alert } from './Alert';

export { schema };
