export default {
	id(doc) {
		return doc.id || doc._id;
	},
	timestamp(doc) {
		return doc.timestamp || new Date(new Date().getTime() - 100000).toISOString();
	},
	stopTimestamp(doc) {
		return doc.stopTimestamp || doc.stop_timestamp || null;
	},
	closeIcon(doc) {
		return doc.closeIcon || doc.close_icon || null;
	},
	closeMessage(doc) {
		return doc.closeMessage || doc.close_message || null;
	},
	status(doc) {
		return doc.status ? doc.status.toUpperCase() : null;
	},
	alertId(doc) {
		return doc.alertId;
	},
	icon(doc) {
		return doc.icon || doc.closeIcon;
	},
	closeAlertReason(doc) {
		return doc.closeAlertReason || null;
	},
	closeAlertExplanation(doc) {
		return doc.closeAlertExplanation || null;
	},
};
