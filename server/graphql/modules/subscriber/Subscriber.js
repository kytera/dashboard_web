import getImageUrl from '../../../utils/get-image-url';
import { permissionOnly } from '../../decorators/index';
import { PermissionResources } from '../../../config/constants/acl';
import { dashboardServiceStatusEnum, caregiverTypeEnum } from '../../utils/subscriber'

const DEFAULT_SYSTEM_STATUS = {
	openAlert: false,
	openNotification: false,
	openServiceCall: false,
};

export default {
	async caregivers(doc, args, {models: {Caregiver}}) {
		const result = await Caregiver.find({$and : [{subscriberId: doc._id},{caregiverType: caregiverTypeEnum.CAREGIVER}]});
		return result;
	},
	avatar(doc, {size}) {
		return doc.avatar && doc.avatar.public_id ? getImageUrl(doc.avatar, size, size) : null;
	},
	async status(doc, args, {models: {UserStatus, System}, sysIdToUserStatusCache, subscriberIdToSystemCache}) {
		// In case of cached data
		if (sysIdToUserStatusCache && subscriberIdToSystemCache) {
			const system = subscriberIdToSystemCache.get(doc.id);
			if (!system) {
				return DEFAULT_SYSTEM_STATUS;
			}

			return sysIdToUserStatusCache.get(system.sysid) || DEFAULT_SYSTEM_STATUS;
		} else {
			const system = await System.findOne({subscriberId: doc._id})
				.sort({createdAt: -1})
				.lean();

			if (!system) {
				return DEFAULT_SYSTEM_STATUS;
			}

			const userStatus = await UserStatus.findOne({sysid: system.sysid})
				.sort({time: -1})
				.lean();

			return userStatus || DEFAULT_SYSTEM_STATUS;

		}
	},
	@permissionOnly(PermissionResources.SERVICE, 'VIEW')
	async system(doc, args, {models: {System, UserStatus}, sysIdToUserStatusCache, subscriberIdToSystemCache}) {

		if (sysIdToUserStatusCache && subscriberIdToSystemCache) {
			const system = subscriberIdToSystemCache.get(doc.id);
			if (!system || !system.baseStation) {
				return null;
			}

			const kyteraSystem = sysIdToUserStatusCache.get(system.sysid);
			return {
				...system.toObject(),
				kyteraSystem: kyteraSystem ? kyteraSystem.toObject() : {},
			};
		} else {
			const system = await System.findOne({subscriberId: doc.id}).sort({createdAt: -1});

			if (!system || !system.baseStation) {
				return null;
			}

			const kyteraSystem = await UserStatus.findOne({sysid: system.sysid});
			return {
				...system.toObject(),
				kyteraSystem: kyteraSystem ? kyteraSystem.toObject() : {},
			};
		}
	},

	dashboardServiceStatus(doc) {
		return dashboardServiceStatusEnum[doc.dashboardServiceStatus] || dashboardServiceStatusEnum.suspended;
	},

	email(doc) {
		return doc.email || '';
	}
};
