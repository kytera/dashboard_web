import { rolesOnly, permissionOnly } from '../../decorators';
import ROLES, { PermissionResources } from '../../../config/constants/acl';
import { UnauthorizedError } from "../../../utils/errors";
import { caregiverTypeEnum } from '../../utils/subscriber';
import { omit } from 'lodash'

async function createSystemsCache(subscribers, System) {
	const subscribersIds = subscribers.map(s => s._id);
	const systems = await System.find({subscriberId: {$in: subscribersIds}})
		.sort({createdAt: 1})
		.lean();
	const systemsMap = new Map();
	systems.forEach(s => systemsMap.set(s.subscriberId.toString(), s));
	return systemsMap;
}

async function createUserStatusesCache(systemsMap, UserStatus) {
	const systemIds = [...systemsMap.values()].map(s => s.sysid);
	const userStatuses = await UserStatus.find({sysid: {$in: systemIds}})
		.sort({time: 1})
		.lean();
	const userStatusesMap = new Map();
	userStatuses.forEach(us => userStatusesMap.set(us.sysid, us));
	return userStatusesMap;
}

export default {
	@rolesOnly(ROLES.ADMIN, ROLES.OPERATOR, ROLES.SUPER_ADMIN)
	async subscribers(root, {companiesIds}, context) {
		const {models: {Subscriber, System, UserStatus}, currentUser} = context;

		if (currentUser.role !== ROLES.SUPER_ADMIN && !companiesIds.includes(currentUser.companyId.toString())) {
			throw new UnauthorizedError();
		}

		const subscribers = await Subscriber.find({$or: [{callCenterCompanyId: {$in: companiesIds}}, {dealerCompanyId: {$in: companiesIds}}]});

		const systemsMap = await createSystemsCache(subscribers, System);
		context.subscriberIdToSystemCache = systemsMap;
		context.sysIdToUserStatusCache = await createUserStatusesCache(systemsMap, UserStatus);

		return subscribers;
	},
	@permissionOnly(PermissionResources.SUBSCRIBER, 'VIEW')
	subscriber(root, {id}, {models: {Subscriber}}) {
		return Subscriber.findOneOrError(id);
	},

	@permissionOnly(PermissionResources.SUBSCRIBER, 'VIEW')
	async caregiversByType(root, {id, type}, {models: {Caregiver}}) {
		var caregivers = await Caregiver.find({$and: [{subscriberId : id}, {caregiverType: type}]})

		return caregivers;
	}
};
