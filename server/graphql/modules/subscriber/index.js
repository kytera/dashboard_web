import schema from './schema.graphqls';

export { default as Query } from './Query';
export { default as Mutation } from './Mutation';
export { default as Subscriber } from './Subscriber';
export { default as SubscriberStatusItem } from './SubscriberStatusItem';
export { default as SubscriberStatus } from './SubscriberStatus';

export { schema };
