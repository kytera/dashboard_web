export default {
	outdoor(doc) {
		return doc.outdoor || {};
	},
	sleep(doc) {
		return doc.sleep || {};
	},
	wear(doc) {
		return doc.wear || {};
	},
	distress_alert(doc) {
		return doc.distress_alert || {};
	},
	battery(doc) {
		return doc.battery || {};
	},
	home(doc) {
		return doc.home || {};
	},
	openAlert(doc) {
		return doc.system && doc.system.openAlert || false;
	},
	openNotification(doc) {
		return doc.system && doc.system.openNotification || false;
	},
	openServiceCall(doc) {
		return doc.system && doc.system.openServiceCall || false;
	}
};
