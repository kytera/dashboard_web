export default {
	icon(doc) {
		return doc.icon || null;
	},
	title(doc) {
		return doc.title || null;
	},
	message(doc) {
		return doc.message && typeof doc.message === 'string' ? doc.message : null;
	},
};
