import mongoose from 'mongoose';

import { rolesOnly, uploadImage, permissionOnly } from '../../decorators';
import ROLES, { PermissionResources } from '../../../config/constants/acl';
import { graphqlLogger as logger } from '../../../utils/logger';
import { changeDashboardServiceStatus, sendAppInvite, createSystem, newCaregiver } from "../../../services/kytera";
import { wasActivated } from '../../utils/subscriber'

const ObjectId = mongoose.Types.ObjectId

export default {
	@rolesOnly(ROLES.ADMIN, ROLES.OPERATOR)
	@uploadImage('subscriber.avatar')
	async createSubscriber(root, args, ctx) {
		let {
			subscriber: subscriberInput,
			dealerCompanyId,
		} = args;

		subscriberInput.dealerCompanyId = dealerCompanyId

		let sysVars = await createSystem(subscriberInput);

		subscriberInput = {...subscriberInput, ...sysVars.data}

		const {
			models: { Subscriber, System },
			currentUser,
		} = ctx;

		// 1. Create new subscriber
		const subscriber = new Subscriber({
			...subscriberInput,
			createdByUserId: currentUser.id,
		});
		await subscriber.save();

		return subscriber;
	},
	@permissionOnly(PermissionResources.SUBSCRIBER, 'EDIT')
	@uploadImage('subscriber.avatar')
	async updateSubscriber(root, { subscriber: subscriberInput, caregivers, caregiversType, id }, { models: { Subscriber, Caregiver } }) {

		let keep = caregivers.reduce((list, caregiver) => {
			if(caregiver.id) {
				list.push(new ObjectId(caregiver.id));
			}
			return list;
		},[]);

		await Caregiver.deleteMany({subscriberId: id, _id: {$nin: keep}, caregiverType: caregiversType});

		const subscriber = await Subscriber.findOneOrError(id);

		for(let i = 0; i < caregivers.length; i++) { // not foreaching here because of awaits.
			let caregiverInput = caregivers[i];

			// sending invites if necessary
			let activated = wasActivated(subscriber)


			let caregiver = null;
			if(caregiverInput.id) {
				caregiver = await Caregiver.findOneOrError(caregiverInput.id);
				caregiver.set(caregiverInput);
				await caregiver.save();
			} else {
				caregiver = new Caregiver({ ...caregiverInput, subscriberId: id, sysid: subscriber.sysid});
				await caregiver.save();
			}
            
            if (!caregiver.username){
                const result = await newCaregiver(caregiverInput.firstName, caregiverInput.middleName, caregiverInput.lastName, caregiverInput.email, caregiverInput.phones, caregiverInput.settings);
                caregiver.username = result.data.username;                
                logger.debug(`updateSubscriber: New caregiver result: ${JSON.stringify(result.data)}`);
                await caregiver.save();
            }
            
			if(activated && caregiverInput.sendInvite === true) {
				logger.debug(`updateSubscriber: sending invite to ${caregiverInput.email}`);
				sendAppInvite(caregiver.username);
				caregiver.sendInvite = false;
				await caregiver.save();
			}

		}

		subscriber.set(subscriberInput);
		return subscriber.save();
	},
	@permissionOnly(PermissionResources.SUBSCRIBER, 'EDIT')
	async setSubscriberDashboardServiceStatus(root, { subscriberId, operatorId, newServiceStatus }, { models: { Subscriber, Caregiver } }) {
		// Call keytera api
		try {
			const result = await changeDashboardServiceStatus(subscriberId,operatorId, newServiceStatus.toLowerCase());

			if(newServiceStatus === "ACTIVATE") {
				const caregivers = await Caregiver.find({subscriberId: subscriberId, sendInvite: true});

				caregivers.forEach(async (caregiver) => {
					logger.debug(`updateSubscriber 2: sending invite to ${caregiver.email}`)
					caregiver.sendInvite = false;

					const result = await newCaregiver(caregiverInput.firstName, caregiverInput.middleName, caregiverInput.lastName, caregiverInput.email, caregiverInput.phones, caregiverInput.settings)
					logger.debug(`updateSubscriber 2: New caregiver result: ${JSON.stringify(result.data)}`)
					caregiver.username = result.data.username;
					await caregiver.save();
					sendAppInvite(caregiver.username);
				})
			}

			logger.debug(`changeDashboardServiceStatus: For subscriber ${subscriberId}, result is ${result.data}`);
			return Subscriber.findOneOrError(subscriberId);
		} catch(ex) {
			console.log(ex);
			throw ex;
		}
	},
	@permissionOnly(PermissionResources.SETTINGS, 'EDIT')
	async updateSubscriberSettings(root, { settings, id }, { models: { Subscriber } }) {
		const subscriber = await Subscriber.findOneOrError(id);
		subscriber.set('settings', settings);
		return subscriber.save();
	},

};
