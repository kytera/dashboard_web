import { rolesOnly } from '../../decorators';
import ROLES from '../../../config/constants/acl';
import { CompanyNotFound } from '../../../utils/errors';

export default {
	@rolesOnly(ROLES.SUPER_ADMIN)
	async addNewDevice(_, { transaction: transactionInput }, { models: { Company, InventoryTransaction } }) {
		const { serialNumber, companyId, deviceType } = transactionInput;
		const company = await Company.findOne({ _id: companyId });

		if (!company) {
			throw new CompanyNotFound({
				message: `Company with the given companyId(${companyId}) was not found`,
			});
		}

		return InventoryTransaction.addNewDevice(serialNumber, companyId, deviceType);
	},
};
