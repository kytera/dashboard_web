export default {
	sysid(transaction) {
		return transaction.sysid || null;
	},
};
