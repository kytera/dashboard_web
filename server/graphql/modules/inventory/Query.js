import { groupBy } from 'lodash';
import { rolesOnly } from '../../decorators';
import ROLES from '../../../config/constants/acl';

export default {
	@rolesOnly(ROLES.ADMIN, ROLES.OPERATOR)
	async devices(_, { companyId }, { models: { InventoryTransaction } }) {
		const devices = await InventoryTransaction.find({ companyId }).sort({ createdAt: -1 });
		const grouped = groupBy(devices, d => d.serialNumber);
		return Object.keys(grouped).map((serialNumber) => {
			const arr = grouped[serialNumber];
			return arr[0];
		});
	},
};
