import schema from './schema.graphqls';

export { default as Query } from './Query';
export { default as Mutation } from './Mutation';
export { default as InventoryTransaction } from './InventoryTransaction';

export { schema };
