import { MockList } from 'graphql-tools';
import { sample } from 'lodash';

const UserMocks = {
	firstName: () => sample(['John', 'Robert', 'Christopher']),
	lastName: () => sample(['Doe', 'Mass', 'Volonsky']),
	createdAt: new Date(),
	role: 'ADMIN',
	isPasswordTemporary: false,
	company: {
		id: 'Company123',
		name: 'Dealer Company',
	},
	jobTitle: () => sample(['General', 'Operator', 'Admin', 'Novice Technician']),
	avatar: '/static/images/default-avatar.png',
};

export default {
	SuperAdmin: () => ({
		...UserMocks,
	}),
	Admin: () => ({
		...UserMocks,
	}),
	Operator: () => ({
		...UserMocks,
		operators: () => new MockList([0, 12]),
		subscribers: () => new MockList([0, 12]),
	}),
	Subscriber: () => ({
		...UserMocks,
	}),
	Caregiver: () => ({
		...UserMocks,
	}),
	Alert: () => ({
		title: sample(['John detected lying motionless in the bathroom', 'John detected lying motionless in the hallway']),
		summary: 'John woked up from 2 hours sleep, went to the bathroom, and then is detected to be very static at the bathroom',
		closeMessage: 'Paramedic closed the event',
		closeIcon: 'nurse',
		icon: 'alert_triangle',
	}),
	SubscriberActivityLog: () => ({
		room: sample(['KITCHEN', 'LIVING ROOM', 'BATHROOM']),
		icon: 'crawling',
		message: 'John detected crawling',
	}),
	AlertLog: () => ({
		icon: sample(['arrow_right', 'mail', 'phone']),
		room: sample(['KITCHEN', 'LIVING ROOM', 'BATHROOM']),
	}),
	NotificationLog: () => ({
		icon: sample(['wristband', 'home']),
		message: sample(['Out of home at irregular time', 'Low battery level']),
	}),
	Query: () => ({
		subscriberActivity: () => new MockList([0, 12]),
		alertLogs: () => new MockList([0, 20]),
		notifications: () => new MockList([0, 20]),
		allLogs: () => new MockList([2, 20]),
		subscribers: () => new MockList([8, 10]),
		alerts: () => new MockList([2, 10]),
	}),
	DateTime: () => new Date(+(new Date()) - Math.floor(Math.random() * 1000000000)),
};
