import { permissionOnly } from '../../decorators';
import { PermissionResources } from '../../../config/constants/acl';
import { NoSystem } from '../../../utils/errors';

export default {
	@permissionOnly(PermissionResources.ALERT, 'VIEW')
	async activeAlertLogs(_, {subscriberId, types}, {models: {DistressAlertLog, DistressAlert, System}}) {
		const system = await System.findOne({subscriberId}).sort({createdAt: -1});

		if (!system) {
			throw new NoSystem();
		}

		const activeAlert = await DistressAlert
			.findOne({sysid: system.sysid, status: DistressAlert.STATUSES.OPEN})
			.sort({timestamp: -1})
			.lean();

		if (!activeAlert) {
			throw new Error('cannot get the active alert ');
		}
		const query = {
			alertId: Number(activeAlert.alertId),
			sysid: system.sysid,
		};

		if (types) {
			query.type = types;
		}

		return DistressAlertLog.find(query).lean();
	},
	async alertLogs(_, {alertId, subscriberId, types}, {models: {System, DistressAlertLog}}) {
		const system = await System.findOne({subscriberId}).sort({createdAt: -1}).lean();

		if (!system) {
			throw new NoSystem();
		}

		const query = {alertId: Number(alertId), sysid: system.sysid};

		if (types && types.length) {
			query.type = types;
		}

		return DistressAlertLog.find(query)
			.sort({timestamp: 1})
			.lean();
	},
	@permissionOnly(PermissionResources.NOTIFICATIONS, 'VIEW')
	async notifications(_, {subscriberId, cursor, count = 100}, {models: {NotificationLog, System}}) {
		const system = await System.findOne({subscriberId}).sort({createdAt: -1});
		if (!system) {
			return {
				nextCursor: null,
				hasMore: false,
				logs: [],
			};
		}

		const sysid = system.sysid;
		const query = {sysid};
		if (cursor) {
			query.timestamp = {'$lt': cursor};
		}
		const notifications = await NotificationLog.find(query).sort({timestamp: -1}).limit(count);
		let hasMore = false;
		let nextCursor = null;
		if (notifications && notifications.length >= count) {
			hasMore = true;
			nextCursor = notifications[count - 1].timestamp;
		}
		return {
			nextCursor,
			hasMore,
			logs: notifications
		};
	},
};
