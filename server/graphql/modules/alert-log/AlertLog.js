export default {
	id(doc) {
		return doc.id || doc._id;
	},
	title(doc) {
		return doc.title || null;
	},
	type(doc) {
		return doc.type || null;
	},
	room(doc) {
		return doc.room || null;
	},
	timestamp(doc) {
		return doc.timestamp || null;
	},
	alertId(doc) {
		return doc.alertId;
	},
};
