export default {
	id(doc) {
		return doc.id || doc._id;
	},
};
