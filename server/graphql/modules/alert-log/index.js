import schema from './schema.graphqls';

export { default as Query } from './Query';
export { default as Mutation } from './Mutation';
export { default as AlertLog } from './AlertLog';
export { default as Notification } from './Notification';

export { schema };
