import { permissionOnly } from '../../decorators';
import { PermissionResources } from '../../../config/constants/acl';
import { updateDistressAlert } from '../../../services/kytera';
import { NoSystem } from '../../../utils/errors';

export default {
	@permissionOnly(PermissionResources.ALERT, 'VIEW')
	async createUpdateLog(root, { subscriberId, alertId, log: logInput }, { currentUser, models: { System } }) {
		const system = await System.findOne({ subscriberId }).sort({ createdAt: -1 });

		if (!system) {
			throw new NoSystem();
		}

		return updateDistressAlert(system.sysid, alertId, currentUser._id, logInput.message, logInput.icon)
			.catch((e) => {
				console.error('Error when updating alert log', e);
				throw e;
			});
	},
};
