import schema from './schema.graphqls';

export { GraphQLDate as Date, GraphQLDateTime as DateTime } from 'graphql-iso-date';
export { GraphQLUpload as Upload } from 'apollo-upload-server';
export { default as Mutation } from './Mutation';
export { default as Query } from './Query';

export { schema };
