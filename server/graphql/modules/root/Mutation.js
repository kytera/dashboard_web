import userControllers from '../../../controllers/user-controllers';

export default {
	async login(root, { email, password }, { models }) {
		const loginResponse = await userControllers.login(models, email, password);
		return loginResponse;
	},
};
