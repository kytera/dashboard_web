import {getSettings} from '../../../services/keytera-config'
import { rolesOnly } from '../../decorators';
import ROLES from '../../../config/constants/acl';
import { kyteraInfo } from '../../../services/kytera';

export default {
  @rolesOnly(ROLES.ADMIN, ROLES.SUPER_ADMIN)
  kyteraSettings(root, {}, {models}) {
    return getSettings();
  },

  @rolesOnly(ROLES.ADMIN, ROLES.SUPER_ADMIN)
	async kyteraInfo(root, {subscriberId}, {models : {Subscriber}}) {
    try {

      const susbcriber =  await Subscriber.findOneOrError(subscriberId);

      const info = await kyteraInfo(susbcriber.sysid);

      if(!info || !info.data || !Array.isArray(info.data)) return []

      const result = info.data.map(section => {
        section.values = section.values.map(value => {
          value.tooltip = value.tooltip || ''
          return value
        })
        return section
      })

      return result;
	  } catch(ex) {
      console.log(ex)
      throw ex;
    }
  }
};
