export default function getProjection(fieldASTs) {
	return fieldASTs.selectionSet.selections.reduce((projections, selection) => {
		const newProjects = projections;
		newProjects[selection.name.value] = 1;
		return newProjects;
	}, {});
}
