
export const dashboardServiceStatusEnum = {
	pending_installation: 'pending_installation',
  pending_activation: 'pending_activation',
	active: 'active',
  suspended: 'suspended',
  archived: 'archived'
};

export const caregiverTypeEnum = {
  CAREGIVER: 'CAREGIVER',
  HOMECARE: 'HOMECARE',
  KYTERA: 'KYTERA'
}

export const wasActivated = (subscriber)  => {
  return [dashboardServiceStatusEnum.active, dashboardServiceStatusEnum.suspended, dashboardServiceStatusEnum.archived].includes(subscriber.dashboardServiceStatus)
}
