const adminEmails = [
	'hlandao@gmail.com', 'omri.klinger@gmail.com',
];

export const isAdmin = user => user.email && (adminEmails.indexOf(user.email) !== -1);
export default isAdmin;

