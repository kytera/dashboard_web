import { UnauthorizedError } from '../../utils/errors';
import ROLES, { PermissionResources } from '../../config/constants/acl';
import { graphqlLogger as logger } from '../../utils/logger';

export default (resource, action) => function permissionOnly(target, name, descriptor) {
	if (!Object.values(PermissionResources).includes(resource)) {
		throw new Error('Unknown permission resource passed to the decorator factory');
	}
	if (!['VIEW', 'EDIT'].includes(action)) {
		throw new Error('Unknown permission action passed to the decorator factory');
	}

	const method = descriptor.value;

	/* eslint-disable no-param-reassign */
	descriptor.value = async function checkPermission(root, params, ctx) {
		// Check user permission. Admin can do everything.
		if (
			ctx.currentUser
			&& ctx.currentUser.permissions
			&& (
				ctx.currentUser.role === ROLES.ADMIN
				|| ctx.currentUser.role === ROLES.SUPER_ADMIN
				|| ctx.currentUser.permissions.some(item => item.resource === resource && item.action === action)
			)
		) {
			return method.call(this, root, params, ctx);
		}
		logger.error(ctx);
		throw new UnauthorizedError({ data: { requiredPermission: { resource, action } } });
	};
};
