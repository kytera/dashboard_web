import { UnauthorizedError } from '../../utils/errors';
import ROLES from '../../config/constants/acl';
import { graphqlLogger as logger } from '../../utils/logger';

export default (...roles) => function rolesOnly(target, name, descriptor) {
	const method = descriptor.value;

	/* eslint-disable no-param-reassign */
	descriptor.value = async function checkRole(root, params, ctx) {
		// Check user role. Super admin can do everything.
        console.log("ctx.currentUser.role");
        console.log(ctx.currentUser.role);
        
		if (!ctx.currentUser ||
			((!roles.includes(ctx.currentUser.role) && ctx.currentUser.role != ROLES.SUPER_ADMIN))
		) {
			logger.error(ctx);
			throw new UnauthorizedError({ data: { requiredRole: roles } });
		}
		return method.call(this, root, params, ctx);
	};
};
