import cloudinary from 'cloudinary';
import { get, set } from 'lodash';

import { UploadFileError } from '../../utils/errors';
import { graphqlLogger as logger } from '../../utils/logger';


const uploadStreamToCloudinary = (stream) => {
	return new Promise((resolve, reject) => {
		const uploadStream = cloudinary.v2.uploader.upload_stream((error, uploadedImage) => {
			if (error) {
				logger.error('Error uploading file', error.message);
				reject(UploadFileError({message: error.message}));

			} else {
				logger.debug('Image uploaded: %s', uploadedImage.url);
				resolve(uploadedImage);
			}
		});
		stream.pipe(uploadStream);
	});
};

export default argsPath => function uploadImage(target, name, descriptor) {
	const method = descriptor.value;

	/* eslint-disable no-param-reassign */
	descriptor.value = async function upload(root, args, ctx) {
		const imagePromise = get(args, argsPath);
		if (imagePromise) {
			const {stream, filename, mimetype, encoding} = await imagePromise;

			// upload image to cloudinary if exists
			const uploadedImage = await uploadStreamToCloudinary(stream);
			set(args, argsPath, uploadedImage);

			// call resolver then delete uploaded image if error thrown
			try {
				return method.call(this, root, args, ctx);
			} catch (error) {
				await cloudinary.uploader.destroy(uploadedImage.public_id);
				throw error;
			}

		} else {
			return method.call(this, root, args, ctx);
		}
	};
};

