import { UnauthenticatedError } from '../../utils/errors';

export default function authenticatedOnly(target, name, descriptor) {
	const method = descriptor.value;

	/* eslint-disable no-param-reassign */
	descriptor.value = async function authorize(root, params, ctx) {
		if (!ctx.currentUser) {
			throw new UnauthenticatedError();
		}
		return method.call(this, root, params, ctx);
	};
}
