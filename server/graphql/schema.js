import { addCatchUndefinedToSchema, addErrorLoggingToSchema, addMockFunctionsToSchema } from 'graphql-tools';

import { graphqlLogger as logger } from '../utils/logger';
import mocks from './modules/mocks';
import makeExecutableSchemaWithModules from './utils/make-executable-schema-with-modules';

import * as rootModule from './modules/root';
import * as userModule from './modules/user';
import * as companyModule from './modules/company';
import * as subscribersModule from './modules/subscriber';
import * as systemModule from './modules/system';
import * as alertLogModule from './modules/alert-log';
import * as alertModule from './modules/alert';
import * as inventoryModule from './modules/inventory';

const executableSchema = makeExecutableSchemaWithModules(
	[
		rootModule,
		userModule,
		companyModule,
		subscribersModule,
		systemModule,
		alertModule,
		inventoryModule,
		alertLogModule,
	],
	{
		resolverValidationOptions: {
			requireResolversForNonScalar: false,
		},
	},
);

addErrorLoggingToSchema(executableSchema, {
	log: (e) => {
		logger.error(e);
	},
});

if (process.env.NODE_ENV !== 'production') {
	addCatchUndefinedToSchema(executableSchema);
	if (process.env.USE_MOCKS) {
		addMockFunctionsToSchema({ schema: executableSchema, mocks });
	}
}

export default executableSchema;

