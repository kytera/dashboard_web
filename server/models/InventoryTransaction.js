import mongooseTimestamp from 'mongoose-timestamp';
import { Model, Plugin } from 'mongoose-model-decorators';
import mongoose from 'mongoose';
import {
	SerialNumberNotFound,
	SerialNumberNotAvailable,
	SerialNumberDeviceTypeIncorrect,
	IncorrectSysidForTransaction,
	InputMissingError,
	SerialNumberIsAlreadyTaken,
} from '../utils/errors';
import Subscriber from './Subscriber';

/* eslint-disable max-len */

const DEVICE_TYPES = {
	SENSOR: 'SENSOR',
	WRISTBAND: 'WRISTBAND',
	BASE_STATION: 'BASE_STATION',
	CHARGER: 'CHARGER',
};

@Model
@Plugin(mongooseTimestamp)
export default class InventoryTransaction {
	static collection = 'inventory_transaction'

	static DEVICE_TYPES = DEVICE_TYPES;

	static schema = {
		companyId: mongoose.SchemaTypes.ObjectId,
		serialNumber: String,
		deviceType: {
			type: String,
			enum: Object.keys(DEVICE_TYPES),
		},
		sysid: String,
	}

	static configureSchema (schema) {
		schema.index({ companyId: 1, serialNumber: 1 }, { unique: false })
	}

	static verifySerialNumber = async (serialNumber, companyId, lastSysid, deviceType) => {
		const transaction = await InventoryTransaction.findOne({ serialNumber, companyId }).sort({ createdAt: -1 });

		if (!transaction) {
			throw new SerialNumberNotFound({
				message: `Couldn't find any device with serial ${serialNumber}`,
				data: {
					serialNumber,
					lastSysid,
					deviceType,
				},
			});
		}
		if (transaction.sysid && transaction.sysid !== lastSysid) {
			const subscriber = await Subscriber.findOne({ sysid: transaction.sysid });
			throw new SerialNumberNotAvailable({
				message: `${deviceType} ${serialNumber} is already associated with subscriber ${subscriber.firstName} ${subscriber.middleName ? subscriber.middleName : ''} ${subscriber.lastName}`,
				data: {
					serialNumber,
					lastSysid,
					deviceType,
				},
			});
		}
		if (transaction.deviceType !== deviceType) {
			throw new SerialNumberDeviceTypeIncorrect({
				message: `The serialNumber(${serialNumber}) of company(${companyId}) is defined for type(${transaction.deviceType}) and not for the requested type(${deviceType})`,
				data: {
					serialNumber,
					lastSysid,
					deviceType,
				},
			});
		}
	}

	static addNewDevice = async (serialNumber, companyId, deviceType) => {
		if (!serialNumber) {
			throw new InputMissingError({
				message: 'serialNumber is required',
			});
		}
		if (!companyId) {
			throw new InputMissingError({
				message: 'companyId is required',
			});
		}
		if (!deviceType) {
			throw new InputMissingError({
				message: 'deviceType is required',
			});
		}

		const query = {
			serialNumber,
			companyId,
			deviceType,
		}

		const transaction = await InventoryTransaction.findOne(query);

		if (transaction) {
			throw new SerialNumberIsAlreadyTaken({
				message: `Cannot add new device because serialNumber(${serialNumber}) is already taken`,
				data: {
					serialNumber,
				},
			});
		}

		return InventoryTransaction.findOneAndUpdate(query, { $set: query }, { upsert: true, new: true});
	}

	static addTransaction = async (serialNumber, companyId, lastSysid, newSysid) => {
		if (!serialNumber) {
			throw new InputMissingError({
				message: 'serialNumber is required',
			});
		}
		if (!companyId) {
			throw new InputMissingError({
				message: 'companyId is required',
			});
		}

		const transaction = await InventoryTransaction.findOne({ serialNumber, companyId }).sort({ createdAt: -1 });

		if (!transaction) {
			throw new SerialNumberNotFound({
				message: `serialNumber(${serialNumber}) is not found for companyId(${companyId})`,
				data: {
					serialNumber,
					companyId,
				},
			});
		}

		if (transaction.sysid && transaction.sysid !== lastSysid) {
			throw new IncorrectSysidForTransaction({
				message: `Device with serialNumber(${serialNumber}) is taken. The provided sysid(${lastSysid}) does not match the most recent sysid(${transaction.sysid})`,
				data: {
					serialNumber,
					lastSysid,
					newSysid,
				},
			});
		}

		const newTransaction = new InventoryTransaction({
			serialNumber,
			companyId,
			deviceType: transaction.deviceType,
			sysid: newSysid,
		});
		return newTransaction.save();
	}
}
