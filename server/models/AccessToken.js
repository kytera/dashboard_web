import mongoose, { Schema } from 'mongoose';
import uuid from 'node-uuid';
import jwt from 'jsonwebtoken';

import logger from '../utils/logger';
import generateJwtAccessToken from '../services/auth/generate-jwt-access-token';
import { AccessTokenInvalidError, JWTVerifyFailedError } from '../utils/errors';

const ACCESS_TOKEN_COLLECTION = 'access_tokens';

const AccessTokenSchema = new Schema({
	userId: Schema.Types.ObjectId,
	tokenRef: String,
	isValid: Boolean,
	createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date, default: Date.now },
});

class AccessToken {
	static async createJWTAccessToken(userId) {
		const tokenRef = uuid.v1();
		const jwtToken = generateJwtAccessToken(userId, tokenRef);

		const accessToken = {
			userId,
			tokenRef,
			isValid: true,
			createdAt: Date.now(),
		};

		await new this(accessToken).save();
		return jwtToken;
	}


	/**
   * get user with a valid and not expired loginMutation token
   */
	static async validateJWTToken(jwtPayload) {
		logger.debug('Validating access token', jwtPayload.tokenRef);

		const query = {
			userId: jwtPayload.userId,
			tokenRef: jwtPayload.tokenRef,
			isValid: true,
		};

		const loginTokenResult = await this.findOne(query).exec();

		if (!loginTokenResult) {
			throw new AccessTokenInvalidError({
				data: { context: 'validateToken' },
			});
		}
	}

	static async invalidateToken(userId, token) {
		let decodedToken = {};

		try {
			decodedToken = jwt.verify(token, process.env.SECRET);
		} catch (e) {
			throw new JWTVerifyFailedError();
		}

		const query = {
			tokenRef: decodedToken.tokenRef,
			userId,
		};

		return this.update(query, { $set: { isValid: false } });
	}
}

AccessTokenSchema.loadClass(AccessToken);
export default mongoose.model(ACCESS_TOKEN_COLLECTION, AccessTokenSchema);

