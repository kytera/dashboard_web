import { Model } from 'mongoose-model-decorators';

import { getKyteraDBConnection } from '../services/kytera';

const TYPES = {
	LOG: 'log',
	UPDATE: 'update',
	ACTIVITY: 'activity',
};

@Model({ connection: getKyteraDBConnection() })
export default class Log {
	static collection = 'distress_alerts_logs'

	static TYPES = TYPES

	static schema = {
		title: {
			type: String,
			default: null,
		},
		message: {
			type: String,
			default: null,
		},
		timestamp: Date,
		generated_timestamp: Date,
		icon: Number,
		alertId: Number,
		type: {
			type: String,
			enum: Object.values(TYPES),
		},
		room: String,
		sysid: String,
		frame: {
			type: Boolean,
			default: false,
		},
	}
}
