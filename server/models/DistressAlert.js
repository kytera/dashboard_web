import { Model } from 'mongoose-model-decorators';
import mongoose from 'mongoose';


import { getKyteraDBConnection } from '../services/kytera';

const STATUSES = {
	CLOSED: 'closed',
	OPEN: 'open',
};

@Model({ connection: getKyteraDBConnection() })
export default class DistressAlert {
	static collection = 'distress_alerts'

	static STATUSES = STATUSES

	static schema = {
		title: {
			type: String,
			default: null,
		},
		summary: {
			type: String,
			default: null,
		},
		timestamp: Date,
		alertId: Number,
		subscriberId: mongoose.SchemaTypes.ObjectId,
		sysid: String,
		status: {
			type: String,
			enum: Object.values(STATUSES),
		},
		icon: String,
		closeIcon: {
			type: Number,
			default: null,
		},
		close_message: {
			type: String,
			default: null,
		},
		stop_timestamp: Date,
	}
}
