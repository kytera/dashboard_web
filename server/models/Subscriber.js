import mongooseTimestamp from 'mongoose-timestamp';
import { Model, Plugin, post, pre } from 'mongoose-model-decorators';
import mongoose from 'mongoose';
import { UserNotFoundError } from '../utils/errors';
import System from './System';
import { dashboardServiceStatusEnum } from '../graphql/utils/subscriber'

@Model({
	runSettersOnQuery: true,
	toObject: {
		virtuals: true,
	},
})
@Plugin(mongooseTimestamp)
export default class Subscriber {
	static collection = 'subscribers';

	static schema = {
		dealerCompanyId: {
			type: mongoose.SchemaTypes.ObjectId,
		},
		createdByUserId: {
			type: mongoose.SchemaTypes.ObjectId,
		},
		sysid: {
			type: String,
		},
		serviceStatus: {
			type: String
		},
		env: {
			type: String
		},
		agencies: [{
			agency_type: { type: String, default: null },
			number: { type: String }
		}],
		address: {
			country: { type: String, default: null },
			street: { type: String, default: null },
			streetOptional: { type: String, default: null },
			city: { type: String, default: null },
			county: { type: String, default: null },
			state: { type: String, default: null },
			zipCode: { type: String, default: null },
			additional: { type: String, default: null },
			timezone: {	type: String,	default: null	}
		},
		email: {
			type: String,
			lowercase: true,
		},
		firstName: {
			type: String,
			default: null,
		},
		middleName: {
			type: String,
			default: null
		},
		lastName: {
			type: String,
			default: null,
		},
		phones: [{
			type: String,
			default: null
		}],
		homePhone: {
			type: String,
			default: null
		},
		receivedDate: {
			type: Date,
			default: null
		},
		referralSource: {
			type: String,
			default: null
		},
		medicalHistory: {
			type: String,
			default: null
		},
		avatar: {
			type: Object,
		},
		birthDate: {
			type: Date,
			default: null,
		},
		gender: {
			type: String,
			enum: ['MALE', 'FEMALE'],
		},
		dashboardServiceStatus: {
			type: String,
			enum: Object.values(dashboardServiceStatusEnum),
			default: dashboardServiceStatusEnum.pending_installation
		},
		settings: {
			UNWEAR_TIME: {
				type: Number,
				default: 15,
			},
		},
		medicalAlarmServiceOn: {
			type: Boolean,
			default: true,
		},
		medicalAlarmService: {
			type: {
				displayName: String,
				siteGroupPrefix: String,
				siteGroupId:Number
			},
			required: false,
			default: null
		},
		wellnessService: {
			type : {
				displayName: String,
				additionalInfo: String
			},
			required: false,
			default: null
		}
	};

	/**
	 * @description takes user id OR find term, and throws UserNotFoundError if user not found
	 * @param term {ObjectID|object}
	 * @param args
	 * @returns {Promise.<*>}
	 */
	static async findOneOrError(term, ...args) {
		const user = (typeof term === 'string')
			? await this.findById(term, ...args)
			: await this.findOne(term, ...args);
		if (!user) {
			throw new UserNotFoundError();
		}
		return user;
	}

	@pre('save')
	setWasNew(next) {
		this.wasNew = this.isNew;
		next();
	}

	@post('save')
	async createSystem() {
		if (this.wasNew) {
			const systemInput = {
				subscriberId: this._id,
				sysid: this.sysid,
                env: this.env,
			};
			systemInput.createdByUserId = this.createdByUserId;
			const newSystem = new System(systemInput)
			await newSystem.save();
		}
	}
}
