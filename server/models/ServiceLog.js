import { Model } from 'mongoose-model-decorators';
import mongoose from 'mongoose';

import { getKyteraDBConnection } from '../services/kytera';

@Model({ connection: getKyteraDBConnection() })
export default class ServiceLog {
	static collection = 'service_logs'

	static schema = {
		title: {
			type: String,
			default: null,
		},
		message: {
			type: String,
			default: null,
		},
		timestamp: Date,
		generatedTimestamp: Date,
		icon: {
			type: String,
			default: null,
		},
		sysid: String,
		frame: {
			type: Boolean,
			default: false,
		},
		operatorId: mongoose.SchemaTypes.ObjectId,
	}
}
