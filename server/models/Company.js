import mongooseTimestamp from 'mongoose-timestamp';
import { Model, Plugin } from 'mongoose-model-decorators';

import COMPANY_TYPES from '../config/constants/companies';

@Model
@Plugin(mongooseTimestamp)
export default class Company {
	static collection = 'companies'

	static runSettersOnQuery = true

	static COMPANY_TYPES = COMPANY_TYPES

	static schema = {
		name: { type: String },
		type: {
			type: String,
			enum: Object.values(COMPANY_TYPES),
		},
		medicalAlarmServices: [{ 
			displayName: String,
			siteGroupPrefix: String,
			siteGroupId:Number 
		}],
		wellnessServices: [{
			displayName: String,
			additionalInfo: String
		}]
	}
}
