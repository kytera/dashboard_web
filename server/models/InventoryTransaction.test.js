import mongoose from 'mongoose';
import { Mockgoose } from 'mockgoose';

import InventoryTransaction from './InventoryTransaction';

let mockgoose = new Mockgoose(mongoose);

const companyId1 = mongoose.Types.ObjectId('5968ed4bd13d5b3ecbff9f18');
const companyId2 = mongoose.Types.ObjectId('5968ed4bd13d5b3ecbff9f18');

const transactionData1 = {
	companyId: companyId1,
	serialNumber: 'baseStation1',
	deviceType: 'BASE_STATION',
};
const transactionData2 = {
	companyId: companyId1,
	serialNumber: 'sensor1',
	deviceType: 'SENSOR',
	sysid: 'sysid1'
};
const transactionData3 = {
	companyId: companyId2,
	serialNumber: 'sensor2',
	deviceType: 'SENSOR',
};


beforeAll((done) => {
	mockgoose.prepareStorage().then(function() {
		mongoose.Promise = Promise;
		mongoose.connect('mongodb://example.com/TestingDB', function(err) {
			done(err);
		});
	});
});

describe('InventoryTransaction', () => {

	it('should add transactions for new devices', () => {
		const transactionsData = [transactionData1, transactionData2, transactionData3];
		const promises = transactionsData.map(data => {
			return InventoryTransaction.addNewDevice(data.serialNumber, data.companyId, data.deviceType);
		});

		return Promise.all(promises)
			.then(transactions => {
				transactions.forEach((transaction, index) => {
					expect(transaction.companyId).toEqual(transactionsData[index].companyId);
					expect(transaction.serialNumber).toEqual(transactionsData[index].serialNumber);
					expect(transaction.deviceType).toEqual(transactionsData[index].deviceType);
				});
			});
	});

	it('should add a transaction for an existing device', async () => {
		const {
			serialNumber,
			companyId,
			sysid,
		} = transactionData2;

		await InventoryTransaction.addTransaction(serialNumber, companyId, null, sysid);
	});


	it('should verify a valid inventory transaction for non-assigned sys', async () => {
		const {
			serialNumber,
			companyId,
			deviceType,
		} = transactionData1;

		await InventoryTransaction.verifySerialNumber(serialNumber, companyId, null, deviceType);
	});

	it('should verify a valid inventory transaction for the last sysid', async () => {
		const {
			serialNumber,
			companyId,
			deviceType,
			sysid
		} = transactionData2;

		await InventoryTransaction.verifySerialNumber(serialNumber, companyId, sysid, deviceType);
	});

	it('should throw if serialNumber is already taken', async () => {
		const {
			serialNumber,
			companyId,
			deviceType,
		} = transactionData2;

		await expect(InventoryTransaction.verifySerialNumber(serialNumber, companyId, null, deviceType)).rejects.toBeDefined()
	});

	it('should throw if serialNumber is not exists', async () => {
		const {
			companyId,
			deviceType,
		} = transactionData2;

		await expect(InventoryTransaction.verifySerialNumber('not a serial', companyId, null, deviceType)).rejects.toBeDefined()
	});

	it('should throw if deviceType not fit', async () => {
		const {
			serialNumber,
			companyId,
		} = transactionData1;

		await expect(InventoryTransaction.verifySerialNumber(serialNumber, companyId, null, 'SENSOR')).rejects.toBeDefined()
	});
});
