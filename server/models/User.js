import mongooseTimestamp from 'mongoose-timestamp';
import { Model, pre, Plugin, post } from 'mongoose-model-decorators';
import mongoose from 'mongoose';

import hashPassword from '../utils/hash-password';
import comparePasswords from '../utils/compare-passwords';
import { DuplicateEmailError, DocumentNotFoundError } from '../utils/errors';
import logger from '../utils/logger';
import ROLES from '../config/constants/acl';


@Model({
	runSettersOnQuery: true,
	toObject: {
		virtuals: true,
	},
})
@Plugin(mongooseTimestamp)
export default class User {
	static collection = 'users';

	static ROLES = ROLES;

	static schema = {
		email: {
			type: String,
			lowercase: true,
			index: true,
			unique: true,
		},
		password: { type: String },
		passwordHash: { type: String },
		role: {
			type: String,
			enum: Object.values(ROLES),
			default: ROLES.USER,
		},
		isActive: {
			type: Boolean,
			default: true,
		},
		firstName: {
			type: String,
			default: null,
		},
		lastName: {
			type: String,
			default: null,
		},
		phones: [{
			type: String,
			default: null
		}],
		avatar: {
			type: Object,
		},
		isPasswordTemporary: {
			type: Boolean,
			default: true,
		},
		canEditSubscribers: {
			type: Boolean,
			default: true,
		},
		companyId: {
			type: mongoose.SchemaTypes.ObjectId,
		},
		companiesIds: {
			type: [mongoose.SchemaTypes.ObjectId],
		},
		permissions: {
			type: Array,
			default: [],
		},
		jobTitle: {
			type: String,
			default: null,
		},
	}

	get phone() {
		return this.phones && this.phones[0];
	}

	/**
	 * @description returns false if user doesn't exist OR password is wrong because of security
	 * @param email
	 * @param password
	 * @returns {Promise.<boolean>}
	 */
	static async findByCredentials(email, password) {
		const user = await this.findOne({ email });
		if (!user) return false;
		return await comparePasswords(user.passwordHash, password) ? user : false;
	}

	/**
	 * @description takes user id OR find term, and throws UserNotFoundError if user not found
	 * @param term {ObjectID|object}
	 * @param args
	 * @returns {Promise.<*>}
	 */
	static async findOneOrError(term, ...args) {
		const user = (typeof term === 'string')
			? await this.findById(term, ...args)
			: await this.findOne(term, ...args);
		if (!user) {
			throw new DocumentNotFoundError();
		}
		return user;
	}

	@post('save')
	handleDuplicateEmailError(error, { email }, next) {
		if (error.name === 'MongoError' && error.code === 11000) {
			next(new DuplicateEmailError({ data: { email } }));
		} else {
			next(error);
		}
	}

	@pre('validate')
	hashPassword(next) {
		if (this.isModified('password') && this.password) {
			hashPassword(this.password).then((passwordHash) => {
				this.passwordHash = passwordHash;
				this.password = undefined;
				next();
			}).catch((e) => {
				logger.error(e);
				next();
			});
		} else {
			next();
		}
	}
}
