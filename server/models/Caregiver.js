import mongooseTimestamp from 'mongoose-timestamp';
import { Model, Plugin, pre } from 'mongoose-model-decorators';
import mongoose from 'mongoose';
import { DocumentNotFoundError } from '../utils/errors';
import { caregiverTypeEnum } from '../graphql/utils/subscriber';

// import logger from '../utils/logger';
// import ROLES from '../config/constants/acl';

@Model({
	runSettersOnQuery: true,
	toObject: {
		virtuals: true,
	}
})

@Plugin(mongooseTimestamp)
export default class Caregiver {
	static collection = 'caregivers';

	static schema = {
		subscriberId: {
			type: mongoose.SchemaTypes.ObjectId,
		},
		sysid: {
			type: String
		},
		username: {
			type: String
		},
		email: {
			type: String,
			lowercase: true,
		},
		firstName: {
			type: String,
			default: null,
		},
		middleName: {
			type: String,
			default: null,
		},
		lastName: {
			type: String,
			default: null,
		},
		phones: [{
			type: String,
			default: null,
		}],
		isPrimary: {
			type: Boolean,
			default: false,
		},
		type: {
			type: String,
			default: null
		},
		caregiverType: {
			type: String,
			enum: Object.keys(caregiverTypeEnum),
			default:caregiverTypeEnum.CAREGIVER
		},
		settings: [{
			settingType: {
				type: String,
				required: true
			},
			value: {
				type: Boolean,
				default: true,
			}
		}],
		relation: {
			type: String,
			default: null
		},
		sendInvite: {
			type: Boolean,
			default: false,
		},
		isActivated: {
			type: Boolean,
			default: false,
		}
	}

	@pre('validate')
	setIsPrimaryOnCaregiver(next) {
		return next();
	}

	/**
	 * @description takes user id OR find term, and throws UserNotFoundError if user not found
	 * @param term {ObjectID|object}
	 * @param args
	 * @returns {Promise.<*>}
	 */
	static async findOneOrError(term, ...args) {
		const user = (typeof term === 'string')
			? await this.findById(term, ...args)
			: await this.findOne(term, ...args);
		if (!user) {
			throw new DocumentNotFoundError();
		}
		return user;
	}
}
