import mongooseTimestamp from 'mongoose-timestamp';
import { Model, Plugin } from 'mongoose-model-decorators';
import mongoose, { Schema } from 'mongoose';

const Device = new Schema({
	location: String,
	serialNumber: {
		type: String,
		default: null,
	},
});

@Model
@Plugin(mongooseTimestamp)
export default class System {
	static collection = 'systems'

	static schema = {
		sysid: String,
        env: String,
		baseStation: Device,
		wristband: Device,
		sensors: [Device],
		isStopped: {
			type: Boolean,
			default: true,
		},
		subscriberId: {
			type: mongoose.SchemaTypes.ObjectId,
		},
		createdByUserId: mongoose.SchemaTypes.ObjectId,
	};
}
