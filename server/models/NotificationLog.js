import { Model } from 'mongoose-model-decorators';

import { getKyteraDBConnection } from '../services/kytera';

@Model({ connection: getKyteraDBConnection() })
export default class Notification {
	static collection = 'notifications_logs'

	static schema = {
		message: {
			type: String,
			default: null,
		},
		title:{
			type: String,
			default: null,
		},
		frame: {
			type: Boolean,
			default: false,
		},
		generatedTimestamp: Date,
		timestamp: Date,
		icon: String,
		sysid: String,
	}
}
