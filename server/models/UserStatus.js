import { Model } from 'mongoose-model-decorators';
import mongoose from 'mongoose'; // eslint-disable-line
import { getKyteraDBConnection } from '../services/kytera';

const opts = {
	collection: 'user_status',
};
const conn = getKyteraDBConnection();
if (conn) {
	opts.connection = conn;
}

@Model(opts)
export default class UserStatus {
}
