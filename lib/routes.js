const nextRoutes = require('next-routes');

const routes = nextRoutes();

routes
	.add('subscriber-alert', '/subscriber/:id/alert', 'subscriber/alert')
	.add('subscriber-alerts-history', '/subscriber/:id/alerts-history/:alertId?', 'subscriber/alerts-history')
	.add('subscriber-service', '/subscriber/:id/service', 'subscriber/service')
	.add('subscriber-notifications', '/subscriber/:id/notifications', 'subscriber/notifications')
	.add('subscriber-info', '/subscriber/:id/info', 'subscriber/info')
	.add('admin-operator', '/admin/operator/:operatorId', 'admin')
	.add('subscriber-home-care', '/subscriber/:id/home-care', 'subscriber/home-care')
	.add('subscriber-kytera', '/subscriber/:id/kytera', 'subscriber/kytera');

module.exports = routes;
