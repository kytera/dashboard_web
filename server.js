import 'dotenv/config'
import next from 'next';
import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { isUndefined } from 'lodash';

import { initMongo } from "./server/init/initMongo";
import { initServer } from "./server/init/initExpress";
import routes from './lib/routes';
import { logger } from "./server/logger";
import { initCloudinary } from "./server/init/initCloudinary";


logger.info(`Starting server NODE_ENV=${process.env.NODE_ENV}`);

const REMOTE_ENVS = ['production', 'staging', 'review'];
const dev = REMOTE_ENVS.indexOf(process.env.NODE_ENV) === -1;

// override BABEL_ENV in development to make babel using nextjs presets
if (dev) {
	logger.info(`Setting BABEL_ENV=frontend`);
  process.env.BABEL_ENV = 'frontend';
}

const app = next({ dev });
const handler = routes.getRequestHandler(app);
const PORT = process.env.PORT || 3000;
const BASE_URL = process.env.BASE_URL;

app.prepare()
	.then(async () => {
		const server = express();

    server.use(compression());
    server.use(cors());
    server.use(bodyParser.urlencoded({ extended: true }));
    server.use(bodyParser.json({ extended: true }));
    server.use(cookieParser(process.env.SECRET));

		// Register health check endpoint
		server.get('/health', (req, res) => res.json({ health: true }));

    await initMongo();
    await initServer(server);
		initCloudinary();

    server.get('*', (req, res) => handler(req, res));

		server.listen(PORT, (err) => {
			if (err) throw err;
			logger.info(`> Ready on https://${BASE_URL}:${PORT}`);
		});
	});
